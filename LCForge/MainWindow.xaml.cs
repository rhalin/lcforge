﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LCForgeUI;
using Microsoft.Win32;
using NodeLynx.CustomData;
using NodeLynx.CustomData.DataTypes;
using NodeLynx.CustomData.UI;
using NodeLynx.lib;
using NodeLynx.Module;
using NodeLynx.UI;
using NodeLynx.UI.Dialogs;

namespace LCForge
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, INLDocumentManager
	{
		/*
		static void DataTableEvents()
		{
			DataTable table = new DataTable("Customers");
			// Add two columns, id and name.
			table.Columns.Add("id", typeof(int));
			table.Columns.Add("name", typeof(string));

			// Set the primary key. 
			table.Columns["id"].Unique = true;
			table.PrimaryKey = new DataColumn[] { table.Columns["id"] };

			// Add a RowChanged event handler.
			table.RowChanged += new DataRowChangeEventHandler(Row_Changed);

			// Add a RowChanging event handler.
			table.RowChanging += new DataRowChangeEventHandler(Row_Changing);

			// Add a RowDeleted event handler.
			table.RowDeleted += new DataRowChangeEventHandler(Row_Deleted);

			// Add a RowDeleting event handler.
			table.RowDeleting += new DataRowChangeEventHandler(Row_Deleting);

			// Add a ColumnChanged event handler.
			table.ColumnChanged += new
				DataColumnChangeEventHandler(Column_Changed);

			// Add a ColumnChanging event handler.
			table.ColumnChanging += new
				DataColumnChangeEventHandler(Column_Changing);

			// Add a TableNewRow event handler.
			table.TableNewRow += new
				DataTableNewRowEventHandler(Table_NewRow);

			// Add a TableCleared event handler.
			table.TableCleared += new
				DataTableClearEventHandler(Table_Cleared);

			// Add a TableClearing event handler.
			table.TableClearing += new
				DataTableClearEventHandler(Table_Clearing);

			// Add a customer.
			DataRow row = table.NewRow();
			row["id"] = 1;
			row["name"] = "Customer1";
			table.Rows.Add(row);

			table.AcceptChanges();

			// Change the customer name.
			table.Rows[0]["name"] = "ChangedCustomer1";

			// Delete the row.
			table.Rows[0].Delete();

			// Clear the table.
			table.Clear();
		}


		private static void Row_Changed(object sender, DataRowChangeEventArgs e)
		{
			Console.WriteLine("Row_Changed Event: name={0}; action={1}",
				e.Row["name"], e.Action);
		}

		private static void Row_Changing(object sender, DataRowChangeEventArgs e)
		{
			Console.WriteLine("Row_Changing Event: name={0}; action={1}",
				e.Row["name"], e.Action);
		}

		private static void Row_Deleted(object sender, DataRowChangeEventArgs e)
		{
			Console.WriteLine("Row_Deleted Event: name={0}; action={1}",
				e.Row["name", DataRowVersion.Original], e.Action);
		}

		private static void Row_Deleting(object sender,
		DataRowChangeEventArgs e)
		{
			Console.WriteLine("Row_Deleting Event: name={0}; action={1}",
				e.Row["name"], e.Action);
		}

		private static void Column_Changed(object sender, DataColumnChangeEventArgs e)
		{
			Console.WriteLine("Column_Changed Event: ColumnName={0}; RowState={1}",
				e.Column.ColumnName, e.Row.RowState);
		}

		private static void Column_Changing(object sender, DataColumnChangeEventArgs e)
		{
			Console.WriteLine("Column_Changing Event: ColumnName={0}; RowState={1}",
				e.Column.ColumnName, e.Row.RowState);
		}

		private static void Table_NewRow(object sender,
			DataTableNewRowEventArgs e)
		{
			Console.WriteLine("Table_NewRow Event: RowState={0}",
				e.Row.RowState.ToString());
		}

		private static void Table_Cleared(object sender, DataTableClearEventArgs e)
		{
			Console.WriteLine("Table_Cleared Event: TableName={0}; Rows={1}",
				e.TableName, e.Table.Rows.Count.ToString());
		}

		private static void Table_Clearing(object sender, DataTableClearEventArgs e)
		{
			Console.WriteLine("Table_Clearing Event: TableName={0}; Rows={1}",
				e.TableName, e.Table.Rows.Count.ToString());
		}*/
		LCFWindowManager WindowManager;
		//NLTreeView treeFolders;
		List<ucMetaWindow> OpenEditorMetaWindows = new List<ucMetaWindow>();
		public MainWindow()
		{
			InitializeComponent();
			//MainWindow.DataTableEvents();
			InitializeWindowSystem();
		}

		private void InitializeWindowSystem()
		{
			//cvsMain.Children.Clear();
			//OpenEditorMetaWindows.Clear();
			WindowManager = new LCFWindowManager(cvsMain);

			WindowManager.NewMetaWindow += WindowManager_NewMetaWindow;
			WindowManager.ClosedMetaWindow += WindowManager_ClosedMetaWindow;
			//treeFolders = new NLTreeView();

			ucMetaWindow win = WindowManager.GetFirstWindow();
			OpenEditorMetaWindows.Add(win);
			//cvsMain.Opacity = 0;
			SetupLastNamePane();
			//();
		}
		void VisibleWindow()
		{
			DoubleAnimation da = new DoubleAnimation();
			da.From = 0;
			da.To = 1;
			da.Duration = new Duration(TimeSpan.FromSeconds(1));
			Storyboard s = new Storyboard();
			s.Duration = new Duration(TimeSpan.FromSeconds(1));
			s.Children.Add(da);
			Storyboard.SetTarget(da, cvsMain);
			Storyboard.SetTargetProperty(da, new PropertyPath("Opacity"));
			s.Begin();
		}
		void WindowManager_ClosedMetaWindow(object sender, RoutedEventArgs e)
		{
			ClosedMetaWindowEventArgs args = (ClosedMetaWindowEventArgs)e;
			OpenEditorMetaWindows.Remove((ucMetaWindow)args.OriginalSource);
		}

		void WindowManager_NewMetaWindow(object sender, RoutedEventArgs e)
		{
			NewMetaWindowEventArgs args = (NewMetaWindowEventArgs)e;
			OpenEditorMetaWindows.Add((ucMetaWindow)args.OriginalSource);
			SetupLastNamePane();
		}
		void SetupLastNamePane()
		{
			Canvas cvs = OpenEditorMetaWindows[OpenEditorMetaWindows.Count - 1].NavigationPane;
			cvs.Children.Add(new NLTreeView(OpenEditorMetaWindows[OpenEditorMetaWindows.Count - 1].ContentPane, (FrameworkElement)OpenEditorMetaWindows[OpenEditorMetaWindows.Count - 1]));
			((FrameworkElement)cvs.Children[0]).Width = cvs.ActualWidth;
			((FrameworkElement)cvs.Children[0]).Height = cvs.ActualHeight;
			OpenEditorMetaWindows[OpenEditorMetaWindows.Count - 1].ContentPane.Children.Add(new ucWidgetPanel2());
			((NLTreeView)cvs.Children[cvs.Children.Count - 1]).docManager = this;
			((NLTreeView)cvs.Children[cvs.Children.Count - 1]).CurrentWidgetPanel = (ucWidgetPanel2)OpenEditorMetaWindows[OpenEditorMetaWindows.Count - 1].ContentPane.Children[0];

			//Canvas cvsOpen = OpenEditorMetaWindows[0].NavigationPane;
			if (DataProject.CurrentProject != null)
			{
				((NLTreeView)cvs.Children[cvs.Children.Count - 1]).SetProject(DataProject.CurrentProject);
			}
		}
		private void NewDefinition_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucNewItem ni = new ucNewItem();
			chooseModule.SetDialogControl(ni);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ni.Validate())
				{

					if (DataProject.CurrentProject.DefinitionExists(ni.GetItemName()))
					{
						MessageBox.Show("Definition names must be unique");
					}
					else
					{
						DatatypeDefinition def = new DatatypeDefinition();
						def.DataName = ni.GetItemName();
						def.Initialize(DataProject.CurrentProject.RegisteredDatatypes);
						DataProject.CurrentProject.AddDefinition("./defs/" + def.DataName + ".def", def);


						DataFolder df = DataProject.CurrentProject.RootFolder["[Definitions]"].CreateSubfolder(def.DataName);
						df.FolderType = DataFolder.NLFOLDERTYPE.NONE;
						df.Tag = def;
						df.FolderName = def.DataName;
						//treeFolders.AddFolder(df, DataProject.CurrentProject.RootFolder["[Definitions]"]);
						AddTreeFolder(df, DataProject.CurrentProject.RootFolder["[Definitions]"]);
						DataProject.CurrentProject.SaveProject();
						LoadPlugins();
					}
				}
				else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}
		}

		private void NewLibrary_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucNewDataInstance ndi = new ucNewDataInstance();
			chooseModule.SetDialogControl(ndi);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ndi.Validate())
				{

					if (DataProject.CurrentProject.RootFolder["[Libraries]"].ContainsFolder(ndi.LibraryName()))
					{
						MessageBox.Show("Library names must be unique");
					}
					else
					{
						string name = ndi.LibraryName();
						DataFolder df = DataProject.CurrentProject.CreateLibrary(name, ndi.GetDefinition(), DataProject.CurrentProject.RootFolder["[Libraries]"]);

						//treeFolders.AddFolder(df, DataProject.CurrentProject.RootFolder["[Libraries]"]);
						AddTreeFolder(df, DataProject.CurrentProject.RootFolder["[Libraries]"]);
					}
				}
				else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}
		}

		private void NewAssetFolder_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucNewItem ni = new ucNewItem();
			chooseModule.SetDialogControl(ni);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ni.Validate())
				{

					if (DataProject.CurrentProject.RootFolder["[Assets]"].ContainsFolder(ni.GetItemName()))
					{
						MessageBox.Show("Folder names must be unique at each level");
					}
					else
					{
						string name = ni.GetItemName();

						DataFolder df = DataProject.CurrentProject.AddAssetFolder(name,
							DataProject.CurrentProject.RootFolder["[Assets]"],
							(DataCollection)DataProject.CurrentProject.RootFolder["[Assets]"].Tag);

						//treeFolders.AddFolder(df, DataProject.CurrentProject.RootFolder["[Assets]"]);
						AddTreeFolder(df, DataProject.CurrentProject.RootFolder["[Assets]"]);
					}
				}
				else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}
		}
		
		private void NewFolder_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucNewItem ni = new ucNewItem();
			chooseModule.SetDialogControl(ni);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ni.Validate())
				{

					if (DataProject.CurrentProject.RootFolder.ContainsFolder(ni.GetItemName()))
					{
						MessageBox.Show("Folder names must be unique at each level");
					}
					else
					{
						string name = ni.GetItemName();
						DataFolder df = DataProject.CurrentProject.RootFolder.CreateSubfolder(name);
						df.FolderType = DataFolder.NLFOLDERTYPE.NONE;
						df.FolderName = name;
						//treeFolders.AddFolder(df, DataProject.CurrentProject.RootFolder);
						AddTreeFolder(df, DataProject.CurrentProject.RootFolder);

					}
				}
				else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}
		}

		private void GenerateModule_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic namePrompt = new dlgBasic();
			ucNewItem ni = new ucNewItem();
			ni.SetPrompt("Please enter a name for this module (alpha characters only):");
			namePrompt.SetDialogControl(ni);
			bool? ok = namePrompt.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ni.Validate())
				{
					if (DataProject.GetModuleByName(ni.GetItemName()) == null)
					{
						dlgBasic fqnPrompt = new dlgBasic();
						ucNewItem ni2 = new ucNewItem();
						ni2.SetPrompt("Please enter an FQN for this module (example: com.domain.type.subtype):");
						fqnPrompt.SetDialogControl(ni2);
						bool? ok2 = fqnPrompt.ShowDialog();
						if (ok2 != null && ok2 == true)
						{
							if (ni2.Validate())
							{
								if (DataProject.GetModuleByFQN(ni2.GetItemName()) == null)
								{
									DataProject.CurrentProject.GenerateModule(ni.GetItemName(), ni2.GetItemName());
								}
								else
								{
									MessageBox.Show("FQN Must be unique.");
								}
							}
						}
					}
					else
					{
						MessageBox.Show("Name must be unique");
					}
				}
			}
		}

		private void MenuItemSave_Click(object sender, RoutedEventArgs e)
		{
			DataProject.CurrentProject.SaveProject();
		}
		private void LoadPlugins()
		{
			//Assembly testDLL = Assembly.LoadFile("C:\\dll\\test.dll");
			string modPath = new Uri(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath + "/Modules/";
			string[] fileEntries = Directory.GetFiles((new Uri(modPath).LocalPath));
			foreach (string fileName in fileEntries)
			{
				//load each 
				Assembly module = Assembly.LoadFile(fileName);
				Type[] Types = module.GetTypes();
				foreach (Type t in Types)
				{
					ICustomModule moduleInstance = t as ICustomModule;
					if (moduleInstance != null)
					{
						//todo... save this somewhere?
					}
				}
			}
		}
		List<INLTabClient> Clients = new List<INLTabClient>();
		List<Type> RegisteredEditors = new List<Type>();
		//datatypedefinition -> list of editors
		Dictionary<DatatypeDefinition, List<Type>> RegEditorLookup = new Dictionary<DatatypeDefinition, List<Type>>();
		Dictionary<Type, string> EditorButtonNames = new Dictionary<Type, string>();
		private void ClearRegisteredEditors()
		{
			RegisteredEditors.Clear();
			RegEditorLookup.Clear();
			EditorButtonNames.Clear();
		}
		private void MenuItem_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();

			dlg.DefaultExt = ".proj";
			dlg.Filter = "Projects (.proj)|*.proj";



			// Display OpenFileDialog by calling ShowDialog method
			Nullable<bool> result = dlg.ShowDialog();
			// Get the selected file name and display in a TextBox
			if (result == true)
			{
				//InitializeWindowSystem();

				ClearRegisteredEditors();
				// Open document
				string filename = dlg.FileName;
				RegisteredTypes RegTypes = RegisteredTypes.DefaultTypes();

				//treeFolders.Reset();
				ResetTreeFolders();

				DataProject proj = DataProject.LoadProject(filename, RegTypes);
				DataProject.CurrentProject = proj;
				DataProject.CurrentProject.CurrentManager = this;

				//treeFolders.SetProject(proj);
				SetTreeFolderProject(proj);
				/*
				foreach (Type t in DataProject.CurrentProject.RegisteredEditors)
				{
					RegisterDocumentEditor(t);
				}*/
				LoadPlugins();
				ucChooseModule cm = new ucChooseModule();
				ItemCollection modules = cm.GetModules();
				foreach (ListBoxItem cbi in modules)
				{
					if (proj.ModuleFQNs.Contains(((ICustomModule)cbi.Tag).GetFQN()))
					{
						foreach (Type t in ((ICustomModule)cbi.Tag).GetEditors())
						{
							RegisterDocumentEditor(t);
						}
						//break;
					}
				}
				mnuProject.IsEnabled = true;
				VisibleWindow();

				//RegisterDefaultEditors();

				//ucDefinitionEditor defEditor = new ucDefinitionEditor();
				//OpenDocument(defEditor);
			}



		}
		private void MenuItemNew_Click(object sender, RoutedEventArgs e)
		{
			SaveFileDialog sfd = new SaveFileDialog();
			sfd.DefaultExt = ".proj";
			sfd.Filter = "Projects (.proj)|*.proj";
			Nullable<bool> result = sfd.ShowDialog();
			// Get the selected file name and display in a TextBox
			if (result == true)
			{
				// new document
				dlgBasic chooseModule = new dlgBasic();
				ucChooseModule cm = new ucChooseModule();
				chooseModule.SetDialogControl(cm);

				bool? ok = chooseModule.ShowDialog();
				if (ok != null && ok == true)
				{
					//InitializeWindowSystem();
					ClearRegisteredEditors();
					mnuProject.IsEnabled = true;
					string filename = sfd.FileName;
					//treeFolders.Reset();
					ResetTreeFolders();
					RegisteredTypes RegTypes = RegisteredTypes.DefaultTypes();

					DataProject.CurrentProject = DataProject.CreateNewProject(filename, RegTypes, cm.SelectedModules());
					//treeFolders.SetProject(DataProject.CurrentProject);
					SetTreeFolderProject(DataProject.CurrentProject);
					DataProject.CurrentProject.SaveProject();
					DataProject.CurrentProject.CurrentManager = this;
					foreach (Type t in DataProject.CurrentProject.RegisteredEditors)
					{
						RegisterDocumentEditor(t);
					}
					VisibleWindow();
				}
			}
		}
		void ResetTreeFolders()
		{
			foreach(ucMetaWindow win in OpenEditorMetaWindows)
			{
				((NLTreeView)win.NavigationPane.Children[0]).Reset();
			}
		}
		void SetTreeFolderProject(DataProject proj)
		{
			foreach (ucMetaWindow win in OpenEditorMetaWindows)
			{
				((NLTreeView)win.NavigationPane.Children[0]).SetProject(proj);
			}
		}
		void AddTreeFolder(DataFolder at, DataFolder parent)
		{
			foreach (ucMetaWindow win in OpenEditorMetaWindows)
			{
				((NLTreeView)win.NavigationPane.Children[0]).AddFolder(at, parent);
			}
		}
		public void RegisterDocumentEditor(Type EditorType)
		{
			RegisteredEditors.Add(EditorType);
			//create one
			INLTabClient instance = (INLTabClient)Activator.CreateInstance(EditorType);
			EditorButtonNames[EditorType] = instance.GetButtonName();
			List<DatatypeDefinition> defs = instance.SupportedEditorFor();
			foreach (DatatypeDefinition def in defs)
			{
				if (!RegEditorLookup.ContainsKey(def))
				{
					RegEditorLookup[def] = new List<Type>();
				}
				RegEditorLookup[def].Add(EditorType);
			}
		}

		public void OpenDocument(INLTabClient ctrl, EditorSynchro sync = null)
		{
			//generate new tab
			DatatypeDefinition def = ctrl.GetDatatypeDefinition();
			//find types
			if (sync != null)
			{
				ucMetaWindow Win = (ucMetaWindow)sync.CurrentWindow;
				Win.ToolbarCanvas.Children.Clear();
				if (def != null)
				{
					string tabName = ctrl.GetTabName();
					if (ctrl is ucDefinitionEditor)
					{
						//yes we have a different path for the definition editor... it really isn't the same at all
						//just add it by itself
						//sync.ContentCanvas.Children.Clear();
						//sync.ContentCanvas.Children.Add((FrameworkElement)ctrl);
						//((FrameworkElement)sync.ContentCanvas.Children[0]).Width = sync.ContentCanvas.ActualWidth;
						//((FrameworkElement)sync.ContentCanvas.Children[0]).Height = sync.ContentCanvas.ActualHeight;
						Win.SetContent(ctrl);
					}
					else
					{
						//if (sync == null)
						//{
						//	sync = new EditorSynchro(ctrl.GetDataInstance(), null, ctrl.GetRowID());
						//}
						Canvas tabCanvas = Win.TabCanvas;
						StackPanel tabStack = new StackPanel();
						tabCanvas.Children.Clear();
						tabStack.Orientation = Orientation.Horizontal;
						tabStack.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
						tabStack.Width = tabCanvas.ActualWidth;
						tabStack.FlowDirection = System.Windows.FlowDirection.RightToLeft;
						tabCanvas.Children.Add(tabStack);
						//Button btnTabClient = new Button();

						//btnTabClient.Click += btnTabClient_Click;


						List<DatatypeDefinition> Types = new List<DatatypeDefinition>();
						Types.Add(DataProject.CurrentProject.GetDefinition(Guid.Empty.ToString()));
						Types.Add(def);
						if (def.InheritsDefinition != Guid.Empty.ToString())
						{
							DatatypeDefinition.InheritedDefs(def, ref Types);
						}

						//check each
						List<string> Opened = new List<string>();
						foreach (DatatypeDefinition definition in Types)
						{
							if (RegEditorLookup.ContainsKey(definition))
							{
								foreach (Type t in RegEditorLookup[definition])
								{
									
									//create a button
									if (!Opened.Contains(t.FullName))
									{
										Button tab = new Button();

										tab.Content = EditorButtonNames[t];
										tabStack.Children.Add(tab);
										if (t == ctrl.GetType())
										{
											//btnTabClient.SelectedIndex = btnTabClient.Items.Count - 1;
											//WindowData data = new WindowData(sync.CurrentWindow, sync.ContentCanvas, ctrl.GetDataInstance());
											//tab.Content = data;
											tab.Tag = ctrl;
											SetupContentCanvas(ctrl);
										}
										else
										{
											INLTabClient instance = (INLTabClient)Activator.CreateInstance(t);
											instance.SetEditorSynchro(sync);
											instance.SetDataSource(ctrl.GetDatatypeDefinition(), ctrl.GetDataInstance(), ctrl.GetRowID());
											tab.Tag = instance;

										}
										Opened.Add(t.FullName);
										tab.Click += tab_Click;
									}
									//((FrameworkElement)tab.Content).HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
								}
							}
						}

						//doc.Content = btnTabClient;
					}
					Clients.Add(ctrl);
					//DocPane.Children.Add(doc);
					//DocPane.SelectedContentIndex = DocPane.Children.Count - 1;

				}


				//if (sync.ContentCanvas != null)
				//{
				//	sync.ContentCanvas.Children.Clear();
				//	sync.ContentCanvas.Children.Add((FrameworkElement)ctrl);
				//	((FrameworkElement)sync.ContentCanvas.Children[0]).Width = sync.ContentCanvas.ActualWidth;
				//	((FrameworkElement)sync.ContentCanvas.Children[0]).Height = sync.ContentCanvas.ActualHeight;
				//}
			}
		}

		void tab_Click(object sender, RoutedEventArgs e)
		{
			INLTabClient client = (INLTabClient)((Button)sender).Tag;
			SetupContentCanvas(client);
			e.Handled = true;
		}

		private static void SetupContentCanvas(INLTabClient client)
		{
			EditorSynchro sync = client.GetEditorSynchro();
			sync.ContentCanvas.Children.Clear();
			sync.ContentCanvas.Children.Add((FrameworkElement)client);
			((FrameworkElement)sync.ContentCanvas.Children[0]).Width = sync.ContentCanvas.ActualWidth;
			((FrameworkElement)sync.ContentCanvas.Children[0]).Height = sync.ContentCanvas.ActualHeight;
			client.TabShown();
			client.SetupToolbar(((ucMetaWindow)sync.CurrentWindow).ToolbarCanvas);
		}



		public void OpenWidgets(FrameworkElement CurrentWindow, Canvas ContentPane, DataFolder df)
		{
			ContentPane.Children.Clear();
			ucMetaWindow win = (ucMetaWindow)CurrentWindow;
			win.TabCanvas.Children.Clear();
			ucWidgetPanel2 WidgetPanel = new ucWidgetPanel2();
			WidgetPanel.DocumentManager = this;
			ContentPane.Children.Add(WidgetPanel);
			if (df.Tag is DataInstance)
			{
				DataInstance inst = (DataInstance)df.Tag;
				WidgetPanel.SetData(df, inst, CurrentWindow, ContentPane);
				//add a "new" button to toolbar
				((ucMetaWindow)CurrentWindow).ToolbarCanvas.Children.Clear();
				Button btnNew = new Button();
				btnNew.Content = "New " + inst.Definition.PrintableName;
				WindowData data = new WindowData(CurrentWindow, ContentPane, inst);
				btnNew.Tag = data;
				btnNew.Click +=btnNewLibraryItem_Click;
				((ucMetaWindow)CurrentWindow).ToolbarCanvas.Children.Add(btnNew);

				//recurse?
				if (df.FolderType != DataFolder.NLFOLDERTYPE.LIBRARY_ROW)
				{
					AddInstanceRowsToTreeView(df, inst);
				}

			}
			else if (df.Tag is LCDataRow)
			{
				WidgetPanel.AddWidgetFolders(df, CurrentWindow, ContentPane);
			} else //maybe need if df.tag is datacollection
			{
				WidgetPanel.SetAssetFolder(df, CurrentWindow, ContentPane);
			}
			((FrameworkElement)ContentPane.Children[0]).Width = ContentPane.ActualWidth;
			((FrameworkElement)ContentPane.Children[0]).Height = ContentPane.ActualHeight;
		}
		private void AddInstanceRowsToTreeView(DataFolder Parent, DataInstance inst)
		{
			//reset this
			Parent.ClearChildren(true);
			foreach (ucMetaWindow win in OpenEditorMetaWindows)
			{
				((NLTreeView)win.NavigationPane.Children[0]).ClearChildren(Parent);
			}
			foreach (LCDataRow row in inst.Data.Rows)
			{
				DataFolder fld = Parent.CreateSubfolder(inst.Definition.PrintableName + " " + IDConverter.DecimalToID((long)row[0]));
				fld.Tag = row;
				fld.FolderType = DataFolder.NLFOLDERTYPE.LIBRARY_ROW;
				fld.LibraryID = inst.ID.ToString();
				foreach (ucMetaWindow win in OpenEditorMetaWindows)
				{
					((NLTreeView)win.NavigationPane.Children[0]).AddFolder(fld, Parent);
				}
				
				foreach (SerializableKeyValuePair<Guid, DatatypeField> field in inst.Definition.Fields)
				{
					if (field.Value.DataTypeInfo is NLDataInstance)
					{
						DataInstance childInstance = DataProject.CurrentProject.GetInstance((string)row[field.Value.FieldName]);
						CreateChildInstanceFolders(childInstance, fld);
					}
				}
			}
		}
		private void CreateChildInstanceFolders(DataInstance tempIn, DataFolder CurrentParentFolder)
		{
			DataFolder child = DataProject.CurrentProject.CreateLibraryFolder(CurrentParentFolder, tempIn);
			foreach (ucMetaWindow win in OpenEditorMetaWindows)
			{
				((NLTreeView)win.NavigationPane.Children[0]).AddFolder(child, CurrentParentFolder);
			}
		}
		private void btnNewLibraryItem_Click(object sender, RoutedEventArgs e)
		{
			Button btn = (Button)sender;
			WindowData winData = (WindowData)btn.Tag;
			ucDataEditor editor = new ucDataEditor();

			EditorSynchro sync = new EditorSynchro(winData.Instance, winData.ContentCanvas, winData.Window);
			LCDataRow dr = winData.Instance.NewRow();
			winData.Instance.AddRow(dr);
			sync.GotoRowID((long)dr[0]);
			editor.SetEditorSynchro(sync);
			editor.SetDataSource(winData.Instance.Definition, winData.Instance);
			OpenDocument(editor, sync);
			e.Handled = true;
		}
	}
}
