﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using LCForge;

namespace LCForgeUI
{
	public class NewMetaWindowEventArgs : RoutedEventArgs
	{
		public Canvas content;
		public Canvas nav;
		public NewMetaWindowEventArgs(RoutedEvent routedEvent, object source, Canvas navPanel, Canvas contentPanel)
			: base(routedEvent, source)
		{ nav = navPanel; content = contentPanel; }
	}
	public class ClosedMetaWindowEventArgs : RoutedEventArgs
	{
		public Canvas content;
		public Canvas nav;
		public ClosedMetaWindowEventArgs(RoutedEvent routedEvent, object source, Canvas navPanel, Canvas contentPanel)
			: base(routedEvent, source)
		{ nav = navPanel; content = contentPanel; }
	}
	public class LCFWindowManager : UserControl
	{
		//we want to keep track of window info using this class.
		// it will be responsible for determing size, and sliding the non-active sliders along with the current one
		Panel CurrentControl;
		double VisibleHeight = 0;
		double VisibleWidth = 0;
		bool IsDrawn = false;
		List<FrameworkElement> Elements = new List<FrameworkElement>();
		bool LayoutStructureChanged = false;
		bool WinMgrInitialized = false;
		FrameworkElement RightMostWindow = null;
		FrameworkElement LeftMostWindow = null;
		VertWindowDragger Dragging = null;
		Point LastDraggingPosition;
		public LCFWindowManager(Panel Control)
		{
			CurrentControl = Control;
			CurrentControl.SizeChanged += CurrentControl_SizeChanged;
			CurrentControl.Loaded += CurrentControl_Loaded;
			CurrentControl.MouseMove += CurrentControl_MouseMove;
			CurrentControl.MouseLeftButtonUp += CurrentControl_MouseLeftButtonUp;

			Elements.Add(new VertWindowDragger(this));
			Elements.Add(new ucMetaWindow(this));
			Elements.Add(new VertWindowDragger(this));
			Elements[0].Width = 18;			
			Elements[2].Width = 18;
			((VertWindowDragger)Elements[0]).vertDocked = VertWindowDragger.Docked.DOCKLEFT;
			((VertWindowDragger)Elements[2]).vertDocked = VertWindowDragger.Docked.DOCKRIGHT;

			RightMostWindow = Elements[1];
			//Elements[0].VerticalAlignment = VerticalAlignment.Stretch;
			//Elements[1].VerticalAlignment = VerticalAlignment.Stretch;
			//Elements[2].VerticalAlignment = VerticalAlignment.Stretch;

			LayoutStructureChanged = true;
		}
		public ucMetaWindow GetFirstWindow()
		{
			return (ucMetaWindow)Elements[1];
		}
		public static readonly RoutedEvent NewMetaWindowEvent =
			EventManager.RegisterRoutedEvent(
				"NewMetaWindowEvent",
				RoutingStrategy.Bubble,
				typeof(RoutedEventHandler),
				typeof(ucMetaWindow));
		public static readonly RoutedEvent ClosedMetaWindowEvent =
			EventManager.RegisterRoutedEvent(
				"ClosedMetaWindowEvent",
				RoutingStrategy.Bubble,
				typeof(RoutedEventHandler),
				typeof(ucMetaWindow));


		public event RoutedEventHandler NewMetaWindow
		{
			add { AddHandler(NewMetaWindowEvent, value); }
			remove { RemoveHandler(NewMetaWindowEvent, value); }
		}
		public event RoutedEventHandler ClosedMetaWindow
		{
			add { AddHandler(ClosedMetaWindowEvent, value); }
			remove { RemoveHandler(ClosedMetaWindowEvent, value); }
		}
		void CurrentControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			UpdateDragger(e);
			e.Handled = true;
		}

		void CurrentControl_MouseMove(object sender, MouseEventArgs e)
		{
			UpdateDragger(e);
			e.Handled = true;
		}
		public void HandleLayout()
		{
			if(LayoutStructureChanged && IsDrawn)
			{
				if(IsDrawn && !WinMgrInitialized)
				{
					Elements[1].Width = VisibleWidth - (36);
					WinMgrInitialized = true;
				}
				CurrentControl.Children.Clear();
				FrameworkElement prevElement = null;
				FrameworkElement nextElement = null;
				FrameworkElement currElement = null;
				LeftMostWindow = null;
				double MarginLeft = 0;
				double Width = 0;
				for(int i = 0; i < Elements.Count;i++)
				{
					currElement = Elements[i];
					if(i != 0)
					{
						prevElement = Elements[i-1];
					} else
					{
						prevElement = null;
					}

					if(i + 1 < Elements.Count)
					{
						nextElement = Elements[i+1];
					} else
					{
						nextElement = null;
					}
					if(Elements[i] is ucMetaWindow)
					{
						RightMostWindow = Elements[i];
						if(LeftMostWindow == null)
						{
							LeftMostWindow = Elements[i];
						}
					}
					if (i == Elements.Count - 1)
					{
						if (currElement.Margin.Left + currElement.Width > VisibleWidth)
						{
							//if(prevElement.)
							//if (prevElement.Width - 18 > 0)
							//	prevElement.Width = prevElement.Width - 18;
							//MarginLeft -= 18;
						}
						currElement.Margin = new Thickness(MarginLeft, 10, 0, 10);
						currElement.Height = VisibleHeight - 20;
						CurrentControl.Children.Add(currElement);
						//Console.WriteLine("MarginLeft: " + MarginLeft);
						MarginLeft += currElement.Width;
						//Console.WriteLine("Final left: " + MarginLeft);
					}
					else
					{
						currElement.Margin = new Thickness(MarginLeft, 10, 0, 10);
						currElement.Height = VisibleHeight - 20;
						CurrentControl.Children.Add(currElement);
						MarginLeft += currElement.Width;
					}
				}
			}
		}
		void CurrentControl_Loaded(object sender, RoutedEventArgs e)
		{
			SetSizes();
		}

		void CurrentControl_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			SetSizes();
			
		}
		void SetSizes()
		{
			double width = CurrentControl.ActualWidth;
			double height = CurrentControl.ActualHeight;
			if (!double.IsInfinity(height))
			{
				if(IsDrawn)
				{
					RightMostWindow.Width += width - VisibleWidth;
				}
				VisibleHeight = height;
				VisibleWidth = width;
				IsDrawn = true;
			} else
			{
				IsDrawn = false;
			}
			HandleLayout();
		}
		void SpawnWindow(double Diff, bool Left=true)
		{
			ucMetaWindow win = new ucMetaWindow(this);
			win.Width = 0;
			if (Left)
			{
				FrameworkElement nextElem = Elements[1];
				Thickness margin = nextElem.Margin;
				margin.Left += Diff;	
				nextElem.Margin = margin;
				nextElem.Width -= Diff;

				margin = Elements[0].Margin;
				margin.Left += Diff;
				Elements[0].Margin = margin;

				Elements.Insert(0, win);
				win.Margin = new Thickness(18, 10, 0, 10);
				win.Width = 36;
				win.Height = VisibleHeight - 20;
				VertWindowDragger drag = new VertWindowDragger(this);
				drag.Margin = new Thickness(0, 10, 0, 10);
				drag.Width = 18;
				//drag.Background = new SolidColorBrush(Colors.Blue);
				drag.vertDocked = VertWindowDragger.Docked.DOCKLEFT;
				drag.Height = VisibleHeight - 20;
				CurrentControl.Children.Add(drag);
				Elements.Insert(0, drag);
			} else
			{
				FrameworkElement prevElem = Elements[Elements.Count - 2];
				FrameworkElement prevDragger = Elements[Elements.Count - 1];
				//Thickness margin = prevElem.Margin;
				//margin.Left -= Diff;
				//prevElem.Margin = margin;
				prevElem.Width += Diff;

				Thickness margin = prevDragger.Margin;
				margin.Left += Diff;
				prevDragger.Margin = margin;

				Elements.Add(win);
				win.Margin = new Thickness((prevDragger.Margin.Left + prevDragger.Width), 10, 0, 10);
				win.Width = (VisibleWidth - win.Margin.Left) - 18;
				win.Height = VisibleHeight - 20;
				VertWindowDragger drag = new VertWindowDragger(this);
				drag.Margin = new Thickness(win.Margin.Left + win.Width, 10, 0, 10);
				drag.Width = 18;
				drag.Height = VisibleHeight - 20;
				drag.vertDocked = VertWindowDragger.Docked.DOCKRIGHT;
				CurrentControl.Children.Add(drag);
				Elements.Add(drag);
				//Console.WriteLine(Diff);
				//Console.WriteLine(win.Margin.Left + " - " + win.Width + " -> " + drag.Margin.Left + " - " + drag.Width + " ??? " + VisibleWidth);
			}
			CurrentControl.Children.Add(win);
			LayoutStructureChanged = true;
			RaiseEvent(new NewMetaWindowEventArgs(NewMetaWindowEvent, win,win.cvsNav, win.cvsContent));
			//HandleLayout();
		}
		bool PickedUpDrag = false;
		bool CollapsedRight = false;
		bool CollapsedLeft = false;
		public void UpdateDragger(MouseEventArgs e)
		{
			if(Dragging != null)
			{
				Point NewMousePos = Mouse.GetPosition(CurrentControl);
				//GridLength gr = new GridLength(CurrentControl.ColumnDefinitions[0].Width.Value + (Mouse.GetPosition(CurrentControl).X - LastDraggingPosition.X));
				double Difference = NewMousePos.X - LastDraggingPosition.X;
				if (Difference < -54 && ((VertWindowDragger)Dragging).vertDocked == VertWindowDragger.Docked.DOCKRIGHT
					|| Difference > 54 && ((VertWindowDragger)Dragging).vertDocked == VertWindowDragger.Docked.DOCKLEFT
					|| PickedUpDrag
					|| ((VertWindowDragger)Dragging).vertDocked == VertWindowDragger.Docked.NONE )
				{
					PickedUpDrag = true;
					bool SpawnedRight = false;
					bool Spawned = false;
					bool SpawnedLeft = false;
					if (Dragging.vertDocked == VertWindowDragger.Docked.DOCKLEFT)
					{
						Dragging.vertDocked = VertWindowDragger.Docked.NONE;
						SpawnWindow(Difference, true); //create a new window to the left
						SpawnedLeft = true;
						Spawned = true;
						LastDraggingPosition = NewMousePos;
					}
					else if (Dragging.vertDocked == VertWindowDragger.Docked.DOCKRIGHT)
					{
						Dragging.vertDocked = VertWindowDragger.Docked.NONE;
						SpawnWindow(Difference, false); //create a new window to the right
						Spawned = true;
						SpawnedRight = true;
						LastDraggingPosition = NewMousePos;
					}
					
					if (!Spawned)
					{
						//at this point, we have windows both to the left and the right
						int index = Elements.IndexOf(Dragging);
						Thickness margin = Elements[index + 1].Margin;
						if(Difference > 0) //moving right
						{
							if (CollapsedLeft)
							{
								if (Difference > 18)
								{
									UnCollapseLeft(ref NewMousePos, index, ref margin);
								}
							}
							else
							{
								if (Elements[index + 1].Width - Difference > 18)
								{
									ShrinkRight(ref NewMousePos, Difference, index, ref margin);

								}
								else if (Difference > 18)
								{
									if (index == Elements.Count - 3)
									{
										CollapseRight(ref NewMousePos, index, ref margin);
									} 
								}
							}
						} else //left
						{
							if (CollapsedRight)
							{
								//reset visibility
								if (Difference < -18)
								{
									UnCollapseRight(ref NewMousePos, index, ref margin);
								}
							}
							else
							{
								if(Elements[index - 1].Width + Difference > 18)
								{
									ShrinkLeft(ref NewMousePos, Difference, index, ref margin);

								} else if( Difference < -18)
								{
									if (index == 2)
									{
										CollapseLeft(ref NewMousePos, index, ref margin);
									}
								}
							}
						}
					}

				}
				if (e.LeftButton == MouseButtonState.Released)
				{
					StopDragging();
				}
			}
		}

		private void ShrinkLeft(ref Point NewMousePos, double Difference, int index, ref Thickness margin)
		{
			Elements[index + 1].Margin = new Thickness(margin.Left + Difference, margin.Top, margin.Right, margin.Bottom);
			//if ((Elements[index + 1].Margin.Left + Elements[index + 1].Width) - Difference < VisibleWidth - 18)
			{
				Elements[index + 1].Width -= Difference;
			}
			margin = Elements[index].Margin;
			Dragging.Margin = new Thickness(margin.Left + Difference, margin.Top, margin.Right, margin.Bottom);

			//prev window
			margin = Elements[index - 1].Margin;
			if (Elements[index - 1].Width + Difference > 0)
				Elements[index - 1].Width += Difference;
			LastDraggingPosition = NewMousePos;
		}
		private void ShrinkRight(ref Point NewMousePos, double Difference, int index, ref Thickness margin)
		{
			Elements[index + 1].Margin = new Thickness(margin.Left + Difference, margin.Top, margin.Right, margin.Bottom);
			//if ((Elements[index + 1].Margin.Left + Elements[index + 1].Width) - Difference < VisibleWidth - 18)
			{
				Elements[index + 1].Width -= Difference;
			}
			margin = Elements[index].Margin;
			Dragging.Margin = new Thickness(margin.Left + Difference, margin.Top, margin.Right, margin.Bottom);

			//prev window
			margin = Elements[index - 1].Margin;
			if (Elements[index - 1].Width + Difference > 0)
				Elements[index - 1].Width += Difference;

			LastDraggingPosition = NewMousePos;
		}
		private void UnCollapseRight(ref Point NewMousePos, int index, ref Thickness margin)
		{
			Elements[index + 1].Visibility = Visibility.Visible;
			Elements[index + 2].Visibility = Visibility.Visible;
			margin = Elements[index].Margin;
			double LastLeft = margin.Left;
			margin.Left = VisibleWidth + 18;
			Elements[index].Margin = margin;
			Elements[index - 1].Width -= (margin.Left - LastLeft);
			CollapsedRight = false;
			LastDraggingPosition = NewMousePos;
		}
		private void UnCollapseLeft(ref Point NewMousePos, int index, ref Thickness margin)
		{
			Elements[index - 1].Visibility = Visibility.Visible;
			Elements[index - 2].Visibility = Visibility.Visible;
			margin = Elements[index].Margin;
			double LastLeft = margin.Left;
			margin.Left = 36;
			Elements[index].Margin = margin;

			margin = Elements[index + 1].Margin;
			margin.Left += 36;
			Elements[index + 1].Margin = margin;
			Elements[index + 1].Width -= 36;
			CollapsedLeft = false;
			LastDraggingPosition = NewMousePos;
		}


		private void CollapseRight(ref Point NewMousePos, int index, ref Thickness margin)
		{
			Elements[index + 1].Visibility = Visibility.Collapsed;
			Elements[index + 2].Visibility = Visibility.Collapsed;
			margin = Elements[index].Margin;
			double LastLeft = margin.Left;
			margin.Left = VisibleWidth - 18;
			Elements[index].Margin = margin;
			Elements[index - 1].Width += (margin.Left - LastLeft);
			CollapsedRight = true;
			LastDraggingPosition = NewMousePos;
		}
		private void CollapseLeft(ref Point NewMousePos, int index, ref Thickness margin)
		{
			Elements[index - 1].Visibility = Visibility.Collapsed;
			Elements[index - 2].Visibility = Visibility.Collapsed;
			margin = Elements[index].Margin;
			double LastLeft = margin.Left;
			double diff = 0;
			margin.Left = 0;
			Elements[index].Margin = margin;
			diff = LastLeft - margin.Left;
			margin = Elements[index + 1].Margin;
			margin.Left -= (diff);
			Elements[index + 1].Margin = margin;
			Elements[index + 1].Width += diff;
			CollapsedLeft = true;
			LastDraggingPosition = NewMousePos;
		}
		public void BeginDrag(VertWindowDragger drg)
		{
			Dragging = drg;
			LastDraggingPosition = Mouse.GetPosition(CurrentControl);
			Mouse.OverrideCursor = Cursors.Hand;
		}
		public void StopDragging()
		{
			Dragging = null;
			PickedUpDrag = false;
			Mouse.OverrideCursor = null;
			if(CollapsedRight)
			{
				//despawn
				//Console.WriteLine(Elements[Elements.Count - 1].Visibility.ToString());
				CurrentControl.Children.Remove(Elements[Elements.Count - 1]);
				CurrentControl.Children.Remove(Elements[Elements.Count - 2]);
				ucMetaWindow win = (ucMetaWindow)Elements[Elements.Count - 2];
				Elements.RemoveAt(Elements.Count - 1);
				Elements.RemoveAt(Elements.Count - 1);
				CollapsedRight = false;
				((VertWindowDragger)Elements[Elements.Count - 1]).vertDocked = VertWindowDragger.Docked.DOCKRIGHT;
				CurrentControl.InvalidateVisual();
				RaiseEvent(new ClosedMetaWindowEventArgs(ClosedMetaWindowEvent, win, win.cvsNav, win.cvsContent));
			}
			if(CollapsedLeft)
			{
				CurrentControl.Children.Remove(Elements[0]);
				CurrentControl.Children.Remove(Elements[1]);
				ucMetaWindow win = (ucMetaWindow)Elements[1];
				Elements.RemoveAt(0);
				Elements.RemoveAt(0);
				CollapsedLeft = false;
				((VertWindowDragger)Elements[0]).vertDocked = VertWindowDragger.Docked.DOCKLEFT;
				CurrentControl.InvalidateVisual();
				RaiseEvent(new ClosedMetaWindowEventArgs(ClosedMetaWindowEvent, win, win.cvsNav, win.cvsContent));
			}
		}
	}
}
