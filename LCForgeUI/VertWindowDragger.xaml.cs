﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LCForge;

namespace LCForgeUI
{
	/// <summary>
	/// Interaction logic for VertWindowDragger.xaml
	/// </summary>
	public partial class VertWindowDragger : UserControl
	{
		public bool Dragging { get; set; }
		public ucMetaWindow ParentWindow { get; set; }

		LCFWindowManager WinManager;

		public enum Docked { DOCKLEFT, DOCKRIGHT, NONE };
		public Docked vertDocked = Docked.NONE;

		public VertWindowDragger(LCFWindowManager winMgr)
		{
			InitializeComponent();
			WinManager = winMgr;
		}

		private void brdDragger_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			WinManager.BeginDrag(this);
			e.Handled = true;
		}

		private void brdDragger_MouseMove(object sender, MouseEventArgs e)
		{
			WinManager.UpdateDragger(e);
			if(e.LeftButton == MouseButtonState.Released)
			{
				WinManager.StopDragging();
			}
			e.Handled = true;
		}

		private void brdDragger_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			WinManager.UpdateDragger(e);
			WinManager.StopDragging();
			e.Handled = true;
		}
	}
}
