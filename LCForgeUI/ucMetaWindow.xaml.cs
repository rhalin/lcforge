﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LCForgeUI;
using NodeLynx.CustomData.UI;
using NodeLynx.UI;

namespace LCForge
{
	public class PanelSet
	{
		public List<INLTabClient> Clients = new List<INLTabClient>();		
	}

	/// <summary>
	/// Interaction logic for ucMetaWindow.xaml
	/// </summary>
	public partial class ucMetaWindow : UserControl
	{
		public Canvas TabCanvas
		{
			get { return cvsTabs; }
			set { cvsTabs = value; }
		}
		public Canvas ToolbarCanvas
		{
			get { return cvsToolbar; }
			set { cvsToolbar = value; }
		}
		LCFWindowManager WinManager;
		public ucMetaWindow(LCFWindowManager winMgr)
		{
			InitializeComponent();
			WinManager = winMgr;
		}
		public Canvas NavigationPane
		{
			get { return cvsNav; }
			private set { }
		}
		public Canvas ContentPane
		{
			private set { }
			get
			{
				return cvsContent;
			}
		}
		List<EditorSynchro> History = new List<EditorSynchro>();
		EditorSynchro CurrentSynchro;
		INLTabClient CurrentClient;

		bool DraggingNav = false;
		Point mouseStart;
		private void NavSep_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			DraggingNav = true;
			mouseStart = Mouse.GetPosition(grdMain);
			e.Handled = true;
			Mouse.OverrideCursor = Cursors.SizeWE;
		}

		private void NavSeperator_MouseLeave(object sender, MouseEventArgs e)
		{
			//catch up
			UpdateNavMove(e);
			e.Handled = true;
		}

		private void UpdateNavMove(MouseEventArgs e)
		{
			if (DraggingNav)
			{
				Point NewMousePos = Mouse.GetPosition(NavSeperator);
				//double top = Canvas.GetTop(ellipse) + Mouse.GetPosition(ellipse).Y - point.Y;
				//Canvas.SetTop(ellipse, top);
				GridLength gr = new GridLength(grdMain.ColumnDefinitions[0].Width.Value + (Mouse.GetPosition(grdMain).X - mouseStart.X));
				if (gr.Value < 400 && gr.Value > NavSeperator.ActualWidth)
				{
					grdMain.ColumnDefinitions[0].Width = gr;
						//double left = Canvas.GetLeft(NavSeperator) + Mouse.GetPosition(NavSeperator).X - mouseStart.X;
						//Canvas.SetLeft(NavSeperator, left);
					//Console.WriteLine(grdMain.ColumnDefinitions[0].Width);
					mouseStart = Mouse.GetPosition(grdMain);
					SetNavCollapse(gr);
				}
				if(e.LeftButton == MouseButtonState.Released)
				{
					DraggingNav = false;
					Mouse.OverrideCursor = null;
					UnstickGrid();
				}
				RecalcContentSize();
			}
		}

		private void SetNavCollapse(GridLength gr)
		{
			if (gr.Value <= 30)
			{
				grdTreeView.ColumnDefinitions[1].Width = new GridLength(30);
				NavLabel.Visibility = System.Windows.Visibility.Visible;
			}
			else
			{
				grdTreeView.ColumnDefinitions[1].Width = new GridLength(3);
				NavLabel.Visibility = System.Windows.Visibility.Collapsed;
			}
			if(cvsNav.Children.Count > 0)
			{
				if (grdMain.ColumnDefinitions[0].Width.Value - grdTreeView.ColumnDefinitions[1].Width.Value > 0)
				{
					((FrameworkElement)cvsNav.Children[0]).Width = grdMain.ColumnDefinitions[0].Width.Value - grdTreeView.ColumnDefinitions[1].Width.Value;
				} else
				{
					((FrameworkElement)cvsNav.Children[0]).Width = 0;
				}
			}
		}

		public void UnstickGrid()
		{
			GridSticky = false;
		}

		private void NavSeperator_MouseMove(object sender, MouseEventArgs e)
		{
			UpdateNavMove(e);
			e.Handled = true;
		}

		private void NavSeperator_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			UpdateNavMove(e);
			if (DraggingNav)
			{
				DraggingNav = false;
				Mouse.OverrideCursor = null;
				UnstickGrid();
			}
			e.Handled = true;
		}

		private void grdMain_MouseUp(object sender, MouseButtonEventArgs e)
		{
			if(DraggingNav)
			{
				DraggingNav = false;
				UnstickGrid();
				Mouse.OverrideCursor = null;
				
			}
		}

		private void grdMain_MouseMove(object sender, MouseEventArgs e)
		{
			UpdateNavMove(e);
		}
		bool GridSticky = false;
		double StickAt = 0.0;
		private void grdMain_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if(e.NewSize.Width < 410 && e.NewSize.Width >= 10)// || GridSticky)
			{
				if (GridSticky)
				{
					if (e.NewSize.Width - 10 < grdMain.ColumnDefinitions[0].Width.Value)
					{
						grdMain.ColumnDefinitions[0].Width = new GridLength(e.NewSize.Width - 10);
						//Console.WriteLine(StickAt.ToString() + " -> " + e.NewSize.Width.ToString());
						//StickAt = grdMain.ColumnDefinitions[0].Width.Value;
						//GridSticky = true;
					} else //if(e.NewSize.Width > StickAt)
					{
						if (e.NewSize.Width - 10 < StickAt)
						{
							grdMain.ColumnDefinitions[0].Width = new GridLength(e.NewSize.Width - 10);
							//Console.WriteLine("**" + StickAt.ToString() + " -> " + e.NewSize.Width.ToString());
						}
					}
				}
				else
				{
					if (e.NewSize.Width - 10 < grdMain.ColumnDefinitions[0].Width.Value)
					{
						StickAt = grdMain.ColumnDefinitions[0].Width.Value;
						grdMain.ColumnDefinitions[0].Width = new GridLength(e.NewSize.Width - 10);
						GridSticky = true;
					}
				}
				SetNavCollapse(grdMain.ColumnDefinitions[0].Width);
			}
			if (cvsNav.Children.Count > 0)
			{
				if (grdMain.ColumnDefinitions[0].Width.Value - grdTreeView.ColumnDefinitions[1].Width.Value > 0)
				{
					((FrameworkElement)cvsNav.Children[0]).Width = grdMain.ColumnDefinitions[0].Width.Value - grdTreeView.ColumnDefinitions[1].Width.Value;
				}
				else
				{
					((FrameworkElement)cvsNav.Children[0]).Width = 0;
				}
				((FrameworkElement)cvsNav.Children[0]).Height = cvsNav.ActualHeight;
			}

			RecalcContentSize();
		}

		private void RecalcContentSize()
		{
			if (cvsTabs.Children.Count > 0 && cvsTabs.Children[0] is StackPanel)
			{
				((FrameworkElement)cvsTabs.Children[0]).Width = cvsTabs.ActualWidth;
			}
			if (cvsContent.Children.Count > 0)
			{
				((FrameworkElement)cvsContent.Children[0]).MinWidth = 0;
				((FrameworkElement)cvsContent.Children[0]).Width = cvsContent.ActualWidth;
				((FrameworkElement)cvsContent.Children[0]).Height = cvsContent.ActualHeight;

			}
		}
		public void SetContent(INLTabClient client)
		{
			EditorSynchro sync = client.GetEditorSynchro();
			History.Add(sync);
			sync.ContentCanvas.Children.Clear();
			sync.ContentCanvas.Children.Add((FrameworkElement)client);
			((FrameworkElement)sync.ContentCanvas.Children[0]).Width = sync.ContentCanvas.ActualWidth;
			((FrameworkElement)sync.ContentCanvas.Children[0]).Height = sync.ContentCanvas.ActualHeight;
		}
	}
}
