﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.OneNote;
using System.Xml.Linq;
using System.IO;

namespace OneNoteTests
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			var onenoteApp = new Microsoft.Office.Interop.OneNote.Application();

			string notebookXml;
			onenoteApp.GetHierarchy(null, HierarchyScope.hsPages, out notebookXml);

			var doc = XDocument.Parse(notebookXml);
			var ns = doc.Root.Name.Namespace;
			foreach (var notebookNode in from node in doc.Descendants(ns + "Notebook") select node)
			{
				Console.WriteLine(notebookNode.Attribute("name").Value);
				if (notebookNode.Attribute("name").Value == "API Test Notebook")
				{
					foreach (var sectionNode in from node in
													notebookNode.Descendants(ns + "Section")
												select node)
					{
						Console.WriteLine("  " + sectionNode.Attribute("name").Value);
					}
					foreach (var pageNode in from node in
													notebookNode.Descendants(ns + "Page")
												select node)
					{
						Console.WriteLine("  " + pageNode.Attribute("name").Value);

						string pageXml;
						onenoteApp.GetPageContent(pageNode.Attribute("ID").Value, out pageXml);
						StreamWriter sw = File.CreateText(pageNode.Attribute("name").Value + ".xml");
						sw.Write(pageXml);

						//Console.WriteLine(XDocument.Parse(pageXml));
						sw.Flush();
						sw.Close();
						if (pageNode.Attribute("name").Value == "Whatup")
						{
							var pageData = XDocument.Parse(pageXml);
							foreach (var child in from node in
													  pageData.Descendants(ns + "Outline")
													 select node)
							{

								//child.SetAttributeValue("lastModifiedTime", DateTime.Now.ToFileTimeUtc);
								Console.WriteLine(child.ToString());
								XElement meta = new XElement(ns + "Meta");
								meta.SetAttributeValue("name","meta-name");
								meta.SetAttributeValue("content","meta content goes here");
								child.FirstNode.NextNode.AddAfterSelf(meta);
								Console.WriteLine(pageData.ToString());

								onenoteApp.UpdatePageContent(pageData.ToString(), DateTime.MinValue);
								break;
							}
						}
					}
				}

			}

		}
	}
}
