﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using NodeLynx.CustomData;
using NodeLynx.CustomData.DataTypes;
using NodeLynx.CustomData.UI;
using NodeLynx.Module;
using NodeLynx.Node;
using NodeLynx.UI;
using NodeLynx.UI.Dialogs;
using Xceed.Wpf.AvalonDock.Layout;

namespace StoryForge
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, INLDocumentManager
	{
		List<INLTabClient> Clients = new List<INLTabClient>();
		List<Type> RegisteredEditors = new List<Type>();
		//datatypedefinition -> list of editors
		Dictionary<DatatypeDefinition, List<Type>> RegEditorLookup = new Dictionary<DatatypeDefinition, List<Type>>();
		Dictionary<Type, string> EditorButtonNames = new Dictionary<Type, string>();
		NLTreeView treeFolders;
		DragDropManager ddManager = new DragDropManager();
		public MainWindow()
		{
			InitializeComponent();
			
			WidgetPanel.myCanvas = mainCanvas;
			mainCanvas.MouseMove += mainCanvas_MouseMove;
			ddManager.SetFrames(mainCanvas, this);
			WidgetPanel.ddManager = ddManager;


			//treeFolders = new NLTreeView();

			treeFolders.docManager = this;
			/*
			ucNodeCanvas nc = new ucNodeCanvas();
			OpenDocument(nc);
			nc.SetDragDropManager(ddManager);
			nc.Initialize(); */
			//treeFolders.CurrentWidgetPanel = WidgetPanel;  //todo widget panel fix
			WidgetPanel.DocumentManager = this;
			//defEditor.Initialize();
			
			//TestEditor te = new TestEditor();


			//te.Show();
		}

		private void LoadPlugins()
		{
			//Assembly testDLL = Assembly.LoadFile("C:\\dll\\test.dll");
			string modPath = new Uri(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath + "/Modules/";
			string[] fileEntries = Directory.GetFiles((new Uri(modPath).LocalPath));
			foreach (string fileName in fileEntries)
			{
				//load each 
				Assembly module = Assembly.LoadFile(fileName);
				Type[] Types = module.GetTypes();
				foreach(Type t in Types)
				{
					ICustomModule moduleInstance = t as ICustomModule;
					if (moduleInstance != null)
					{
						//todo... save this somewhere?
					}
				}
			}
		}

		void mainCanvas_MouseMove(object sender, MouseEventArgs e)
		{
			Console.WriteLine("moved");
		}

		public void OpenDocument(INLTabClient ctrl, EditorSynchro sync)
		{

			//generate new tab
			DatatypeDefinition def = ctrl.GetDatatypeDefinition();
			//find types
			if (def != null)
			{
				
				LayoutDocument doc = new LayoutDocument();
				doc.Title = ctrl.GetTabName();
				if (ctrl is ucDefinitionEditor)
				{
					//yes we have a different path for the definition editor... it really isn't the same at all
					doc.Content = ctrl;
				}
				else
				{
					if (sync == null)
					{
						sync = new EditorSynchro(ctrl.GetDataInstance(),null, null, ctrl.GetRowID());
					}
					TabControl tabctrl = new TabControl();
					tabctrl.Background = new SolidColorBrush(Color.FromRgb(62,62,66));
					tabctrl.SelectionChanged +=tabctrl_SelectionChanged;
					//StackPanel spvert = new StackPanel();
					//StackPanel sphoriz = new StackPanel();
					//spvert.Children.Add(sphoriz);

					List<DatatypeDefinition> Types = new List<DatatypeDefinition>();
					Types.Add(DataProject.CurrentProject.GetDefinition(Guid.Empty.ToString()));
					Types.Add(def);
					if (def.InheritsDefinition != Guid.Empty.ToString())
					{
						InheritedDefs(def, ref Types);
					}

					//check each
					foreach (DatatypeDefinition definition in Types)
					{
						if (RegEditorLookup.ContainsKey(definition))
						{
							foreach (Type t in RegEditorLookup[definition])
							{
								//create a button
								TabItem tab = new TabItem();
								
								tab.Header = EditorButtonNames[t];
								tabctrl.Items.Add(tab);
								if (t == ctrl.GetType())
								{
									tabctrl.SelectedIndex = tabctrl.Items.Count - 1;
									tab.Content = ctrl;
									//ctrl.SetEditorSynchro(sync);
								} else
								{
									INLTabClient instance = (INLTabClient)Activator.CreateInstance(t);
									instance.SetEditorSynchro(sync);
									instance.SetDataSource(ctrl.GetDatatypeDefinition(), ctrl.GetDataInstance(), ctrl.GetRowID());
									tab.Content = instance;
								}
								((FrameworkElement)tab.Content).HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
							}
						}
					}
					//spvert.Children.Add((FrameworkElement)ctrl);

					doc.Content = tabctrl;//spvert; //ctrl;
				}
				Clients.Add(ctrl);
				DocPane.Children.Add(doc);
				//treeFolders.CurrentWidgetPanel = WidgetPanel; //todo widget panel fix
				DocPane.SelectedContentIndex = DocPane.Children.Count - 1;
			}
		}

		private void tabctrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.Source is TabControl)
			{
				TabControl tabctrl = (TabControl)sender;
				foreach(TabItem tab in tabctrl.Items)
				{
					if(tab.IsSelected)
					{
						if (tab.Content != null)
						{
							((INLTabClient)tab.Content).TabShown();
						}
					}
				}
				e.Handled = true;
			}
		}

		private void InheritedDefs(DatatypeDefinition def, ref List<DatatypeDefinition> Parents)
		{
			if(def.InheritsDefinition != "")
			{
				DatatypeDefinition parentdef = DataProject.CurrentProject.GetDefinition(def.InheritsDefinition);
				if (parentdef != null && !Parents.Contains(parentdef))
				{
					Parents.Add(parentdef);
					InheritedDefs(parentdef, ref Parents);
				}
			}
		}
		private void MenuItem_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();

			dlg.DefaultExt = ".proj";
			dlg.Filter = "Projects (.proj)|*.proj";
			


			// Display OpenFileDialog by calling ShowDialog method
			Nullable<bool> result = dlg.ShowDialog();
			// Get the selected file name and display in a TextBox
			if (result == true)
			{
				ClearRegisteredEditors();
				// Open document
				string filename = dlg.FileName;
				RegisteredTypes RegTypes = RegisteredTypes.DefaultTypes();

				treeFolders.Reset();

				DataProject proj = DataProject.LoadProject(filename, RegTypes);
				DataProject.CurrentProject = proj;
				DataProject.CurrentProject.CurrentManager = this;

				treeFolders.SetProject(proj);
				/*
				foreach (Type t in DataProject.CurrentProject.RegisteredEditors)
				{
					RegisterDocumentEditor(t);
				}*/
				LoadPlugins();
				ucChooseModule cm = new ucChooseModule();
				ItemCollection modules = cm.GetModules();
				foreach(ListBoxItem cbi in modules)
				{
					if (proj.ModuleFQNs.Contains(((ICustomModule)cbi.Tag).GetFQN()))
					{
						foreach (Type t in ((ICustomModule)cbi.Tag).GetEditors())
						{
							RegisterDocumentEditor(t);
						}
						break;
					}
				}
				mnuProject.IsEnabled = true;

				//RegisterDefaultEditors();

				//ucDefinitionEditor defEditor = new ucDefinitionEditor();
				//OpenDocument(defEditor);
			}



		}

		private void ClearRegisteredEditors()
		{
			RegisteredEditors.Clear();
			RegEditorLookup.Clear();
			EditorButtonNames.Clear();
		}

		private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			foreach(INLTabClient client in Clients)
			{
				client.OnParentSizeChanged(sender, e);
			}
		}


		public void RegisterDocumentEditor(Type EditorType)
		{
			RegisteredEditors.Add(EditorType);
			//create one
			INLTabClient instance = (INLTabClient)Activator.CreateInstance(EditorType);
			EditorButtonNames[EditorType] = instance.GetButtonName();
			List<DatatypeDefinition> defs = instance.SupportedEditorFor();
			foreach(DatatypeDefinition def in defs)
			{
				if(!RegEditorLookup.ContainsKey(def))
				{
					RegEditorLookup[def] = new List<Type>();
				}
				RegEditorLookup[def].Add(EditorType);
			}			
		}


		private void MenuItemNew_Click(object sender, RoutedEventArgs e)
		{
			SaveFileDialog sfd = new SaveFileDialog();
			sfd.DefaultExt = ".proj";
			sfd.Filter = "Projects (.proj)|*.proj";
			Nullable<bool> result = sfd.ShowDialog();
			// Get the selected file name and display in a TextBox
			if (result == true)
			{
				// new document
				dlgBasic chooseModule = new dlgBasic();
				ucChooseModule cm = new ucChooseModule();
				chooseModule.SetDialogControl(cm);

				bool? ok = chooseModule.ShowDialog();
				if(ok != null && ok == true)
				{
					ClearRegisteredEditors();
					mnuProject.IsEnabled = true;
					string filename = sfd.FileName;
					treeFolders.Reset();
					RegisteredTypes RegTypes = RegisteredTypes.DefaultTypes();
					
					DataProject.CurrentProject = DataProject.CreateNewProject(filename, RegTypes, cm.SelectedModules());
					treeFolders.SetProject(DataProject.CurrentProject);
					DataProject.CurrentProject.SaveProject();
					DataProject.CurrentProject.CurrentManager = this;
					foreach(Type t in DataProject.CurrentProject.RegisteredEditors)
					{
						RegisterDocumentEditor(t);
					}
				}
			}
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (DataProject.CurrentProject != null)
				DataProject.CurrentProject.SaveProject();
		}

		private void NewDefinition_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucNewItem ni = new ucNewItem();
			chooseModule.SetDialogControl(ni);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if(ni.Validate())
				{
					
					if(DataProject.CurrentProject.DefinitionExists(ni.GetItemName()))
					{
						MessageBox.Show("Definition names must be unique");
					} else
					{
						DatatypeDefinition def = new DatatypeDefinition();
						def.DataName = ni.GetItemName();
						def.Initialize(DataProject.CurrentProject.RegisteredDatatypes);
						DataProject.CurrentProject.AddDefinition("./defs/" + def.DataName + ".def",def);


						DataFolder df = DataProject.CurrentProject.RootFolder["[Definitions]"].CreateSubfolder(def.DataName);
						df.FolderType = DataFolder.NLFOLDERTYPE.NONE;
						df.Tag = def;
						df.FolderName = def.DataName;
						treeFolders.AddFolder(df, DataProject.CurrentProject.RootFolder["[Definitions]"]);

						DataProject.CurrentProject.SaveProject();
						LoadPlugins();
					}
				} else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}
		}

		private void NewLibrary_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucNewDataInstance ndi = new ucNewDataInstance();
			chooseModule.SetDialogControl(ndi);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ndi.Validate())
				{

					if (DataProject.CurrentProject.RootFolder["[Libraries]"].ContainsFolder(ndi.LibraryName()))
					{
						MessageBox.Show("Library names must be unique");
					}
					else
					{
						string name = ndi.LibraryName();
						DataFolder df = DataProject.CurrentProject.CreateLibrary(name, ndi.GetDefinition(), DataProject.CurrentProject.RootFolder["[Libraries]"]);

						treeFolders.AddFolder(df, DataProject.CurrentProject.RootFolder["[Libraries]"]);
					}
				}
				else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}
		}

		private void NewAssetFolder_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucNewItem ni = new ucNewItem();
			chooseModule.SetDialogControl(ni);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ni.Validate())
				{

					if (DataProject.CurrentProject.RootFolder["[Assets]"].ContainsFolder(ni.GetItemName()))
					{
						MessageBox.Show("Folder names must be unique at each level");
					}
					else
					{
						string name = ni.GetItemName();

						DataFolder df = DataProject.CurrentProject.AddAssetFolder(name, 
							DataProject.CurrentProject.RootFolder["[Assets]"], 
							(DataCollection)DataProject.CurrentProject.RootFolder["[Assets]"].Tag);

						treeFolders.AddFolder(df, DataProject.CurrentProject.RootFolder["[Assets]"]);
					}
				}
				else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}
		}

		private void NewFolder_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucNewItem ni = new ucNewItem();
			chooseModule.SetDialogControl(ni);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ni.Validate())
				{

					if (DataProject.CurrentProject.RootFolder.ContainsFolder(ni.GetItemName()))
					{
						MessageBox.Show("Folder names must be unique at each level");
					}
					else
					{
						string name = ni.GetItemName();
						DataFolder df = DataProject.CurrentProject.RootFolder.CreateSubfolder(name);
						df.FolderType = DataFolder.NLFOLDERTYPE.NONE;
						df.FolderName = name;
						treeFolders.AddFolder(df, DataProject.CurrentProject.RootFolder);
					}
				}
				else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}
		}

		private void GenerateModule_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic namePrompt = new dlgBasic();
			ucNewItem ni = new ucNewItem();
			ni.SetPrompt("Please enter a name for this module (alpha characters only):");
			namePrompt.SetDialogControl(ni);
			bool? ok = namePrompt.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ni.Validate())
				{
					if (DataProject.GetModuleByName(ni.GetItemName()) == null)
					{
						dlgBasic fqnPrompt = new dlgBasic();
						ucNewItem ni2 = new ucNewItem();
						ni2.SetPrompt("Please enter an FQN for this module (example: com.domain.type.subtype):");
						fqnPrompt.SetDialogControl(ni2);
						bool? ok2 = fqnPrompt.ShowDialog();
						if (ok2 != null && ok2 == true)
						{
							if (ni2.Validate())
							{
								if (DataProject.GetModuleByFQN(ni2.GetItemName()) == null)
								{
									DataProject.CurrentProject.GenerateModule(ni.GetItemName(), ni2.GetItemName());
								} else
								{
									MessageBox.Show("FQN Must be unique.");
								}
							}
						}
					} else
					{
						MessageBox.Show("Name must be unique");
					}
				}
			}			
		}

		private void MenuItemSave_Click(object sender, RoutedEventArgs e)
		{
			DataProject.CurrentProject.SaveProject();
		}


		public void OpenWidgets(FrameworkElement CurrentWindow, Canvas ContentPane, DataFolder df)
		{
			throw new NotImplementedException();
		}
	}
}
