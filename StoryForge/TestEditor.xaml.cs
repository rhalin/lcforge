﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NodeLynx.CustomData;
using NodeLynx.CustomData.DataTypes;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.Widgets;

namespace StoryForge
{
	/// <summary>
	/// Interaction logic for TestEditor.xaml
	/// </summary>
	public partial class TestEditor : Window
	{
		public TestEditor()
		{
			InitializeComponent();

			RegisteredTypes RegTypes = new RegisteredTypes();
			RegTypes.RegisterType(new NLAutoIdentity());
			RegTypes.RegisterType(new NLDateTime());
			RegTypes.RegisterType(new NLInt());
			RegTypes.RegisterType(new NLTextbox());
			RegTypes.RegisterType(new NLShortText());
			RegTypes.RegisterType(new NLSingleReference());
			RegTypes.RegisterType(new NLMultiReference());
			RegTypes.RegisterType(new NLDataInstance());
			RegTypes.RegisterType(new NLDatatype());
			RegTypes.RegisterWidget(new ucWidget());


			if (false)
			{

				//DataProject proj = DataProject.LoadProject("testproj.proj", RegTypes);
				//edEditor.GenerateEditor(proj.Definitions[0], proj, proj.DataInstances[0]);
			}
			else
			{

				DataProject proj = DataProject.CreateNewProject("./derp/derp.proj", RegTypes);
				proj.RegisterAssetTypeClassifier(".png", "asset.image");
				proj.RegisterAssetTypeClassifier(".png", "asset.image.compressed.lossless");
				proj.RegisterAssetTypeClassifier(".jpg", "asset.image.compressed.lossy");
				proj.RegisterAssetTypeClassifier(".mp3", "asset.media.audio");
				proj.RegisterAssetTypeClassifier(".avi", "asset.media.video");
				proj.RegisterAssetTypeClassifier(".wav", "asset.media.audio");


				DatatypeDefinition dd = new DatatypeDefinition("{9F675592-87D4-4686-BF0D-66FF8EF0628E}");
				DatatypeDefinition dd1 = dd;

				dd.WidgetBindings.Add(new NodeLynx.lib.SerializableKeyValuePair<string, string>("shortText","int2"));
				dd.WidgetBindings.Add(new NodeLynx.lib.SerializableKeyValuePair<string, string>("longText", "str2"));

				dd.Initialize(RegTypes);
				dd.DataName = "Test Definition";

				DatatypeField df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.int"));
				df.FieldLabel = "New Int Field";
				df.FieldName = "int1";
				dd.AddField(Guid.NewGuid(), df);

				df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.int"));
				df.FieldLabel = "New Int Field2";
				df.FieldName = "int2";
				dd.AddField(Guid.NewGuid(), df);

				df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.reference.single"));
				df.FieldLabel = "Single Reference";
				df.FieldName = "ref1";
				dd.AddField(Guid.NewGuid(), df);

				df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.reference.multiple"));
				df.FieldLabel = "Multiple References";
				df.FieldName = "ref2";
				dd.AddField(Guid.NewGuid(), df);

				df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.text.short"));
				df.FieldLabel = "first String";
				df.FieldName = "str1";
				dd.AddField(Guid.NewGuid(), df);

				df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.text.long"));
				df.FieldLabel = "longer text";
				df.FieldName = "str2";
				dd.AddField(Guid.NewGuid(), df);

				proj.AddDefinition("./defs/testdef.def", dd);



				dd = new DatatypeDefinition("{2A82ACA5-5E5A-43F5-92EC-81EAD629C182}");


				dd.Initialize(RegTypes);
				dd.DataName = "Test Definition 2";

				df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.int"));
				df.FieldLabel = "New Int Field 2-3";
				df.FieldName = "int1-2";
				dd.AddField(Guid.NewGuid(), df);

				df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.reference.single"));
				df.FieldLabel = "Single Reference - png + def1";
				df.FieldName = "ref11";
				df.AllowedReferenceDatatypes.Add("asset.image.compressed.lossless");
				df.AllowedReferenceDatatypes.Add("9F675592-87D4-4686-BF0D-66FF8EF0628E".ToLower());
				dd.AddField(Guid.NewGuid(), df);

				df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.reference.single"));
				df.FieldLabel = "Single Reference - general asset.image";
				df.AllowedReferenceDatatypes.Add("asset.image");
				df.FieldName = "ref12";
				dd.AddField(Guid.NewGuid(), df);

				df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.reference.single"));
				df.FieldLabel = "Single Reference - no restriction";
				df.FieldName = "ref13";

				dd.AddField(Guid.NewGuid(), df);

				df = new DatatypeField();
				df.SetDatatype(RegTypes.GetDatatype("data.complex.instance"));
				df.ComplexTypeReferences.Add(dd1.ID);
				df.FieldLabel = "Table of Test Definition 1";
				df.FieldName = "datainstance1";
				dd.AddField(Guid.NewGuid(), df);

				proj.AddDefinition("./defs/testdef2.def", dd);
				DataProject.CurrentProject = proj;
				edEditor.SetDataSource(dd, proj.DataInstances[0]);
				proj.SaveProject();
			}
		}
	}
}
