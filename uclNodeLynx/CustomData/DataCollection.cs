﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeLynx.CustomData
{
	public class DataCollection : DataCollectionItemBase
	{
		//can contain references, other collections, and assets
		public List<DataCollectionItemBase> Children = new List<DataCollectionItemBase>();
	}
}
