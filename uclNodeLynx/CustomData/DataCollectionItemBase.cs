﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NodeLynx.CustomData
{
	[XmlInclude(typeof(DataCollectionItemAsset))]
	[XmlInclude(typeof(DataCollection))]
	public class DataCollectionItemBase
	{
		public string PathToItem = "";
		public string LocalName = "";
		public bool AssetsOnly = false;
		public string ID;
		public DataCollectionItemBase()
		{
			ID = Guid.NewGuid().ToString();
		}
	}
	public class DataCollectionItemAsset : DataCollectionItemBase
	{
		//strongly typed, we may want other data in here, such as update history, file type, etc.

		//this data item needs to be a reference.  So we'll need an asset management class that assigns IDs and other properties to files
		// we may even use a Definition/Instance for this with some locked down fields, that way we can let people edit and add data to files directly
		public long AssetID = -1;
		public string InstanceID = "";
		[XmlIgnore]
		DataInstance Instance;
		[XmlIgnore]
		DataRow Row;
		public void LoadInstance()
		{
			Instance = DataProject.CurrentProject.GetInstance(InstanceID);
			Row = Instance.GotoRowID(AssetID);
		}
	}

	public class DataCollectionItemReference : DataCollectionItemBase
	{
		string InstanceID;
		long RowID;
		[XmlIgnore]
		DataInstance Instance;
		[XmlIgnore]
		DataRow Row;

		//should respond to row deleted event
	}
}
