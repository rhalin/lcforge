﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NodeLynx.CustomData.DataTypes;
using NodeLynx.CustomData.UI;
using NodeLynx.lib;
using NodeLynx.UI;

namespace NodeLynx.CustomData
{
	public class DataFolder
	{
		//only allow this kind of data in this folder
		[XmlIgnore]
		public List<DatatypeDefinition> RestrictedToRefs = new List<DatatypeDefinition>();
		//rowID, datatable that its in
		[XmlIgnore]
		public List<SerializableKeyValuePair<long, DataInstance>> ContentsRefs = new List<SerializableKeyValuePair<long, DataInstance>>();

		//for serialization
		public List<string> RestrictedTo = new List<string>();
		//RowID, InstanceKey -> we can use instancekey to get to the definition
		public List<SerializableKeyValuePair<long, string>> Contents = new List<SerializableKeyValuePair<long, string>>();

		[XmlIgnore]
		public object Tag = null;

		[XmlIgnore]
		public DataFolder Parent = null;

		public void ClearChildren(bool Force=false)
		{
			if(SystemFolder || Force)
			{
				Subfolders.Clear();
			}
		}
		//can this folder be deleted, or does the system need it?
		public bool SystemFolder = false;
		public List<DataFolder> Subfolders = new List<DataFolder>();

		public string FolderName = "";

		public bool IsLibrary = false;

		public enum NLFOLDERTYPE { NONE, LIBRARY, DATATYPE, COLLECTION, DEFINITION, LIBRARY_ROW, OTHER };

		public NLFOLDERTYPE FolderType = NLFOLDERTYPE.NONE;

		public string LibraryID = "";
		public string CollectionID = "";
		public DataFolder this[string index]   
		{
			get 
			{
				for (int i = 0; i < Subfolders.Count(); i++)
				{
					if(Subfolders[i].FolderName == index)
					{
						return Subfolders[i];
					}
				}
				return null;
			}
			private set //uhh, no.
			{
			}
		}
		public bool ContainsFolder(string name)
		{
			for (int i = 0; i < Subfolders.Count(); i++)
			{
				if (Subfolders[i].FolderName == name)
				{
					return true;
				}
			}
			return false;
		}
		public void Reload(DataProject proj)
		{
			//reload references from project data
			foreach (SerializableKeyValuePair<long, string> item in Contents)
			{
				DataInstance inst;
				inst = proj.GetInstance(item.Value);
				if(inst != null)
				{
					ContentsRefs.Add(new SerializableKeyValuePair<long, DataInstance>(item.Key, inst));
				}
			}
			foreach (string item in RestrictedTo)
			{
				DatatypeDefinition def;
				def = proj.GetDefinition(item);
				if (def != null)
				{
					RestrictedToRefs.Add(def);
				}
			}

			switch(FolderType)
			{
				case NLFOLDERTYPE.LIBRARY:
					DataInstance inst;
					inst = proj.GetInstance(LibraryID);
					Tag = inst;
					break;
				case NLFOLDERTYPE.DATATYPE:
					Tag = proj.RegisteredDatatypes.GetDatatype(FolderName.Replace("{","").Replace("}",""));
					break;
				case NLFOLDERTYPE.COLLECTION:
					Tag = proj.GetCollection(CollectionID);
					//todo: get collection
					break;
			}
			if(IsLibrary)
			{

			}

			//recurse the rest
			foreach(DataFolder folder in Subfolders)
			{
				folder.Reload(proj);
				folder.Parent = this;
			}
		}
		public bool AddItem(DataInstance di, long RowID)
		{
			//can't put this type here
			if(RestrictedToRefs.Contains(di.Definition))
			{
				return false;
			} else
			{
				bool found = false;
				foreach (SerializableKeyValuePair<long, DataInstance> item in ContentsRefs)
				{
					if(item.Value == di && item.Key == RowID)					
					{
						found = true;
					}
				}
				if(!found)
				{
					ContentsRefs.Add(new SerializableKeyValuePair<long, DataInstance>(RowID, di));
					Contents.Add(new SerializableKeyValuePair<long, string>(RowID, di.ID.ToString()));
				}
				//this works because if it isn't found, its added
				return !found;
			}
		}

		public void RemoveItem(DataInstance di, int RowID)
		{
			SerializableKeyValuePair<long, DataInstance> f = null;
			SerializableKeyValuePair<long, string> f2 = null;
			foreach (SerializableKeyValuePair<long, DataInstance> item in ContentsRefs)
			{
				if (item.Value == di && item.Key == RowID)
				{
					f = item;
				}
			}
			foreach (SerializableKeyValuePair<long, string> item in Contents)
			{
				if (item.Value == di.ID.ToString()  && item.Key == RowID)
				{
					f2 = item;
				}
			}
			if (f != null)
			{
				ContentsRefs.Remove(f);
			}
			if (f2 != null)
			{
				Contents.Remove(f2);
			}
		}
		public DataFolder CreateSubfolder(string name)
		{
			//todo: do we want subfolders to have unique names at a each depth?
			DataFolder folder = new DataFolder();
			folder.FolderName = name;
			Subfolders.Add(folder);
			folder.Parent = this;
			return folder;
		}

		public string GetTreeviewItemText()
		{
			throw new NotImplementedException();
		}

		public Color GetItemColor()
		{
			if (SystemFolder)
				return Colors.Red;
			else
				return Colors.White;
		}


	}
}
