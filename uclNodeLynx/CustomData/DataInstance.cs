﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Serialization;
using NodeLynx.lib;
using NodeLynx.UI;

namespace NodeLynx.CustomData
{
	public class DataInstanceRowUpdatedEventArgs : PropertyChangedEventArgs
	{
		private string _ColumnID = null;
		private string _InstanceID = "";
		private long _RowID = -1;
		private object _Source;
		public virtual string ColumnID { get { return _ColumnID; } }
		public virtual string InstanceID { get { return _InstanceID; } }
		public virtual long RowID { get { return _RowID; } }
		public virtual object Source { get { return _Source; } }
		public DataInstanceRowUpdatedEventArgs(string propertyName, long rowID, string instanceID, string columnID = "", object source=null)
			: base(propertyName)
		{
			_ColumnID = columnID;
			_RowID = rowID;
			_InstanceID = instanceID;
			_Source = source;
		}
	}
	public class DataInstance : INotifyPropertyChanged
	{
		public Guid _ID;
		//public Control DataControl;
		[XmlIgnore]
		public Guid ID { get { return _ID; } private set { } }

		public DatatypeDefinition Definition = new DatatypeDefinition();
		public LCDataTable Data;
		private int RowIndex = -1;
		public string FileName = "";
		public string LibraryName = "";


		public bool SystemDataInstance = false;

		//ucDataEditor Editor = new ucDataEditor();
		public DataInstance(DatatypeDefinition def)
		{
			Definition = def;
			Data = Definition.GetNewTable();
			def.PropertyChanged += def_PropertyChanged;
			_ID = Guid.NewGuid();
			Data.TableName = _ID.ToString();
		}
		public DataInstance(DatatypeDefinition def, string newID)
		{
			Definition = def;
			Data = Definition.GetNewTable();
			def.PropertyChanged += def_PropertyChanged;
			_ID = Guid.Parse(newID);
			Data.TableName = _ID.ToString();
		}
		public void Reinitialize()
		{
			Definition.PropertyChanged += def_PropertyChanged;
		}
		public void RowUpdated(long ID, string ColumnID = "", object source=null)
		{
			RaisePropertyRowUpdated("__RowUpdated", ID, ID.ToString(), ColumnID, source);
		}
		void def_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e is FieldChangedEventArgs)
			{
				FieldChangedEventArgs e2 = (FieldChangedEventArgs)e;
				if (e2.PropertyName == "__FieldName")
				{
					Data.Columns[e2.OldValue].ColumnName = e2.NewValue;
					RaisePropertyChanged("__FieldName");
				} else if(e2.PropertyName == "__FieldLabel")
				{
					RaisePropertyChanged("__FieldLabel");
				}
				else if (e2.PropertyName == "__DataTypeInfo")
				{
					for(int i = 0; i < Data.Columns.Count; i++)
					{
						//we shouldn't have gotten here if this field is locked, so assume its safe to dump the data
						if(Data.Columns[i].ColumnName == e2.Field.FieldName)
						{
							//remove existing column
							Data.Columns.RemoveAt(i);
							if (i < Data.Columns.Count)
							{
								if (Data.Columns[i].ColumnName == e2.Field.FieldName + "_ref")
								{
									Data.Columns.RemoveAt(i);
									//remove reference column
								}
							}
							//create new column
							DataColumn dc = e2.Field.DataTypeInfo.GenerateDataColumn();
							dc.ColumnName = e2.Field.FieldName;
							//dc.ReadOnly = true;
							e2.Field.RowColumn = i;
							//add it
							Data.Columns.Add(dc);
							//move it to the old position
							dc.SetOrdinal(i);
							if (e2.Field.DataTypeInfo.IsReferencetype())
							{
								dc = e2.Field.DataTypeInfo.GenerateDataReferenceColumn();
								dc.ColumnName = e2.Field.FieldName + "_ref";
								//dc.ReadOnly = true;
								e2.Field.RowColumn = i + 1;
								Data.Columns.Add(dc);
								dc.SetOrdinal(i+1);
							}

							InitializeFieldAcrossRows(e2.Field, dc);
							break;
						}
					}
					RaisePropertyChanged("__Definition");
				}
			} else if(e is FieldAddedEventArgs)
			{
				FieldAddedEventArgs e2 = (FieldAddedEventArgs)e;
				DataColumn dc = GenerateColumnData(e2.NewField);
				//initialize field across all existing rows (this...could suck.)
				InitializeFieldAcrossRows(e2.NewField, dc);
				RaisePropertyChanged("__FieldAdded");
			} else if(e is FieldRemovedEventArgs)
			{
				FieldRemovedEventArgs e2 = (FieldRemovedEventArgs)e;
				for(int i = 0; i < Data.Columns.Count; i++)
				{
					if(Data.Columns[i].ColumnName == e2.NewField.FieldName)
					{
						Data.Columns.RemoveAt(i);
						if(e2.NewField.DataTypeInfo.IsReferencetype())
						{
							//remove ref column too
							Data.Columns.RemoveAt(i);
						}
						RaisePropertyChanged("__FieldRemoved");
						break;
					}
				}
			}
		}

		private void InitializeFieldAcrossRows(DatatypeField field, DataColumn dc)
		{
			for (int i = 0; i < Data.Rows.Count; i++)
			{
				Data.Columns[dc.ColumnName].ReadOnly = false;
				object obj = Data.Rows[i][dc];
				field.DataTypeInfo.InitializeNewData(ref obj, Definition, (long)Data.Rows[i][0], field.ComplexTypeReferences, this); //col 0 is ALWAYS RowID
				Data.Rows[i][dc] = obj;
				//Data.Columns[dc.ColumnName].ReadOnly = true;
			}
		}
		public DataInstance(bool LoadingFromFile=true)
		{
			if (LoadingFromFile == false)
				throw new Exception("you had better be.");
		}
		public DataInstance()
		{
			//don't use this
		}
		public void LoadInstance(string FileName)
		{
			Data = Definition.GetNewTable();

			XmlSerializer SerializerObj = new XmlSerializer(typeof(LCDataTable));
			FileStream ReadFileStream = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
			// Load the object saved above by using the Deserialize function
			Data = (LCDataTable)SerializerObj.Deserialize(ReadFileStream);
			ReadFileStream.Close();
		}
		public void SaveInstance(string projPath)
		{
			Directory.SetCurrentDirectory(Path.GetDirectoryName(projPath));
			if (!Directory.Exists(Path.GetDirectoryName(FileName)))
			{
				Directory.CreateDirectory(Path.GetDirectoryName(FileName));
			}
			Data.TableName = ID.ToString();
			XmlSerializer SerializerObj = new XmlSerializer(typeof(DataInstance));
			TextWriter WriteFileStream = new StreamWriter(FileName);
			SerializerObj.Serialize(WriteFileStream, this);
			WriteFileStream.Close();
			Console.WriteLine(DateTime.Now.ToLongDateString() + " Saving..." + FileName);
		}

		public LCDataRow NextRow()
		{
			if (RowIndex + 1 < Data.Rows.Count)
			{
				RowIndex++;
				return (LCDataRow)Data.Rows[RowIndex];
			}
			else
				return null;
		}
		public LCDataRow PrevRow()
		{
			if (RowIndex > 0)
			{
				RowIndex--;
				return (LCDataRow)Data.Rows[RowIndex];
			}
			else
				return null;
		}

		public LCDataRow GotoRow(int index)
		{
			if(index > -1 && index < Data.Rows.Count)
			{
				RowIndex = index;
				return (LCDataRow)Data.Rows[RowIndex];
			}
			return null;
		}
		public LCDataRow GotoRowID(long ID)
		{
			EnumerableRowCollection<DataRow> r = Data.AsEnumerable().Where(dr => dr.Field<long>("RowID") == ID);
			if(r.Count() > 0)
			{
				LCDataRow dr = (LCDataRow)r.First();
				RowIndex = Data.Rows.IndexOf(dr);
				return dr;
			} else
			{
				RowIndex = -1;
				return null;
			}
		}
		private string NextID()
		{
			string ID = DataProject.CurrentProject.NextID(this.ID.ToString());			
			return ID;
		}
		public LCDataRow NewRow()
		{
			LCDataRow dr = (LCDataRow)Data.NewRow();
			Data.Columns[0].ReadOnly = false;
			Data.Columns[1].ReadOnly = false;
			Data.Columns[2].ReadOnly = false;
			Data.Columns[3].ReadOnly = false;
			Data.Columns[4].ReadOnly = false;
			dr[0] = IDConverter.IDToDecimal(NextID());
			dr[1] = "[Anonymous]";
			dr[2] = DateTime.Now;
			dr[3] = "[Anonymous]";
			dr[4] = DateTime.Now;
			Data.Columns[0].ReadOnly = true;
			Data.Columns[1].ReadOnly = true;
			Data.Columns[2].ReadOnly = true;
			Data.Columns[3].ReadOnly = true;
			Data.Columns[4].ReadOnly = true;
			Data.Rows.Add(dr);

			RowIndex = Data.Rows.Count - 2;

			//these fields can no longer have their type changed in the editor or be re-ordered.
			Definition.SetFieldsInUse();
			for (int fieldAt = 0, colAt=0; fieldAt < Definition.Fields.Count; fieldAt++, colAt++)
			{
				if(Definition.Fields[fieldAt].Value.DataTypeInfo.IsReferencetype())
				{
					colAt++;
				}
				//can't pass indexers by references.  Kind of negates the reason for explicit reference passing here
				// I suspect there's some C# trickery to do this how I want, but for now this hack.
				Data.Columns[colAt].ReadOnly = false;
				object obj = dr[colAt];
				Definition.Fields[fieldAt].Value.DataTypeInfo.InitializeNewData(ref obj, Definition, (long)dr[0], Definition.Fields[fieldAt].Value.ComplexTypeReferences, this); //col 0 is ALWAYS RowID
				dr[colAt] = obj;
				//Data.Columns[colAt].ReadOnly = true;
			}

			return NextRow();
		}
		public void DeleteRow(long RowID)
		{

			EnumerableRowCollection<DataRow> r = Data.AsEnumerable().Where(dr => dr.Field<long>("RowID") == RowID);
			if (r.Count() > 0)
			{
				DataRow dr = r.First();
				Data.Rows.Remove(dr);
				RaisePropertyChanged("__RowDeleted");
			}
		}

		public void Load(string Filename)
		{
			FileName = Filename;
			//deserialize from file
		}

		public void AddRow(LCDataRow dr)
		{
			//Data.Rows.Add(dr);
			RaisePropertyChanged("__RowAdded");
		}

		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			// take a copy to prevent thread issues
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		private void RaisePropertyRowUpdated(string propertyName, long RowID, string Instance, string ColumnID, object source)
		{
			// take a copy to prevent thread issues
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new DataInstanceRowUpdatedEventArgs(propertyName, RowID, Instance, ColumnID, source));
			}
		}
		private DataColumn GenerateColumnData(DatatypeField Field)
		{
			DataColumn dc = Field.DataTypeInfo.GenerateDataColumn();
			dc.ColumnName = Field.FieldName;
			//dc.ReadOnly = true;
			//todo: here detect if something has changed and change it
			// for now, just rewire, assume definitions don't change
			// might not change here because its the definition, would change after loading data
			// we can compare against this to see if cols changed and deal appropriately
			Field.RowColumn = Data.Columns.Count;
			Data.Columns.Add(dc);

			if (Field.DataTypeInfo.IsReferencetype())
			{
				dc = Field.DataTypeInfo.GenerateDataReferenceColumn();
				dc.ColumnName = Field.FieldName + "_ref";
				//dc.ReadOnly = true;
				Field.RowColumn = Data.Columns.Count;
				Data.Columns.Add(dc);
			}
			return dc;
		}
	}
}
