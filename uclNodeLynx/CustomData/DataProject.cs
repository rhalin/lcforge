﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using NodeLynx.CustomData.DataTypes;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.lib;
using NodeLynx.Module;
using NodeLynx.Module.AssetViewer;
using NodeLynx.Module.Node;
using NodeLynx.Module.QualCode;
using NodeLynx.Module.StoryFlow;
using NodeLynx.Node.Linker;
using NodeLynx.Node.NodeType;
using NodeLynx.UI;
using NodeLynx.UserManagement;

namespace NodeLynx.CustomData
{
	public class DataProject
	{
		public string FileName = "";
		//public string ModuleFQN = "";
		//filenames
		public List<string> DefinitionFiles = new List<string>();
		public List<string> DataInstanceFiles = new List<string>();
		public List<string> CollectionFiles = new List<string>();
		public List<string> ModuleFQNs = new List<string>();

		public UserID CurrentUser = new UserID();

		public DataFolder RootFolder = new DataFolder();
		public DataTable InstanceIDs = null; 
		public RegisteredTypes RegisteredDatatypes;
		public static DataProject CurrentProject;

		[XmlIgnore]
		public INLDocumentManager CurrentManager = null;
		[XmlIgnore]
		public DataCollection AssetCollection;
		public const string AssetLibraryID = "B8D08F13-B706-487E-AC50-3C36DC1C54CE";
		public const string AssetDefinitionID = "D0A7E54C-2212-40AD-BED6-061EE9F23419";

		[XmlIgnore]
		public List<DatatypeDefinition> Definitions = new List<DatatypeDefinition>();
		[XmlIgnore]
		public List<DataInstance> DataInstances = new List<DataInstance>();

		public List<DataCollection> Collections = new List<DataCollection>();
		[XmlIgnore]
		public Dictionary<string, DatatypeDefinition> DefinitionLookup = new Dictionary<string, DatatypeDefinition>();
		[XmlIgnore]
		public Dictionary<string, DataInstance> InstanceLookup = new Dictionary<string, DataInstance>();
		[XmlIgnore]
		public Dictionary<string, DataCollection> CollectionLookup = new Dictionary<string, DataCollection>();
		[XmlIgnore]
		public List<Type> RegisteredEditors = new List<Type>();
		[XmlIgnore]
		private static Dictionary<string, ICustomModule> LoadableModules = new Dictionary<string, ICustomModule>();  //all modules in the plugins dir
		[XmlIgnore]
		private List<ICustomModule> RegisteredModules = new List<ICustomModule>(); //actual modules this project uses
		//extension, assettype
		public List<SerializableKeyValuePair<string, string>> AssetTypeClassifiers = new List<SerializableKeyValuePair<string, string>>();
		static DataProject()
		{
			ReloadModules();
		}
		public static ICustomModule GetModuleByName(string name)
		{
			foreach(KeyValuePair<string, ICustomModule> mod in LoadableModules)
			{
				if(mod.Value.GetName() == name)
				{
					return mod.Value;
				}
			}
			return null;
		}
		public List<Tuple<string, string>> GetDefinitionTypes(bool includeNull=false)
		{
			List<Tuple<string, string>> names = new List<Tuple<string, string>>();
			if(includeNull)
			{
				Tuple<string, string> tup = new Tuple<string, string>(Guid.Empty.ToString(), "[None]");
			}
			foreach (DatatypeDefinition def in Definitions)
			{
				Tuple<string, string> tup = new Tuple<string, string>(def.IDStr.ToLower(),def.DataName);
				names.Add(tup);
			}
			return names;
		}
		public static ICustomModule GetModuleByFQN(string FQN)
		{
			if(LoadableModules.ContainsKey(FQN))
			{
				return LoadableModules[FQN];
			} else
			{
				return null;
			}
		}
		private static void ReloadModules()
		{
			LoadableModules.Clear();
			ICustomModule mod = new AssetViewerModule();
			LoadableModules[mod.GetFQN()] = mod;
			mod = new DefaultModule();
			LoadableModules[mod.GetFQN()] = mod;
			mod = new BaseModule();
			LoadableModules[mod.GetFQN()] = mod;
			mod = new QualCodingModule();
			LoadableModules[mod.GetFQN()] = mod;
			mod = new NodeViewerModule();
			LoadableModules[mod.GetFQN()] = mod;
			mod = new StoryFlowModule();
			LoadableModules[mod.GetFQN()] = mod;


			string modPath = new Uri(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath + "/Modules/";
			if (!Directory.Exists(modPath))
			{
				Directory.CreateDirectory(modPath);
			}
			string[] fileEntries = Directory.GetFiles((new Uri(modPath).LocalPath));
			foreach (string fileName in fileEntries)
			{
				//load each 
				Assembly module = Assembly.LoadFile(fileName);
				Type[] Types = module.GetTypes();
				foreach (Type t in Types)
				{
					if (typeof(ICustomModule).IsAssignableFrom(t))
					{
						ICustomModule custMod = (ICustomModule)Activator.CreateInstance(t);
						LoadableModules[custMod.GetFQN()] = custMod;
					}
				}
			}

		}
		private void LoadModuleRegisteredTypes()
		{
			foreach (ICustomModule mod in RegisteredModules)
			{
				RegisteredTypes regt = mod.GetRequiredDatatypes();
				foreach (KeyValuePair<string, NLDataType> t in regt.GetTypes())
				{
					if (!RegisteredDatatypes.GetTypes().ContainsKey(t.Key))
					{
						RegisteredDatatypes.RegisterType(t.Value);
					}
				}
				foreach (KeyValuePair<string, IDataWidget> t in regt.GetWidgetTypes())
				{
					if (!RegisteredDatatypes.GetWidgetTypes().ContainsKey(t.Key))
					{
						RegisteredDatatypes.RegisterWidget(t.Value);
					}
				}
				foreach (KeyValuePair<string, ucLFNode> t in regt.GetNodeTypes())
				{
					if (!RegisteredDatatypes.GetNodeTypes().ContainsKey(t.Key))
					{
						RegisteredDatatypes.RegisterNode(t.Value);
					}
				}
				foreach (KeyValuePair<string, lnkLFLinker> t in regt.GetNodeLinkerTypes())
				{
					if (!RegisteredDatatypes.GetNodeLinkerTypes().ContainsKey(t.Key))
					{
						RegisteredDatatypes.RegisterNodeLinker(t.Value);
					}
				}
			}
		}
		public static DataProject CreateNewProject(string fileName, RegisteredTypes RegTypes, List<string> Modules = null)
		{
			DataProject.ReloadModules();
			DataProject proj = new DataProject();
			proj.CurrentUser = new UserID();
			if(Modules != null)
			{
				foreach(string mod in Modules)
				{
					proj.ModuleFQNs.Add(mod);
					proj.RegisteredModules.Add(LoadableModules[mod]);
				}
			}
			Directory.SetCurrentDirectory(Path.GetDirectoryName(fileName));
			if (!Directory.Exists("./GeneratedModules/"))
			{
				Directory.CreateDirectory("./GeneratedModules/");
				Directory.CreateDirectory("./GeneratedModules/bin/");
				Directory.CreateDirectory("./GeneratedModules/src/");
			}
			proj.FileName = fileName;

			if (Modules == null)
			{
				proj.RootFolder = DataProject.CreateDefaultFolderSystem();
			} else
			{
				//we'll need to merge folder systems.  Eww.
				proj.RootFolder = proj.RegisteredModules[0].GetFolderSystem();
			}
			proj.RootFolder["[Assets]"].Tag = proj.AssetCollection;

			proj.LoadRegisteredTypes(RegTypes);


			proj.CreateIDSystem();


			if(Modules != null)
			{
				foreach (ICustomModule mod in proj.RegisteredModules)
				{
					RegisteredTypes regt = mod.GetRequiredDatatypes();
					foreach(KeyValuePair<string, NLDataType> t in regt.GetTypes())
					{
						if(!proj.RegisteredDatatypes.GetTypes().ContainsKey(t.Key))
						{
							proj.RegisteredDatatypes.RegisterType(t.Value);
						}
					}
					foreach (KeyValuePair<string, IDataWidget> t in regt.GetWidgetTypes())
					{
						if (!proj.RegisteredDatatypes.GetWidgetTypes().ContainsKey(t.Key))
						{
							proj.RegisteredDatatypes.RegisterWidget(t.Value);
						}
					}
					foreach (KeyValuePair<string, ucLFNode> t in regt.GetNodeTypes())
					{
						if (!proj.RegisteredDatatypes.GetNodeTypes().ContainsKey(t.Key))
						{
							proj.RegisteredDatatypes.RegisterNode(t.Value);
						}
					}
					foreach (KeyValuePair<string, lnkLFLinker> t in regt.GetNodeLinkerTypes())
					{
						if (!proj.RegisteredDatatypes.GetNodeLinkerTypes().ContainsKey(t.Key))
						{
							proj.RegisteredDatatypes.RegisterNodeLinker(t.Value);
						}
					}
				}
				List<DatatypeDefinition> defs = new List<DatatypeDefinition>();
				foreach (ICustomModule mod in proj.RegisteredModules)
				{
					List<DatatypeDefinition> tmpDefs = mod.GetDefinitions();
					foreach(DatatypeDefinition deftmp in tmpDefs)
					{
						if (!defs.Contains(deftmp))  //todo: override = and equals no defs to match ID
						{
							defs.Add(deftmp);
						}
					}
				}
				//proj.LoadRegisteredTypes(Module.GetRequiredDatatypes());
				//List<DatatypeDefinition> defs = Module.GetDefinitions();
				foreach(DatatypeDefinition defin in defs)
				{
					proj.AddDefinition("./defs/" + defin.FileName, defin);
				}

				List<Tuple<string, string>> classes = new List<Tuple<string, string>>();
				foreach (ICustomModule mod in proj.RegisteredModules)
				{
					List<Tuple<string, string>> clastmp = mod.GetAssetTypeClassifiers();
					classes = classes.Union(clastmp).ToList();
				}
				//List<Tuple<string, string>> classes = Module.GetAssetTypeClassifiers();
				foreach (Tuple<string, string> cl in classes)
				{
					proj.RegisterAssetTypeClassifier(cl.Item1, cl.Item2);
				}
				List<Type> editors = new List<Type>();
				foreach (ICustomModule mod in proj.RegisteredModules)
				{
					List<Type> tmp = mod.GetEditors();
					editors = editors.Union(tmp).ToList();
				}
				foreach(Type t in editors)
				{
					if (typeof(INLTabClient).IsAssignableFrom(t))
					{
						proj.RegisteredEditors.Add(t);
					} else
					{
						throw new Exception("Attempting to load custom editor failed - class does not implement INLTabClient");
					}
				}
			}
			DatatypeDefinition def = new DatatypeDefinition();
			def.ID = Guid.Empty;
			def.DataName = "BaseDefinition";
			def.SystemDefinition = true;
			def.PrintableName = "Base";
			def.Initialize(proj.RegisteredDatatypes);
			proj.AddDefinition("./defs/~basedefinition.def", def);

			proj.CreateAssetData(proj.RootFolder["[Assets]"]);
			return proj;
		}

		private void CreateIDSystem()
		{
			InstanceIDs = new DataTable();
			InstanceIDs.TableName = "InstanceIDs";
			DataColumn col = new DataColumn();
			col.DataType = System.Type.GetType("System.Int32");
			col.AutoIncrement = true;
			col.AutoIncrementSeed = 1000;
			col.AutoIncrementStep = 1;
			col.ColumnName = "ID";
			InstanceIDs.Columns.Add(col);

			col = new DataColumn();
			col.DataType = typeof(Guid);
			col.ColumnName = "InstanceID";
			InstanceIDs.Columns.Add(col);

			col = new DataColumn();
			col.DataType = System.Type.GetType("System.Int64");
			col.ColumnName = "LastID";
			InstanceIDs.Columns.Add(col);

			col = new DataColumn();
			col.DataType = typeof(DateTime);
			col.ColumnName = "ModifiedOn";
			InstanceIDs.Columns.Add(col);

			col = new DataColumn();
			col.DataType = typeof(string);
			col.ColumnName = "ModifiedBy";
			InstanceIDs.Columns.Add(col);
		}
		public string NextID(string InstanceID)
		{
			string ID = CurrentUser.ID;
			foreach(DataRow dr in InstanceIDs.Rows)
			{
				if(dr["InstanceID"].ToString() == InstanceID)
				{
					long lastID = (long)dr["LastID"];
					lastID++;
					dr["LastID"] = lastID;
					dr["ModifiedOn"] = DateTime.Now;
					dr["ModifiedBy"] = CurrentUser.Username;
					//convert
					ID += IDConverter.DecimalToID(lastID);
				}
			}
			SaveOnlyProjectFile(FileName); //yeah.
			return ID;
		}
		public static DataFolder CreateDefaultFolderSystem()
		{
			DataFolder fld = new DataFolder();

			fld.FolderName = "[Root]";
			fld.SystemFolder = true;
			//all assets go in here, but can be organized by folders
			//collections can be created anywhere else that reference these assets
			DataFolder subfld = fld.CreateSubfolder("[Assets]");
			subfld.SystemFolder = true;
			DataFolder assetfld = subfld;

			subfld = fld.CreateSubfolder("[Datatypes]");
			subfld.SystemFolder = true;
			subfld = fld.CreateSubfolder("[Definitions]");
			subfld.SystemFolder = true;
			subfld = fld.CreateSubfolder("[Libraries]");
			subfld.SystemFolder = true;
			subfld = fld.CreateSubfolder("[Collections]");
			subfld.SystemFolder = false;

			return fld;
		}
		public void CreateAssetData(DataFolder fld)
		{
			DatatypeDefinition dd = new DatatypeDefinition();
			//this definition is globally identifiable by this ID.  It will always be the same.
			dd.ID = Guid.Parse("{D0A7E54C-2212-40AD-BED6-061EE9F23419}");
			dd.Initialize(RegisteredDatatypes);
			dd.SystemDefinition = true;
			dd.PrintableName = "Assets";

			dd.DisplayWidgetFQN = "widget.asset";
			dd.WidgetBindings.Add(new SerializableKeyValuePair<string, string>("icon","AssetPath"));
			dd.WidgetBindings.Add(new SerializableKeyValuePair<string, string>("extension", "Extension"));
			dd.WidgetBindings.Add(new SerializableKeyValuePair<string, string>("filename", "Filename"));


			DatatypeField f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.short"));
			f.FieldName = "FilePath";
			f.FieldLabel = "FilePath";
			f.IsVisible = false;
			dd.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.short"));
			f.FieldName = "Filename";
			f.FieldLabel = "Filename";
			f.IsNameField = true;
			f.IsVisible = true;
			dd.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.short"));
			f.FieldName = "Extension";
			f.FieldLabel = "Extension";
			f.IsVisible = false;
			dd.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("datatype"));
			f.FieldName = "Datatype";
			f.FieldLabel = "Asset Datatypes";
			f.IsVisible = false;
			dd.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.short"));
			f.FieldName = "AssetPath";
			f.FieldLabel = "AssetPath";
			f.IsVisible = false;
			dd.AddField(Guid.NewGuid(), f);

			dd.DataName = "[AssetData]";

			DataInstance di = new DataInstance(dd, AssetLibraryID);
			di.SystemDataInstance = true;
			AddDefinition("./defs/~AssetData.def", dd);
			AddInstance("./data/~AssetData.db",di);

			AssetCollection = NewCollection("[Assets]", "./Assets");
			AssetCollection.ID = Guid.Empty.ToString();
			AssetCollection.AssetsOnly = true;
			fld.Tag = AssetCollection;



			AddAssetFolder("graphics", fld, AssetCollection);
			AddAssetFolder("sounds", fld, AssetCollection);

			DataFolder fld2 = RootFolder["[Libraries]"].CreateSubfolder("[Assets]");
			fld2.IsLibrary = true;
			fld2.SystemFolder = true;
			fld2.Tag = di;
			fld2.LibraryID = di.ID.ToString();

			/*
			DataCollection dc = new DataCollection();
			dc.PathToItem = AssetCollection.PathToItem + "/graphics";
			dc.LocalName = "graphics";			
			AssetCollection.Children.Add(dc);
			if (!Directory.Exists(dc.PathToItem))
			{
				Directory.CreateDirectory(dc.PathToItem);
			}

			dc = new DataCollection();
			dc.PathToItem = AssetCollection.PathToItem + "/sound";
			dc.LocalName = "sound";
			AssetCollection.Children.Add(dc);
			if (!Directory.Exists(dc.PathToItem))
			{
				Directory.CreateDirectory(dc.PathToItem);
			}
			 * */

		}
		public void LoadRegisteredTypes(RegisteredTypes RegTypes)
		{
			DataFolder dt = RootFolder["[Datatypes]"];
			dt.ClearChildren();
			if (RegisteredDatatypes == null)
			{
				RegisteredDatatypes = new RegisteredTypes();
			}
//			RegisteredDatatypes = RegTypes;
			foreach (KeyValuePair<string, NLDataType> regtype in RegTypes.GetTypes())
			{
				if (!dt.ContainsFolder("{" + regtype.Key + "}"))
				{
					RegisteredDatatypes.RegisterType(regtype.Value);
					DataFolder subfld = dt.CreateSubfolder("{" + regtype.Key + "}");
					subfld.Tag = regtype.Value;
					subfld.FolderType = DataFolder.NLFOLDERTYPE.DATATYPE;
					subfld.SystemFolder = true;
				}
			}
			foreach (KeyValuePair<string, IDataWidget> regtype in RegTypes.GetWidgetTypes())
			{
				RegisteredDatatypes.RegisterWidget(regtype.Value);
			}
			foreach(KeyValuePair<string, ucLFNode> node in RegTypes.GetNodeTypes())
			{
				RegisteredDatatypes.RegisterNode(node.Value);
			}
			foreach (KeyValuePair<string, lnkLFLinker> linker in RegTypes.GetNodeLinkerTypes())
			{
				RegisteredDatatypes.RegisterNodeLinker(linker.Value);
			}
		}
		public void GenerateModule(string name, string FQN)
		{
			ModuleGenerator.GenerateModule(this, name, FQN);
			ModuleGenerator.CompileModule(this, name, FQN);
		}
		public bool DefinitionExists(string name)
		{
			return Definitions.Find(x => x.DataName.ToLower() == name.ToLower()) == null ? false : true;
		}
		public DatatypeDefinition GetDefinition(string ID)
		{
			return Definitions.FirstOrDefault(x => x._ID.ToString().ToLower() == ID.ToLower());
		}
		public DataInstance GetInstance(string ID)
		{
			return DataInstances.FirstOrDefault(x => x.ID.ToString().ToLower() == ID.ToLower());
		}
		public List<Tuple<string, DataInstance>> GetInstancesByType(string DefinitionID)
		{
			List<Tuple<string, DataInstance>> Instances = new List<Tuple<string, DataInstance>>();
			foreach(DataInstance di in DataInstances)
			{
				if(di.Definition.ID.ToString().ToLower() == DefinitionID.ToLower())
				{
					Instances.Add(new Tuple<string,DataInstance>(di.LibraryName, di));
				}
			}
			return Instances;
		}
		public DataCollection GetCollection(string ID)
		{
			return Collections.FirstOrDefault(x => x.ID.ToString().ToLower() == ID.ToLower());
		}
		public DataCollection NewCollection(string name, string path)
		{
			DataCollection col = new DataCollection();
			col.PathToItem = path;
			if (!Directory.Exists(col.PathToItem))
			{
				Directory.CreateDirectory(col.PathToItem);
			}
			col.LocalName = name;
			Collections.Add(col);
			CollectionLookup[col.ID] = col;
			//CollectionFiles.Add(col.PathToItem);

			return col;
		}
		public static DataProject LoadProject(string Filename, RegisteredTypes RegTypes)
		{
			//FileName = Filename;
			XmlSerializer SerializerObj = new XmlSerializer(typeof(DataProject));
			FileStream ReadFileStream = new FileStream(Filename, FileMode.Open, FileAccess.Read, FileShare.Read);
			// Load the object saved above by using the Deserialize function
			DataProject Data = (DataProject)SerializerObj.Deserialize(ReadFileStream);
			//reset the file name
			Data.FileName = Filename;
			Data.CurrentUser = new UserID();
			ReadFileStream.Close();

			Data.RegisteredDatatypes = RegTypes;

			//modules
			if (Data.ModuleFQNs != null)
			{
				foreach (string mod in Data.ModuleFQNs)
				{
					//proj.ModuleFQNs.Add(mod);
					Data.RegisteredModules.Add(LoadableModules[mod]);
				}
				Data.LoadModuleRegisteredTypes();
				
			}

			Directory.SetCurrentDirectory(Path.GetDirectoryName(Filename));
			//load each definition
			foreach (string filename in Data.DefinitionFiles)
			{
				DatatypeDefinition dtd = new DatatypeDefinition();
				SerializerObj = new XmlSerializer(typeof(DatatypeDefinition));
				Directory.SetCurrentDirectory(Path.GetDirectoryName(Filename));
				ReadFileStream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
				dtd = (DatatypeDefinition)SerializerObj.Deserialize(ReadFileStream);
				ReadFileStream.Close();
				dtd.Reload(RegTypes);
				Data.DefinitionLookup[dtd.ID.ToString()] = dtd;
				Data.Definitions.Add(dtd);
			}
			//todo: load parent references

			//load data instances
			Data.RootFolder["[Libraries]"].ClearChildren();
			foreach (string filename in Data.DataInstanceFiles)
			{
				DataInstance di = new DataInstance(true);
				SerializerObj = new XmlSerializer(typeof(DataInstance));
				Directory.SetCurrentDirectory(Path.GetDirectoryName(Filename));
				ReadFileStream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
				di = (DataInstance)SerializerObj.Deserialize(ReadFileStream);
				di.FileName = filename;
				ReadFileStream.Close();
				//rewire this
				di.Definition = Data.DefinitionLookup[di.Definition.ID.ToString()];
				di.Reinitialize();
				Data.InstanceLookup[di.ID.ToString()] = di;
				Data.DataInstances.Add(di);
				di.GotoRow(0);
				if (di.SystemDataInstance)
				{
					DataFolder fld = Data.RootFolder["[Libraries]"].CreateSubfolder("{" + di.Definition.DataName + "}");
					fld.IsLibrary = true;
					fld.Tag = di;
					fld.LibraryID = di.ID.ToString();
				}
				if(di.Data.Rows.Count > 0)
				{
					di.Definition.SetFieldsInUse();
				}
			}

			foreach(DataCollection col in Data.Collections)
			{
				Data.CollectionLookup[col.ID] = col;
			}

			//load asset data
			//todo - we're gonna want this to be asynchronous later, or have asset folders in different files.  For now, this is a good enough hack.
			/*
			SerializerObj = new XmlSerializer(typeof(DataCollection));
			ReadFileStream = new FileStream("./~AssetData.lib", FileMode.Open, FileAccess.Read, FileShare.Read);
			Data.AssetCollection = (DataCollection)SerializerObj.Deserialize(ReadFileStream);
			ReadFileStream.Close();
			*/
			Data.AssetCollection = Data.GetCollection(Guid.Empty.ToString());
			Data.RootFolder.Reload(Data);
			Data.ReloadDefinitionFolder();

			//prep asset folder
			Data.RootFolder["[Assets]"].FolderType = DataFolder.NLFOLDERTYPE.COLLECTION;
			Data.RootFolder["[Assets]"].Tag = Data.AssetCollection;
			DataFolder assetfld = Data.RootFolder["[Assets]"];
			//assetfld.ClearChildren();
			//foreach (DataFolder col in assetfld.Subfolders)
			//{
				//Data.ReloadAssetFolder(col.Tag, assetfld);
			//}
			//Data.ReloadAssetFolder(Data.AssetCollection, Data.RootFolder["[Assets]"]);

			//load editors
			List<Type> editors = new List<Type>();
			foreach (ICustomModule mod in Data.RegisteredModules)
			{
				List<Type> tmp = mod.GetEditors();
				editors = editors.Union(tmp).ToList();
			}
			foreach (Type t in editors)
			{
				if (typeof(INLTabClient).IsAssignableFrom(t))
				{
					Data.RegisteredEditors.Add(t);
				}
				else
				{
					throw new Exception("Attempting to load custom editor failed - class does not implement INLTabClient");
				}
			}
			return Data;
		}
		public DataFolder AddAssetFolder(string name, DataFolder parentDataFolder, DataCollection parentDataCollection)
		{
			if (!parentDataFolder.ContainsFolder(name))
			{
				DataCollection col = NewCollection(name, ((DataCollection)parentDataFolder.Tag).PathToItem + "/" + name);
				//col.PathToItem = ((DataCollection)parentDataFolder.Tag).PathToItem + "/" + name;
				//col.LocalName = name;
				Directory.SetCurrentDirectory(Path.GetDirectoryName(FileName));
				if (!Directory.Exists(col.PathToItem))
				{
					Directory.CreateDirectory(col.PathToItem);
				}

				DataFolder newfld = parentDataFolder.CreateSubfolder(name);
				newfld.FolderType = DataFolder.NLFOLDERTYPE.COLLECTION;
				newfld.Tag = col;
				newfld.CollectionID = col.ID;
				//parentDataFolder.Subfolders.Add(newfld);

				parentDataCollection.Children.Add(col);

				return newfld;
			}
			return null;
		}
		public List<SerializableKeyValuePair<string, string>> GetAssetClassifiers(string extension)
		{
			return AssetTypeClassifiers.Where(x => x.Key == extension).ToList();
		}
		public SerializableKeyValuePair<long, string> ImportAsset(string filename, DataFolder fld)
		{
			//get the path
			string dirPath = ((DataCollection)fld.Tag).PathToItem;
			string newFileLoc = dirPath + "/" + Path.GetFileName(filename);
			string extension = Path.GetExtension(filename).ToLower();
			DataCollection parentCol = (DataCollection)fld.Tag;

			List<SerializableKeyValuePair<string, string>> datatypes = GetAssetClassifiers(extension);
			string types = "";
			if (datatypes.Count > 0)
				types = datatypes[0].Value;
			for(int i = 1; i <= datatypes.Count() - 1; i++)
			{
				types += ";" + datatypes[i];
			}

			SerializableKeyValuePair<long, string> pair = new SerializableKeyValuePair<long, string>();
			//copy the file into the appropriate dir
			try
			{
				Directory.SetCurrentDirectory(Path.GetDirectoryName(FileName));
				File.Copy(filename, newFileLoc, true);
				//add the entry to the asset database
				DataInstance di = GetInstance(AssetLibraryID);

				LCDataRow dr = di.NewRow();
				di.Data.Columns["Filename"].ReadOnly = false;
				dr["Filename"] = Path.GetFileName(filename);
				dr["FilePath"] = newFileLoc;
				dr["Datatype"] = types;
				dr["Extension"] = extension;
				string assetPath = "/" + Path.GetFileName(filename);
				DataFolder parent = fld;
				while (parent.FolderName != "[Assets]")
				{
					assetPath = "/" + parent.FolderName + assetPath;
					parent = parent.Parent;
				}
				assetPath = "./Assets" + assetPath;
				dr["assetPath"] = assetPath;
 
				di.AddRow(dr);
				pair.Key = (long)dr["RowID"];
				pair.Value = AssetLibraryID;
				fld.Contents.Add(pair);
				DataCollectionItemAsset assetColItem = new DataCollectionItemAsset();
				assetColItem.PathToItem = assetPath.TrimStart('.');
				assetColItem.LocalName = dr["Filename"].ToString();
				assetColItem.AssetID = (long)dr["RowID"];
				assetColItem.InstanceID = di.ID.ToString();
				assetColItem.LoadInstance();
				parentCol.Children.Add(assetColItem);
				SaveProject();
			} catch (Exception ex)
			{
				pair = null;
			}
			return pair;
			//raise di changed event			
			//add treeviewitem to treeview
		}
		public DataInstance GetAssetLibrary()
		{
			return GetInstance(AssetLibraryID);
		}
		public void ReloadAssetFolder(DataCollection current, DataFolder fld)
		{
			if(current is DataCollection)
			{
				DataFolder newfld = fld[current.LocalName];
				newfld.Parent = fld;
				//DataFolder newfld = fld.CreateSubfolder(current.LocalName);
				newfld.FolderType = DataFolder.NLFOLDERTYPE.COLLECTION;
				newfld.Tag = current;
				foreach(DataCollectionItemBase col in current.Children)
				{
					if (col is DataCollectionItemAsset)
					{
						SerializableKeyValuePair<long, string> pair = new SerializableKeyValuePair<long, string>();
						DataCollectionItemAsset colitem = (DataCollectionItemAsset)col;
						colitem.LoadInstance();
						pair.Key = colitem.AssetID;
						pair.Value = colitem.InstanceID;
						newfld.Contents.Add(pair);
					}
					else if (col is DataCollection)
					{
						ReloadAssetFolder((DataCollection)col, newfld);
					}
				}
				//todo: add other assets to the treeview
			}
		}
		public void ReloadDefinitionFolder()
		{
			DataFolder fld = RootFolder["[Definitions]"];
			fld.ClearChildren();
			foreach(DatatypeDefinition def in Definitions)
			{
				DataFolder child = fld.CreateSubfolder(def.DataName);
				child.SystemFolder = true;
				child.Tag = def;
				child.FolderType = DataFolder.NLFOLDERTYPE.DEFINITION;
			}
		}
		public void AddDefinition(string filename, DatatypeDefinition dd)
		{
			if (!DefinitionLookup.ContainsKey(dd.ID.ToString()))
			{
				dd.FileName = filename;
				DefinitionFiles.Add(filename);
				DefinitionLookup[dd.ID.ToString()] = dd;
				Definitions.Add(dd);

				//add folder
				DataFolder fld = RootFolder["[Definitions]"];
				DataFolder subfld = fld.CreateSubfolder(dd.DataName);
				subfld.SystemFolder = true;
				subfld.Tag = dd;
				if (!dd.SystemDefinition)
				{
					//do not create libraries for system definitions
					//create library
					CreateDefaultLibrary(dd);
				}
			}
		}
		public void CreateDefaultLibrary(DatatypeDefinition dd)
		{
			//create a default datainstance for each definition			
			DataFolder fld = RootFolder["[Libraries]"].CreateSubfolder("{ " + dd.PrintableName + " }");
			DataInstance di = new DataInstance(dd);
			di.SystemDataInstance = true;
			fld.IsLibrary = true;
			fld.Tag = di;
			fld.LibraryID = di.ID.ToString();
			AddInstance("./data/" + dd.DataName + "-default-" + "library.db", di);
		}
		public DataFolder CreateLibraryFolder(DataFolder parent, DataInstance di)
		{
			DataFolder fld = parent.CreateSubfolder(di.Definition.PrintableName);
			fld.IsLibrary = true;
			fld.Tag = di;
			fld.FolderType = DataFolder.NLFOLDERTYPE.LIBRARY;
			fld.LibraryID = di.ID.ToString();
			return fld;

		}
		public DataFolder CreateLibrary(string LibraryName, DatatypeDefinition dd, DataFolder Parent)
		{
			DataFolder fld = Parent.CreateSubfolder(LibraryName);
			DataInstance di = new DataInstance(dd);
			di.LibraryName = LibraryName;
			fld.IsLibrary = true;
			fld.FolderType = DataFolder.NLFOLDERTYPE.LIBRARY;
			fld.Tag = di;
			fld.LibraryID = di.ID.ToString();
			AddInstance("./data/" + dd.DataName + "-"+di.ID.ToString()+"-" + "library.db", di);
			return fld;
		}
		public void AddInstance(string filename, DataInstance di)
		{
			if (!InstanceLookup.ContainsKey(di.ID.ToString()))
			{
				di.FileName = filename;
				DataInstanceFiles.Add(filename);
				DataInstances.Add(di);
				InstanceLookup[di.ID.ToString()] = di;

				DataRow dr = InstanceIDs.NewRow();
				dr["InstanceID"] = di.ID;
				dr["LastID"] = 60466176; //this is 10000 in base 36 - want to start with a 1 before the userid to the left 
				dr["ModifiedOn"] = DateTime.Now;
				dr["ModifiedBy"] = CurrentUser.Username;
				InstanceIDs.Rows.Add(dr);
			}
		}
		public void SaveProject()
		{
			Directory.SetCurrentDirectory(Path.GetDirectoryName(FileName));
			SaveProject(FileName);
		}
		public void SaveProject(string Filename)
		{
			//write project
			SaveOnlyProjectFile(Filename);

			XmlSerializer SerializerObj;
			TextWriter WriteFileStream;
			//write definitions
			foreach (DatatypeDefinition def in Definitions)
			{
				Directory.SetCurrentDirectory(Path.GetDirectoryName(FileName));

				string path = Path.GetDirectoryName(Path.GetFullPath(def.FileName));
				if (!Directory.Exists(Path.GetDirectoryName(Path.GetFullPath(def.FileName))))
				{
					Directory.CreateDirectory(path);
				}
				SerializerObj = new XmlSerializer(typeof(DatatypeDefinition));
				WriteFileStream = new StreamWriter(def.FileName);
				SerializerObj.Serialize(WriteFileStream, def);
				WriteFileStream.Close();
			}


			//write instances
			foreach (DataInstance di in DataInstances)
			{
				//Directory.SetCurrentDirectory(Path.GetDirectoryName(FileName));
				//if (!Directory.Exists(Path.GetDirectoryName(di.FileName)))
				//{
				//	Directory.CreateDirectory(Path.GetDirectoryName(di.FileName));
				//}
				//di.Data.TableName = di.ID.ToString();
				//SerializerObj = new XmlSerializer(typeof(DataInstance));
				//WriteFileStream = new StreamWriter(di.FileName);
				//SerializerObj.Serialize(WriteFileStream, di);
				//WriteFileStream.Close();
				di.SaveInstance(Filename);
			}
			Directory.SetCurrentDirectory(Path.GetDirectoryName(FileName));
			//write asset collection
			SerializerObj = new XmlSerializer(typeof(DataCollection));
			WriteFileStream = new StreamWriter("./~AssetData.lib");
			SerializerObj.Serialize(WriteFileStream, AssetCollection);
			WriteFileStream.Close();

			if (!Directory.Exists("./icons"))
			{
				Directory.CreateDirectory("./icons");
			}
		}

		private void SaveOnlyProjectFile(string Filename)
		{
			XmlSerializer SerializerObj;
			TextWriter WriteFileStream;
			FileName = Filename;
			SerializerObj = new XmlSerializer(typeof(DataProject));
			WriteFileStream = new StreamWriter(FileName);
			SerializerObj.Serialize(WriteFileStream, this);
			WriteFileStream.Close();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="extension"></param>
		/// <param name="datatype">A fully qualified datatype of type asset.x.x </param>
		public void RegisterAssetTypeClassifier(string extension, string datatype)
		{
			SerializableKeyValuePair<string, string> res = null;
			res = AssetTypeClassifiers.FirstOrDefault(x => (x.Value == datatype && x.Key == extension));
			if (res == null)
			{
				string[] types = datatype.Split('.');
				string at = types[0];

				res = AssetTypeClassifiers.FirstOrDefault(x => (x.Value == at && x.Key == extension));
				SerializableKeyValuePair<string, string> clas = new SerializableKeyValuePair<string, string>();
				if (res == null)
				{
					clas.Key = extension;
					clas.Value = at;
					AssetTypeClassifiers.Add(clas);
				}
				//register all parent types.  So asset.media.video is also an asset.media and an asset
				for (int i = 1; i <= types.Count() - 1; i++)
				{
					at += "." + types[i];
					res = AssetTypeClassifiers.FirstOrDefault(x => (x.Value == at && x.Key == extension));
					if (res == null)
					{
						clas = new SerializableKeyValuePair<string, string>();
						clas.Key = extension;
						clas.Value = at;
						AssetTypeClassifiers.Add(clas);
					}
				}
			}
		}
		public void GenerateIcon(IGenerateIcon NotifyObject)
		{
			WaitCallback callBack;
			NotifyObject.CurrentDirectory = Path.GetDirectoryName(FileName);
			callBack = new WaitCallback(_GenerateIcon);
			ThreadPool.QueueUserWorkItem(callBack, NotifyObject);
		}
		public static void _GenerateIcon(object state)
		{
			IGenerateIcon toIcon = (IGenerateIcon)state;
			Directory.SetCurrentDirectory(toIcon.CurrentDirectory);
			string iconFName = "./icons/icon_" + toIcon.IconAssetID + ".png";
			string iconTempName = Path.GetTempPath() + "/icon_" + toIcon.IconAssetID + ".png";

			//check if file exists
			if (!File.Exists(iconFName))
			{
				//make it quick-like.
				try
				{
					FileStream fs = File.Create(iconFName);
					fs.Flush();
					fs.Close();

					Image img = Image.FromFile(toIcon.IconFilename);
					//small enough, just save it as an icon
					if (img.Width <= 90 && img.Height <= 90)
					{
						img.Save(iconFName);
						img.Dispose();
						Thread.Sleep(50);
						toIcon.IconHasGenerated();
					}
					else
					{

						int width = img.Width > img.Height ? img.Height : img.Width; //get the smaller of the two
						int x = 0;
						int y = 0;
						if (width < img.Width)
						{
							x = (img.Width - width) / 2;
						}
						else if (width < img.Height)
						{
							y = (img.Height - width) / 2;
						}
						Rectangle rect = new Rectangle(x, y, width, width);
						Bitmap orig = new Bitmap(img);
						Bitmap cropped = orig.Clone(rect, orig.PixelFormat);
						Size s = new Size(90, 90);
						Image newIcon = (Image)(new Bitmap(cropped, s));
						newIcon.Save(iconTempName);
						newIcon.Dispose();
						File.Copy(iconTempName, iconFName, true);
						Thread.Sleep(250);
						toIcon.IconHasGenerated();
					}
				} catch (Exception ex)
				{
					toIcon.IconHasGenerated();
				}
			} else
			{
				Thread.Sleep(100);
				toIcon.IconHasGenerated();
			}
		}
	}
}
