﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Threading;

namespace NodeLynx.CustomData
{
	[Serializable]
	public class LCDataTable : DataTable
	{
		public LCDataTable()
			: base()
		{
		}

		public LCDataTable(string tableName)
			: base(tableName)
		{
		}

		public LCDataTable(string tableName, string tableNamespace)
			: base(tableName, tableNamespace)
		{
		}
		
		public override DataTable Clone()
		{
			LCDataTable table = new LCDataTable();
			table.TableName = TableName;
			foreach(DataColumn col in Columns)
			{
				DataColumn col2 = new DataColumn();
				col2.ColumnName = col.ColumnName;
				col2.Caption = col.Caption;
				col2.DataType = col.DataType;
				col2.Unique = col.Unique;
				col2.AutoIncrement = col.AutoIncrement;
				col2.AutoIncrementSeed = col.AutoIncrementSeed;
				col2.AutoIncrementStep = col.AutoIncrementStep;
				col2.AllowDBNull = col.AllowDBNull;
				col2.MaxLength = col.MaxLength;
				table.Columns.Add(col2);
			}
			table.PrimaryKey = new DataColumn[]{table.Columns[PrimaryKey[0].ColumnName]};
			return table;
		}
		/// <summary>
		/// Needs using System.Runtime.Serialization;
		/// </summary>
		public LCDataTable(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

		protected override Type GetRowType()
		{
			return typeof(LCDataRow);
		}

		protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
		{
			return new LCDataRow(builder);
		}
	}

	[Serializable]
	public partial class LCDataRow : DataRow
	{
		public bool MyPropertyThatIdicatesSomething { get; private set; }

		public LCDataRow()
			: base(null)
		{
			Wpfify();
			CoalesceProperyChangeTimer.Interval = new TimeSpan(20000000L); //200 ms?
			CoalesceProperyChangeTimer.Tick += CoalesceProperyChangeTimer_Tick;
		}

		void CoalesceProperyChangeTimer_Tick(object sender, EventArgs e)
		{
			lock (CoalesceProperyChangeTimer)
			{
				lock (ScheduledUpdates)
				{
					CoalesceProperyChangeTimer.IsEnabled = false;
					var ev = PropertyChanged;
					if (ev != null)
					{
						while (ScheduledUpdates.Count > 0)
						{
							UpdatesPending.Remove(ScheduledUpdates.Peek().PropertyName);
							ev(this, ScheduledUpdates.Dequeue());
						}
					}
				}
			}
		}

		public LCDataRow(DataRowBuilder builder)
			: base(builder)
		{
			Wpfify();
			CoalesceProperyChangeTimer.Interval = new TimeSpan(20000000L); //200 ms?
			CoalesceProperyChangeTimer.Tick += CoalesceProperyChangeTimer_Tick;
		}
		DispatcherTimer CoalesceProperyChangeTimer = new DispatcherTimer();
		Queue<PropertyChangedEventArgs> ScheduledUpdates = new Queue<PropertyChangedEventArgs>();
		List<string> UpdatesPending = new List<string>();
	}

	public partial class LCDataRow : INotifyPropertyChanged
	{
		public void Wpfify()
		{
			Table.ColumnChanged += new DataColumnChangeEventHandler(HandleColumnChanged);
		}

		private void HandleColumnChanged(object sender, DataColumnChangeEventArgs e)
		{
			if (e.Row == this)
			{
				OnPropertyChanged(e.Column.ColumnName);
			}
		}
		
		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string propertyName)
		{
			PropertyChangedEventArgs args = new PropertyChangedEventArgs(propertyName);
			lock (CoalesceProperyChangeTimer)
			{
				lock (ScheduledUpdates)
				{
					if (!UpdatesPending.Contains(propertyName))
					{
						ScheduledUpdates.Enqueue(args);
						UpdatesPending.Add(propertyName);
						CoalesceProperyChangeTimer.IsEnabled = true;
					}
				}
			}
			/*
			var ev = PropertyChanged;
			if (ev != null)
			{
				ev(this, new PropertyChangedEventArgs(propertyName));
			}
			 * */
		}
	}

}
