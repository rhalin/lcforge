﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData.UI;

namespace NodeLynx.CustomData.DataTypes
{
	/// <summary>
	/// Empty class, used to designate that no data should be saved/loaded to or from a single control for this object
	/// </summary>
	public class NLNoControlData
	{

	}
	public class ControlDependencies : DependencyObject
	{
		public static readonly DependencyProperty BoundRowProperty =
			DependencyProperty.Register("BoundRowProperty", typeof(LCDataRow), typeof(ControlDependencies));
		public LCDataRow BoundRow
		{
			set { SetValue(BoundRowProperty, value); }
			get { return (LCDataRow)GetValue(BoundRowProperty); }
		}
	}
	/*public interface INLDataType
	{
		bool IsListType();
		bool IsReferencetype();
		string ToStr();
		void FromStr(string fromStr);

		Control GetNewControl();
	}*/
	[DatatypeID(ID = "{B5F96ACD-B2CE-4D43-9636-E855282BFDD3}", FQN = "data.null")]
	public class NLDataType// : INLDataType
	{
		static readonly NLNoControlData _NoControlData;
		protected ControlDependencies BoundData
		{
			get;
			set;
		}


		public Canvas ContainerCanvas
		{
			get;
			set;
		}
		static NLDataType()
		{
			_NoControlData = new NLNoControlData();
		}
		public static NLNoControlData NoControlData()
		{
			return _NoControlData;
		}
		//dynamic Value;
		public NLDataType()
		{
			BoundData = new ControlDependencies();
		}
		public virtual void ModifyGridColumn(ref DataGridColumn col)
		{

		}
		public virtual bool IsListType()
		{
			throw new NotImplementedException();
		}

		public virtual bool IsReferencetype()
		{
			throw new NotImplementedException();
		}

		public virtual object ToObject()
		{
			throw new NotImplementedException();
		}

		public virtual void FromObj(object obj)
		{
			throw new NotImplementedException();
		}


		public virtual FrameworkElement GetNewControl(DatatypeField field)
		{
			throw new NotImplementedException();
		}

		public virtual object GetDataFromControl(FrameworkElement ctrl)
		{			
			throw new NotImplementedException();
		}
		public virtual string GetLabelFromControl(FrameworkElement ctrl)
		{
			return "";
		}
		public virtual void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			throw new NotImplementedException();
		}
		public virtual bool ValidateControl(FrameworkElement ctrl)
		{
			return true;
		}
		public virtual DataColumn GenerateDataColumn()
		{
			throw new NotImplementedException();
		}
		public virtual DataGridBoundColumn GenerateGridColumn()
		{
			throw new NotImplementedException();
		}
		public virtual void SetTopContainer(Canvas cvs)
		{
			ContainerCanvas = cvs;
		}
		/// <summary>
		/// When an instance of a datatype using this field is first created, this function is called
		/// to create/initialize anything the field might need.  The default/empty implementation is
		/// probably appropriate for most uses.
		/// 
		/// todo: need to make sure this is called on every row if a column is to a table in use
		/// </summary>
		/// <param name="DataCell">
		/// The object representing a cell in the datarow this field is a part of. 
		/// 
		/// This is a ref parameter because we may create an entirely new object here and it is best
		/// that the relationship is explicit that this call controls the actual object reference 
		/// without relying on return behavior
		/// </param>
		public virtual void InitializeNewData(ref object DataCell, DatatypeDefinition def, long RowID, List<Guid> ReferenceDefinitionIDs, DataInstance di)
		{

		}
		public string GetID()
		{
			Type t = this.GetType();
			DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
			return did.ID;
		}
		public string GetFQN()
		{
			Type t = this.GetType();
			DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
			return did.FQN;
		}
		public virtual bool IsAutoColumn()
		{
			return false;
		}
		public virtual void OnInstanceChanged(ref LCDataRow Row)
		{

		}
		public virtual DataColumn GenerateDataReferenceColumn()
		{
			throw new NotImplementedException();
		}
		public virtual bool EqualsField(object left, object right)
		{
			return true;
		}
	}
}
