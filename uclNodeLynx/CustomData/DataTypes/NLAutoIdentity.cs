﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData.UI;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{18AAA17E-FC7F-4736-ADD8-F1CD4E24899B}", FQN = "data.int.autoincrement")]
	public class NLAutoIdentity : NLDataType
	{
		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}

		//public override string ToStr()
		//{
		//	return Value.ToString();
		//}

		//public override void FromStr(string fromStr)
		//{
		//	try
		//	{
		//		Value = int.Parse(fromStr);
		//	} catch (Exception ex)
		//	{

		//	}
		//}
		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			Control c = new Label();
			c.Width = 120.00;
			((Label)c).Content = "";
			return null;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			return ((Label)ctrl).Content;
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			if (obj != DBNull.Value)
			{
				((Label)ctrl).Content = ((int)obj).ToString();
			}
			else
			{
				((Label)ctrl).Content = "";
			}
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			int i;
			return int.TryParse(((TextBox)ctrl).Text, out i);
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = System.Type.GetType("System.Int32");
			col.AutoIncrement = true;
			col.AutoIncrementSeed = 1000;
			col.AutoIncrementStep = 1;
			col.DataType = typeof(int);
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override bool IsAutoColumn()
		{
			return true;
		}
	}
}
