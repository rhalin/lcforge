﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using NodeLynx.CustomData.UI;
using Xceed.Wpf.Toolkit;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{91865A28-9B9F-49AD-9BA6-52337AA1CD10}", FQN = "data.color")]
	public class NLColor : NLDataType
	{
		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}

		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			ColorPicker c = new ColorPicker();
			c.ColorMode = ColorMode.ColorCanvas;
			c.SelectedColor = Colors.Blue;
			//c.Width = 120.00;
			//((TextBox)c).Text = "";
			return c;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			if (ValidateControl(ctrl))
			{
				string color = string.Format("#{0:X2}{1:X2}{2:X2}{3:X2}", 
					((ColorPicker)ctrl).SelectedColor.A, 
					((ColorPicker)ctrl).SelectedColor.R, 
					((ColorPicker)ctrl).SelectedColor.G, 
					((ColorPicker)ctrl).SelectedColor.B);
				return color;
			}
			else
			{
				return DBNull.Value;
			}
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			if (obj != DBNull.Value)
			{
				((ColorPicker)ctrl).SelectedColor = (Color)ColorConverter.ConvertFromString((string)obj); 
			}
			else
			{
				((ColorPicker)ctrl).SelectedColor = Colors.Blue;
			}
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			return true;
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(string);
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override void InitializeNewData(ref object DataCell, DatatypeDefinition def, long RowID, List<Guid> ReferenceDefinitionIDs, DataInstance di)
		{
			DataCell =  string.Format("#{0:X2}{1:X2}{2:X2}{3:X2}", Colors.Red.A, Colors.Red.R, Colors.Red.G, Colors.Red.B);
		}
		public override bool EqualsField(object left, object right)
		{
			return ((string)left) == ((string)right);
		}
	}
}
