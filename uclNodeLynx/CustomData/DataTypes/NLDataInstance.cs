﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData.UI;
using NodeLynx.lib;
using NodeLynx.UI;
using NodeLynx.UI.Dialogs;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{6E8D2C55-8B66-4BE3-B12E-8D20777F1590}", FQN="data.complex.instance")]
	public class NLDataInstance : NLDataType
	{

		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}

		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			Grid g = new Grid();
			StackPanel sp = new StackPanel();
			sp.Orientation = Orientation.Horizontal;
			g.Children.Add(sp);
			Label c = new Label();
			c.Width = 420.00;
			sp.Children.Add(c);
			((Label)c).Content = "{Complex Data}";
			return g;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			return NLDataType.NoControlData();
		}
		public class InstanceInfo
		{
			public LCDataRow row;
			public DatatypeField field;
			public DataInstance ParentInstance;
			public DataInstance CurrentInstance;
			public Grid g;
			public object obj;
			public WindowData winData;
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			Grid g = (Grid)ctrl;
			RegenerateControl(winData, obj, field, row, g);
		}

		private void RegenerateControl(WindowData winData, object obj, DatatypeField field, LCDataRow row, Grid g)
		{
			string name = DataProject.CurrentProject.GetInstance((string)obj).Definition.DataName; 
			StackPanel sp = ((StackPanel)g.Children[0]);
			Label c = (Label)sp.Children[0];
			sp.Children.Clear();
			sp.Children.Add(c);
			InstanceInfo instInfo = new InstanceInfo();
			instInfo.field = field;
			instInfo.row = row;
			instInfo.g = g;
			instInfo.obj = obj;
			WindowData windataCopy = new WindowData(winData.Window, winData.ContentCanvas, winData.Instance);
			instInfo.winData = windataCopy;

			WindowData ButtonTag;
			instInfo.ParentInstance = winData.Instance;

			winData.Instance = DataProject.CurrentProject.GetInstance((string)obj); //just in case this isn't populated;
			instInfo.CurrentInstance = winData.Instance;
			string LibName = winData.Instance.LibraryName == "" ? (string)obj : winData.Instance.LibraryName;
			((Label)c).Content = "DataInstance {" + name + " " + LibName + "}";
			Button b = null;
			if (sp.Children.Count == 1)
			{
				b = new Button();
				b.Content = "Edit Data";
				b.Click += btnOpenInstance_Click;
				b.Width = 100;
				b.HorizontalAlignment = HorizontalAlignment.Left;
				sp.Children.Add(b);

				b = new Button();
				b.Content = "Select instance";
				b.Click += btnChangeDataInstance_Click;
				b.Width = 160;
				b.HorizontalAlignment = HorizontalAlignment.Right;
				sp.Children.Add(b);
				//c.Visibility = Visibility.Collapsed;

			}
			b = (Button)sp.Children[1];
			ButtonTag = winData;
			//b.Tag = DataProject.CurrentProject.GetInstance((string)obj);
			b.Tag = ButtonTag;
			b = (Button)sp.Children[2];
			b.Tag = instInfo;
		}

		private void btnChangeDataInstance_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucDlgExistingInstance dlgExistingInstance = new ucDlgExistingInstance();
			chooseModule.SetDialogControl(dlgExistingInstance);
			Button btn = (Button)sender;
			InstanceInfo buttonTag = (InstanceInfo)btn.Tag;
			DataInstance di = buttonTag.ParentInstance;
			dlgExistingInstance.InstanceTypes.Add(buttonTag.CurrentInstance.Definition);
			dlgExistingInstance.SelectedInstance = buttonTag.CurrentInstance;
			dlgExistingInstance.Initialize();
			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (dlgExistingInstance.Validate())
				{
					DataInstance swapping = dlgExistingInstance.GetValue();
					if (swapping != null)
					{
						int i = 0;
						int controlID = 0;
						foreach (SerializableKeyValuePair<Guid, DatatypeField> field in di.Definition.Fields)
						{
							if (field.Value.ID == buttonTag.field.ID)
							{
								buttonTag.row[i] = swapping.ID.ToString();
								buttonTag.obj = swapping.ID.ToString();								
								RegenerateControl(buttonTag.winData, buttonTag.obj, buttonTag.field, buttonTag.row, buttonTag.g);
								break;
							}
							else
							{
								if (field.Value.DataTypeInfo.IsReferencetype())
								{
									i++;
								}
								//field.Value.DataTypeInfo.PopulateControlWithData(winData, currentRow[i], ref ctrl, field.Value, currentRow);
								i++;
							}
						}
					}
				}
			}
		}
		

		void btnOpenInstance_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if(sender is Button)
			{
				Button btn = (Button)sender;
				WindowData buttonTag = (WindowData)btn.Tag;
				DataInstance di = buttonTag.Instance;
				INLTabClient cl = new ucDataEditor();
				EditorSynchro sync = new EditorSynchro(di, buttonTag.ContentCanvas, buttonTag.Window);
				sync.ContentCanvas = buttonTag.ContentCanvas;
				cl.SetEditorSynchro(sync);
				cl.SetDataSource(di.Definition, di);				
				DataProject.CurrentProject.CurrentManager.OpenDocument(cl, sync);
			}
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			int i;			
			return true;
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(string); //instance ID
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override void InitializeNewData(ref object DataCell, DatatypeDefinition def, long RowID, List<Guid> ReferenceDefinitionIDs, DataInstance di)
		{
			//create new data instance
			if (ReferenceDefinitionIDs.Count == 0)
			{
				DataCell = (object)"";
			} else
			{
				//todo: we need to include field here, so we can get the ID and use it in the filename
				DataInstance di2 = new DataInstance(DataProject.CurrentProject.GetDefinition(ReferenceDefinitionIDs[0].ToString()));
				DataProject.CurrentProject.AddInstance("./data/" + def.DataName + "_" + di.ID.ToString().TrimStart('{').TrimEnd('}') + "_" + di2.ID.ToString().TrimStart('{').TrimEnd('}') + "_" + RowID.ToString() + ".db",di2);
				DataCell = (object)di2.ID.ToString();
			}
		}
	
	}
}
