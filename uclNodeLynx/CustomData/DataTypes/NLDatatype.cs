﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData.UI;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{2D313954-1344-4532-A27E-89DC42CA9D43}", FQN = "datatype")]
	public class NLDatatype : NLDataType
	{
		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}

		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			Label c = new Label();
			c.Width = 120.00;
			((Label)c).Content = "{Datatype}";
			return c;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			return NLDataType.NoControlData();
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			((Label)ctrl).Content = "Datatype {" + (string)obj + "}";
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			return true;
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(string); //instance ID
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override void InitializeNewData(ref object DataCell, DatatypeDefinition def, long RowID, List<Guid> ReferenceDefinitionIDs, DataInstance di)
		{
			DataCell = (object)"";
		}
	}
}
