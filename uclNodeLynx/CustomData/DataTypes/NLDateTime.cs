﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData.UI;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{8F3F0281-C561-449D-8C29-B93C3E6C0BE9}", FQN = "data.datetime")]
	public class NLDateTime : NLDataType
	{
		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}


		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			Control c = new TextBox();
			c.Width = 120.00;
			((TextBox)c).Text = "";
			return null;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			return ((TextBox)ctrl).Text;
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			if (obj != DBNull.Value)
			{
				((TextBox)ctrl).Text = ((int)obj).ToString();
			}
			else
			{
				((TextBox)ctrl).Text = "";
			}
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			return true;
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(DateTime);
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			throw new NotImplementedException();
		}
		public override void InitializeNewData(ref object DataCell, DatatypeDefinition def, long RowID, List<Guid> ReferenceDefinitionIDs, DataInstance di)
		{
			DataCell = DateTime.Now;
		}
		public override bool EqualsField(object left, object right)
		{
			return ((DateTime)left) == ((DateTime)right);
		}
	}
}
