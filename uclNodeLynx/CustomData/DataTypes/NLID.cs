﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData.UI;
using NodeLynx.lib;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{FCBC5C90-CE7C-4630-BF3D-E195DC3392A5}", FQN = "data.id")]
	public class NLID : NLDataType
	{
		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}

		//public override string ToStr()
		//{
		//	return Value.ToString();
		//}

		//public override void FromStr(string fromStr)
		//{
		//	try
		//	{
		//		Value = int.Parse(fromStr);
		//	} catch (Exception ex)
		//	{

		//	}
		//}
		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			Control c = new Label();
			c.Width = 120.00;
			((Label)c).Content = "";
			return c;
		}


		public override void FromObj(object obj)
		{
			//throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			return NLDatatype.NoControlData();
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			if (obj != DBNull.Value)
			{
				((Label)ctrl).Content = IDConverter.DecimalToID(((long)obj));
			}
			else
			{
				((Label)ctrl).Content = "";
			}
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			return true;
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = System.Type.GetType("System.Int64");
			col.AutoIncrement = false;
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override bool IsAutoColumn()
		{
			return true;
		}
	}
}
