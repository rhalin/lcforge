﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using NodeLynx.CustomData.UI;

namespace NodeLynx.CustomData.DataTypes
{

	[DatatypeID(ID = "{E047B384-DFCB-41E4-8021-5D30AEF313A2}", FQN="data.int")]
	public class NLInt : NLDataType
	{
		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}

		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			Control c = new TextBox();
			c.Width = 120.00;
			((TextBox)c).Text = "";
			return c;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			if(ValidateControl(ctrl))
			{
				return ((TextBox)ctrl).Text;
			} else
			{
				return DBNull.Value;
			}
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			if (obj != DBNull.Value)
			{
				((TextBox)ctrl).Text = ((int)obj).ToString();
				//BoundRow[DialogDescription]

			} else
			{
				((TextBox)ctrl).Text="";
			}
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			int i;			
			return int.TryParse(((TextBox)ctrl).Text,out i);
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(int);
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override void InitializeNewData(ref object DataCell, DatatypeDefinition def, long RowID, List<Guid> ReferenceDefinitionIDs, DataInstance di)
		{
			DataCell = (object)0;
		}
	}
}
