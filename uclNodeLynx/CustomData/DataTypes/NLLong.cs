﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using NodeLynx.CustomData.UI;

namespace NodeLynx.CustomData.DataTypes
{

	[DatatypeID(ID = "{5071D5A3-10B3-4C05-A7CB-77A77BB7A4B5}", FQN = "data.long")]
	public class NLLong : NLDataType
	{
		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}

		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			Control c = new TextBox();
			c.Width = 120.00;
			((TextBox)c).Text = "";
			return c;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			if(ValidateControl(ctrl))
			{
				return ((TextBox)ctrl).Text;
			} else
			{
				return DBNull.Value;
			}
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			if (obj != DBNull.Value)
			{
				((TextBox)ctrl).Text = ((long)obj).ToString();
			} else
			{
				((TextBox)ctrl).Text="";
			}
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			long i;
			return long.TryParse(((TextBox)ctrl).Text, out i);
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(long);
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override void InitializeNewData(ref object DataCell, DatatypeDefinition def, long RowID, List<Guid> ReferenceDefinitionIDs, DataInstance di)
		{
			DataCell = (object)0;
		}
	}
}
