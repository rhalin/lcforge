﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.DragAndDrop;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.lib;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{CD487189-8753-45A1-BAE7-F90E774E68DD}", FQN = "data.reference.multiple")]
	public class NLMultiReference : NLDataType
	{
		private ucMultiReferenceDropTarget myContainer;
		public override bool IsReferencetype()
		{
			return true;
		}

		//public override string ToStr()
		//{
		//	return Value.ToString();
		//}

		//public override void FromStr(string fromStr)
		//{
		//	try
		//	{
		//		Value = int.Parse(fromStr);
		//	} catch (Exception ex)
		//	{

		//	}
		//}
		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			ucMultiReferenceDropTarget c = new ucMultiReferenceDropTarget();
			c.HorizontalAlignment = HorizontalAlignment.Stretch;
			c.Width = 340.0;
			//Control c = new TextBox();
			//c.Width = 120.00;
			//((TextBox)c).Text = "";
			return c;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}
		public override string GetLabelFromControl(FrameworkElement ctrl)
		{
			ucMultiReferenceDropTarget c = (ucMultiReferenceDropTarget)ctrl;
			return c.Name;
		}
		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			ucMultiReferenceDropTarget c = (ucMultiReferenceDropTarget)ctrl;
			List<SerializableKeyValuePair<string, long>> refs = new List<SerializableKeyValuePair<string, long>>();
			//SerializableKeyValuePair<string, int> ref1 = new SerializableKeyValuePair<string, int>();

			refs = c.GetRefences();
			//refs.Add(ref1);
			return refs;
		}
		List<SerializableKeyValuePair<string, long>> DataItem = new List<SerializableKeyValuePair<string, long>>();
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			//((TextBox)ctrl).Text = ((int)obj).ToString();
			if (obj != DBNull.Value)
			{
				//lookup instance
				DataItem = (List<SerializableKeyValuePair<string, long>>)obj;
				if (DataItem.Count > 0
					&& DataItem[0] != null
					&& DataItem[0].Key != null
					&& DataItem[0].Key != "")
				{
					ucMultiReferenceDropTarget target = (ucMultiReferenceDropTarget)ctrl;
					ClearStack(ref target);
					for (int i = 0; i < DataItem.Count; i++)
					{
						DataInstance inst = DataProject.CurrentProject.GetInstance(DataItem[i].Key);
						var result =
							inst.Data.AsEnumerable().Where(dr => dr.Field<long>("RowID") == DataItem[i].Value);

						if (result != null)
						{
							SetData(inst, (LCDataRow)result.First(), ref target);
						}
					}
					//call SetData
				}
				else
				{
					((ucMultiReferenceDropTarget)ctrl).ReferenceStack.Children.Clear();
				}
				//return result

			} else
			{
				((ucMultiReferenceDropTarget)ctrl).ReferenceStack.Children.Clear();
			}
		}
		public void ClearStack(ref ucMultiReferenceDropTarget ctrl)
		{
			ctrl.ReferenceStack.Children.Clear();
		}
		//todo: relocate this function somewhere more useful, like a static somewhere?
		public void SetData(DataInstance inst, LCDataRow row, ref ucMultiReferenceDropTarget ctrl)
		{

			
			IDataWidget defWidg = inst.Definition.DisplayWidget;
			IDataWidget newWidg = defWidg.Create();
			newWidg.AllowResize = true;
			newWidg.AllowDelete = true;
			newWidg.Iconify(true);
			newWidg.ReferenceDeleted += newWidg_ReferenceDeleted;

			SerializableKeyValuePair<string, long> di = new SerializableKeyValuePair<string, long>();
			di.Key = inst.ID.ToString();
			di.Value = long.Parse(row[0].ToString());
			newWidg.SetDataItem(di);

			DragDecorator decro = new DragDecorator();
			decro.SetChild(newWidg);
			Dictionary<string, object> Bindings = new Dictionary<string, object>();
			foreach (SerializableKeyValuePair<string, string> binding in inst.Definition.WidgetBindings)
			{
				foreach (SerializableKeyValuePair<Guid, DatatypeField> field in inst.Definition.Fields)
				{
					if (field.Value.FieldName == binding.Value)
					{
						Bindings[binding.Key] = row[field.Value.FieldName].ToString();
					}
				}
				//Bindings[binding.Key]
			}
			newWidg.SetBindings(Bindings);
			ctrl.ReferenceStack.Children.Add((FrameworkElement)decro);
			myContainer = ctrl;
		}

		private void newWidg_ReferenceDeleted(object dropped, EventArgs e)
		{
			for (int i = 0; i < myContainer.ReferenceStack.Children.Count; i++)
			{
				if (((DragDecorator)myContainer.ReferenceStack.Children[i]).Child == dropped)
				{
					myContainer.ReferenceStack.Children.RemoveAt(i);
					break;
				}
			}
			//if (myContainer.ReferenceStack.Children.Contains((FrameworkElement)dropped))
			//	myContainer.ReferenceStack.Children.Remove((FrameworkElement)dropped);
			for (int i = 0; i < DataItem.Count; i++)
			{
				IDataWidget wid = (IDataWidget)dropped;
				SerializableKeyValuePair<string, long> r = wid.GetDataItem();
				if (r != null && r.Key == DataItem[i].Key && r.Value == DataItem[i].Value)
				{
					DataItem.RemoveAt(i);
					break;	
				}
			}
				//DataItem.Clear();
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			//int i;
			//return int.TryParse(((TextBox)ctrl).Text, out i);
			return true;
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(string);
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override DataColumn GenerateDataReferenceColumn()
		{
			DataColumn col = new DataColumn();
			//instanceID, Data item rowID
			col.DataType = typeof(List<SerializableKeyValuePair<string, long>>);
			return col;
		}
	}
}
