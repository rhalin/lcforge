﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{2A273450-0843-4DF2-82E1-C9A088D6560E}", FQN = "data.complex.instance.node")]
	public class NLNodeDataInstance : NLDataInstance
	{
		public override void InitializeNewData(ref object DataCell, DatatypeDefinition def, long RowID, List<Guid> ReferenceDefinitionIDs, DataInstance di)
		{
			//create new data instance
			if (ReferenceDefinitionIDs.Count == 0)
			{
				DataCell = (object)"";
			}
			else
			{
				//todo: we need to include field here, so we can get the ID and use it in the filename
				DataInstance di2 = new DataInstance(DataProject.CurrentProject.GetDefinition(ReferenceDefinitionIDs[0].ToString()));
				DataProject.CurrentProject.AddInstance("./data/" + def.DataName + "_" + di.ID.ToString().TrimStart('{').TrimEnd('}') + "_" + di2.ID.ToString().TrimStart('{').TrimEnd('}') + "_" + RowID.ToString() + ".db", di2);
				DataCell = (object)di2.ID.ToString();
			}
		}
	}
}
