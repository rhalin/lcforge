﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData.UI;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{56B2B7E6-0EC0-4741-BA1B-357C5CB9C88A}", FQN = "ui.keybinding.qualcode")]
	public class NLQCKeyBinding : NLDataType
	{
		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}

		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			ComboBox c = new ComboBox();
			c.Items.Add("");
			c.Items.Add("D1");
			c.Items.Add("D2");

			return c;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			if (ValidateControl(ctrl))
			{
				return ((ComboBox)ctrl).SelectedValue;
			}
			else
			{
				return DBNull.Value;
			}
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			if (obj != DBNull.Value)
			{
				((ComboBox)ctrl).SelectedValue = (string)obj;
			}
			else
			{
				((ComboBox)ctrl).SelectedValue = "";
			}
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			return true;
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(string);
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override void InitializeNewData(ref object DataCell, DatatypeDefinition def, long RowID, List<Guid> ReferenceDefinitionIDs, DataInstance di)
		{
			DataCell = (object)"";
		}
	}
}
