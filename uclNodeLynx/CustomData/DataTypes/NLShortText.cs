﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using NodeLynx.CustomData.UI;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{F14534A2-05C2-48FC-84E2-B60B7D8FD667}", FQN = "data.text.short")]
	public class NLShortText : NLDataType
	{
		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}

		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			Control c = new TextBox();
			c.Width = 120.00;
			((TextBox)c).Text = "";
			return c;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			return ((TextBox)ctrl).Text;
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			if (obj != DBNull.Value)
			{
				((TextBox)ctrl).Text = (string)obj;
				////BoundRow[DialogDescription]
			}
			else
			{
				((TextBox)ctrl).Text = "";
			}
			//BoundData.BoundRow = row;
			Binding myBinding = new Binding("["+field.FieldName+"]");
			//myBinding.FallbackValue = "";
			myBinding.Source = row;
			myBinding.Mode = BindingMode.OneWay;
			//myBinding.NotifyOnSourceUpdated = true;
			//myBinding.NotifyOnTargetUpdated = true;
			myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
			((TextBox)ctrl).SetBinding(TextBlock.TextProperty, myBinding);
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			return true;
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(string);
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override bool EqualsField(object left, object right)
		{
			return ((string)left) == ((string)right);
		}
	}
}
