﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.DragAndDrop;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.lib;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{F18CA167-A2E2-4AE7-94F2-4E3ACF3C56D2}", FQN = "data.reference.single")]
	public class NLSingleReference : NLDataType
	{
		private ucSingleReferenceDropTarget myContainer;
		public override bool IsReferencetype()
		{
			return true;
		}

		//public override string ToStr()
		//{
		//	return Value.ToString();
		//}

		//public override void FromStr(string fromStr)
		//{
		//	try
		//	{
		//		Value = int.Parse(fromStr);
		//	} catch (Exception ex)
		//	{

		//	}
		//}
		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			ucSingleReferenceDropTarget c = new ucSingleReferenceDropTarget();
			c.SetAllowedDatatypes(field.AllowedReferenceDatatypes);
			//c.Width = 298.0;			
			//Control c = new TextBox();
			//c.Width = 120.00;
			//((TextBox)c).Text = "";
			return c;
		}


		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}
		public override string GetLabelFromControl(FrameworkElement ctrl)
		{
			ucSingleReferenceDropTarget c = (ucSingleReferenceDropTarget)ctrl;
			return c.Name;
		}
		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			ucSingleReferenceDropTarget c = (ucSingleReferenceDropTarget)ctrl;
			List<SerializableKeyValuePair<string, long>> refs = new List<SerializableKeyValuePair<string, long>>();
			//SerializableKeyValuePair<string, int> ref1 = new SerializableKeyValuePair<string, int>();

			refs = c.GetRefences();
			//refs.Add(ref1);
			return refs;
		}
		List<SerializableKeyValuePair<string, long>> DataItem = new List<SerializableKeyValuePair<string, long>>();
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			//((TextBox)ctrl).Text = ((int)obj).ToString();
			if (obj != DBNull.Value)
			{
				//lookup instance
				DataItem = (List<SerializableKeyValuePair<string, long>>)obj;
				if (DataItem.Count > 0
					&& DataItem[0] != null
					&& DataItem[0].Key != null
					&& DataItem[0].Key != "")
				{
					DataInstance inst = DataProject.CurrentProject.GetInstance(DataItem[0].Key);
					var result =
						inst.Data.AsEnumerable().Where(dr => dr.Field<long>("RowID") == DataItem[0].Value);

					//convert RowID to Rownumber
					if (result != null)
					{
						ucSingleReferenceDropTarget target = (ucSingleReferenceDropTarget)ctrl;
						
						SetData(inst, (LCDataRow) result.First(), ref target);
					}
					//call SetData
				} else
				{
					((ucSingleReferenceDropTarget)ctrl).dropGrid.Children.Clear();
				}
				//return result

			} else
			{
				((ucSingleReferenceDropTarget)ctrl).dropGrid.Children.Clear();
			}
		}
		//todo: relocate this function somewhere more useful, like a static somewhere?
		public void SetData(DataInstance inst, LCDataRow row, ref ucSingleReferenceDropTarget ctrl)
		{
			ctrl.dropGrid.Children.Clear();
			IDataWidget defWidg = inst.Definition.DisplayWidget;
			IDataWidget newWidg = defWidg.Create();
			newWidg.AllowResize = false;
			newWidg.AllowDelete = true;
			newWidg.ReferenceDeleted +=newWidg_ReferenceDeleted;

			SerializableKeyValuePair<string, long> di = new SerializableKeyValuePair<string, long>();
			di.Key = inst.ID.ToString();
			di.Value = long.Parse(row[0].ToString());
			newWidg.SetDataItem(di);

			DragDecorator decro = new DragDecorator();				
			decro.SetChild(newWidg);
			Dictionary<string, object> Bindings = new Dictionary<string, object>();
			foreach (SerializableKeyValuePair<string, string> binding in inst.Definition.WidgetBindings)
			{
				foreach (SerializableKeyValuePair<Guid, DatatypeField> field in inst.Definition.Fields)
				{
					if (field.Value.FieldName == binding.Value)
					{
						Bindings[binding.Key] = row[field.Value.FieldName].ToString();
					}
				}
				//Bindings[binding.Key]
			}
			newWidg.SetBindings(Bindings);
			ctrl.dropGrid.Children.Add((FrameworkElement)decro);
			myContainer = ctrl;
		}

		private void newWidg_ReferenceDeleted(object dropped, EventArgs e)
		{
			myContainer.dropGrid.Children.Clear();
			DataItem.Clear();
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			//int i;
			//return int.TryParse(((TextBox)ctrl).Text, out i);
			return true;
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(string);
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override DataColumn GenerateDataReferenceColumn()
		{
			DataColumn col = new DataColumn();
			//instanceID, Data item rowID
			col.DataType = typeof(List<SerializableKeyValuePair<string, long>>);
			return col;
		}
		public override bool EqualsField(object left, object right)
		{

			if (left == DBNull.Value && right == DBNull.Value)
			{
				return true;
			}
			else
			{
				if (left == DBNull.Value || right == DBNull.Value)
				{
					return false;
				}
				else
				{
					SerializableKeyValuePair<string, long> l;
					SerializableKeyValuePair<string, long> r;
					l = ((List<SerializableKeyValuePair<string, long>>)left)[0];
					r = ((List<SerializableKeyValuePair<string, long>>)right)[0];
					if (l.Key != r.Key || l.Value != r.Value)
					{
						return false;
					} else
					{
						return true;
					}
				}
			}
		}
	}
}
