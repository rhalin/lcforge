﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData.UI;

namespace NodeLynx.CustomData.DataTypes
{
	[DatatypeID(ID = "{6A8A2E55-7938-4CE9-B85D-A3B7CDD252BA}", FQN = "data.text.long")]
	public class NLTextbox : NLDataType
	{
		public override bool IsListType()
		{
			return false;
		}

		public override bool IsReferencetype()
		{
			return false;
		}

		public override FrameworkElement GetNewControl(DatatypeField field)
		{
			Control c = new TextBox();
			c.Width = 500.00;
			c.Height = 200.00;

			((TextBox)c).TextWrapping = TextWrapping.Wrap;
			((TextBox)c).MinLines = 6;
			((TextBox)c).AcceptsReturn = true;
			((TextBox)c).Text = "";
			return c;
		}

		public override void ModifyGridColumn(ref DataGridColumn col)
		{
			col.Visibility = System.Windows.Visibility.Collapsed;
		}
		public override void FromObj(object obj)
		{
			throw new NotImplementedException();
		}

		public override object GetDataFromControl(FrameworkElement ctrl)
		{
			return ((TextBox)ctrl).Text;
		}
		public override void PopulateControlWithData(WindowData winData, object obj, ref FrameworkElement ctrl, DatatypeField field, LCDataRow row)
		{
			if (obj != DBNull.Value)
			{
				((TextBox)ctrl).Text = (obj).ToString();
			}
			else
			{
				((TextBox)ctrl).Text = "";
			}
		}
		public override bool ValidateControl(FrameworkElement ctrl)
		{
			return true;
		}
		public override DataColumn GenerateDataColumn()
		{
			DataColumn col = new DataColumn();
			col.DataType = typeof(string);
			return col;
		}
		public override DataGridBoundColumn GenerateGridColumn()
		{
			DataGridBoundColumn dgc = new DataGridTextColumn();
			return dgc;
		}
		public override bool EqualsField(object left, object right)
		{
			return ((string)left) == ((string)right);
		}
	}
}
