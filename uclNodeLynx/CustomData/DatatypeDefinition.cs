﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using NodeLynx.CustomData.DataTypes;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.lib;
using NodeLynx.Node.NodeType;

namespace NodeLynx.CustomData
{
	public class FieldAddedEventArgs : PropertyChangedEventArgs
	{
		private DatatypeField _newField = null;
		public virtual DatatypeField NewField { get { return _newField; } }
		public FieldAddedEventArgs(string propertyName, DatatypeField newField)
			: base(propertyName)
		{
			_newField = newField;
		}
	}
	public class FieldRemovedEventArgs : PropertyChangedEventArgs
	{
		private DatatypeField _newField = null;
		public virtual DatatypeField NewField { get { return _newField; } }
		public FieldRemovedEventArgs(string propertyName, DatatypeField newField)
			: base(propertyName)
		{
			_newField = newField;
		}
	}
	public class DatatypeDefinition : INotifyPropertyChanged
	{
		public static void InheritedDefs(DatatypeDefinition def, ref List<DatatypeDefinition> Parents)
		{
			if (def.InheritsDefinition != "")
			{
				DatatypeDefinition parentdef = DataProject.CurrentProject.GetDefinition(def.InheritsDefinition);
				if (parentdef != null && !Parents.Contains(parentdef))
				{
					Parents.Add(parentdef);
					InheritedDefs(parentdef, ref Parents);
				}
			}
		}
		public Guid _ID = Guid.Empty;
		[XmlIgnore]
		public Guid ID { get { return _ID; } set { _ID = value; } }
		[XmlIgnore]
		private string _DataName = "";
		public string DataName { get { return _DataName; } 
			set { 
				_DataName = value; 
				RaisePropertyChanged("__DataName"); 
			} 
		}
		/// <summary>
		/// Definitions can be locked or unlocked.  Locked definitions cannot be edited without unlocking 
		/// them - this is a simple safeguard to prevent accidental definition editing
		/// </summary>
		public bool Locked { get; set; }
		private string _PrintableName = "";
		public string PrintableName
		{
			get { return _PrintableName; }
			set
			{
				_PrintableName = value;
				RaisePropertyChanged("__PrintableName");
			}
		}
		[XmlIgnore]
		public string IDStr
		{
			get { return _ID.ToString(); }
			set { ID = new Guid(value); }
		}
		public string FileName = "";
		public bool SystemDefinition = false;
		public string NameField = "";
		public string InheritsDefinition = Guid.Empty.ToString();
		//we only need to save this list
		public List<SerializableKeyValuePair<Guid, DatatypeField>> Fields = new List<SerializableKeyValuePair<Guid, DatatypeField>>();
		[XmlIgnore]
		private Dictionary<Guid, DatatypeField> FieldLookup = new Dictionary<Guid, DatatypeField>();
		[XmlIgnore]
		private LCDataTable DefinitionTable = new LCDataTable();

		//widgetfield, definitionfield
		public List<SerializableKeyValuePair<string, string>> WidgetBindings = new List<SerializableKeyValuePair<string, string>>();

		List<string> ParentIDs = new List<string>();
		public List<string> GetInheritenceIDs()
		{
			List<string> ids = new List<string>();
			ids.CopyTo(ParentIDs.ToArray<string>());
			ids.Add(this.ID.ToString());
			return ids;
		}
		[XmlIgnore]
		public IDataWidget DisplayWidget;
		private string _DisplayWidgetFQN = "widget.default";
		public string DisplayWidgetFQN
		{
			get
			{
				return _DisplayWidgetFQN;
			}
			set
			{
				_DisplayWidgetFQN = value;
				try
				{
					DisplayWidget = RegTypes.GetWidget(_DisplayWidgetFQN);
				} catch(Exception ex)
				{
					//yeah, I know.
				}
			}
		} //= "widget.default"; //default

		//private string _NodeFQN = null; //I guess we can have all data default to the default node?
		//public string NodeFQN
		//{
		//	get
		//	{
		//		return _NodeFQN;
		//	}
		//	set
		//	{
		//		_NodeFQN = value;
		//		//try
		//		//{
		//		//	if(_NodeFQN != null && _NodeFQN != "")
		//		//		Node = RegTypes.GetNode(_NodeFQN);
		//		//}
		//		//catch (Exception ex)
		//		//{
		//		//	//yeah, I know.
		//		//}
		//	}
		//} //= "widget.default"; //default


		//[XmlIgnore]
		///private Dictionary<string, NLDataType> KnownTypes = new Dictionary<string, NLDataType>();
		//[XmlIgnore]
		//private Dictionary<string, NLDataType> KnownTypesFQN = new Dictionary<string, NLDataType>();
		[XmlIgnore]
		public RegisteredTypes RegTypes = new RegisteredTypes();
		public DatatypeDefinition()
		{
			if (_ID == Guid.Empty)
			{
				_ID = Guid.NewGuid();
			}
			Locked = false;
		}
		public DatatypeDefinition(string guid)
		{
			_ID = Guid.Parse(guid);
			Locked = false;
		}
		public void GenerateParentIDs()
		{
			ParentIDs.Clear();
			if(InheritsDefinition != Guid.Empty.ToString())
			{
				DatatypeDefinition def = DataProject.CurrentProject.GetDefinition(InheritsDefinition);
				if(def != null)
				{
					GetNextID(def);
				}
			}
		}
		public bool Inherits(string ID)
		{
			return ParentIDs.Contains(ID);
		}
		private void GetNextID(DatatypeDefinition def)
		{
			if(!ParentIDs.Contains(def.IDStr))
			{
				ParentIDs.Add(def.IDStr);
				if(def.InheritsDefinition != Guid.Empty.ToString())
				{
					DatatypeDefinition def2 = DataProject.CurrentProject.GetDefinition(def.InheritsDefinition);
					if (def2 != null)
					{
						GetNextID(def2);
					}
				}
			}
		}
		public void Initialize(RegisteredTypes reg)
		{
			RegTypes = reg;
			DisplayWidget = RegTypes.GetWidget(DisplayWidgetFQN);

			if (Fields.Count == 0)
			{
				//add defaults?
				DatatypeField f = new DatatypeField();
				f.SetDatatype(RegTypes.GetDatatype("data.id"));
				f.FieldName = "RowID";
				f.FieldLabel = "Row ID";
				f.IsVisible = true;
				AddField(Guid.NewGuid(), f);

				f = new DatatypeField();
				f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
				f.FieldName = "CreateUser";
				f.FieldLabel = "Create User";
				f.IsVisible = false;
				AddField(Guid.NewGuid(), f);

				f = new DatatypeField();
				f.SetDatatype(RegTypes.GetDatatype("data.datetime"));
				f.FieldName = "CreateDate";
				f.FieldLabel = "Create Date";
				f.IsVisible = false;
				AddField(Guid.NewGuid(), f);

				f = new DatatypeField();
				f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
				f.FieldName = "ModifyUser";
				f.FieldLabel = "Modify User";
				f.IsVisible = false;
				AddField(Guid.NewGuid(), f);

				f = new DatatypeField();
				f.SetDatatype(RegTypes.GetDatatype("data.datetime"));
				f.FieldName = "ModifyDate";
				f.FieldLabel = "Modify Date";
				f.IsVisible = false;
				AddField(Guid.NewGuid(), f);

				//for later
				f = new DatatypeField();
				f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
				f.FieldName = "CheckedOutTo";
				f.FieldLabel = "CheckedOutTo";
				f.IsVisible = false;
				AddField(Guid.NewGuid(), f);

				f = new DatatypeField();
				f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
				f.FieldName = "TechnicalName";
				f.FieldLabel = "Technical Name";
				f.IsVisible = true;
				AddField(Guid.NewGuid(), f);

			}
		}
		public void SetFieldsInUse()
		{
			foreach (SerializableKeyValuePair<Guid, DatatypeField> field in Fields)
			{
				field.Value.InUse = true;
			}
		}
		public void Reload(RegisteredTypes reg)
		{
			RegTypes = reg;
			DefinitionTable = new LCDataTable();
			FieldLookup = new Dictionary<Guid, DatatypeField>();


			IDataWidget widg = RegTypes.GetWidget(DisplayWidgetFQN);
			if (widg != null)
			{
				DisplayWidget = widg;
				//todo, error handle this if the widget is no longer loaded
			}
			foreach(SerializableKeyValuePair<Guid, DatatypeField> field in Fields)
			{

				field.Value.DataTypeInfo = RegTypes.GetDatatype(field.Value.DataTypeFQN);
				AddField(field.Key, field.Value, true);
			}
		}
		public void AddField(Guid ID, DatatypeField Field, bool Reload=false)
		{
			if (!Reload)
			{
				if (FieldLookup.ContainsKey(ID))
					throw new Exception("BROKEN ID");
				Fields.Add(new SerializableKeyValuePair<Guid, DatatypeField>(ID, Field));
			}
			FieldLookup[ID] = Field;
			Field.PropertyChanged += Field_PropertyChanged;
			GenerateColumnData(Field);
			FieldAddedEventArgs args = new FieldAddedEventArgs("__FieldAdded", Field);
			RaiseFieldAdded(args);
		}

		void Field_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if(e is FieldChangedEventArgs)
			{
				//rebuild definition table
				DefinitionTable.PrimaryKey = null;
				DefinitionTable.Columns.Clear();
				foreach (SerializableKeyValuePair<Guid, DatatypeField> field in Fields)
				{
					GenerateColumnData(field.Value);
				}
				RaisePropertyChanged((FieldChangedEventArgs)e);
			}
		}

		private void GenerateColumnData(DatatypeField Field)
		{
			DataColumn dc = Field.DataTypeInfo.GenerateDataColumn();
			dc.ColumnName = Field.FieldName;
			//dc.ReadOnly = true;
			
			//todo: here detect if something has changed and change it
			// for now, just rewire, assume definitions don't change
			// might not change here because its the definition, would change after loading data
			// we can compare against this to see if cols changed and deal appropriately
			Field.RowColumn = DefinitionTable.Columns.Count;
			DefinitionTable.Columns.Add(dc);
			if (Field.FieldName == "RowID")
				DefinitionTable.PrimaryKey = new DataColumn[]{dc};
			if (Field.IsNameField)
				NameField = Field.FieldName;
			if (Field.DataTypeInfo.IsReferencetype())
			{
				dc = Field.DataTypeInfo.GenerateDataReferenceColumn();
				dc.ColumnName = Field.FieldName + "_ref";
				//dc.ReadOnly = true;
				Field.RowColumn = DefinitionTable.Columns.Count;
				DefinitionTable.Columns.Add(dc);
			}
		}

		public void RemoveField(DatatypeField fld)
		{
			//remove from all lists
			if (FieldLookup.ContainsKey(fld.ID))
			{
				FieldLookup.Remove(fld.ID);
			}
			for (int i = 0; i < Fields.Count; i++)
			{
				if (Fields[i].Key == fld.ID)
				{
					Fields.RemoveAt(i);
					break;
				}
			}
			DataColumn dc = DefinitionTable.Columns[fld.FieldName];
			dc.ReadOnly = false;
			if(fld.DataTypeInfo.IsReferencetype())
			{
				DataColumn dc2 = DefinitionTable.Columns[fld.FieldName + "_ref"];
				dc2.ReadOnly = false;
				DefinitionTable.Columns.Remove(dc2);
			}
			DefinitionTable.Columns.Remove(dc);
			FieldRemovedEventArgs args = new FieldRemovedEventArgs("__FieldRemoved", fld);
			RaiseFieldRemoved(args);
			//we should also throw a propertyeventchanged and have instances subscribe to changes
		}

		public LCDataTable GetNewTable()
		{
			return (LCDataTable)DefinitionTable.Clone();
		}
		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			// take a copy to prevent thread issues
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		private void RaisePropertyChanged(FieldChangedEventArgs args)
		{
			// take a copy to prevent thread issues
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, args);
			}
		}
		private void RaiseFieldAdded(FieldAddedEventArgs args)
		{
			// take a copy to prevent thread issues
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, args);
			}
		}
		private void RaiseFieldRemoved(FieldRemovedEventArgs args)
		{
			// take a copy to prevent thread issues
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, args);
			}
		}
	}
}
