﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Xml.Serialization;
using NodeLynx.CustomData.DataTypes;

namespace NodeLynx.CustomData
{
	public class FieldChangedEventArgs : PropertyChangedEventArgs
	{
		private string _oldValue = "";
		private string _newValue = "";
		private DatatypeField _field = null;
		public virtual string OldValue { get { return _oldValue; } }
		public virtual string NewValue { get { return _newValue; } }
		public virtual DatatypeField Field { get { return _field; } }
		public FieldChangedEventArgs(string propertyName, DatatypeField field, string oldValue, string newValue)
			: base(propertyName)
		{
			_oldValue = oldValue;
			_newValue = newValue;
			_field = field;
		}
	}
	[Serializable]
	public class DatatypeField : INotifyPropertyChanged
	{
		public Guid _ID;
		//public Control DataControl;
		[XmlIgnore]
		public Guid ID { get { return _ID; }  set { _ID = value; } }
		private string _FieldName="";
		public string _FieldLabel = "";
		public bool _IsNameField = false;
		public bool _IsIconField = false;
		public bool Inheritied = false;
		public string InheritedFrom = "";
		//has this field been used, or is it still editable?
		public bool InUse = false;
		public string FieldName 
		{ get { return _FieldName; } 
			set 
			{
				string old = _FieldName;
				_FieldName = value; 
				RaisePropertyChanged("__FieldName", old, _FieldName); 
			} 
		}
		//something that isn't our internal ID, and isn't a label, can be used as a column or field name on data export
		public string _TechnicalName = "";
		public string TechnicalName
		{
			get { return _TechnicalName; }
			set
			{
				string old = _TechnicalName;
				_TechnicalName = value;
				RaisePropertyChanged("__TechnicalName", old, _FieldName);
			}
		}
		public string FieldLabel { get { return _FieldLabel; } 
			set {
				string old = _FieldLabel;
				_FieldLabel = value; 
				RaisePropertyChanged("__FieldLabel", old, _FieldLabel); 
			} 
		}
		public bool IsNameField { get { return _IsNameField; } set { _IsNameField = value; RaisePropertyChanged("__IsNameField"); } }
		public bool IsIconField { get { return _IsIconField; } set { _IsIconField = value; RaisePropertyChanged("__IsIconField"); } }
		public bool IsVisible = true;

		//for internal use
		public int RowColumn = 0;

		public DatatypeField Clone()
		{
			DatatypeField fld = new DatatypeField();
			fld.FieldName = _FieldName;
			fld.FieldLabel = _FieldLabel;
			fld.SetDatatype(DataTypeInfo);
			fld.InheritedFrom = ID.ToString();
			fld.Inheritied = true;
			fld.IsVisible = IsVisible;
			fld.InUse = false;

			return fld;
		}

		public List<Guid> ComplexTypeReferences = new List<Guid>();
		public DatatypeField()
		{
			_ID = Guid.NewGuid();
			_IsNameField = false;
			_IsIconField = false;
			_FieldName = "";
			_FieldLabel = "";
			//DataControl = Value.GetNewControl();
			//DataControl.Name = _ID.ToString();
		}

		public string RegExConstraint = "";
		public bool ReferenceNotNull = false;
		public List<string> AllowedReferenceDatatypes = new List<string>();
		
		public void SetDatatype(NLDataType type)
		{
			string oldFQN = DataTypeFQN;
			_DataTypeInfo = type;
			DataTypeIdent = type.GetID();
			_DataTypeFQN = type.GetFQN();
			RaisePropertyChanged("__DataTypeInfo", oldFQN, DataTypeFQN);
		}
		[XmlIgnore]
		private NLDataType _DataTypeInfo = null;
		[XmlIgnore]
		public NLDataType DataTypeInfo
		{
			get
			{
				return _DataTypeInfo;
			}
			set
			{
				SetDatatype(value);
				
			}
		}
		public string DataTypeIdent { get; set; }
		private string _DataTypeFQN = "";
		public string DataTypeFQN
		{
			get
			{ return _DataTypeFQN; }
			set
			{
				if (DataProject.CurrentProject != null)
				{
					SetDatatype(DataProject.CurrentProject.RegisteredDatatypes.GetDatatype(value));
				} else
				{
					_DataTypeFQN = value;
				}
			}
		}
		
		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName, string oldValue, string newValue)
		{
			// take a copy to prevent thread issues
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new FieldChangedEventArgs(propertyName, this, oldValue, newValue));
			}
		}
		private void RaisePropertyChanged(string propertyName)
		{
			// take a copy to prevent thread issues
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
