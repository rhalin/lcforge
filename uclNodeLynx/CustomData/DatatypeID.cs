﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeLynx.CustomData
{

	[AttributeUsage(AttributeTargets.Class)]
	public class DatatypeID : Attribute
	{
		private string _ID = "";
		private string _FQN = "";
		public string ID { get { return _ID; } set { _ID = value; } }
		public string FQN { get { return _FQN; } set { _FQN = value; } }
	}
}
