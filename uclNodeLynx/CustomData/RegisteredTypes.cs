﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using NodeLynx.CustomData.DataTypes;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.Node.Linker;
using NodeLynx.Node.NodeType;

namespace NodeLynx.CustomData
{
	public class RegisteredTypes
	{
		[XmlIgnore]
		private Dictionary<string, NLDataType> KnownTypes = new Dictionary<string, NLDataType>();
		[XmlIgnore]
		private Dictionary<string, NLDataType> KnownTypesFQN = new Dictionary<string, NLDataType>();
		[XmlIgnore]
		private List<string> _TypeFQNs = new List<string>();
		[XmlIgnore]
		private Dictionary<string, IDataWidget> KnownWidgetTypes = new Dictionary<string, IDataWidget>();
		[XmlIgnore]
		private Dictionary<string, IDataWidget> KnownWidgetTypesFQN = new Dictionary<string, IDataWidget>();
		[XmlIgnore]
		private List<string> _WidgetTypeFQNs = new List<string>();

		[XmlIgnore]
		private Dictionary<string, ucLFNode> KnownNodeTypes = new Dictionary<string, ucLFNode>();
		[XmlIgnore]
		private Dictionary<string, ucLFNode> KnownNodeTypesFQN = new Dictionary<string, ucLFNode>();
		[XmlIgnore]
		private List<string> _NodeTypeFQNs = new List<string>();

		[XmlIgnore]
		private Dictionary<string, lnkLFLinker> KnownNodeLinkerTypes = new Dictionary<string, lnkLFLinker>();
		[XmlIgnore]
		private Dictionary<string, lnkLFLinker> KnownNodeLinkerTypesFQN = new Dictionary<string, lnkLFLinker>();
		[XmlIgnore]
		private List<string> _NodeLinkerTypeFQNs = new List<string>();
		public static RegisteredTypes DefaultTypes()
		{
			RegisteredTypes RegTypes = new RegisteredTypes();
			RegTypes.RegisterType(new NLID());
			RegTypes.RegisterType(new NLAutoIdentity());
			RegTypes.RegisterType(new NLDateTime());
			RegTypes.RegisterType(new NLInt());
			RegTypes.RegisterType(new NLLong());
			RegTypes.RegisterType(new NLDouble());
			RegTypes.RegisterType(new NLTextbox());
			RegTypes.RegisterType(new NLShortText());
			RegTypes.RegisterType(new NLSingleReference());
			RegTypes.RegisterType(new NLMultiReference());
			RegTypes.RegisterType(new NLDataInstance());
			RegTypes.RegisterType(new NLDatatype());
			RegTypes.RegisterType(new NLColor());
			RegTypes.RegisterType(new NLDuration());
			RegTypes.RegisterType(new NLQCKeyBinding());
			
			RegTypes.RegisterWidget(new ucWidget());
			RegTypes.RegisterWidget(new ucAssetWidget());

			RegTypes.RegisterNode(new ucLFNode());
			RegTypes.RegisterNodeLinker(new lnkLFLinker());
			return RegTypes;
		}
		public List<string> TypeFQNs
		{
			private set { }
			get
			{
				return _TypeFQNs;
			}
		}
		public List<string> WidgetTypeFQNs
		{
			private set { }
			get
			{
				return _WidgetTypeFQNs;
			}
		}
		public NLDataType GetDatatype(string FQN)
		{
			//Console.WriteLine(FQN);
			return KnownTypesFQN[FQN];
		}
		public void RegisterType(NLDataType datatype)
		{
			
			Type t = datatype.GetType();
			DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
			if (!KnownTypesFQN.ContainsKey(did.FQN))
			{
				KnownTypes[did.ID] = datatype;
				KnownTypesFQN[did.FQN] = datatype;
				_TypeFQNs.Add(did.FQN);
			}
		}
		public Dictionary<string, NLDataType> GetTypes()
		{
			return KnownTypesFQN;
		}


		public IDataWidget GetWidget(string FQN)
		{
			return KnownWidgetTypesFQN[FQN];
		}

		public void RegisterWidget(IDataWidget widget)
		{
			Type t = widget.GetType();
			DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
			if (!KnownWidgetTypesFQN.ContainsKey(did.FQN))
			{
				KnownWidgetTypes[did.ID] = widget;
				KnownWidgetTypesFQN[did.FQN] = widget;
				_WidgetTypeFQNs.Add(did.FQN);
			}
		}
		public Dictionary<string, IDataWidget> GetWidgetTypes()
		{
			return KnownWidgetTypesFQN;
		}

		public ucLFNode GetNode(string FQN)
		{
			return KnownNodeTypesFQN[FQN];
		}

		public void RegisterNode(ucLFNode node)
		{
			Type t = node.GetType();
			DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
			if (!KnownNodeTypesFQN.ContainsKey(did.FQN))
			{
				KnownNodeTypes[did.ID] = node;
				KnownNodeTypesFQN[did.FQN] = node;
				_NodeTypeFQNs.Add(did.FQN);
			}
		}
		public Dictionary<string, ucLFNode> GetNodeTypes()
		{
			return KnownNodeTypesFQN;
		}

		public lnkLFLinker GetNodeLinker(string FQN)
		{
			return KnownNodeLinkerTypesFQN[FQN];
		}

		public void RegisterNodeLinker(lnkLFLinker linker)
		{
			Type t = linker.GetType();
			DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
			if (!KnownNodeTypesFQN.ContainsKey(did.FQN))
			{
				KnownNodeLinkerTypes[did.ID] = linker;
				KnownNodeLinkerTypesFQN[did.FQN] = linker;
				_NodeLinkerTypeFQNs.Add(did.FQN);
			}
		}
		public Dictionary<string, lnkLFLinker> GetNodeLinkerTypes()
		{
			return KnownNodeLinkerTypesFQN;
		}
	}
}
