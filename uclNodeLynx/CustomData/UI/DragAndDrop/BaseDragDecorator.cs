﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace NodeLynx.CustomData.UI.DragAndDrop
{

	public abstract class BaseDragDecorator : Decorator
	{
		protected UIElement DecoratedUIElement
		{
			get
			{
				if (this.Child is BaseDragDecorator)
				{
					return ((BaseDragDecorator)this.Child).DecoratedUIElement;
				}
				return this.Child;
			}
		}
	}
}
