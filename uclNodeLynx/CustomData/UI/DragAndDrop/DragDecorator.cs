﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using NodeLynx.CustomData.UI.Widgets;

namespace NodeLynx.CustomData.UI.DragAndDrop
{

	public class DragDecorator : BaseDragDecorator
	{
		private DragDropManager ddManager;

		static DragDecorator()
		{
		}

		public DragDecorator()
			: base()
		{

		}
		public void SetDragDropManager(DragDropManager ddmgr)
		{
			ddManager = ddmgr;
		}
		public void SetChild(IDataWidget wid)
		{
			this.Child = (FrameworkElement)wid;
			if (!(base.DecoratedUIElement is IDataWidget))
			{
				throw new InvalidCastException(string.Format("ItemsControlDragDecorator cannot have child of type {0}", Child.GetType()));
			}
			FrameworkElement DraggingElement = (FrameworkElement)DecoratedUIElement;
			FrameworkElement container = ((IDataWidget)DecoratedUIElement).GetContainer();
			container.MouseDown += container_MouseDown;
			
		}


		void container_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			//FrameworkElement wid = ((IDataWidget)DecoratedUIElement).GetContainer();
			DragDropDataContainer container = new DragDropDataContainer((IDataWidget)DecoratedUIElement);
			DataObject dragData = new DataObject((DragDropDataContainer)container);
			DragDrop.DoDragDrop((FrameworkElement)DecoratedUIElement, dragData, DragDropEffects.Move);
			DecoratedUIElement.Focus();
			//todo: wire up querycontinuedrag and have an item moved -off- a drop container get deleted from it 
			// - but we need a property on a drop container to override this (for containers that cant have items removed/added, 
			//  like libraries)
		}
	}
}
