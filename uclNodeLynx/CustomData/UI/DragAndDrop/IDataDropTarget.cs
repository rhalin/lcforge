﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using NodeLynx.lib;

namespace NodeLynx.CustomData.UI.DragAndDrop
{
	public interface IDataDropTarget : IFrameworkInputElement, IDropTargetHost
	{
		FrameworkElement GetDropTargetContainer();
		void SetDropOver();
		void RemoveDropOver();

		List<SerializableKeyValuePair<string, long>> GetRefences();
		string GetDataName();
		void SetAllowedDatatypes(List<string> datatypes);
	}
}
