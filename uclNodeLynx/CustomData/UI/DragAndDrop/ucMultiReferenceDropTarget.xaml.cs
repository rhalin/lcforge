﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.lib;

namespace NodeLynx.CustomData.UI.DragAndDrop
{
	/// <summary>
	/// Interaction logic for ucMultiReferenceDropTarget.xaml
	/// </summary>
	public delegate void ReferenceDroppedEventHandler(object dropped, EventArgs e);

	public partial class ucMultiReferenceDropTarget : UserControl, INotifyPropertyChanged
	{
		List<string> AllowedDatatypes = new List<string>();
		public ucMultiReferenceDropTarget()
		{
			InitializeComponent();
		}
		public IDataWidget Dropping = null;
		Storyboard sbFadeIn;
		Storyboard sbScroll;
		DataInstance Instance { get; set; }


		public event ReferenceDroppedEventHandler ReferenceDropped;
		protected virtual void OnReferenceDropped(object droppped, EventArgs e)
		{
			if (ReferenceDropped != null)
				ReferenceDropped(droppped, e);
		}
		private void ReferenceStack_DragEnter(object sender, DragEventArgs e)
		{
			if (Dropping == null)
			{
				DragDropDataContainer cont = (DragDropDataContainer)e.Data.GetData(typeof(DragDropDataContainer));		
				IDataWidget wid = cont.WidgetData;  //(IDataWidget)e.Data.GetData(typeof(IDataWidget));

				//we check the current stack to see if this item already exists in it (exact reference)
				//this allows the item to remain drag/droppable, but not get dropped into the same container it came from.
								bool AllowedDrop = true;
				if (AllowedDatatypes.Count > 0)
				{
					List<string> dtypes = wid.GetDatatypes();
					List<string> common = dtypes.Intersect(AllowedDatatypes).ToList();
					if (common.Count == 0)
						AllowedDrop = false;
				}
				if (AllowedDrop)
				{
					bool Contains = false;
					for (int i = 0; i < ReferenceStack.Children.Count; i++)
					{
						if (((DragDecorator)ReferenceStack.Children[i]).Child == wid)
						{
							Contains = true;
							break;
						}

					}
					if (!Contains)
					{
						IDataWidget cloneWid = wid.Clone();
						ReferenceStack.Children.Add((FrameworkElement)cloneWid);
						Dropping = cloneWid;
						Dropping.Name = "DROP_" + Guid.NewGuid().ToString().Replace('-', '_');
						((FrameworkElement)Dropping).Opacity = 0;
						this.RegisterName(Dropping.Name, Dropping);
						((FrameworkElement)Dropping).IsHitTestVisible = false;

						DoubleAnimation myDoubleAnimation = new DoubleAnimation();
						myDoubleAnimation.From = 0;
						myDoubleAnimation.To = 1;
						myDoubleAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(300));

						// Configure the animation to target the button's Width property.
						Storyboard.SetTargetName(myDoubleAnimation, Dropping.Name);
						Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(FrameworkElement.OpacityProperty));

						sbFadeIn = new Storyboard();
						sbFadeIn.Children.Add(myDoubleAnimation);
						UpdateLayout();
						//sbFadeIn.Begin(dropGrid);
						((FrameworkElement)cloneWid).Loaded += new RoutedEventHandler(ucMultiReferenceDropTarget_Loaded);
					}
				}
			}
		}

		void ucMultiReferenceDropTarget_Loaded(object sender, RoutedEventArgs e)
		{
			sbFadeIn.Begin(this);
		}

		private void ReferenceStack_DragLeave(object sender, DragEventArgs e)
		{
			if (Dropping != null)
			{
				ReferenceStack.Children.Remove((FrameworkElement)Dropping);
				Dropping = null;
			}
		}

		private void ReferenceStack_Drop(object sender, DragEventArgs e)
		{
			if (Dropping != null)
			{
				Dropping.AllowResize = true;
				Dropping.AllowDelete = true;
				Dropping.Iconify(true);
				Console.WriteLine("DROP:" + sender.ToString() + " " + e.Data.ToString());
				((FrameworkElement)Dropping).IsHitTestVisible = true;

				ReferenceStack.Children.Remove((FrameworkElement)Dropping);

				DragDecorator decro = new DragDecorator();
				//decro.SetDragDropManager(ddManager);
				decro.SetChild(Dropping);
				ReferenceStack.Children.Add((FrameworkElement)decro);

				scrollRefs.ScrollToRightEnd();
				OnReferenceDropped(Dropping, null);

				Dropping.ReferenceDeleted += Dropping_ReferenceDeleted;
				
				Dropping = null;
				DataItem = GetRefences();
				OnPropertyChanged("References");
			}
		}

		void Dropping_ReferenceDeleted(object dropped, EventArgs e)
		{
			bool Contains = false;
			for (int i = 0; i < ReferenceStack.Children.Count; i++)
			{
				if (((DragDecorator)ReferenceStack.Children[i]).Child == dropped)
				{
					Contains = true;
					ReferenceStack.Children.RemoveAt(i);
					DataItem = GetRefences();
					OnPropertyChanged("References");
					break;
				}
			}
		}

		private void btnNext_Click(object sender, RoutedEventArgs e)
		{
			//scrollRefs.ScrollToRightEnd();

			scrollRefs.ScrollToHorizontalOffset(scrollRefs.HorizontalOffset + 30);
			/*
			DoubleAnimation myDoubleAnimation = new DoubleAnimation();
			myDoubleAnimation.From = scrollRefs.HorizontalOffset;
			myDoubleAnimation.To = scrollRefs.HorizontalOffset + 30;
			myDoubleAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(300));

			Storyboard.SetTargetName(myDoubleAnimation, "scrollRefs");
			Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(ScrollAnimationBehavior.VerticalOffsetPropert));


			sbScroll = new Storyboard();
			sbScroll.Children.Add(myDoubleAnimation);
			sbScroll.Begin(this);
			 * */
		}

		private void btnPrev_Click(object sender, RoutedEventArgs e)
		{
			if(scrollRefs.HorizontalOffset >= 30)
			{
				scrollRefs.ScrollToHorizontalOffset(scrollRefs.HorizontalOffset - 30);
			} else
			{
				scrollRefs.ScrollToHorizontalOffset(0);
			}
		}
		List<SerializableKeyValuePair<string, long>> DataItem = new List<SerializableKeyValuePair<string, long>>();
		public List<SerializableKeyValuePair<string, long>> GetRefences()
		{
			if (ReferenceStack.Children.Count > 0)
			{
				List<SerializableKeyValuePair<string, long>> lst = new List<SerializableKeyValuePair<string, long>>();
				for (int i = 0; i < ReferenceStack.Children.Count; i++)
				{
					IDataWidget wid = (IDataWidget)((((DragDecorator)ReferenceStack.Children[i]).Child));
					lst.Add(wid.GetDataItem());
				}
				return lst;
			}
			else
			{
				return null;
			}
		}

		public string GetDataName()
		{
			if (ReferenceStack.Children.Count > 0)
			{
				IDataWidget wid = ((IDataWidget)((DragDecorator)ReferenceStack.Children[0]).Child);
				return "[DATA]";
			}
			else
			{
				return "";
			}
		}
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}
		public event PropertyChangedEventHandler PropertyChanged;


		public void SetData(DataInstance inst, LCDataRow row)
		{

			Instance = inst;
			IDataWidget defWidg = inst.Definition.DisplayWidget;
			IDataWidget newWidg = defWidg.Create();
			newWidg.AllowResize = true;
			newWidg.AllowDelete = true;
			newWidg.Iconify(true);
			newWidg.ReferenceDeleted += newWidg_ReferenceDeleted;

			SerializableKeyValuePair<string, long> di = new SerializableKeyValuePair<string, long>();
			di.Key = inst.ID.ToString();
			di.Value = long.Parse(row[0].ToString());
			newWidg.SetDataItem(di);

			DragDecorator decro = new DragDecorator();
			decro.SetChild(newWidg);
			Dictionary<string, object> Bindings = new Dictionary<string, object>();
			foreach (SerializableKeyValuePair<string, string> binding in inst.Definition.WidgetBindings)
			{
				foreach (SerializableKeyValuePair<Guid, DatatypeField> field in inst.Definition.Fields)
				{
					if (field.Value.FieldName == binding.Value)
					{
						Bindings[binding.Key] = row[field.Value.FieldName].ToString();
					}
				}
				//Bindings[binding.Key]
			}
			newWidg.SetBindings(Bindings);
			ReferenceStack.Children.Add((FrameworkElement)decro);
			//DataItem = GetRefences();
		}
		private void newWidg_ReferenceDeleted(object dropped, EventArgs e)
		{
			bool changed = false;
			for (int i = 0; i < ReferenceStack.Children.Count; i++)
			{
				if (((DragDecorator)ReferenceStack.Children[i]).Child == dropped)
				{
					ReferenceStack.Children.RemoveAt(i);
					changed = true;
					break;
				}
			}
			//if (myContainer.ReferenceStack.Children.Contains((FrameworkElement)dropped))
			//	myContainer.ReferenceStack.Children.Remove((FrameworkElement)dropped);
			for (int i = 0; i < DataItem.Count; i++)
			{
				IDataWidget wid = (IDataWidget)dropped;
				SerializableKeyValuePair<string, long> r = wid.GetDataItem();
				if (r != null && r.Key == DataItem[i].Key && r.Value == DataItem[i].Value)
				{
					DataItem.RemoveAt(i);
					changed = true;
					break;
				}
			}
			if(changed)
			{
				OnPropertyChanged("References");
			}
			//DataItem.Clear();
		}
		public virtual void PopulateControlWithData(object obj)
		{
			//((TextBox)ctrl).Text = ((int)obj).ToString();
			if (obj != DBNull.Value)
			{
				//lookup instance
				DataItem = (List<SerializableKeyValuePair<string, long>>)obj;
				if (DataItem.Count > 0
					&& DataItem[0] != null
					&& DataItem[0].Key != null
					&& DataItem[0].Key != "")
				{
					
					ReferenceStack.Children.Clear();
					for (int i = 0; i < DataItem.Count; i++)
					{
						DataInstance inst = DataProject.CurrentProject.GetInstance(DataItem[i].Key);
						var result =
							((DataTable)inst.Data).AsEnumerable().Where(dr => dr.Field<long>("RowID") == DataItem[i].Value);

						if (result != null)
						{
							SetData(inst, (LCDataRow)result.First());
						}
					}
					//call SetData
				}
				else
				{
					ReferenceStack.Children.Clear();
				}
				//return result

			}
			else
			{
				ReferenceStack.Children.Clear();
			}
		}
	}
}
