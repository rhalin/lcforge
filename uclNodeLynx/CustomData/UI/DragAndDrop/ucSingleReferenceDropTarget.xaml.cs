﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.lib;

namespace NodeLynx.CustomData.UI.DragAndDrop
{
	/// <summary>
	/// Interaction logic for ucSingleReferenceDropTarget.xaml
	/// </summary>
	public partial class ucSingleReferenceDropTarget : UserControl, IDataDropTarget, INotifyPropertyChanged
	{
		DragDropManager dragdropManager;
		public IDataWidget Dropping = null;
		public FrameworkElement Existing = null;
		List<string> AllowedDatatypes = new List<string>();
		DataInstance Instance { get; set; }
		List<SerializableKeyValuePair<string, long>> DataItem = new List<SerializableKeyValuePair<string, long>>();
		public ucSingleReferenceDropTarget()
		{
			InitializeComponent();
		}

		public FrameworkElement GetDropTargetContainer()
		{
			return dropGrid;
		}

		public void SetDragDropManager(DragDropManager ddManager)
		{
			dragdropManager = ddManager;
		}


		public void SetDropOver()
		{
			
		}

		public void RemoveDropOver()
		{
			
		}
		Storyboard sbFadeIn;
		public void dropGrid_DragEnter(object sender, DragEventArgs e)
		{
			//dropGrid.Background = Brushes.Yellow;
			if (Dropping == null)
			{
				DragDropDataContainer cont = (DragDropDataContainer)e.Data.GetData(typeof(DragDropDataContainer));
				if (cont != null)
				{
					IDataWidget wid = cont.WidgetData;//(IDataWidget)e.Data.GetData(typeof(IDataWidget));
					bool AllowedDrop = true;
					if (AllowedDatatypes.Count > 0)
					{
						List<string> dtypes = wid.GetDatatypes();
						List<string> common = dtypes.Intersect(AllowedDatatypes).ToList();
						if (common.Count == 0)
							AllowedDrop = false;
					}
					if (AllowedDrop)
					{
						bool Contains = false;
						for (int i = 0; i < dropGrid.Children.Count; i++)
						{
							if (((DragDecorator)dropGrid.Children[i]).Child == wid)
							{
								Contains = true;
								break;
							}

						}
						if (!Contains)
						{
							if (dropGrid.Children.Count == 1 && Existing == null)
							{
								Existing = (FrameworkElement)dropGrid.Children[0];
								//Existing = (IDataWidget)((((DragDecorator)dropGrid.Children[0]).Child));
								dropGrid.Children.Clear();
							}
							IDataWidget cloneWid = wid.Clone();
							dropGrid.Children.Add((FrameworkElement)cloneWid);
							Dropping = cloneWid;
							Dropping.AllowResize = false;
							Dropping.ReferenceDeleted += Dropping_ReferenceDeleted;

							Dropping.Name = "DROP_" + Guid.NewGuid().ToString().Replace('-', '_');
							((FrameworkElement)Dropping).Opacity = 0;
							this.RegisterName(Dropping.Name, Dropping);
							((FrameworkElement)Dropping).IsHitTestVisible = false;

							DoubleAnimation myDoubleAnimation = new DoubleAnimation();
							myDoubleAnimation.From = 0;
							myDoubleAnimation.To = 1;
							myDoubleAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(300));

							// Configure the animation to target the button's Width property.
							Storyboard.SetTargetName(myDoubleAnimation, Dropping.Name);
							Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(FrameworkElement.OpacityProperty));

							sbFadeIn = new Storyboard();
							sbFadeIn.Children.Add(myDoubleAnimation);
							UpdateLayout();
							//sbFadeIn.Begin(dropGrid);
							((FrameworkElement)cloneWid).Loaded += new RoutedEventHandler(ucSingleReferenceDropTarget_Loaded);
						}
					}
				}
			}
		}

		void Dropping_ReferenceDeleted(object dropped, EventArgs e)
		{
			dropGrid.Children.Clear();
		}

		private void ucSingleReferenceDropTarget_Loaded(object sender, RoutedEventArgs e)
		{
			sbFadeIn.Begin(this);
		}

		public void dropGrid_DragLeave(object sender, DragEventArgs e)
		{
			//dropGrid.Background = Brushes.Green;
			dropGrid.Children.Clear();
			if(Existing != null)
			{
				dropGrid.Children.Add((FrameworkElement)Existing);
			}
			Existing = null;
			Dropping = null;
		}

		public void dropGrid_Drop(object sender, DragEventArgs e)
		{
			if (Dropping != null)
			{
				Console.WriteLine("DROP:" + sender.ToString() + " " + e.Data.ToString());
				((FrameworkElement)Dropping).IsHitTestVisible = true;


				Dropping.AllowResize = false;
				Dropping.AllowDelete = true;
				Console.WriteLine("DROP:" + sender.ToString() + " " + e.Data.ToString());
				((FrameworkElement)Dropping).IsHitTestVisible = true;

				dropGrid.Children.Remove((FrameworkElement)Dropping);

				DragDecorator decro = new DragDecorator();
				//decro.SetDragDropManager(ddManager);
				decro.SetChild(Dropping);
				dropGrid.Children.Add((FrameworkElement)decro);


				Dropping.ReferenceDeleted += Dropping_ReferenceDeleted;
				OnPropertyChanged("References");

				Existing = null;
				Dropping = null;
			}
		}


		public List<SerializableKeyValuePair<string, long>> GetRefences()
		{
			if (dropGrid.Children.Count > 0)
			{
				List<SerializableKeyValuePair<string, long>> lst = new List<SerializableKeyValuePair<string, long>>();
				IDataWidget wid = (IDataWidget)((((DragDecorator)dropGrid.Children[0]).Child));
				lst.Add(wid.GetDataItem());
				return lst;
			} else
			{
				return null;
			}
		}

		public string GetDataName()
		{
			if (dropGrid.Children.Count > 0)
			{
				IDataWidget wid = ((IDataWidget)((DragDecorator)dropGrid.Children[0]).Child);
				return "[DATA]";
			}
			else
			{
				return "";
			}
		}


		public void SetAllowedDatatypes(List<string> datatypes)
		{
			AllowedDatatypes = datatypes;
		}

		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}
		public event PropertyChangedEventHandler PropertyChanged;


		public void SetData(DataInstance inst, LCDataRow row)
		{

			Instance = inst;
			IDataWidget defWidg = inst.Definition.DisplayWidget;
			IDataWidget newWidg = defWidg.Create();
			newWidg.AllowResize = false;
			newWidg.AllowDelete = true;
			newWidg.Iconify(false);
			newWidg.ReferenceDeleted += newWidg_ReferenceDeleted;

			SerializableKeyValuePair<string, long> di = new SerializableKeyValuePair<string, long>();
			di.Key = inst.ID.ToString();
			di.Value = long.Parse(row[0].ToString());
			newWidg.SetDataItem(di);

			DragDecorator decro = new DragDecorator();
			decro.SetChild(newWidg);
			Dictionary<string, object> Bindings = new Dictionary<string, object>();
			foreach (SerializableKeyValuePair<string, string> binding in inst.Definition.WidgetBindings)
			{
				foreach (SerializableKeyValuePair<Guid, DatatypeField> field in inst.Definition.Fields)
				{
					if (field.Value.DataTypeInfo.IsReferencetype())
					{
						if (field.Value.FieldName  == binding.Value)
						{
							Bindings[binding.Key] = row[field.Value.FieldName + "_ref"];
						}
					}
					else
					{
						if (field.Value.FieldName == binding.Value)
						{
							Bindings[binding.Key] = row[field.Value.FieldName];
						}
					}
				}
				//Bindings[binding.Key]
			}
			newWidg.SetBindings(Bindings);
			dropGrid.Children.Add((FrameworkElement)decro);
			DataItem = GetRefences();
		}
		private void newWidg_ReferenceDeleted(object dropped, EventArgs e)
		{
			bool changed = false;
			for (int i = 0; i < dropGrid.Children.Count; i++)
			{
				if (((DragDecorator)dropGrid.Children[i]).Child == dropped)
				{
					dropGrid.Children.RemoveAt(i);
					changed = true;
					break;
				}
			}
			//if (myContainer.ReferenceStack.Children.Contains((FrameworkElement)dropped))
			//	myContainer.ReferenceStack.Children.Remove((FrameworkElement)dropped);
			for (int i = 0; i < DataItem.Count; i++)
			{
				IDataWidget wid = (IDataWidget)dropped;
				SerializableKeyValuePair<string, long> r = wid.GetDataItem();
				if (r != null && r.Key == DataItem[i].Key && r.Value == DataItem[i].Value)
				{
					DataItem.RemoveAt(i);
					changed = true;
					break;
				}
			}
			if (changed)
			{
				OnPropertyChanged("References");
			}
			//DataItem.Clear();
		}
		public virtual void PopulateControlWithData(object obj)
		{
			//((TextBox)ctrl).Text = ((int)obj).ToString();
			if (obj != DBNull.Value)
			{
				//lookup instance
				DataItem = (List<SerializableKeyValuePair<string, long>>)obj;
				if (DataItem.Count > 0
					&& DataItem[0] != null
					&& DataItem[0].Key != null
					&& DataItem[0].Key != "")
				{

					dropGrid.Children.Clear();
					for (int i = 0; i < DataItem.Count; i++)
					{
						DataInstance inst = DataProject.CurrentProject.GetInstance(DataItem[i].Key);
						var result =
							((DataTable)inst.Data).AsEnumerable().Where(dr => dr.Field<long>("RowID") == DataItem[i].Value);

						if (result != null)
						{
							SetData(inst, (LCDataRow)result.First());
							break; //we only get to have one instance in a single-ref drop target!
						}
					}
					//call SetData
				}
				else
				{
					dropGrid.Children.Clear();
				}
				//return result

			}
			else
			{
				dropGrid.Children.Clear();
			}
		}
	}
}
