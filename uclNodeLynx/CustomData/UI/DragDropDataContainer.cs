﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx.CustomData.UI.Widgets;

namespace NodeLynx.CustomData.UI
{
	public class DragDropDataContainer
	{
		public IDataWidget WidgetData = null;
		public DragDropDataContainer(IDataWidget wid)
		{
			WidgetData = wid;
		}
	}
}
