﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using NodeLynx.CustomData.UI.DragAndDrop;


namespace NodeLynx.CustomData.UI
{
	public class DragDropManager
	{
		FrameworkElement Dragging;
		List<IDataDropTarget> DropTargets = new List<IDataDropTarget>();
		public Canvas adornCanvas;
		public Window TopWindow;
		double FirstXPos;
		double FirstYPos;
		public void SetFrames(Canvas can, Window win)
		{
			adornCanvas = can;
			//adornCanvas.MouseMove += adornCanvas_MouseMove;
			//adornCanvas.IsHitTestVisible = false;
			TopWindow = win;
		}


		public void SetDragging(FrameworkElement elem)
		{
			Dragging = elem;
		}
		public void SetOver()
		{
			//foreach(IDataDropTarget over in DropTargets)
			//{
			//	Rect r = ((FrameworkElement)over).PointToScreen
				
			//	if(over.IsMouseDirectlyOver)
			//	{
			//		over.SetDropOver();
			//	} else
			//	{
			//		over.RemoveDropOver();
			//	}
			//}
		}
		public void RegisterDropTarget(IDataDropTarget DropTarget)
		{
			((FrameworkElement)DropTarget).AllowDrop = true;
			((FrameworkElement)DropTarget).DragEnter +=DragDropManager_DragEnter;
			((FrameworkElement)DropTarget).Drop += DragDropManager_Drop;
			//DropTarget. += DropTarget_MouseMove;
			//DropTarget.MouseLeave += DropTarget_MouseLeave;
			DropTargets.Add(DropTarget);
		}

		void DragDropManager_Drop(object sender, DragEventArgs e)
		{
			Console.WriteLine(sender.ToString(), e.Data.ToString());

		}

		private void DragDropManager_DragEnter(object sender, DragEventArgs e)
		{
			((IDataDropTarget)sender).SetDropOver();
		}

		void DropTarget_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			((IDataDropTarget)sender).RemoveDropOver();
		}

		void DropTarget_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
		{
			if(Dragging != null)
			{
				((IDataDropTarget)sender).SetDropOver();
			}
		}
		public void RemoveDropTarget(IDataDropTarget DropTarget)
		{

		}
	}
}
