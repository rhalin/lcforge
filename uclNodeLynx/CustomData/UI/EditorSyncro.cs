﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.UI;

namespace NodeLynx.CustomData.UI
{
	public class EditorSynchro
	{
		List<INLTabClient> Clients = new List<INLTabClient>();
		public long RowID { get; set; }
		public int RowIndex { get; set; }
		DataInstance di;
		public DataInstance Instance
		{
			get { return di; }
			private set { }
		}
		public LCDataRow CurrentRow { get; set; }
		public Canvas ContentCanvas;
		public FrameworkElement CurrentWindow;
		INLTabClient CurrentClient = null;
		public EditorSynchro(DataInstance instance, Canvas contentCanvas, FrameworkElement currWindow, long Row=-1)
		{
			ContentCanvas = contentCanvas;
			CurrentWindow = currWindow;
			di = instance;
			if(Row != -1)
			{
				CurrentRow = di.GotoRowID(Row);
				RowIndex = di.Data.Rows.IndexOf(CurrentRow);
				RowID = (long)CurrentRow[0];
			} else
			{
				RowIndex = -1;
				RowID = -1;
				CurrentRow = null;
			}
		}
		public void GotoRow(int index)
		{
			if (index > -1 && index < di.Data.Rows.Count)
			{
				RowIndex = index;
				CurrentRow = (LCDataRow)di.Data.Rows[RowIndex];
				RowID = (long)CurrentRow[0];
			} else
			{
				RowIndex = -1;
				RowID = -1;
				CurrentRow = null;
			}
			UpdateEditors();
		}
		public void NextRow()
		{
			
			if (RowIndex + 1 < di.Data.Rows.Count)
			{
				RowIndex++;
				CurrentRow = (LCDataRow)di.Data.Rows[RowIndex];
			}
			else
				CurrentRow = null;
			UpdateEditors();
		}
		public void AddEditor(INLTabClient client)
		{
			if (CurrentClient == null)
			{
				CurrentClient = client;
			}
			Clients.Add(client);
		}
		public void SelectClient(INLTabClient client)
		{
			CurrentClient = client;
			ContentCanvas.Children.Clear();
			ContentCanvas.Children.Add((FrameworkElement)client);
		}
		public void PrevRow()
		{

		}
		public void RowSearch(long SearchCriteria, string FieldName)
		{
			EnumerableRowCollection<DataRow> r = di.Data.AsEnumerable().Where(dr => dr.Field<long>(FieldName) == SearchCriteria);
			if (r.Count() > 0)
			{
				DataRow dr = r.First();
				RowIndex = di.Data.Rows.IndexOf(dr);
				CurrentRow = (LCDataRow)di.Data.Rows[RowIndex];
				RowID = (long)CurrentRow[0];
			}
			else
			{
				RowIndex = -1;
				RowID = -1;
				CurrentRow = null;
			}
			UpdateEditors();
		}
		
		public void GotoRowID(long ID)
		{
			EnumerableRowCollection<DataRow> r = di.Data.AsEnumerable().Where(dr => dr.Field<long>("RowID") == ID);
			if (r.Count() > 0)
			{
				DataRow dr = r.First();
				RowIndex = di.Data.Rows.IndexOf(dr);
				CurrentRow = (LCDataRow)di.Data.Rows[RowIndex];
				RowID = ID;
			}
			else
			{
				RowIndex = -1;
				RowID = -1;
				CurrentRow = null;
			}
			UpdateEditors();
		}
		public void UpdateEditors()
		{
			foreach(INLTabClient client in Clients)
			{
				client.SetEditorRow(CurrentRow, RowID);
			}
		}
	}
}
