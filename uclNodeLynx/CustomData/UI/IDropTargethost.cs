﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeLynx.CustomData.UI
{
	//if this control, of children of this control may host a drop target, it should implement this interface
	public interface IDropTargetHost
	{
		void SetDragDropManager(DragDropManager ddManager);
	}
}
