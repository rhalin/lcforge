﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.lib;
using NodeLynx.UI;

namespace NodeLynx.CustomData.UI.Widgets
{
	public delegate void ReferenceDeletedHandler(object dropped, EventArgs e);
	public interface IDataWidget : IFrameworkInputElement, IGenerateIcon
	{
		Dictionary<string, object> GetBindableFields();
		void SetBindings(Dictionary<string, object> binds);

		IDataWidget Create();
		FrameworkElement GetContainer();
		IDataWidget Clone();

		void SetDataItem(SerializableKeyValuePair<string, long> obj);
		SerializableKeyValuePair<string, long> GetDataItem();
		void Iconify(bool ToIcon);
		bool AllowResize
		{
			get;
			set;
		}
		bool AllowDelete
		{
			get;
			set;
		}
		event ReferenceDeletedHandler ReferenceDeleted;
		bool IsAsset();
		List<string> GetDatatypes();
		string FQN { get; set; }
		INLDocumentManager DocumentManager { get; set; }
		FrameworkElement CurrentWindow { get; set; }
		Canvas ContentCanvas { get; set; }
	}
}
