﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeLynx.CustomData.UI.Widgets
{
	public interface IGenerateIcon
	{
		void IconHasGenerated();
		string IconFilename { get; set; }
		string IconAssetID { get; set; }
		string CurrentDirectory { get; set; }
	}
}
