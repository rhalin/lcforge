﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace NodeLynx.CustomData.UI
{
	public class WindowData
	{
		public Canvas ContentCanvas;
		public DataInstance Instance;
		public FrameworkElement Window;
		public object Tag;
		public WindowData(FrameworkElement window, Canvas contentCanvas, DataInstance instance)
		{
			Window = window;
			ContentCanvas = contentCanvas;
			Instance = instance;
		}
	}
}
