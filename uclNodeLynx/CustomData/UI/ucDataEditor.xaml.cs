﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData.DataTypes;
using NodeLynx.lib;
using NodeLynx.UI;

namespace NodeLynx.CustomData.UI
{
	/// <summary>
	/// Interaction logic for ucDataEditor.xaml
	/// </summary>
	public partial class ucDataEditor : UserControl, INLTabClient
	{
		List<FrameworkElement> BoundControls = new List<FrameworkElement>();
		public DataInstance di; //
		LCDataRow currentRow = null;
		DatatypeDefinition Def;
		private DataSet ds;
		public DataProject Project = new DataProject();
		DragDropManager ddManager;
		public UserControl GetControl()
		{
			return this;
		}
		public string GetTabName()
		{
			//return "Basic Data Editor";
			return Def.DataName + " edit";
		}
		public ucDataEditor()
		{
			InitializeComponent();
		}
		private static Dictionary<LCDataTable, DataSet> DataSets = new Dictionary<LCDataTable, DataSet>();
		public FrameworkElement SetDataSource(DatatypeDefinition Definition, DataInstance loadinstance = null, long RowID = -1)
		{
			StackPanel sp = GenerateEditor(Definition, loadinstance, true);
			if(edtSync != null)
			{
				RowID = edtSync.RowID;
			}
			if(RowID != -1)
			{
				
				if (edtSync != null)
				{
					edtSync.GotoRowID(RowID);
					currentRow = edtSync.CurrentRow;
				} else
				{
					LCDataRow dr = di.GotoRowID(RowID);
					if (dr != null)
					{
						currentRow = dr;

					}
					else
					{
						currentRow = null;
					}
				}
				BindCurrentRow();
				SetEnablePanel();
			} else
			{
				if (di.Data.Rows.Count > 0)
				{
					LCDataRow dr = (LCDataRow)di.Data.Rows[0];
					di.GotoRowID((long)dr["RowID"]);
					if (edtSync != null && dr != null)
					{
						edtSync.GotoRowID((long)dr["RowID"]);
					}
					if (dr != null)
					{
						currentRow = dr;

					}
					else
					{
						currentRow = null;
					}
				}
				BindCurrentRow();
				SetEnablePanel();
			}
			//if(currentRow != null)
			//{
				//currentRow.PropertyChanged +=currentRow_PropertyChanged;
			//}
			return sp;
		}

		private void currentRow_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			BindCurrentRow();
			SetEnablePanel();		
		}
		public string LibraryName
		{
			get
			{
				if (di != null)
				{
					return di.LibraryName;
				}
				else
					return "";
			}
			set
			{
				if (di != null)
				{
					string oldName = di.LibraryName;
					di.LibraryName = value;
				}
				//todo: set all child data instance names?
			}
		}
		private StackPanel GenerateEditor(DatatypeDefinition Definition, DataInstance loadinstance, bool WireEvents)
		{
			Project = DataProject.CurrentProject;
			StackPanel sp = spEditor;
			Def = Definition;
			//todo: subscribe to datainstance.propertychanged
			int i = 0;
			foreach (SerializableKeyValuePair<Guid, DatatypeField> field in Definition.Fields)
			{
				ucDataField dField = new ucDataField();
				dField.GenerateControl(field.Value);
				//Control ctrl = field.Value.DataTypeInfo.GetNewControl();
				if (field.Value.IsVisible)
				{
					sp.Children.Add(dField);
					dField.Foreground = new SolidColorBrush(Colors.Black);
				}
				BoundControls.Add(dField.ValueControl);
				if (field.Value.IsVisible)
				{
					DataGridBoundColumn dgc = field.Value.DataTypeInfo.GenerateGridColumn();
					dgc.Header = field.Value.FieldLabel;
					dgc.Binding = new Binding(string.Format("[{0}]", i));
					dgc.SortMemberPath = string.Format("[{0}]", i);
					dgc.CanUserSort = false;
					//dgc.IsReadOnly = true;
					dgCurrentData.Columns.Add(dgc);
					if (field.Value.DataTypeInfo.IsReferencetype())
					{
						i++;
					}
				}
				i++;
			}
			if (loadinstance == null)
			{
				di = new DataInstance(Def);
				Project.AddInstance("./data/" + di.ID.ToString() + ".db", di);
			}
			else
			{
				di = loadinstance;
			}
			
			if (DataSets.ContainsKey(di.Data))
			{
				ds = DataSets[di.Data];
			}
			else
			{
				if (di.Data.DataSet != null)
				{
					DataSets[di.Data] = di.Data.DataSet;
					ds = di.Data.DataSet;
				}
				else
				{
					ds = new DataSet();
					ds.Tables.Add(di.Data);
					DataSets[di.Data] = ds;
				}
			}
			if (di != null)
				txtLibraryName.DataContext = this;
			//	LibraryName = di.LibraryName;
			//todo: autogeneratecolumns - lets replace this with something more controlable
			SuspendSelectionChanged = true;
			dgCurrentData.ItemsSource = ds.Tables[0].DefaultView;
			SuspendSelectionChanged = false;
			dgCurrentData.IsReadOnly = true;
			spEditor.IsEnabled = false;
			//don't need to do this every time
			if (WireEvents)
			{
				//respond to the instance changed, not the definition
				// we need the instance updated before we can reload the editor
				// and every editor will have an instance somewhere
				di.PropertyChanged += di_PropertyChanged;
			}
			return sp;
		}

		void di_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "FieldName" || e.PropertyName == "FieldLabel")
			{
				//cant save a new column, deleted column, or datatype change
				SaveCurrentEdits();
			}
			long RowID = -1;
			if(currentRow != null)
				RowID = long.Parse(currentRow["RowID"].ToString());

	
			spEditor.Children.Clear();
			DataSets[di.Data].Tables.Remove(di.Data);
			DataSets.Clear();
			dgCurrentData.Columns.Clear();
			BoundControls.Clear();

			GenerateEditor(Def, di, false);
			if(RowID != -1)
			{
				LCDataRow dr = di.GotoRowID(RowID);
				if(dr != null)
				{
					currentRow = dr;

				} else
				{
					currentRow = null;
				}
				BindCurrentRow();
				SetEnablePanel();
			}
		}


		void SetEnablePanel()
		{
			if (currentRow != null)
			{
				spEditor.IsEnabled = true;
				//currentRow.PropertyChanged += currentRow_PropertyChanged;
			}
			else
				spEditor.IsEnabled = false;
		}
		public void NewItem()
		{
			SaveCurrentEdits();
			UnregisterRowPropertyChanged();
			currentRow = di.NewRow();
			edtSync.GotoRowID((long)currentRow[0]);
			//BindCurrentRow();
			//SetEnablePanel();
		}
		public void BindCurrentRow()
		{
			if (currentRow != null)
			{
				int i = 0;
				int controlID = 0;
				foreach (SerializableKeyValuePair<Guid, DatatypeField> field in Def.Fields)
				{
					FrameworkElement ctrl = BoundControls[controlID];

					if (ctrl != null && field.Value.IsVisible)
					{
						if (field.Value.DataTypeInfo.IsReferencetype())
						{
							i++;
						}
						WindowData winData = new WindowData(CurrentWindow, ContentPane, GetDataInstance());
						field.Value.DataTypeInfo.PopulateControlWithData(winData, currentRow[i], ref ctrl, field.Value, currentRow);
						if(field.Value.DataTypeInfo is NLInt)
						{
						}
					}
					i++;
					controlID++;
				}
			}
			dgCurrentData.SelectedIndex = di.Data.Rows.IndexOf(currentRow);
		}
		public void SaveCurrentEdits()
		{
			bool Changed = false;
			List<Tuple<long, string>> ChangedData = new List<Tuple<long, string>>(); 
			// we actually need 3 params to do this: definition, rowid, field
			//List<SerializableKeyValuePair<string, object>> ChangedData = new List<SerializableKeyValuePair<string, object>>();
			if (currentRow != null)
			{
				int i = 0;
				bool IsValid = true;
				foreach (SerializableKeyValuePair<Guid, DatatypeField> field in Def.Fields)
				{
					if (BoundControls[i] != null && field.Value.IsVisible)
					{
						if (field.Value.DataTypeInfo.ValidateControl(BoundControls[i]) == false)
						{
							IsValid = false;
							break;
						}
					}
					i++;
				}
				if (IsValid)
				{
					i = 0;
					int controlID = 0;
					foreach (SerializableKeyValuePair<Guid, DatatypeField> field in Def.Fields)
					{
						if (BoundControls[controlID] != null && field.Value.IsVisible)
						{
							object reference = field.Value.DataTypeInfo.GetDataFromControl(BoundControls[controlID]);
							if (reference != NLDataType.NoControlData() && reference != currentRow[i])
							{
								di.Data.Columns[i].ReadOnly = false;
								if (field.Value.DataTypeInfo.IsReferencetype())
								{
									
									if(reference != null && reference != NLDataType.NoControlData())
									{
										List<SerializableKeyValuePair<string, long>> refs = (List<SerializableKeyValuePair<string, long>>)reference;
										if (refs != currentRow[i+1])
										{
											//shortcut, was null before, isn't now.  Changed
											if (currentRow[i+1] == DBNull.Value)
											{
												Changed = true;
												di.Data.Columns[i + 1].ReadOnly = false;
												currentRow[i + 1] = refs;
												//di.Data.Columns[i + 1].ReadOnly = true;

											}
											else
											{
												List<SerializableKeyValuePair<string, long>> existingRef = (List<SerializableKeyValuePair<string, long>>)currentRow[i+1];
												//shortcut, counts are different, so must have changed
												if (existingRef.Count != refs.Count)
												{
													Changed = true;
													di.Data.Columns[i + 1].ReadOnly = false;
													currentRow[i + 1] = refs;
													//di.Data.Columns[i + 1].ReadOnly = true;
												}
												else
												{
													//the long way.  Have we changed?
													for (int i2 = 0; i2 < existingRef.Count; i2++)
													{
														if (existingRef[i2].Key != refs[i2].Key || existingRef[i2].Value != refs[i2].Value)
														{
															Changed = true;
															di.Data.Columns[i + 1].ReadOnly = false;
															currentRow[i + 1] = refs;
															//di.Data.Columns[i + 1].ReadOnly = true;
															break;
														}
													}
												}
											}
										}
									}
								}
								else
								{
									object newDataObj = field.Value.DataTypeInfo.GetDataFromControl(BoundControls[controlID]);
									if (currentRow[i] == DBNull.Value && newDataObj != DBNull.Value)
									{
										currentRow[i] = newDataObj;
										Changed = true;
									}
									else
									{
										if (newDataObj != NLDataType.NoControlData() && !field.Value.DataTypeInfo.EqualsField(newDataObj, currentRow[i]))
										{
											currentRow[i] = newDataObj;
											Changed = true;
										}
									}
								}

								//di.Data.Columns[i].ReadOnly = true;
								if (Changed)
								{
									//di.RowUpdated((long)currentRow["RowID"], di.Data.Columns[i].ColumnName, this);
									ChangedData.Add(new Tuple<long, string>((long)currentRow["RowID"], di.Data.Columns[i].ColumnName));
								}
								//todo: record what changed here in temporary list
							}
							if (field.Value.DataTypeInfo.IsReferencetype())
							{
								i++;
							}
						}
						i++;
						controlID++;
					}
					if (Changed)
					{
						foreach (SerializableKeyValuePair<Guid, DatatypeField> field in Def.Fields)
						{
							LCDataRow row = currentRow;
							field.Value.DataTypeInfo.OnInstanceChanged(ref row);
						}
						//di.SaveInstance(DataProject.CurrentProject.FileName);
						
					}
				}
				if (Changed)
				{
					Project.SaveProject();
					//fire event
					// we hold off firing until we're done, so we don't end up with funky meta-changes
					foreach(Tuple<long,string> item in ChangedData)
					{
						di.RowUpdated(item.Item1, item.Item2, this);
					}
				}
			}
		}
		public void NextRow()
		{
			SaveCurrentEdits();
			edtSync.NextRow();
			//currentRow = 
			//BindCurrentRow();
			//SetEnablePanel();
		}
		public void PrevRow()
		{
			SaveCurrentEdits();
			//currentRow = di.PrevRow();
			//BindCurrentRow();
			//SetEnablePanel();
			edtSync.PrevRow();
		}

		private void btnNew_Click(object sender, RoutedEventArgs e)
		{
			NewItem();
		}

		private void btnPrev_Click(object sender, RoutedEventArgs e)
		{
			PrevRow();
		}

		private void btnNext_Click(object sender, RoutedEventArgs e)
		{
			NextRow();
		}
		private bool SuspendSelectionChanged = false;
		private void dgCurrentData_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!SuspendSelectionChanged)
			{
				SaveCurrentEdits();
				int rowIndex = dgCurrentData.SelectedIndex; //drv.Row.GetIndex();
				edtSync.GotoRow(rowIndex);
				//currentRow = di.GotoRow(rowIndex);
				//BindCurrentRow();
				//SetEnablePanel();
			}
		}


		public void OnParentSizeChanged(object sender, SizeChangedEventArgs e)
		{
		}
		public void SetDragDropManager(DragDropManager dragdropManager)
		{
			ddManager = dragdropManager;
		}


		public List<DatatypeDefinition> SupportedEditorFor()
		{
			List<DatatypeDefinition> defs = new List<DatatypeDefinition>();
			//all definitions inherit the base definition at some level, so we support editing all definition
			defs.Add(DataProject.CurrentProject.GetDefinition(Guid.Empty.ToString()));
			return defs;
		}


		public DatatypeDefinition GetDatatypeDefinition()
		{
			return Def;
		}


		public string GetButtonName()
		{
			return "Basic Data Editor";
		}


		public DataInstance GetDataInstance()
		{
			return di;
		}

		public long GetRowID()
		{
			if (currentRow != null)
			{
				return (long)currentRow[0];
			} 
			else
			{
				return -1;
			}
		}
		public void TabShown()
		{
			SetEditorRow(edtSync.CurrentRow, -1);
		}

		private void btnSave_Click(object sender, RoutedEventArgs e)
		{
			SaveCurrentEdits();
		}


		public void SetEditorRow(LCDataRow dr, long RowID)
		{
			//SaveCurrentEdits();
			UnregisterRowPropertyChanged();
			currentRow = dr;
			BindCurrentRow();
			SetEnablePanel();
		}
		EditorSynchro edtSync;
		Canvas ContentPane;
		FrameworkElement CurrentWindow;
		public void SetEditorSynchro(EditorSynchro sync)
		{
			edtSync = sync;
			sync.AddEditor(this);
			ContentPane = sync.ContentCanvas;
			CurrentWindow = sync.CurrentWindow;
		}
		public EditorSynchro GetEditorSynchro()
		{
			return edtSync;
		}
		public void SetupToolbar(Canvas toolbar)
		{

		}

		private void UserControl_Unloaded(object sender, RoutedEventArgs e)
		{
			UnregisterRowPropertyChanged();
		}

		private void UnregisterRowPropertyChanged()
		{
			if (currentRow != null)
			{
				currentRow.PropertyChanged -= currentRow_PropertyChanged;
			}
		}
	}
}
