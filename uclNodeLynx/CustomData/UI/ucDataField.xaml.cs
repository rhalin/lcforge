﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NodeLynx.CustomData.UI
{
	/// <summary>
	/// Interaction logic for ucDataField.xaml
	/// </summary>
	public partial class ucDataField : UserControl
	{
		public FrameworkElement ValueControl = null;
		public ucDataField()
		{
			InitializeComponent();
			//GenerateControl();
		}

		public FrameworkElement GenerateControl(DatatypeField Field)
		{
			if (Field.DataTypeInfo is DatatypeDefinition)
			{
				//insert sub panel into itself
			}
			else
			{
				ValueControl = Field.DataTypeInfo.GetNewControl(Field);
				if (ValueControl != null)
				{
					MainPanel.Children.Add(ValueControl);
					lblName.Content = Field.FieldLabel;
				} else
				{
					this.Visibility = System.Windows.Visibility.Collapsed;
				}
			}
			return ValueControl;
		}
	}
}
