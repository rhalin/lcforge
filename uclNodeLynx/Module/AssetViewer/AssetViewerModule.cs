﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx.CustomData;

namespace NodeLynx.Module.AssetViewer
{
	[DatatypeID(ID = "{2783F77F-0AFD-45BC-92EB-8D20A1784304}", FQN = "com.dreamsmithfoundry.storyforge.assetviewermodule")]
	public class AssetViewerModule : DefaultModule
	{
		public AssetViewerModule()
		{
			Editors.Add(typeof(ucAssetViewer));
			Name = "AssetViewer";
		}
	}
}
