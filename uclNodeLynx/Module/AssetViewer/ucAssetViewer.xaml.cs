﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;
using NodeLynx.UI;

namespace NodeLynx.Module.AssetViewer
{
	/// <summary>
	/// Interaction logic for ucAssetViewer.xaml
	/// </summary>
	public partial class ucAssetViewer : UserControl, INLTabClient
	{
		DatatypeDefinition Definition { get; set; }
		DataInstance Instance { get; set; }
		long Row { get; set; }
		public ucAssetViewer()
		{
			InitializeComponent();
		}
		public void SetupToolbar(Canvas toolbar)
		{

		}
		public UserControl GetControl()
		{
			return this;
		}

		public string GetTabName()
		{
			return "Asset Viewer";
		}

		public string GetButtonName()
		{
			return "Asset Viewer";
		}

		public void OnParentSizeChanged(object sender, SizeChangedEventArgs e)
		{
			//throw new NotImplementedException();
		}

		public void SetDragDropManager(DragDropManager dragdropManager)
		{
			//throw new NotImplementedException();
		}

		public List<DatatypeDefinition> SupportedEditorFor()
		{
			List<DatatypeDefinition> Defs = new List<DatatypeDefinition>();
			Defs.Add(DataProject.CurrentProject.GetAssetLibrary().Definition);
			return Defs;
		}

		public FrameworkElement SetDataSource(DatatypeDefinition def, DataInstance di = null, long RowID = -1)
		{
			Definition = def;
			Instance = di;
			Row = RowID;
			if (RowID != -1 && di != null)
			{

				LCDataRow dr = Instance.GotoRowID(Row);
				LoadImage(dr);
			}
			return this;
		}

		private void LoadImage(LCDataRow dr)
		{
			if (dr["AssetPath"].ToString().EndsWith(".png")
				|| dr["AssetPath"].ToString().EndsWith(".jpg")
				|| dr["AssetPath"].ToString().EndsWith(".bmp")
				|| dr["AssetPath"].ToString().EndsWith(".gif")
				)
			{
				BitmapImage img = new BitmapImage();
				Directory.SetCurrentDirectory(System.IO.Path.GetDirectoryName(DataProject.CurrentProject.FileName));
				img.BeginInit();
				img.UriSource = new Uri(dr["AssetPath"].ToString(), UriKind.Relative);
				img.CacheOption = BitmapCacheOption.OnLoad;
				img.EndInit();

				imgAsset.Source = img;
			}
		}

		public DatatypeDefinition GetDatatypeDefinition()
		{
			return Definition;
		}

		public DataInstance GetDataInstance()
		{
			return Instance;
		}

		public long GetRowID()
		{
			return Row;
		}
		public void TabShown()
		{

		}
		public void SetEditorRow(LCDataRow dr, long RowID)
		{
			Row = RowID;
			LoadImage(dr);
		}
		EditorSynchro edtSync;
		public void SetEditorSynchro(EditorSynchro sync)
		{
			edtSync = sync;
			sync.AddEditor(this);
		}
		public EditorSynchro GetEditorSynchro()
		{
			return edtSync;
		}

	}
}
