﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx.CustomData;
using NodeLynx.UI;

namespace NodeLynx.Module
{
	[DatatypeID(ID = "{6E1F9EB7-DF7D-4266-AAC1-E42979F03CE8}", FQN = "com.dreamsmithfoundry.storyforge.basemodule")]
	public class BaseModule : ICustomModule
	{
		protected List<Type> Editors = new List<Type>();
		protected List<DatatypeDefinition> Definitions = new List<DatatypeDefinition>();
		protected RegisteredTypes RegTypes = new RegisteredTypes();
		protected DataFolder FolderSystem = DataProject.CreateDefaultFolderSystem();
		protected List<Tuple<string, string>> AssetTypeClassifiers = new List<Tuple<string, string>>();
		protected string Name = "Base";
		public BaseModule()
		{

		}
		public virtual List<DatatypeDefinition> GetDefinitions()
		{
			return Definitions;
		}



		public virtual RegisteredTypes GetRequiredDatatypes()
		{
			return RegTypes;
		}

		public virtual RegisteredTypes GetOptionalDatatypes()
		{
			throw new NotImplementedException();
		}




		public virtual DataFolder GetFolderSystem()
		{
			return FolderSystem;
		}

		public virtual List<Tuple<string, string>> GetAssetTypeClassifiers()
		{
			return AssetTypeClassifiers;
		}


		public virtual List<Type> GetEditors()
		{
			return Editors;
		}

		public virtual string GetFQN()
		{
			Type t = this.GetType();
			DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
			return did.FQN;
		}


		public string GetName()
		{
			return Name;
		}
	}
}
