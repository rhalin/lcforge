﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;
using NodeLynx.Node;

namespace NodeLynx.Module
{
	[DatatypeID(ID = "{E9B87105-5A6B-463A-8C32-E281AD83A13A}", FQN = "com.dreamsmithfoundry.storyforge.defaultmodule")]
	public class DefaultModule : BaseModule
	{
		public DefaultModule()
		{
			RegTypes = RegisteredTypes.DefaultTypes();
			Editors.Add(typeof(ucDataEditor));
			//Editors.Add(typeof(ucNodeCanvas));
			Name = "Default";
		}
	}
}
