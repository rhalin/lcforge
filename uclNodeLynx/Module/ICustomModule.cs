﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx.CustomData;
using NodeLynx.UI;

namespace NodeLynx.Module
{
	//todo: this will all eventually need wiring to sandbox modules
	public interface ICustomModule
	{
		List<DatatypeDefinition> GetDefinitions();
		//todo: I want to have the below, but its added work right now
		//List<string> GetDefinitionIDs();
		RegisteredTypes GetRequiredDatatypes();
		RegisteredTypes GetOptionalDatatypes();
		List<Type> GetEditors(); //where type must be an INLTabClient
		//INLTabClient CreateEditor(Type t);
		//todo: also provide folder structure
		DataFolder GetFolderSystem();
		List<Tuple<string, string>> GetAssetTypeClassifiers();
		string GetFQN();
		string GetName();
	}
}
