﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.CSharp;
using NodeLynx.CustomData;
using NodeLynx.lib;

namespace NodeLynx.Module
{
	public class ModuleGenerator
	{
		public static void GenerateModule(DataProject proj, string name, string FQN)
		{
			StringBuilder sb = new StringBuilder();
			StringBuilder sbDef = new StringBuilder();
			//generate nameModule.cs
			//generate definitions

			sb.AppendLine(@"using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx.CustomData;
using NodeLynx.UI;
using NodeLynx.lib;

namespace NodeLynx.Module
{");
			sb.AppendLine("  [DatatypeID(ID = \"{" + Guid.NewGuid().ToString() + "}\", FQN = \"" + FQN + "\")]");
			sb.AppendLine("  public class " + name + "Module : DefaultModule");
			sb.AppendLine("  {");
			sb.AppendLine("    public " + name + "Module() : base()"); //calling base will get use RegTypes
			sb.AppendLine("    {");
			sb.AppendLine("        Name = \""+name+"\";");

			///====================================DEFINITIONS=========================
			foreach (DatatypeDefinition Def in proj.Definitions)
			{
				if (Def.DataName != "BaseDefinition" && Def.ID.ToString().ToLower() != DataProject.AssetDefinitionID.ToLower()) //don't add the base definition or asset definition
				{
					sbDef.AppendLine("    public DatatypeDefinition GenerateDefinition_" + Def.DataName + "(RegisteredTypes RegisteredDatatypes){");
					sbDef.AppendLine("      DatatypeDefinition def = new DatatypeDefinition();");
					sbDef.AppendLine("      def.DisplayWidgetFQN = \"" + Def.DisplayWidgetFQN + "\";");
					sbDef.AppendLine("      def.Initialize(RegisteredDatatypes);");
					sbDef.AppendLine("      def.DataName=\"" + Def.DataName + "\";");
					sbDef.AppendLine("      def.PrintableName=\"" + Def.PrintableName + "\";");
					sbDef.AppendLine("      def.FileName=\"" + Def.FileName + "\";");
					sbDef.AppendLine("      def.ID=Guid.Parse(\"" + Def.ID.ToString() + "\");");

					sbDef.AppendLine("      DatatypeField f = null;");
					sbDef.AppendLine(""); //intentionally blank line
					int at = 0;
					foreach (SerializableKeyValuePair<Guid, DatatypeField> field in Def.Fields)
					{
						if (at >= 6) //we skip the fields that will get added automatically
						{
							DatatypeField f = field.Value;
							sbDef.AppendLine("      f = new DatatypeField();");
							sbDef.AppendLine("      f.SetDatatype(RegisteredDatatypes.GetDatatype(\"" + f.DataTypeFQN + "\"));");
							sbDef.AppendLine("      f.FieldName = \"" + f.FieldName + "\";");
							sbDef.AppendLine("      f.FieldLabel = \"" + f.FieldLabel + "\";");
							sbDef.AppendLine("      f.IsVisible = " + (f.IsVisible ? "true" : "false") + ";");
							sbDef.AppendLine("      def.AddField(Guid.Parse(\"" + f.ID.ToString() + "\"),f);");
							sbDef.AppendLine(""); //intentionally blank line
						}
						at++;
					}

					//==================================== Widget Bindings =========================
					foreach (SerializableKeyValuePair<string, string> binding in Def.WidgetBindings)
					{
						//dd.WidgetBindings.Add(new SerializableKeyValuePair<string, string>("filename", "Filename"));
						sbDef.AppendLine("      def.WidgetBindings.Add(new SerializableKeyValuePair<string, string>(\"" + binding.Key + "\", \"" + binding.Value + "\"));");
					}
					sbDef.AppendLine(""); //intentionally blank line

					sbDef.AppendLine("      return def;");
					sbDef.AppendLine("    }");
					sb.AppendLine("      Definitions.Add(GenerateDefinition_" + Def.DataName + "(RegTypes));");
				}
			}


			//clean up
			sb.AppendLine("    }"); //constructor
			sb.Append(sbDef.ToString()); //append def generation functions
			sb.AppendLine("  }"); //class
			sb.AppendLine("}"); //namespace
			string[] lines = { sb.ToString() };
			System.IO.File.WriteAllLines(Path.GetDirectoryName(proj.FileName) + "/GeneratedModules/src/" + name + "Module.cs", lines);
		}
		public static void CompileModule(DataProject proj, string name, string FQN)
		{
			CSharpCodeProvider codeProvider = new CSharpCodeProvider(new Dictionary<String, String>{{ "CompilerVersion","v4.0" }});
			//ICodeCompiler icc = codeProvider.CreateCompiler();
			
			System.CodeDom.Compiler.CompilerParameters parameters = new CompilerParameters();
			//CompilerResults results = icc.CompileAssemblyFromSource(parameters, SourceString);

			//copy referenced assemblies
			string modDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\Modules\\";
			string[] fileEntries = Directory.GetFiles((new Uri(modDir).LocalPath));
			parameters.GenerateInMemory = false;
			parameters.GenerateExecutable = false;
			parameters.IncludeDebugInformation = true;
			parameters.ReferencedAssemblies.Add("System.dll");
			parameters.ReferencedAssemblies.Add("System.Core.dll");
			parameters.ReferencedAssemblies.Add(Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().GetName().CodeBase).LocalPath) + "\\uclNodeLynx.dll");
			foreach (string fileName in fileEntries)
			{
				parameters.ReferencedAssemblies.Add((new Uri(fileName).LocalPath));
			}

			parameters.OutputAssembly = Path.GetDirectoryName(proj.FileName) + "\\GeneratedModules\\bin\\" + name + "Module.dll";

			CompilerResults results = codeProvider.CompileAssemblyFromFile(parameters, Path.GetDirectoryName(proj.FileName) + "\\GeneratedModules\\src\\" + name + "Module.cs");
			if(results.Errors.Count == 0)
			{
				//copy!
				try
				{
					File.Copy((new Uri(Path.GetDirectoryName(proj.FileName)).LocalPath) + "\\GeneratedModules\\bin\\" + name + "Module.dll", new Uri(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath + "/Modules/" + name + "Module.dll", true);
				} catch (Exception ex)
				{

				}
			}
		}
	}
}
