﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;

namespace NodeLynx.Node
{
	public class AutoSizeCanvas : Canvas
	{
		public AutoSizeCanvas()
		{
			this.MouseWheel += new System.Windows.Input.MouseWheelEventHandler(AutoSizeCanvas_MouseWheel);
		}
		const double SCALE_RATE = 1.1;
		private double currentScale = 0;
		void AutoSizeCanvas_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
		{
			/*
			if (e.Delta > 0)
			{
				st.ScaleX *= SCALE_RATE;
				st.ScaleY *= SCALE_RATE;
			}
			else
			{
				st.ScaleX /= SCALE_RATE;
				st.ScaleY /= SCALE_RATE;
			}
			currentScale = st.ScaleX;
			//this.LayoutTransform = st;
			foreach (FrameworkElement elem in Children)
			{
				elem.LayoutTransform = st;
			}
			this.InvalidateMeasure();*/
		}
		ScaleTransform st = new ScaleTransform();
		protected override System.Windows.Size MeasureOverride(System.Windows.Size constraint)
		{
			base.MeasureOverride(constraint);
			double width = base
				.InternalChildren
				.OfType<UIElement>()
				.Max(i => i.DesiredSize.Width + (double)i.GetValue(Canvas.LeftProperty));

			double height = base
				.InternalChildren
				.OfType<UIElement>()
				.Max(i => i.DesiredSize.Height + (double)i.GetValue(Canvas.TopProperty));


			width = (int)(Parent as ScrollViewer).ViewportWidth < width
				? width
				: (int)(Parent as ScrollViewer).ViewportWidth;


			height = (int)(Parent as ScrollViewer).ViewportHeight < height
				? height
				: (int)(Parent as ScrollViewer).ViewportHeight;

			(Parent as ScrollViewer).InvalidateScrollInfo();
			if (currentScale != 0.0)
			{
				return new Size(width, height);
			}
			else
			{
				return new Size(width, height);
			}
		}
	}
}
