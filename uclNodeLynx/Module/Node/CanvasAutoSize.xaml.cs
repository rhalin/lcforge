﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NodeLynx.Node
{
	/// <summary>
	/// Interaction logic for CanvasAutoSize.xaml
	/// </summary>
	public partial class ucCanvasAutoSize : Canvas
	{
		public ucCanvasAutoSize()
		{
			//base.InitializeComponent();
		}
		ScaleTransform st = new ScaleTransform();

		protected override System.Windows.Size MeasureOverride(System.Windows.Size constraint)
		{
			base.MeasureOverride(constraint);
			double width = base
				.InternalChildren
				.OfType<UIElement>()
				.Max(i => i.DesiredSize.Width + (double)i.GetValue(Canvas.LeftProperty));
			
			width = (int)(Parent as ScrollViewer).ViewportWidth < width
				? width
				: (int)(Parent as ScrollViewer).ViewportWidth;
			

			//(Parent as ScrollViewer).ViewportWidth
			double height = base
				.InternalChildren
				.OfType<UIElement>()
				.Max(i => i.DesiredSize.Height + (double)i.GetValue(Canvas.TopProperty));
			(Parent as ScrollViewer).InvalidateScrollInfo();

			height = (int)(Parent as ScrollViewer).ViewportHeight < height
				? height
				: (int)(Parent as ScrollViewer).ViewportHeight;

			return new Size(width, height);
		}
		const double SCALE_RATE = 1.1;

		private void CanvasAutoSize_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			if (e.Delta > 0)
			{
				st.ScaleX *= SCALE_RATE;
				st.ScaleY *= SCALE_RATE;
			}
			else
			{
				st.ScaleX /= SCALE_RATE;
				st.ScaleY /= SCALE_RATE;
			}
			this.LayoutTransform = st;
		}

	}
}
