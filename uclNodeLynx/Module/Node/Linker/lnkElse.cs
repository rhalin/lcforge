﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

namespace NodeLynx.Node.Linker
{
	public class lnkElse : ucNodeLinker
	{
		public lnkElse()
		{
			base.InitializeComponent();
			BaseStyle = new Style();
			BaseStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(3.0)));
			BaseStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(3.0)));
			BaseStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromRgb(220, 220, 240))));
			BaseStyle.Setters.Add(new Setter(Border.BorderBrushProperty, Brushes.DarkRed));

			ConnectingStyle = new Style();
			ConnectingStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(3.0)));
			ConnectingStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(3.0)));
			ConnectingStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromRgb(220, 220, 240))));
			ConnectingStyle.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromArgb(255, 230, 0, 0))));

			ConnectableStyle = new Style();
			ConnectableStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(3.0)));
			ConnectableStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(3.0)));
			ConnectableStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromRgb(220, 220, 240))));
			ConnectableStyle.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromArgb(255, 0, 0, 230))));

			ConnectedStyle = new Style();
			ConnectedStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(3.0)));
			ConnectedStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(3.0)));
			ConnectedStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromRgb(220, 220, 240))));
			ConnectedStyle.Setters.Add(new Setter(Border.BorderBrushProperty, Brushes.Red));

			MaximumConnections = 1;
			this.brdLinker.Style = BaseStyle;
			CanDuplicate = false;
		}

	}
}
