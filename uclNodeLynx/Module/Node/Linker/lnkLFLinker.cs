﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx.CustomData;

namespace NodeLynx.Node.Linker
{
	[DatatypeID(ID = "{926C792E-C2D8-4832-B7DA-B6FF9523012E}", FQN = "node.linker.default")]
	public class lnkLFLinker : ucNodeLinker
	{

		public lnkLFLinker() : base()
		{
			base.InitializeComponent();
		}
		public Guid ID { get; set; }

		DatatypeDefinition _Definition;
		public DatatypeDefinition Definition { get { return _Definition; } set { _Definition = value; DefinitionSet(); } }

		DataInstance _Instance;
		public DataInstance Instance { get { return _Instance; } set { _Instance = value; InstanceSet(); } }
		private void DefinitionSet()
		{
		}
		private void InstanceSet()
		{
			ID = Guid.Parse(_Instance.Data.Rows[0].ToString());
		}
		public override void Duplicate()
		{
			MyNode.AddNodeLinker<lnkLFLinker>(LinkerDirection); 
		}

		public static DatatypeDefinition GetNewLinkConnectionDefinition(RegisteredTypes RegTypes)
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.Initialize(RegTypes);
			def.DataName = "NodeLinks";
			def.PrintableName = "Links";
			def.FileName = "./defs/LFLinker.def";

			//linker index in the list
			DatatypeField f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.int"));
			f.FieldName = "PrevIndex";
			f.FieldLabel = "PrevIndex";
			f.IsVisible = true;
			def.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.int"));
			f.FieldName = "NextIndex";
			f.FieldLabel = "NextIndex";
			f.IsVisible = true;
			def.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.reference.single"));
			f.FieldName = "PrevNode";
			f.FieldLabel = "PrevNode";
			f.IsVisible = true;
			//ucLFNode base class
			f.AllowedReferenceDatatypes.Add("99D79D67-4604-40E3-9D44-FCF0E4B94459".ToLower());
			def.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.reference.single"));
			f.FieldName = "NextNode";
			f.FieldLabel = "NextNode";
			f.IsVisible = true;
			//ucLFNode base class
			f.AllowedReferenceDatatypes.Add("99D79D67-4604-40E3-9D44-FCF0E4B94459".ToLower());
			def.AddField(Guid.NewGuid(), f);


			//we'll need to make sure and populate these correctly from default linkers on the node before generating new linkers
			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
			f.FieldName = "PrevLinkerTypeFQN";
			f.FieldLabel = "PrevLinkerTypeFQN";
			f.IsVisible = false;  //at the moment, don't let them edit this in the editor.  We need a special control and properties for this
			def.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
			f.FieldName = "NextLinkerTypeFQN";
			f.FieldLabel = "NextLinkerTypeFQN";
			f.IsVisible = false;  //at the moment, don't let them edit this in the editor.  We need a special control and properties for this
			def.AddField(Guid.NewGuid(), f);


			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
			f.FieldName = "LinkNote";
			f.FieldLabel = "Link Note";
			f.IsVisible = true;  
			def.AddField(Guid.NewGuid(), f);

			//These two fields are used to store script code that affects the flow, such as conditionals

			//we may not need a previous node conditional, but add it just in case for now
			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
			f.FieldName = "PrevNodeContext";
			f.FieldLabel = "Previous Node Context";
			f.IsVisible = false;
			def.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
			f.FieldName = "NextNodeContext";
			f.FieldLabel = "Next Node Context";
			f.IsVisible = false;
			def.AddField(Guid.NewGuid(), f);

			return def;
		}
		public string FQN
		{
			get
			{
				Type t = this.GetType();
				DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
				return did.FQN;
			}
			set
			{

			}
		}
	}
}
