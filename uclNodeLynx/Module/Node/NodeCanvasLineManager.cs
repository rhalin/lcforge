﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace NodeLynx.Node
{
	public class NodeCanvasLineManager
	{
		ucNodeLinkLine linkLine = null;
		public ucNodeCanvas MyNodeCanvas { get; set; }
		public NodeCanvasLineManager(ucNodeCanvas myCanvas)
		{
			MyNodeCanvas = myCanvas;
		}
		List<ucNodeLinkLine> Lines = new List<ucNodeLinkLine>();
		public void Reset()
		{
			Lines.Clear();
		}

		public List<ucNodeLinkLine> GetAttachedLines(ucNodeLinker linker)
		{
			List<ucNodeLinkLine> l = new List<ucNodeLinkLine>();
			foreach(ucNodeLinkLine line in Lines)
			{
				if (line.StartLinker == linker || line.EndLinker == linker)
					l.Add(line);
			}
			return l;
		}
		public ucNodeLinkLine LastLinkLine { get { return linkLine; } private set{} }
		public static Path GetLine(double X1, double Y1, double X2, double Y2, Color color)
		{
			PathGeometry pathGeometry = GetLineGeometry(X1, Y1, X2, Y2);
			Path path = new Path();
			path.Data = pathGeometry;
			path.Stroke = new SolidColorBrush(color);
			path.StrokeThickness = 1;
			//Canvas.SetZIndex(path, (int)Layer.Line); 
			//canvas.Children.Add(path); 
			return path;
		}
		public static PathGeometry GetLineGeometry(double X1, double Y1, double X2, double Y2)
		{
			//QuadraticBezierSegment qbs = new QuadraticBezierSegment(new Point(X2, Y1), new Point(X2, Y2), true);
			BezierSegment seg = new BezierSegment(new Point((X1 + X2) / 2, Y1), new Point((X1 + X2) / 2, Y2), new Point(X2, Y2), true);
			PathSegmentCollection pscollection = new PathSegmentCollection();
			pscollection.Add(seg);
			PathFigure pf = new PathFigure();
			pf.Segments = pscollection; pf.StartPoint = new Point(X1, Y1);
			PathFigureCollection pfcollection = new PathFigureCollection();
			pfcollection.Add(pf);
			PathGeometry pathGeometry = new PathGeometry();
			pathGeometry.Figures = pfcollection;
			return pathGeometry;
		}
		public bool DropCurrentLine(ucNodeLinker currentLinker, bool NewLine=true)
		{
			bool Added = false;
			if (linkLine.StartLinker != currentLinker
				&& linkLine.StartLinker.MyNode != currentLinker.MyNode
				&& linkLine.StartLinker.LinkerDirection != currentLinker.LinkerDirection)
			{
				linkLine.EndLinker = currentLinker;
				currentLinker.ConnectedLines.Add(linkLine);
				linkLine.StartLinker.ConnectedLines.Add(linkLine);
				MyNodeCanvas.SetLink(NewLine);
				ReculateLinkLines(currentLinker);
				ReculateLinkLines(linkLine.StartLinker);
				Added = true;
				Lines.Add(linkLine);

				if(NewLine)
					MyNodeCanvas.RequestDelayedSave();
			}
			return Added;
		}
		private Point GetLinkPoint(ucNodeLinker linker)
		{
			Point pnt;
			if (linker.LinkerDirection == ucNodeLinker.Direction.DIR_NEXT)
			{
				Point nxtPnt = new Point(linker.CONN_POINT_NEXT_X, linker.CONN_POINT_NEXT_Y);
				//nxtPnt = MyNode.RenderTransform.Transform(nxtPnt);
				pnt = linker.TranslatePoint(nxtPnt, MyNodeCanvas);
				//pnt = MyNode.RenderTransform.Transform(nxtPnt);
			}
			else
			{
				pnt = linker.TranslatePoint(new Point(linker.CONN_POINT_PREV_X, linker.CONN_POINT_PREV_Y), MyNodeCanvas);
			}
			return pnt;
		}
		public void NewLine(ucNodeLinker Linker)
		{
			linkLine = new ucNodeLinkLine(MyNodeCanvas);
			linkLine.StartPoint = GetLinkPoint(Linker);
			linkLine.EndPoint = new Point(0.0, 0.0);

			linkLine.line = new Line();
			linkLine.line.Tag = linkLine;
			linkLine.line.Focusable = true;
			

			linkLine.line.X1 = linkLine.StartPoint.X;
			linkLine.line.Y1 = linkLine.StartPoint.Y;
			linkLine.line.X2 = linkLine.StartPoint.X;
			linkLine.line.Y2 = linkLine.StartPoint.Y;
			linkLine.line.Stroke = Brushes.Black;
			linkLine.line.StrokeThickness = 2;
			linkLine.StartLinker = Linker;


			//brdLinker.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 230, 0, 0));
			Linker.brdLinker.Style = Linker.ConnectingStyle;
			MyNodeCanvas.SetLine(linkLine);
		}
		public void ReculateLinkLines(ucNodeLinker Linker)
		{
			foreach (ucNodeLinkLine linkLine in Linker.ConnectedLines)
			{
				//get position on grid
				Point pnt = GetLinkPoint(Linker);
				//pnt = MyNode.RenderTransform.Transform(pnt);
				if (linkLine.StartLinker == Linker)
				{
					linkLine.line.X2 = pnt.X;
					linkLine.line.Y2 = pnt.Y;
				}
				else
				{
					linkLine.line.X1 = pnt.X;
					linkLine.line.Y1 = pnt.Y;
				}
				linkLine.line.Tag = linkLine;
				if (linkLine.BezierPath == null)
				{
					Path geom = NodeCanvasLineManager.GetLine(
						linkLine.line.X1, linkLine.line.Y1, linkLine.line.X2, linkLine.line.Y2,
						Color.FromRgb(0, 0, 0));
					//linkLine.BezierPath = geom;
					//linkLine.centerPoint 
					linkLine.SetBezierPath(geom);
					MyNodeCanvas.nodeCanvas.Children.Add(linkLine.BezierPath);
					Canvas.SetZIndex(linkLine.BezierPath, -2);
					MyNodeCanvas.nodeCanvas.Children.Remove(linkLine.line);
					MyNodeCanvas.nodeCanvas.Children.Add(linkLine.textComment);
					MyNodeCanvas.nodeCanvas.Children.Add(linkLine.centerPoint);
					linkLine.BezierPath.Tag = linkLine;
					linkLine.BezierPath.Focusable = true;

				}
				linkLine.centerPoint.Height = 16;
				linkLine.centerPoint.Width = 16;
				linkLine.centerPoint.Style = linkLine.CenterEllipseStyle;

				Canvas.SetLeft(linkLine.textComment, ((linkLine.line.X1 + linkLine.line.X2) / 2) - (linkLine.textComment.Width / 2));
				Canvas.SetTop(linkLine.textComment, ((linkLine.line.Y1 + linkLine.line.Y2) / 2) - (linkLine.textComment.ActualHeight / 2));
				Canvas.SetLeft(linkLine.centerPoint, ((linkLine.line.X1 + linkLine.line.X2) / 2) - (linkLine.centerPoint.Width / 2));
				Canvas.SetTop(linkLine.centerPoint, ((linkLine.line.Y1 + linkLine.line.Y2) / 2) - (linkLine.centerPoint.Height / 2));


				linkLine.BezierPath.Data = NodeCanvasLineManager.GetLineGeometry(linkLine.line.X1, linkLine.line.Y1, linkLine.line.X2, linkLine.line.Y2);

			}
		}
	}
}
