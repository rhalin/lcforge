﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.DragAndDrop;
using NodeLynx.CustomData.UI.Widgets;

namespace NodeLynx.Node.NodeType
{
	public class nodeDropTarget : ucNode
	{
		ucMultiReferenceDropTarget srDropTarget;
		public nodeDropTarget()
		{
			InitializeComponent();
			TopControl = this;
			srDropTarget = new ucMultiReferenceDropTarget();
			srDropTarget.ReferenceDropped += srDropTarget_ReferenceDropped;

			NodeContent.Children.Add(srDropTarget);
			txtTitle.Text = "Drop Target";
		}

		void srDropTarget_ReferenceDropped(object dropped, EventArgs e)
		{
			Console.Write("yep, dropping..." + ((IDataWidget)dropped).Name);
		}



		public void SetDragDropManager(DragDropManager ddManager)
		{
			//srDropTarget.SetDragDropManager(ddManager);
		}


		public void SetDropOver()
		{
			//stackContent.Background = new SolidColorBrush(Colors.Yellow);
		}

		public void RemoveDropOver()
		{
			//stackContent.Background = new SolidColorBrush(Colors.White);
		}
	}
}
