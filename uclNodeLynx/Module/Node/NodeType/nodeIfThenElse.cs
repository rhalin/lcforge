﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx;
using System.Windows.Controls;
using System.Windows;
using NodeLynx.Node.Linker;

namespace NodeLynx.Node.NodeType
{
	class nodeIfThenElse : ucNode
	{
		public nodeIfThenElse()
		{
			base.InitializeComponent();
			TopControl = this;
		}
		public override void SetCanvas()
		{
			MyCanvas = ((ucNodeCanvas)((((Canvas)TopControl.Parent).Parent as FrameworkElement).Parent as FrameworkElement).Parent);
			lnkThen LinkNextThen = new lnkThen();
			LinkNextThen.SetNode(this);
			LinkNextThen.LinkerDirection = ucNodeLinker.Direction.DIR_NEXT;
			stackNext.Children.Add(LinkNextThen);

			lnkElse LinkNextElse = new lnkElse();
			LinkNextElse.SetNode(this);
			LinkNextElse.LinkerDirection = ucNodeLinker.Direction.DIR_NEXT;
			stackNext.Children.Add(LinkNextElse);

			ucNodeLinker ucNodeLinkPrev = new ucNodeLinker();
			ucNodeLinkPrev.SetNode(this);
			ucNodeLinkPrev.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
			ucNodeLinkPrev.LinkerDirection = ucNodeLinker.Direction.DIR_PREV;
			stackPrev.Children.Add(ucNodeLinkPrev);

			CanvasSet = true;
			txtTitle.Text = "If-Then-Else Node";
		}
	}
}
