﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using NodeLynx.CustomData;
using NodeLynx.Module.StoryFlow;
using NodeLynx.Node.NodeType;

namespace NodeLynx.Module.Node.NodeType
{
	[DatatypeID(ID = "{BFF7000E-B5E5-46F2-BC14-E48ADC3708E0}", FQN = "node.example.child")]
	public class ucLFChildNode : ucLFNode
	{
		public ucLFChildNode() : base()
		{
			OnlySubmerged = true;
			NodeTypeName = "Child";

		}
		public override DatatypeDefinition GetNodeClassExtensionDataDefinition(RegisteredTypes RegTypes)
		{
			DatatypeDefinition def = base.GetNodeClassExtensionDataDefinition(RegTypes);
			def.DataName = "LFChildNodeExt";
			def.FileName = ""; //auto fill this
			def.ID = Guid.Parse("{CF6D72A4-F76E-4DC7-A122-D4AF8FEABCF3}");
			return def;
		}
	}
}
