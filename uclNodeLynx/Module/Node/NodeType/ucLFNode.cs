﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Threading;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;
using NodeLynx.lib;
using NodeLynx.Node.Linker;

namespace NodeLynx.Node.NodeType
{
	[DatatypeID(ID = "{99D79D67-4604-40E3-9D44-FCF0E4B94459}", FQN = "node.default")]
	public class ucLFNode : ucNode, INotifyPropertyChanged
	{

		public ucLFNode ParentNode { get; set; }
		List<Type> _ValidChildNodeTypes = new List<Type>();
		public List<Type> ValidChildNodeTypes
		{
			get { return _ValidChildNodeTypes; }
			set { _ValidChildNodeTypes = value; }
		}

		public bool OnlySubmerged { get; set; }
		public ucLFNode() : base()
		{
			base.InitializeComponent();
			OnlySubmerged = false;
			NodeTypeName = "LF Base Node";
		}
		public virtual void SetupNodeBindings()
		{
			//string txt = (string)NodeInfo["TechnicalName"];
			//if (NodeInfo["TechnicalName"] != DBNull.Value)
			//{
			//	NodeBinding bind = new NodeBinding();
			//	bind.BoundTo = (string)NodeInfo["TechnicalName"];
			//	bind.BoundToPropertyName = "Text";
			//	bind.BoundToNodeField = titleText;
			//	NodeBindings.Add(bind);
			//}
		}

		public override void AddDefaultPrevLinkers()
		{

			lnkLFLinker ucNodeLinkPrev = new lnkLFLinker();
			ucNodeLinkPrev.SetNode(this);
			ucNodeLinkPrev.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
			ucNodeLinkPrev.LinkerDirection = ucNodeLinker.Direction.DIR_PREV;
			stackPrev.Children.Add(ucNodeLinkPrev);

		}
		public override void AddDefaultNextLinkers()
		{
			//no more default linkers =(
			lnkLFLinker ucNodeLinkNext = new lnkLFLinker();
			ucNodeLinkNext.SetNode(this);
			ucNodeLinkNext.LinkerDirection = ucNodeLinker.Direction.DIR_NEXT;
			stackNext.Children.Add(ucNodeLinkNext);

		}
		//public virtual List<DatatypeDefinition> GenerateDefinitions(DatatypeDefinition def)
		//{
		//	List<DatatypeDefinition> Definitions = new List<DatatypeDefinition>();
		//	//responsibility of the caller to add the root node definition and add the returned definitions
		//	SetNewNodeDefinition(def);

		//	foreach(Type t in LinkerTypes)
		//	{
		//		lnkLFLinker lnk = (lnkLFLinker)Activator.CreateInstance(t);
		//		DatatypeDefinition lnkDef = lnk.GetNewLinkConnectionDefinition();
		//		Definitions.Add(lnkDef);

		//		DatatypeField  df = new DatatypeField();
		//		df.SetDatatype(DataProject.CurrentProject.RegisteredDatatypes.GetDatatype("data.complex.instance"));
		//		df.ComplexTypeReferences.Add(lnkDef.ID);
		//		df.FieldLabel = "Linker Definition - " + lnkDef.DataName;
		//		df.FieldName = "LinkerDef__" + lnk.FQN.Replace(".","_");
		//		def.AddField(Guid.NewGuid(), df);
		//	}
		//	return Definitions;
		//}
		public virtual DatatypeDefinition GetNodeClassExtensionDataDefinition(RegisteredTypes RegTypes)
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.Initialize(RegTypes);
			def.DataName = "BaseNodeExt";
			def.FileName = "";
			//DO NOT GENERATE AN ID FOR THIS DEFINITION
			//this definition must have a static ID so that it can be universal and so it is not duplicated
			//in the registered definitions.
			// DO NOT USE THE EXAMPLE HERE EITHER
			def.ID = Guid.Parse("{56FA83F4-F722-4D74-893E-68DAE6C0341A}");

			//KEEP THIS COLUMN
			DatatypeField df = new DatatypeField();
			df.SetDatatype(RegTypes.GetDatatype("data.long"));
			df.FieldName = "NodeID";
			df.FieldLabel = "NodeID";
			df.IsVisible = false; //no, don't let people edit this.
			//DO NOT CHANGE THIS GUID
			def.AddField(Guid.Parse("{0237772B-4C2D-4351-94E8-3A39DD4F4120}"), df);

			//base class has no extensions, this table is basically empty
			//However!  once created, we can add a field to ALL nodes
			return def;
		}
		public static DatatypeDefinition GenerateDefinition(RegisteredTypes RegTypes)
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.Initialize(RegTypes);
			def.ID = Guid.Parse("{8FAB7F94-2EA0-4D14-B1F0-3E1488DFADD9}");
			def.DataName = "ucLFNodeDef";
			def.PrintableName = "Nodes";
			def.FileName = "./defs/LFNode.def";

			DatatypeField f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
			f.FieldName = "NodeName";
			f.FieldLabel = "Node Name";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{D1C2AEC5-6826-4995-850B-A21D8F02DFAC}"), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.color"));
			f.FieldName = "NodeColor";
			f.FieldLabel = "Node Color";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{B590D245-E398-443A-8EC4-332D3115367A}"), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.double"));
			f.FieldName = "X";
			f.FieldLabel = "X";
			f.IsVisible = true;
			def.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.double"));
			f.FieldName = "Y";
			f.FieldLabel = "Y";
			f.IsVisible = true;
			def.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.double"));
			f.FieldName = "Width";
			f.FieldLabel = "Width";
			f.IsVisible = true;
			def.AddField(Guid.NewGuid(), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.double"));
			f.FieldName = "Height";
			f.FieldLabel = "Height";
			f.IsVisible = true;
			def.AddField(Guid.NewGuid(), f);

			//used for subflows - Parent will be set, and a linker either links to a member of that subflow or the parent
			// if prev is the parent, then it is the starting node, if next is the parent, it is an exit node
			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.reference.single"));
			f.FieldName = "ParentNode";
			f.FieldLabel = "Parent Node";
			f.IsVisible = true;
			//ucLFNode base class
			f.AllowedReferenceDatatypes.Add("99D79D67-4604-40E3-9D44-FCF0E4B94459".ToLower());
			def.AddField(Guid.NewGuid(), f);

			//for associating specific data across all instances of a node type
			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
			f.FieldName = "NodeTypeFQN";
			f.FieldLabel = "Node Type FQN";
			f.IsVisible = false;
			def.AddField(Guid.Parse("41AB8948-ED97-4AF0-92EB-C7BC6E435803"), f);

			//for associating specific data with a specific node instance
			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.reference.single"));
			f.FieldName = "AdditionalData";
			f.FieldLabel = "Additional Data Reference";
			f.IsVisible = true;

			//ucLFNode base class
			f.AllowedReferenceDatatypes.Add(Guid.Empty.ToString());
			def.AddField(Guid.NewGuid(), f);

			return def;
		}
		public Guid ID { get; set; }
		public long RowID { get; set; }
		LCDataRow _NodeInfo;
		LCDataRow NodeInfo
		{
			get
			{
				return _NodeInfo;
			}
			set
			{
				_NodeInfo = value;
			}
		}
		LCDataRow _ExtNodeInfo;
		LCDataRow ExtNodeInfo
		{
			get
			{
				return _ExtNodeInfo;
			}
			set
			{
				_ExtNodeInfo = value;
				//todo raise property changed
			}
		}


		public virtual string GetFQN()
		{
			Type t = this.GetType();
			DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
			return did.FQN;
		}
		public virtual string GetTypeID()
		{
			Type t = this.GetType();
			DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
			return did.ID;
		}
		private void InitializeNodeInfo()
		{
			LCDataRow dr = NodeInfo;

			Binding myBinding = new Binding(); //do we need property path strings?  no path really?
			this.Width = (double)dr["Width"];
			myBinding.Source = dr["Width"];
			//myBinding.Mode = BindingMode.TwoWay;
			//myBinding.Path = new PropertyPath(".");
			myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
			this.SetBinding(WidthProperty, myBinding);
			var descriptor = DependencyPropertyDescriptor.FromProperty(
				WidthProperty, typeof(ucLFNode));
			descriptor.AddValueChanged(this, OnWidthChange);


			this.Height = (double)dr["Height"];
			myBinding = new Binding();
			myBinding.Source = dr["Height"];
			//myBinding.Mode = BindingMode.TwoWay;
			//myBinding.Path = new PropertyPath(".");
			myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
			this.SetBinding(HeightProperty, myBinding);
			descriptor = DependencyPropertyDescriptor.FromProperty(
				HeightProperty, typeof(ucLFNode));
			descriptor.AddValueChanged(this, OnHeightChange);

			this.SetValue(Canvas.LeftProperty, (double)dr["X"]);
			myBinding = new Binding();
			myBinding.Source = dr["X"];
			//myBinding.Mode = BindingMode.TwoWay;
			//myBinding.Path = new PropertyPath(".");
			myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
			this.SetBinding(Canvas.LeftProperty, myBinding);
			 descriptor = DependencyPropertyDescriptor.FromProperty(
				Canvas.LeftProperty, typeof(ucLFNode));
			descriptor.AddValueChanged(this, OnCanvasLeftChanged);


			this.SetValue(Canvas.TopProperty, (double)dr["Y"]);
			myBinding = new Binding();
			myBinding.Source = dr["Y"];
			//myBinding.Mode = BindingMode.TwoWay;
			//myBinding.Path = new PropertyPath(".");
			myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
			this.SetBinding(Canvas.TopProperty, myBinding);
			descriptor = DependencyPropertyDescriptor.FromProperty(
				Canvas.TopProperty, typeof(ucLFNode));
			descriptor.AddValueChanged(this, OnCanvasTopChanged);

			txtTitle.Text = (string)dr["NodeName"];
			myBinding = new Binding("[NodeName]");
			myBinding.Source = dr;
			myBinding.Mode = BindingMode.TwoWay;
			//myBinding.Path = new PropertyPath("NodeName");
			myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
			txtTitle.SetBinding(TextBox.TextProperty, myBinding);

			if (dr["NodeColor"] == DBNull.Value)
				dr["NodeColor"] = string.Format("#{0:X2}{1:X2}{2:X2}{3:X2}",
					NodeColor.A,
					NodeColor.R,
					NodeColor.G,
					NodeColor.B);
			NodeColor = (Color)ColorConverter.ConvertFromString((string)dr["NodeColor"]);
			TypeConverter t = TypeDescriptor.GetConverter(typeof(Color));
			myBinding = new Binding("[NodeColor]");
			myBinding.Source = dr;
			myBinding.Mode = BindingMode.TwoWay;
			myBinding.Converter = new ColorValueConverter();
			//myBinding.Path = new PropertyPath("NodeName");
			myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
			this.SetBinding(NodeColorProperty, myBinding);
			//txtTitle.SetBinding(TextBox.TextProperty, myBinding);

			RowID = (long)dr[0];

		}

		private void OnHeightChange(object sender, EventArgs e)
		{
			LCDataRow dr = NodeInfo;
			dr["Height"] = this.GetValue(ActualHeightProperty);
			MyCanvas.RequestDelayedSave();
			this.Dispatcher.BeginInvoke((Action)this.ReculateLinkPositions, System.Windows.Threading.DispatcherPriority.ContextIdle);
		}

		private void OnWidthChange(object sender, EventArgs e)
		{
			LCDataRow dr = NodeInfo;
			dr["Width"] = this.GetValue(ActualWidthProperty);
			MyCanvas.RequestDelayedSave();
			this.Dispatcher.BeginInvoke((Action)this.ReculateLinkPositions, System.Windows.Threading.DispatcherPriority.ContextIdle);
		}

		public string NodeName
		{
			get
			{
				if (NodeInfo != null)
				{
					return (string)NodeInfo["NodeName"];
				} else
				{
					return "Node";
				}
			}
			set
			{
				NodeInfo["NodeName"] = value;
				OnPropertyChanged("NodeName");
			}
		}
		private void OnCanvasTopChanged(object sender, EventArgs e)
		{
			LCDataRow dr = NodeInfo;
			if (this.RenderTransform != null)
			{
				dr["Y"] = this.RenderTransform.Inverse.Transform(new Point(0, (double)this.GetValue(Canvas.TopProperty))).Y;
			}
			else
			{
				dr["Y"] = this.GetValue(Canvas.TopProperty);
			}
			MyCanvas.RequestDelayedSave();

		}
		


		private void OnCanvasLeftChanged(object sender, EventArgs e)
		{
			LCDataRow dr = NodeInfo;
			if (this.RenderTransform != null)
			{
				dr["X"] = this.RenderTransform.Inverse.Transform(new Point((double)this.GetValue(Canvas.LeftProperty), 0)).X;
			}
			else
			{
				dr["X"] = this.GetValue(Canvas.LeftProperty);
			}
			MyCanvas.RequestDelayedSave();
		}
		public DataInstance Instance { get; set; }
		public virtual void SetDataSource(DataInstance di, LCDataRow BaseNodeInfo, LCDataRow extNodeInfo)
		{
			NodeInfo = BaseNodeInfo;
			InitializeNodeInfo();
			ExtNodeInfo = extNodeInfo;
			Instance = di;
			//don't do anything with ext node info, because.. we dont really care.
		}
		EditorSynchro _edtSync;
		public virtual EditorSynchro EditSync
		{
			get { return _edtSync; }
			set { _edtSync = value; }
		}

	}
}
