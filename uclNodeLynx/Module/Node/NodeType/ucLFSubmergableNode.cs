﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using NodeLynx.CustomData;
using NodeLynx.Node.NodeType;

namespace NodeLynx.Module.Node.NodeType
{
	[DatatypeID(ID = "{A6A0D2CE-8F6B-4F00-905F-331A0E8584D3}", FQN = "node.example.submergable")]
	public class ucLFSubmergableNode : ucLFNode
	{
		public ucLFSubmergableNode() : base()
		{
			ValidChildNodeTypes.Add(typeof(ucLFChildNode));
			NodeColor = Colors.Orange;
			ucNodes.MouseDoubleClick += ucNodes_MouseDoubleClick;
			NodeTypeName = "Submergable";
		}

		void ucNodes_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			//SUBMERGE!
			MyCanvas.Submerge(this);
		}
		public override DatatypeDefinition GetNodeClassExtensionDataDefinition(RegisteredTypes RegTypes)
		{
			DatatypeDefinition def = base.GetNodeClassExtensionDataDefinition(RegTypes);
			def.DataName = "LFSubmergableNodeExt";
			def.FileName = ""; //auto fill this
			def.ID = Guid.Parse("{89F17FB4-E50C-4319-9CEC-24B2DB169C78}");
			return def;
		}
	}
}
