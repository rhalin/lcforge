﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx.CustomData;
using NodeLynx.Module.Node.NodeType;
using NodeLynx.Node;
using NodeLynx.Node.Linker;
using NodeLynx.Node.NodeType;

namespace NodeLynx.Module.Node
{
	[DatatypeID(ID = "{B8E66B49-1CBE-4F5F-810D-2BF48D1B96D6}", FQN = "com.dreamsmithfoundry.storyforge.node")]

	public class NodeViewerModule : DefaultModule
	{
		protected List<ucLFNode> SupportedNodes = new List<ucLFNode>();
		protected string FlowType = "Default";
		public NodeViewerModule()
			: base()
		{
			SetupNodeViewerModule();
		}

		private void SetupNodeViewerModule()
		{
			Editors.Add(typeof(ucNodeCanvas));
			Name = "Node Canvas";
			ucLFNode n = new ucLFNode();
			ucLFChildNode n2 = new ucLFChildNode();
			ucLFSubmergableNode n3 = new ucLFSubmergableNode();
			RegTypes.RegisterNode(n);
			RegTypes.RegisterNode(n2);
			RegTypes.RegisterNode(n3);
			RegTypes.RegisterNodeLinker(new lnkLFLinker());
			SupportedNodes.Add(n);
			SupportedNodes.Add(n2);
			SupportedNodes.Add(n3);
			GenerateFlow();
		}
		protected virtual void GenerateFlow()
		{
			//create node def
			DatatypeDefinition def = ucLFNode.GenerateDefinition(RegTypes);
			Guid NodeID = def.ID;
			Definitions.Add(def);
			//create link def
			def = lnkLFLinker.GetNewLinkConnectionDefinition(RegTypes);
			Guid LinkID = def.ID;
			Definitions.Add(def);
			
			//setup definition
			 def = new DatatypeDefinition();
			 def.Initialize(RegTypes);
			 def.ID = Guid.Parse("{0AE198B8-9C1F-4B74-9232-851DC514AA46}");
			 def.DataName = "Flow";
			 def.PrintableName = "Flow";
			 def.FileName = "./defs/LFNode_Flow.def";

			 DatatypeField df = new DatatypeField();
			 df.SetDatatype(RegTypes.GetDatatype("data.text.short"));
			 df.FieldName = "Name";
			 df.FieldLabel = "Flow Name";
			 df.IsVisible = true;
			 def.AddField(Guid.Parse("{8FE5FA24-19DB-4CC9-9009-C84D7DEA82AC}"), df);

			 df = new DatatypeField();
			 df.SetDatatype(RegTypes.GetDatatype("data.complex.instance"));
			 df.ComplexTypeReferences.Add(NodeID);
			 df.FieldLabel = "Nodes";
			 df.FieldName = "Nodes";
			 def.AddField(Guid.Parse("{8CF812FA-A8C4-4809-B6C3-E6AD6D4DACF0}"), df);

			 df = new DatatypeField();
			 df.SetDatatype(RegTypes.GetDatatype("data.complex.instance"));
			 df.ComplexTypeReferences.Add(LinkID);
			 df.FieldLabel = "Links";
			 df.FieldName = "Links";
			 def.AddField(Guid.Parse("{FC368C7C-8A8B-4AEE-8133-E8A0D189CC5B}"), df);
			
			//add extension definitions
			foreach(ucLFNode node in SupportedNodes)
			{
				DatatypeDefinition ExtDef = node.GetNodeClassExtensionDataDefinition(RegTypes);
				if (ExtDef != null)
				{
					if(ExtDef.FileName == "")
					{
						ExtDef.FileName = "./defs/LFNode_Ext_" + node.GetFQN().Replace(".", "_") + ".def";
					}
					Definitions.Add(ExtDef);
					df = new DatatypeField();
					df.IsVisible = true;
					df.SetDatatype(RegTypes.GetDatatype("data.complex.instance"));
					df.ComplexTypeReferences.Add(ExtDef.ID);
					df.FieldLabel = node.NodeTypeName + " Nodes";
					df.FieldName = "__NODE_" + node.GetFQN().Replace(".", "_");
					//using the type id here assures that the field is tightly bound to a specific nodetype globally
					// and thus tables of the same nodetype can be swapped between projects or flows
					def.AddField(Guid.Parse(node.GetTypeID()), df);
				}
			}

			Definitions.Add(def);
		}
	}
}
