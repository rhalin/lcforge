﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx;
using NodeLynx.CustomData;

namespace NodeLynx.Node
{
	/// <summary>
	/// Interaction logic for ucNode.xaml
	/// </summary>
	public class NodeBinding //: UserControl
	{
		public object BoundTo { get; set; }
		public string BoundToPropertyName { get; set; }
		public string BoundToNodeField { get; set; }

		//public static readonly DependencyProperty BoundToNodeFieldProperty =
		//DependencyProperty.Register("BoundToNodeField", typeof(string), typeof(NodeBinding));
		//public string BoundToNodeField
		//{
		//	set { SetValue(BoundToNodeFieldProperty, value); }
		//	get { return (string)GetValue(BoundToNodeFieldProperty); }
		//}

		//public static readonly DependencyProperty BoundToPropertyNameProperty =
		//DependencyProperty.Register("BoundToPropertyName", typeof(string), typeof(NodeBinding));
		//public string BoundToPropertyName
		//{
		//	set { SetValue(BoundToPropertyNameProperty, value); }
		//	get { return (string)GetValue(BoundToPropertyNameProperty); }
		//}

		//public static readonly DependencyProperty BoundToProperty =
		//DependencyProperty.Register("BoundTo", typeof(object), typeof(NodeBinding));
		//public object BoundTo
		//{
		//	set { SetValue(BoundToProperty, value); }
		//	get { return (object)GetValue(BoundToProperty); }
		//}


	}
	public partial class ucNode : UserControl, INotifyPropertyChanged
	{
		double FirstXPos;
		double FirstYPos;
		ucNode MovingObject;

		public FrameworkElement TopControl;
		public ucNodeCanvas MyCanvas;

		protected bool CanvasSet = false;
		public ScaleTransform st = null;

		public string NodeTypeName { get; set; }

		public static readonly DependencyProperty NodeColorProperty =
			DependencyProperty.Register("NodeColor", typeof(Color), typeof(ucNode));

		public Color NodeColor 
		{
		    set { SetValue(NodeColorProperty, value); }
			get { return  (Color)GetValue(NodeColorProperty);} 
		}

		public static readonly DependencyProperty ResizableProperty =
			DependencyProperty.Register("Resizable", typeof(bool), typeof(ucNode), new UIPropertyMetadata(true));

		public bool Resizable
		{
			set {
				SetValue(ResizableProperty, value); 
				if(value == true)
				{
					thbBottom.Visibility = System.Windows.Visibility.Visible;
					thbRight.Visibility = System.Windows.Visibility.Visible;
					thbBottomRight.Visibility = System.Windows.Visibility.Visible;
				}
				else
				{
					thbBottom.Visibility = System.Windows.Visibility.Collapsed;
					thbRight.Visibility = System.Windows.Visibility.Collapsed;
					thbBottomRight.Visibility = System.Windows.Visibility.Collapsed;
				}
			}
			get { return (bool)GetValue(ResizableProperty); }
		}

		List<NodeBinding> _NodeBindings = new List<NodeBinding>();
		public List<NodeBinding> NodeBindings
		{
			get { return _NodeBindings; }
			private set { _NodeBindings = value; }
		}
		public virtual void DataBind()
		{
			foreach(NodeBinding binding in NodeBindings)
			{
				if(binding.BoundToNodeField == "titleText")
				{
					Binding myBinding = new Binding(binding.BoundToPropertyName);
					myBinding.Source = binding.BoundTo;
					txtTitle.SetBinding(TextBlock.TextProperty, myBinding);
				}
			}
		}
		public int MaxNextLinkers { get; set; }
		public int MaxPrevLinkers { get; set; }
		//can a jump node come back to this?
		public bool CanJumpTo { get; set; }
		public bool Submergable { get; set; }
		public static readonly DependencyProperty SlimNodeTextProperty =
			DependencyProperty.Register("SlimNodeText", typeof(string), typeof(ucNode));
		public string SlimNodeText
		{
			set { SetValue(SlimNodeTextProperty, value); }
			get { return (string)GetValue(SlimNodeTextProperty); }
		}

		//autogenerate a slim node
		ucSlimNode _SlimNode = new ucSlimNode();
		public ucSlimNode SlimNode
		{
			get { return _SlimNode; }
			private set { }
		}
		public ucNode()
		{
			InitializeComponent();
			TopControl = this;
			MaxNextLinkers = 6;
			MaxPrevLinkers = 6;
			NodeColor = Colors.Red;
			NodeBindings.Add(new NodeBinding());
			NodeBindings[0].BoundToNodeField = "titleText";

			//hardwire this for now
			txtTitle.TextChanged += txtTitle_TextChanged;

			//init slim node
			_SlimNode.Node = this;
		}
		static ucNode()
		{
			LinkerTypes.Add(typeof(ucNodeLinker));
		}
		void txtTitle_TextChanged(object sender, TextChangedEventArgs e)
		{
			SlimNodeText = txtTitle.Text;
		}

		public static List<Type> LinkerTypes = new List<Type>();

		public virtual void SetCanvas()
		{
			MyCanvas = ((ucNodeCanvas)((((Canvas)TopControl.Parent).Parent as FrameworkElement).Parent as FrameworkElement));

			/* //no more default linkers =(
			ucNodeLinker ucNodeLinkNext = new ucNodeLinker();
			ucNodeLinkNext.SetNode(this);
			ucNodeLinkNext.LinkerDirection = ucNodeLinker.Direction.DIR_NEXT;
			stackNext.Children.Add(ucNodeLinkNext);

			ucNodeLinker ucNodeLinkPrev = new ucNodeLinker();
			ucNodeLinkPrev.SetNode(this);
			ucNodeLinkPrev.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
			ucNodeLinkPrev.LinkerDirection = ucNodeLinker.Direction.DIR_PREV;
			stackPrev.Children.Add(ucNodeLinkPrev);
			*/
			CanvasSet = true;
		}
		public void RemoveNodeLinker(ucNodeLinker linker)
		{
			if (linker.LinkerDirection == ucNodeLinker.Direction.DIR_NEXT)
			{
				stackNext.Children.Remove(linker);
			}
			else
			{
				stackPrev.Children.Remove(linker);
			}
			this.Dispatcher.BeginInvoke((Action)ReculateLinkPositions);
		}
		//called when we have a canvas
		public virtual void AddDefaultPrevLinkers()
		{
			//should setnode(this);
		}
		public virtual void AddDefaultNextLinkers()
		{
			//should setnode(this);
		}
		public void AddNodeLinker<T>(ucNodeLinker.Direction dir) where T : ucNodeLinker
		{
			int count = 0;
			int max = 6;
			if (dir == ucNodeLinker.Direction.DIR_NEXT)
			{
				count = stackNext.Children.Count;
				max = MaxNextLinkers;
			}
			else
			{
				count = stackPrev.Children.Count;
				max = MaxPrevLinkers;
			}
			//just so we don't have to deal with space, atm
			if (count < max)
			{
				ucNodeLinker NewLink = (ucNodeLinker)Activator.CreateInstance(typeof(T)); //new ucNodeLinker();
				NewLink.Index = -1;
				NewLink.LinkerDirection = dir;
				AddLinkerToNode(NewLink);
			}
		}

		public ucNodeLinker AddLinkerToNode(ucNodeLinker NewLink)
		{
			NewLink.SetNode(this);
			if (NewLink.LinkerDirection == ucNodeLinker.Direction.DIR_NEXT)
			{
				if (NewLink.Index == -1)
				{
					NewLink.Index = stackNext.Children.Count;
					stackNext.Children.Add(NewLink);
				}
				else
				{
					//find its place
					FindLinkerInsertIndex(ref NewLink, stackNext.Children);
				}
			}
			else
			{
				NewLink.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
				if (NewLink.Index == -1)
				{
					NewLink.Index = stackNext.Children.Count;
					stackPrev.Children.Add(NewLink);
				} else
				{
					FindLinkerInsertIndex(ref NewLink, stackPrev.Children);
				}
			}
			//this.Dispatcher.BeginInvoke((Action)ReculateLinkPositions);
			return NewLink;
		}
		//public void CoalesceLinkers(UIElementCollection col)
		//{
		//	Dictionary<int, List<ucNodeLinker>> Linkers = new Dictionary<int, List<ucNodeLinker>>();
		//	int min = int.MaxValue;
		//	int max = int.MinValue;

		//	//gather
		//	for(int i = 0; i < col.Count; i++)
		//	{
		//		ucNodeLinker lnk = (ucNodeLinker)col[i];
		//		if (!Linkers.ContainsKey(lnk.Index))
		//		{
		//			Linkers[lnk.Index] = new List<ucNodeLinker>();
		//		}
		//		Linkers[lnk.Index].Add(lnk);
		//		if (lnk.Index > max)
		//			max = lnk.Index;
		//		if (lnk.Index < min)
		//			min = lnk.Index;
		//	}
		//	List<ucNodeLinker> FinalLinkers = new List<ucNodeLinker>();
		//	for(int i = min; i <= max; i++)
		//	{
		//		if (Linkers.ContainsKey(i))
		//		{
		//			List<ucNodeLinker> lnks = Linkers[i];
		//			if(lnks.Count == 1)
		//			{
		//				FinalLinkers.Add(lnks[0]); // don't do anything, this is ok.
		//			} else
		//			{
		//				//compact this index
		//				while(lnks.Count > 1)
		//				{
		//					ucNodeLinker lastLnk = lnks[lnks.Count - 1];
		//					List<ucNodeLinkLine> lines = MyCanvas.LineManager.GetAttachedLines(lastLnk);
		//					foreach(ucNodeLinkLine line in lines)
		//					{
		//						if(line.StartLinker == lastLnk)
		//						{
		//							line.StartLinker = lnks[0];
		//						}
		//						if (line.EndLinker == lastLnk)
		//						{
		//							line.EndLinker = lnks[0];
		//						}
		//					}
		//					lnks.Remove(lastLnk);
		//				}
		//				//add to the final list
		//				FinalLinkers.Add(lnks[0]);
		//			}
		//		}
		//	}
		//	//re-add
		//	col.Clear();
		//	for (int i = 0; i < FinalLinkers.Count; i++)
		//	{
		//		//reindex
		//		FinalLinkers[i].Index = i;
		//		col.Add(FinalLinkers[i]);
		//	}
		//	this.Dispatcher.BeginInvoke((Action)ReculateLinkPositions);
		//}
		private void FindLinkerInsertIndex(ref ucNodeLinker NewLink, UIElementCollection col)
		{
			bool found = false;
			for (int i = 0; i < col.Count && !found; i++)
			{
				if (i == 0 && ((ucNodeLinker)col[i]).Index > NewLink.Index)
				{
					col.Insert(0, NewLink);
					found = true;
				}
				else if (((ucNodeLinker)col[i]).Index == NewLink.Index && (((ucNodeLinker)col[i]).GetType() == NewLink.GetType()))
				{
					NewLink = (ucNodeLinker)col[i];
					found = true;
				} else
					if (i > 0 && (((ucNodeLinker)col[i]).Index >= NewLink.Index && ((ucNodeLinker)col[i - 1]).Index < NewLink.Index))
				{
					if ((((ucNodeLinker)col[i]).GetType() == NewLink.GetType()) && ((ucNodeLinker)col[i]).Index == NewLink.Index)
					{
						NewLink = (ucNodeLinker)col[i]; //reuse linker if type and index are the same
					}
					else
					{
						col.Insert(i, NewLink);
					}
					found = true;
				}
			}
			if (!found)
				col.Add(NewLink);
		}

		public void ReculateLinkPositions()
		{
			foreach (ucNodeLinker nodeLinker in stackNext.Children)
			{
				MyCanvas.LineManager.ReculateLinkLines(nodeLinker);
			}
			foreach (ucNodeLinker nodeLinker in stackPrev.Children)
			{
				MyCanvas.LineManager.ReculateLinkLines(nodeLinker);
			}
		}



		private void border1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			BeginDragNode(e);
		}

		private void BeginDragNode(MouseButtonEventArgs e)
		{
			this.Focus();
			Keyboard.Focus(this);
			MyCanvas.SelectedNode = this;
			//In this event, we get current mouse position on the control to use it in the MouseMove event.
			Point pnt = e.GetPosition(TopControl);
			pnt = this.RenderTransform.Transform(pnt);

			FirstXPos = pnt.X;
			FirstYPos = pnt.Y;

			MovingObject = (ucNode)TopControl;
			MyCanvas.movingNode = MovingObject;
			if (MyCanvas.FrontNode != null)
			{
				Canvas.SetZIndex(MyCanvas.FrontNode, 1);
			}
			MyCanvas.FrontNode = this;
			Canvas.SetZIndex(MyCanvas.FrontNode, 2);
		}

		private void border1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			DropNode(e);
		}

		private void DropNode(MouseButtonEventArgs e)
		{
			if (e.LeftButton != MouseButtonState.Pressed)
			{
				MovingObject = null;
				MyCanvas.movingNode = null;
			}
			else
			{
				MoveNode(e);
			}
		}

		private void border1_MouseMove(object sender, MouseEventArgs e)
		{
			MoveNode(e);
		}

		public void MoveNode(MouseEventArgs e)
		{
			if (e.LeftButton == MouseButtonState.Pressed &&
				MovingObject == TopControl)
			{

				Point pnt = e.GetPosition(TopControl.Parent as FrameworkElement);

				(TopControl).SetValue(Canvas.LeftProperty,
					 pnt.X - FirstXPos - TopControl.Margin.Left);//-20

				(TopControl).SetValue(Canvas.TopProperty,
					 pnt.Y - FirstYPos - TopControl.Margin.Top); //-20

				foreach (ucNodeLinker nodeLinker in stackNext.Children)
				{
					MyCanvas.LineManager.ReculateLinkLines(nodeLinker);

				}
				foreach (ucNodeLinker nodeLinker in stackPrev.Children)
				{
					MyCanvas.LineManager.ReculateLinkLines(nodeLinker);
				}
				OnPropertyChanged("Canvas.Top");
				OnPropertyChanged("Canvas.Left");
				MyCanvas.nodeCanvas.InvalidateMeasure();
				this.Focus();
				Keyboard.Focus(this);
				MyCanvas.SelectedNode = this;

			}
		}

		private void stackNext_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			foreach (ucNodeLinker nodeLinker in stackNext.Children)
			{
				MyCanvas.LineManager.ReculateLinkLines(nodeLinker);
			}
		}

		private void stackPrev_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			foreach (ucNodeLinker nodeLinker in stackPrev.Children)
			{
				MyCanvas.LineManager.ReculateLinkLines(nodeLinker);
			}
		}

		private void border2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			BeginDragNode(e);
		}

		private void brdNodeTopChrome_MouseMove(object sender, MouseEventArgs e)
		{
			MoveNode(e);
		}

		private void brdNodeTopChrome_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			DropNode(e);
		}
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void ucNodes_GotFocus(object sender, RoutedEventArgs e)
		{
			brdMain.BorderBrush = new SolidColorBrush(Colors.Orange);
			MyCanvas.SelectedNode = this;
		}

		private void ucNodes_LostFocus(object sender, RoutedEventArgs e)
		{
			brdMain.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF1E4F90"));
		}

		public static readonly DependencyProperty NodeBorderBrushProperty =
		DependencyProperty.Register("NodeBorderBrush", typeof(Brush), typeof(ucNode));
		public Brush NodeBorderBrush
		{
			set { SetValue(NodeBorderBrushProperty, value); }
			get { return (Brush)GetValue(NodeBorderBrushProperty); }
		}

		private void thbBottom_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{
			if(e.VerticalChange != 0)
			{
				if (this.Height + e.VerticalChange > this.MinHeight)
				{
					this.Height += e.VerticalChange;
					OnPropertyChanged("Height");

				}
			}
		}

		//private void thbLeft_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		//{
		//	if (e.HorizontalChange != 0)
		//	{
		//		if (this.Width + e.HorizontalChange > this.MinWidth)
		//		{
		//			this.Width += e.HorizontalChange;
		//			Console.WriteLine(Canvas.GetLeft(this) - e.HorizontalChange);
		//			//Canvas.SetLeft(this, Canvas.GetLeft(this) - e.HorizontalChange);
		//		}
		//	}
		//}

		private void thbRight_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{
			if (e.HorizontalChange != 0)
			{
				if (this.Width + e.HorizontalChange > this.MinWidth)
				{
					this.Width += e.HorizontalChange;
					OnPropertyChanged("Width");
				}
			}
		}

		private void thbBottomRight_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{
			if (e.VerticalChange != 0)
			{
				if (this.Height + e.VerticalChange > this.MinHeight)
				{
					this.Height += e.VerticalChange;
					OnPropertyChanged("Height");

				}
			}
			if (e.HorizontalChange != 0)
			{
				if (this.Width + e.HorizontalChange > this.MinWidth)
				{
					this.Width += e.HorizontalChange;
					OnPropertyChanged("Width");
				}
			}
		}

	}
}
