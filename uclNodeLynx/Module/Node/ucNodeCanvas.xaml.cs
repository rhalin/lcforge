﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using NodeLynx;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.DragAndDrop;
using NodeLynx.lib;
using NodeLynx.Module.Node;
using NodeLynx.Node.Linker;
using NodeLynx.Node.NodeType;
using NodeLynx.UI;
using Xceed.Wpf.Toolkit;

namespace NodeLynx.Node
{
	/// <summary>
	/// Interaction logic for ucNodeCanvas.xaml
	/// </summary>
	public partial class ucNodeCanvas : UserControl, INLTabClient, INotifyPropertyChanged
	{
		public ucNodeLinkLine currentLine;
		public ucNode FrontNode = null;
		public ucNode movingNode = null;
		public DragDropManager ddManager;
		bool MovingCanvas = false;
		public NodeCanvasLineManager LineManager { get; set; }
		public Dictionary<long, ucLFNode> Nodes = new Dictionary<long, ucLFNode>();
		public void SetLink(bool NewLine=true)
		{
			if (NewLine)
			{
				LCDataRow row = LinkSync.Instance.NewRow();

				row["PrevIndex"] = LineManager.LastLinkLine.StartLinker.Index;
				row["NextIndex"] = LineManager.LastLinkLine.EndLinker.Index;

				List<SerializableKeyValuePair<string, long>> nodeRef = new List<SerializableKeyValuePair<string, long>>();
				nodeRef.Add(new SerializableKeyValuePair<string, long>());
				nodeRef[0].Key = ((ucLFNode)LineManager.LastLinkLine.StartLinker.MyNode).Instance.ID.ToString();
				nodeRef[0].Value = ((ucLFNode)LineManager.LastLinkLine.StartLinker.MyNode).RowID;
				row["PrevNode_ref"] = nodeRef;

				nodeRef = new List<SerializableKeyValuePair<string, long>>();
				nodeRef.Add(new SerializableKeyValuePair<string, long>());
				nodeRef[0].Key = ((ucLFNode)LineManager.LastLinkLine.EndLinker.MyNode).Instance.ID.ToString();
				nodeRef[0].Value = ((ucLFNode)LineManager.LastLinkLine.EndLinker.MyNode).RowID;
				row["NextNode_ref"] = nodeRef;

				row["PrevLinkerTypeFQN"] = ((lnkLFLinker)LineManager.LastLinkLine.StartLinker).FQN;
				row["NextLinkerTypeFQN"] = ((lnkLFLinker)LineManager.LastLinkLine.EndLinker).FQN;

				LinkSync.Instance.AddRow(row);
				//RequestDelayedSave();
			}
			currentLine = null;
		}
		DispatcherTimer DelaySaveTimer = new DispatcherTimer();
		public ucNodeCanvas()
		{
			InitializeComponent();
			DelaySaveTimer.Tick += DelaySaveTimer_Tick;
			DelaySaveTimer.Interval = new TimeSpan(100000000L); //10s ?
			
			LineManager = new NodeCanvasLineManager(this);
			//nodeCanvas.Children.Add(ifelse);
			//ifelse.SetCanvas();
			Node1.Width = 500;
			Node1.SetCanvas();
			Node2.SetCanvas();
			Node3.SetCanvas();
			
			UpdateCanvas();

		}
		public void RequestDelayedSave()
		{
			if (!DelaySaveTimer.IsEnabled)
			{
				DelaySaveTimer.Start();
			}
		}
		private void _DelayedSave()
		{
			DelaySaveTimer.Stop();
			this.Dispatcher.BeginInvoke((Action)((ucNodeCanvas)this).SaveFlowData, System.Windows.Threading.DispatcherPriority.ContextIdle);
		}
		void DelaySaveTimer_Tick(object sender, EventArgs e)
		{
			_DelayedSave();
		}
		public void Initialize()
		{
			nodeIfThenElse ifelse = new nodeIfThenElse();
			AddNode(ifelse);
			nodeDropTarget droptarg = new nodeDropTarget();
			AddNode(droptarg);
			Canvas.SetLeft(droptarg, 120);
			Canvas.SetTop(droptarg, 310);
		}
		public void CenterOn(Point pnt)
		{
			double w = nodeCanvas.ActualWidth;
			double h = nodeCanvas.ActualHeight;
			double screenCenterX = w / 2;
			double screenCenterY = h / 2;

			double offsetX = pnt.X - screenCenterX;
			double offsetY = pnt.Y - screenCenterY;
			Point pnt2 = new Point(offsetX, offsetY);

			foreach (FrameworkElement element in nodeCanvas.Children)
			{

				if (element is ucNode || element is ucNodeSubmergedView)
				{
					Point transPnt = element.RenderTransform.Transform(pnt2);
					//Matrix m = element.RenderTransform.Value;
					Console.WriteLine((Canvas.GetLeft(element) - transPnt.X) + "," + (Canvas.GetTop(element) - transPnt.Y));
					Canvas.SetLeft(element, Canvas.GetLeft(element) - transPnt.X);
					Canvas.SetTop(element, Canvas.GetTop(element) - transPnt.Y);

					//element.RenderTransform = new MatrixTransform(m);
					if (element is ucNode)
						((ucNode)element).Dispatcher.BeginInvoke((Action)((ucNode)element).ReculateLinkPositions, System.Windows.Threading.DispatcherPriority.ContextIdle);

				}
			}
		}
		public void SetTopLeft(Point pnt)
		{
			double w = nodeCanvas.ActualWidth;
			double h = nodeCanvas.ActualHeight;
			//double screenCenterX = w / 2;
			//double screenCenterY = h / 2;

			double offsetX = pnt.X;// -w;
			double offsetY = pnt.Y;// -h;
			Point pnt2 = new Point(offsetX, offsetY);

			foreach (FrameworkElement element in nodeCanvas.Children)
			{

				if (element is ucNode || element is ucNodeSubmergedView)
				{
					Point transPnt = element.RenderTransform.Transform(pnt2);
					//Matrix m = element.RenderTransform.Value;
					Console.WriteLine((Canvas.GetLeft(element) - transPnt.X) + "," + (Canvas.GetTop(element) - transPnt.Y));
					Canvas.SetLeft(element, Canvas.GetLeft(element) - transPnt.X);
					Canvas.SetTop(element, Canvas.GetTop(element) - transPnt.Y);

					//element.RenderTransform = new MatrixTransform(m);
					if (element is ucNode)
						((ucNode)element).Dispatcher.BeginInvoke((Action)((ucNode)element).ReculateLinkPositions, System.Windows.Threading.DispatcherPriority.ContextIdle);

				}
			}
		}
		public void AddNode(ucNode node)
		{
			nodeCanvas.Children.Add(node);
			node.SetCanvas();
			//node.AddDefaultLinkers();
			
			if(node is IDataDropTarget)
			{
				((IDataDropTarget)node).SetDragDropManager(ddManager);
				//ddManager.RegisterDropTarget((IDataDropTarget)node);
			}
			UpdateCanvas();
		}
		public void SetLine(ucNodeLinkLine line)
		{
			currentLine = line;
			nodeCanvas.Children.Add(line.line);
			Canvas.SetZIndex(line.line, -1);
		}

		private void nodeCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			Console.WriteLine("mouse up");
			MovingCanvas = false;
			if (currentLine != null)
			{
				DropCurrentLine();
			}
		}

		private void DropCurrentLine()
		{
			if (currentLine.StartLinker.ConnectedLines.Count > 0)
			{
				currentLine.StartLinker.brdLinker.Style = currentLine.StartLinker.ConnectedStyle;
			}
			else
			{
				currentLine.StartLinker.brdLinker.Style = currentLine.StartLinker.BaseStyle;
			}

			//currentLine.StartLinker.brdLinker.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
			nodeCanvas.Children.Remove(currentLine.line);
			currentLine = null;
		}
		public void UpdateCanvas()
		{/*
			foreach(UIElement child in nodeCanvas.Children)
			{
				if(child is ucNode)
				{
					Point pnt = this.TranslatePoint
					nodeCanvas.Width = nodeCanvas.Width > ((ucNode)child).Width + ((ucNode)child). ? nodeCanvas.Width :((ucNode)child).Width + ((ucNode)child).Margin.Left;
					nodeCanvas.Height = nodeCanvas.Height > ((ucNode)child).Height + ((ucNode)child).Margin.Top ? nodeCanvas.Height :((ucNode)child).Height + ((ucNode)child).Margin.Top;
					//nodeCanvas.Height = this.Height;
				}
			}*/
		}
		private void nodeCanvas_MouseMove(object sender, MouseEventArgs e)
		{
			if(e.LeftButton == MouseButtonState.Released)
			{
				if(currentLine != null)
					DropCurrentLine();
				MovingCanvas = false;
			}
			if (currentLine != null)
			{
				currentLine.line.X1 = e.GetPosition(this).X - 1;
				currentLine.line.Y1 = e.GetPosition(this).Y - 1;
			} else if(movingNode != null)
			{
				movingNode.MoveNode(e);
			} else if(MovingCanvas == true)
			{
				Point newPos = e.GetPosition(this);
				Point diff = new Point(startPoint.X - newPos.X, startPoint.Y - newPos.Y);
				foreach (FrameworkElement element in nodeCanvas.Children)
				{
					Point p = e.MouseDevice.GetPosition(element);
					Matrix m = element.RenderTransform.Value;
					m.Translate(diff.X, diff.Y);
					Canvas.SetLeft(element, Canvas.GetLeft(element) - diff.X);
					Canvas.SetTop(element, Canvas.GetTop(element) - diff.Y);

						//m.Translate(-m.OffsetX, -m.OffsetY);
						//element.RenderTransform = new MatrixTransform(m);
					if (element is ucNode)
					{
						((ucNode)element).Dispatcher.BeginInvoke((Action)((ucNode)element).ReculateLinkPositions, System.Windows.Threading.DispatcherPriority.ContextIdle);
					}
				}
				startPoint = newPos;
			}
		}

		private void scrollViewer1_SizeChanged(object sender, SizeChangedEventArgs e)
		{

		}

		private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			//maximize / minimize and the first draw events are funny
			// they don't have layouts calculated yet, so we can't actually handle these measurements.
			// instead, an async invoke to invalidate will wait till the next event cycle, when all these values
			// are available
			this.Dispatcher.BeginInvoke((Action)nodeCanvas.InvalidateMeasure);
		}

		public UserControl GetControl()
		{
			return this;
		}

		public string GetTabName()
		{
			return "Node Canvas";
		}

		public void SaveFlowData()
		{
			edtSync.Instance.SaveInstance(DataProject.CurrentProject.FileName);
			LinkSync.Instance.SaveInstance(DataProject.CurrentProject.FileName);
			NodeSync.Instance.SaveInstance(DataProject.CurrentProject.FileName);
			foreach(KeyValuePair<string,EditorSynchro> sync in NodeExtSync)
			{
				sync.Value.Instance.SaveInstance(DataProject.CurrentProject.FileName);
			}
		}
		public void OnParentSizeChanged(object sender, SizeChangedEventArgs e)
		{
			//UserControl_SizeChanged(sender, e);
		}


		public void SetDragDropManager(DragDropManager dragdropManager)
		{
			ddManager = dragdropManager;
		}


		public List<DatatypeDefinition> SupportedEditorFor()
		{
			List<DatatypeDefinition> defs = new List<DatatypeDefinition>();
			defs.Add(DataProject.CurrentProject.GetDefinition("0AE198B8-9C1F-4B74-9232-851DC514AA46".ToLower()));
			return defs;
		}
		DatatypeDefinition Definition;
		DataInstance Instance;
		long Row;
		public FrameworkElement SetDataSource(DatatypeDefinition def, DataInstance di = null, long RowID = -1)
		{
			Definition = def;
			Instance = di;
			Row = RowID;
			if(Row != -1)
			{
				edtSync.GotoRowID(Row);
				LoadFlow();
				if(Nodes.ContainsKey(Row))
				{
					Nodes[Row].Focus();
					Keyboard.Focus(Nodes[Row]);
					SelectedNode = Nodes[Row];
				}
			} else
			{
				Console.WriteLine("Node canvas requires a flow!");
			}
			
			return this;
		}
		ucLFNode SubmergedInThisNode = null;
		Stack<ucLFNode> SubmergedNodes = new Stack<ucLFNode>();
		private void LoadFlow()
		{
			nodeCanvas.Children.Clear();
			Nodes.Clear();
			if (CurrentRow != null)
			{
				InitializeFlowData();
				//all initialized

				//load some nodes!
				LoadNodes();

				//Load links
				LoadLinks();

				//re-iterate through all the nodes, and re-arrange to match the Index they're assigned
				//once linkers are added, we need to call 
				
				//generate UI

			}
		}

		private void LoadLinks()
		{
			LinkSync.GotoRow(0);
			LineManager.Reset();
			while (LinkSync.CurrentRow != null)
			{
				//NOTE - nodes will have default linkers added by this point!
				// should they?
				// maybe we only add them on totally new nodes.				

				//get prev
				List<SerializableKeyValuePair<string, long>> PrevNodeRef = (List<SerializableKeyValuePair<string, long>>)LinkSync.CurrentRow["PrevNode_ref"];
				List<SerializableKeyValuePair<string, long>> NextNodeRef = (List<SerializableKeyValuePair<string, long>>)LinkSync.CurrentRow["NextNode_ref"];
				//get the node, if its viewable
				if (Nodes.ContainsKey(PrevNodeRef[0].Value) && Nodes.ContainsKey(NextNodeRef[0].Value))
				{
					int PrevIndex = (int)LinkSync.CurrentRow["PrevIndex"];
					ucLFNode PrevNode = Nodes[PrevNodeRef[0].Value];
					string PrevFQN = (string)LinkSync.CurrentRow["PrevLinkerTypeFQN"];
					ucNodeLinker prevLinker = DataProject.CurrentProject.RegisteredDatatypes.GetNodeLinker(PrevFQN);
					prevLinker = (lnkLFLinker)Activator.CreateInstance(prevLinker.GetType());
					prevLinker.Index = PrevIndex;
					//these are reversed, since in the context of the line, the "prev" node would be one pointing "next"
					prevLinker.LinkerDirection = ucNodeLinker.Direction.DIR_NEXT;
					prevLinker = PrevNode.AddLinkerToNode(prevLinker);

					//get next
					int NextIndex = (int)LinkSync.CurrentRow["NextIndex"];
					ucLFNode NextNode = Nodes[NextNodeRef[0].Value];
					string NextFQN = (string)LinkSync.CurrentRow["NextLinkerTypeFQN"];
					ucNodeLinker nextLinker = DataProject.CurrentProject.RegisteredDatatypes.GetNodeLinker(NextFQN);
					nextLinker = (lnkLFLinker)Activator.CreateInstance(nextLinker.GetType());
					nextLinker.Index = NextIndex;
					nextLinker.LinkerDirection = ucNodeLinker.Direction.DIR_PREV;
					//we need a version of AddNodeLinker that doesn't override index
					nextLinker = NextNode.AddLinkerToNode(nextLinker);

					//both linkers added, add the line
					LineManager.NewLine(prevLinker);
					LineManager.DropCurrentLine(nextLinker, false);
					prevLinker.InitStyle();
					nextLinker.InitStyle();
				}
				LinkSync.NextRow();
			}
			foreach(KeyValuePair<long, ucLFNode> node in Nodes)
			{
				//node.Value.CoalesceLinkers(node.Value.stackNext.Children);
				//node.Value.CoalesceLinkers(node.Value.stackPrev.Children);
				if(node.Value.stackPrev.Children.Count == 0)
				{
					node.Value.AddDefaultPrevLinkers();
				}
				if (node.Value.stackNext.Children.Count == 0)
				{
					node.Value.AddDefaultNextLinkers();
				}
			}
		}
		double minX;
		double minY;
		double maxX;
		double maxY;

		private void LoadNodes()
		{
			minX = double.MaxValue;
			minY = double.MaxValue;
			maxX = double.MinValue;
			maxY = double.MinValue;
			NodeSync.GotoRow(0);
			//we have at least one row, set by callback
			while (NodeSync.CurrentRow != null)
			{
				List<SerializableKeyValuePair<string, long>> ParentNode = null;
				if (NodeSync.CurrentRow["ParentNode_ref"] != DBNull.Value)
					ParentNode = (List<SerializableKeyValuePair<string, long>>)NodeSync.CurrentRow["ParentNode_ref"];
				if (ParentNode == null && SubmergedInThisNode == null || (SubmergedInThisNode == null && ParentNode.Count == 0))
				{
					//go get the ext data if we can find it
					//get the ext sync for this node type
					string NodeExtFQN = (string)NodeSync.CurrentRow["NodeTypeFQN"];
					//search by the current node id in that data
					NodeExtSync[NodeExtFQN].RowSearch((long)NodeSync.CurrentRow[0], "NodeID");
					LCDataRow nodeExtRow = NodeExtSync[NodeExtFQN].CurrentRow;

					ucLFNode nodeTemplate = DataProject.CurrentProject.RegisteredDatatypes.GetNode((string)NodeSync.CurrentRow["NodeTypeFQN"]);
					nodeTemplate = (ucLFNode)Activator.CreateInstance(nodeTemplate.GetType());
					nodeTemplate.SetDataSource(NodeSync.Instance, NodeSync.CurrentRow, nodeExtRow);
					Console.WriteLine(NodeSync.CurrentRow["RowID"] + "<-" +nodeExtRow["NodeID"] + " ->" + nodeExtRow["RowID"]);
					AddNode(nodeTemplate);
					nodeTemplate.AddDefaultPrevLinkers();
					nodeTemplate.AddDefaultNextLinkers();
					Nodes[nodeTemplate.RowID] = nodeTemplate;
					nodeTemplate.SetupNodeBindings();
					nodeTemplate.DataBind();

					if (Canvas.GetTop(nodeTemplate) < minY)
						minY = Canvas.GetTop(nodeTemplate);
					if (Canvas.GetTop(nodeTemplate) + nodeTemplate.Height > maxY)
						maxY = Canvas.GetTop(nodeTemplate) + nodeTemplate.Height;
					if (Canvas.GetLeft(nodeTemplate) < minX)
						minX = Canvas.GetLeft(nodeTemplate);
					if (Canvas.GetLeft(nodeTemplate) + nodeTemplate.Width > maxX)
						maxX = Canvas.GetLeft(nodeTemplate) + nodeTemplate.Width;

				}
				else if (SubmergedInThisNode != null 
				  && ParentNode != null // has a parent
				  && ParentNode.Count == 1  //we are submerged, and we do have a parent in this node
				  && SubmergedInThisNode.RowID == ParentNode[0].Value // same rowid!
				  && SubmergedInThisNode.Instance.ID.ToString().ToLower() == ParentNode[0].Key.ToString().ToLower()) // same instance id!
				{
					//todo: implement submersion
					//go get the ext data if we can find it
					//get the ext sync for this node type
					string NodeExtFQN = (string)NodeSync.CurrentRow["NodeTypeFQN"];
					//search by the current node id in that data
					NodeExtSync[NodeExtFQN].RowSearch((long)NodeSync.CurrentRow[0], "NodeID");
					LCDataRow nodeExtRow = NodeExtSync[NodeExtFQN].CurrentRow;

					ucLFNode nodeTemplate = DataProject.CurrentProject.RegisteredDatatypes.GetNode((string)NodeSync.CurrentRow["NodeTypeFQN"]);
					nodeTemplate = (ucLFNode)Activator.CreateInstance(nodeTemplate.GetType());
					nodeTemplate.SetDataSource(NodeSync.Instance, NodeSync.CurrentRow, nodeExtRow);
					AddNode(nodeTemplate);
					nodeTemplate.AddDefaultPrevLinkers();
					nodeTemplate.AddDefaultNextLinkers();
					Nodes[nodeTemplate.RowID] = nodeTemplate;
					nodeTemplate.SetupNodeBindings();
					nodeTemplate.DataBind();
					nodeTemplate.PropertyChanged +=submergedNode_PropertyChanged;

					if (Canvas.GetTop(nodeTemplate) < minY)
						minY = Canvas.GetTop(nodeTemplate);
					if (Canvas.GetTop(nodeTemplate) + nodeTemplate.Height > maxY)
						maxY = Canvas.GetTop(nodeTemplate) + nodeTemplate.Height;
					if (Canvas.GetLeft(nodeTemplate) < minX)
						minX = Canvas.GetLeft(nodeTemplate);
					if (Canvas.GetLeft(nodeTemplate) + nodeTemplate.Width > maxX)
						maxX = Canvas.GetLeft(nodeTemplate) + nodeTemplate.Width;
				}
				NodeSync.NextRow();
			}
			if (SubmergedInThisNode == null)
			{
				double centX = (maxX + minX) / 2;
				double centY = (maxY + minY) / 2;
				CenterOn(new Point(centX, centY));
			}
		}

		private void submergedNode_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if(SubmergedInThisNode != null)
			{
				if(e.PropertyName == "Canvas.Top" || e.PropertyName == "Canvas.Left")
				{
					ucNode s = (ucNode)sender;
					double left = Canvas.GetLeft(s);
					double top = Canvas.GetTop(s);
					double right = left + s.RenderTransform.Transform(new Point(s.ActualWidth, s.ActualHeight)).X;
					double bottom = top + s.RenderTransform.Transform(new Point(s.ActualWidth, s.ActualHeight)).Y;

					double currMinX = Canvas.GetLeft(SubmergedView);
					double currMinY = Canvas.GetTop(SubmergedView);
					double currMaxX = currMinX + SubmergedView.RenderTransform.Transform(new Point(SubmergedView.ActualWidth, SubmergedView.ActualHeight)).X;//Canvas.GetRight(SubmergedView);
					double currMaxY = currMinY + SubmergedView.RenderTransform.Transform(new Point(SubmergedView.ActualWidth, SubmergedView.ActualHeight)).Y;
					Point MinPnt = new Point(50, 50);
					Point MaxPnt = new Point(currMinX, currMaxY);
					//MinPnt = SubmergedView.RenderTransform.Inverse.Transform(MinPnt);
					MinPnt = SubmergedView.RenderTransform.Transform(MinPnt);
					double Scale = MinPnt.X;

					if(left - Scale < currMinX)
					{
						double diff = currMinX - (left - Scale);
						Canvas.SetLeft(SubmergedView, (left - Scale)); //SubmergedView.RenderTransform.Transform(new Point((left - 50), 0)).X);
						MinPnt.X = left - Scale;
						SubmergedView.Width += diff * 2;
					}
					if (top - Scale < currMinY)
					{
						double diff = currMinY - (top - Scale);
						Canvas.SetTop(SubmergedView, top - Scale);
						SubmergedView.Height += diff * 2;
					}
					if (right + Scale > currMaxX)
					{
						double diff = (right + Scale) - currMaxX;
						Point pnt = new Point(diff, diff);
						pnt = SubmergedView.RenderTransform.Transform(pnt);
						SubmergedView.Width += pnt.X;//(right + Scale) - Canvas.GetRight(SubmergedView);
					}
					if (bottom + Scale > currMaxY)
					{
						double diff = (bottom + Scale) - currMaxY;
						Point pnt = new Point(diff, diff);
						pnt = SubmergedView.RenderTransform.Transform(pnt);
						SubmergedView.Height += pnt.Y;//(right + Scale) - Canvas.GetRight(SubmergedView);
						//SubmergedView.Height += (bottom + Scale) - currMaxY;//Canvas.GetTop(SubmergedView); ;
					}
				}
			}
		}
		public Dictionary<string, ucLFNode> FlowSupportedNodes { get; set; }
		public List<string> FlowSupportedNodesFQNs
		{
			get;
			set;
		}
		private void InitializeFlowData()
		{
			FlowSupportedNodes = new Dictionary<string, ucLFNode>();
			FlowSupportedNodesFQNs = new List<string>();
			//this canvas is bound to a flow instance, with many nodes.
			DataInstance nodeInstance = DataProject.CurrentProject.GetInstance((string)CurrentRow["Nodes"]);
			NodeSync = new EditorSynchro(nodeInstance, edtSync.ContentCanvas, edtSync.CurrentWindow);

			DataInstance linkInstance = DataProject.CurrentProject.GetInstance((string)CurrentRow["Links"]);
			LinkSync = new EditorSynchro(linkInstance, edtSync.ContentCanvas, edtSync.CurrentWindow);

			//we should generate synchros to get around the other instances
			//get all known node types
			Dictionary<string, ucLFNode> nodeTypes = DataProject.CurrentProject.RegisteredDatatypes.GetNodeTypes();

			//check row to see if appropriate field name exists
			foreach (KeyValuePair<string, ucLFNode> item in nodeTypes)
			{
				string name = "__NODE_" + item.Key.Replace(".", "_");
				if (CurrentRow.Table.Columns.Contains(name))
				{
					//success!
					FlowSupportedNodes[item.Key] = item.Value;
					FlowSupportedNodesFQNs.Add(item.Key);
					// go get that data
					DataInstance extNodeInstance = DataProject.CurrentProject.GetInstance((string)CurrentRow[name]);
					EditorSynchro extSync = new EditorSynchro(extNodeInstance, edtSync.ContentCanvas, edtSync.CurrentWindow);
					NodeExtSync[item.Key] = extSync;
				}
			}
		}
		EditorSynchro NodeSync;
		EditorSynchro LinkSync;
		//nodetype_fqn -> synchro
		Dictionary<string, EditorSynchro> NodeExtSync = new Dictionary<string, EditorSynchro>();

		public DatatypeDefinition GetDatatypeDefinition()
		{
			return Definition;
		}


		public string GetButtonName()
		{
			return "Node Graph";
		}


		public DataInstance GetDataInstance()
		{
			return Instance;
		}

		public long GetRowID()
		{
			return Row;
		}
		public void TabShown()
		{

		}
		LCDataRow CurrentRow;
		public void SetEditorRow(LCDataRow dr, long RowID)
		{
			CurrentRow = dr;
			Row = RowID;
			if (CurrentRow != null)
			{
				LoadFlow();
			}
		}
		EditorSynchro edtSync;
		public void SetEditorSynchro(EditorSynchro sync)
		{
			edtSync = sync;
			sync.AddEditor(this);
		}
		public EditorSynchro GetEditorSynchro()
		{
			return edtSync;
		}
		Matrix lastMatrix = Matrix.Identity;
		private void nodeCanvas_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			foreach (FrameworkElement element in nodeCanvas.Children)
			{

				if (element is ucNode || element is ucNodeSubmergedView)
				{
					Point p = e.MouseDevice.GetPosition(element);
					Matrix m = element.RenderTransform.Value;
					if (e.Delta > 0)
						m.ScaleAtPrepend(1.05, 1.05, p.X, p.Y);
					else
						m.ScaleAtPrepend(1 / 1.05, 1 / 1.05, p.X, p.Y);
					lastMatrix = new Matrix(m.M11, m.M12, m.M21, m.M22, m.OffsetX, m.OffsetY);
					Canvas.SetLeft(element, Canvas.GetLeft(element) + m.OffsetX);
					Canvas.SetTop(element, Canvas.GetTop(element) + m.OffsetY);
					m.Translate(-m.OffsetX, -m.OffsetY);
					element.RenderTransform = new MatrixTransform(m);
					if(element is ucNode)
						((ucNode)element).Dispatcher.BeginInvoke((Action)((ucNode)element).ReculateLinkPositions, System.Windows.Threading.DispatcherPriority.ContextIdle);

				}
			}
		}
		Point startPoint;
		private void nodeCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			MovingCanvas = true;
			startPoint = e.GetPosition(this);
		}
		ucNodeSubmergedView SubmergedView;
		public void Submerge(ucLFNode submergeto)
		{
			//CLEAR THE CANVAS PEOPLE!
			nodeCanvas.Children.Clear();
			Nodes.Clear();
			SubmergedInThisNode = submergeto;
			SubmergedNodes.Push(SubmergedInThisNode);



			//load some nodes!
			LoadNodes();

			//Load links
			LoadLinks();
			ReinitToolbar();

			//this.RenderTransform.Inverse.Transform(new Point(0, (double)this.GetValue(Canvas.TopProperty))).Y;
			//setup submerge
			//load the container
			SubmergedView = new ucNodeSubmergedView(this);
			Panel.SetZIndex(SubmergedView, -1);
			if (Nodes.Count > 0)
			{
				SubmergedView.Width = (maxX - minX) + 250;
				SubmergedView.Height = (maxY - minY) + 250;//nodeCanvas.ActualHeight;
				SubmergedView.VerticalAlignment = System.Windows.VerticalAlignment.Top;
				SubmergedView.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
				Canvas.SetLeft(SubmergedView, minX - 100);
				Canvas.SetTop(SubmergedView, minY - 110);
				nodeCanvas.Children.Add(SubmergedView);
				SetTopLeft(new Point(minX - 100, minY - 110));
			} else
			{
				SubmergedView.Width = nodeCanvas.ActualWidth - 100;
				SubmergedView.Height = nodeCanvas.ActualHeight - 100;
				Canvas.SetLeft(SubmergedView, 50);
				Canvas.SetTop(SubmergedView, 50);
				SubmergedView.VerticalAlignment = System.Windows.VerticalAlignment.Top;
				SubmergedView.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
				nodeCanvas.Children.Add(SubmergedView);
			}
			movingNode = null;
		}



		Canvas MyToolbar;
		public void ReinitToolbar()
		{
			SetupToolbar(MyToolbar);
		}
		public void SetupToolbar(Canvas toolbar)
		{
			MyToolbar = toolbar;
			toolbar.Children.Clear();
			StackPanel sp = new StackPanel();
			ComboBox cb = new ComboBox();
			sp.Orientation = Orientation.Horizontal;
			toolbar.Children.Add(sp);
			sp.Children.Add(cb);
			foreach (KeyValuePair<string, ucLFNode> nodetype in FlowSupportedNodes)
			{

				if ((nodetype.Value.OnlySubmerged == false && SubmergedInThisNode == null) ||
					(nodetype.Value.OnlySubmerged == true
						&& SubmergedInThisNode != null 
						&& SubmergedInThisNode.ValidChildNodeTypes.Find(item => item.Equals(nodetype.Value.GetType())) != null))
				{
					Button btnAddNode = new Button();
					btnAddNode.Tag = nodetype.Value;
					btnAddNode.Content = nodetype.Key;
					btnAddNode.Click += btnAddNode_Click;
					cb.Items.Add(btnAddNode);
					//sp.Children.Add(btnAddNode);
					//edtSync.CurrentWindow //we need a way to get back to the toolbar to add this button...without know its a metawindow.  Argh.
					// maybe metawindow calls down int this somehow?
					// INLTabClient -> "PopulateToolbar" function
					// INLDocManager -> while loading, calls populatetoolbar
				}
			}
			if(cb.Items.Count > 0)
			{
				cb.SelectedIndex = 0;
			}
			c = new ColorPicker();
			c.ColorMode = ColorMode.ColorCanvas;
			c.Width = 50;
			sp.Children.Add(c);

			BindColorPicker();
		}

		private void BindColorPicker()
		{
			if (c != null)
			{
				Binding myBinding = new Binding("NodeColor");
				myBinding.Source = SelectedNode;
				myBinding.Mode = BindingMode.TwoWay;
				//myBinding.Path = new PropertyPath("NodeName");
				myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
				c.SetBinding(ColorPicker.SelectedColorProperty, myBinding);
			}
		}
		ColorPicker c;
		public void Emerge()
		{
			ucLFNode lastNode = SubmergedInThisNode;
			SubmergedNodes.Pop();
			if (SubmergedNodes.Count == 0)
			{
				SubmergedInThisNode = null;
			} else
			{
				SubmergedInThisNode = SubmergedNodes.Peek();
			}

			//CLEAR THE CANVAS PEOPLE!
			nodeCanvas.Children.Clear();
			Nodes.Clear();

			//load some nodes!
			LoadNodes();

			//Load links
			LoadLinks();
			ReinitToolbar();
			lastNode = Nodes[lastNode.RowID];
			Point center = new Point((double)lastNode.GetValue(Canvas.LeftProperty) + (lastNode.Width / 2), (double)lastNode.GetValue(Canvas.TopProperty) + (lastNode.Width / 2));
			CenterOn(center);

			movingNode = null;
		}
		
		void btnAddNode_Click(object sender, RoutedEventArgs e)
		{
			Button btn = (Button)sender;
			
			ucLFNode node = (ucLFNode)btn.Tag;
			node = (ucLFNode)Activator.CreateInstance(node.GetType());
			if(SubmergedInThisNode != null)
			{
				node.PropertyChanged += submergedNode_PropertyChanged;
			}

			AddNode(node);
			node.AddDefaultPrevLinkers();
			node.AddDefaultNextLinkers();
			node.Focus();

			if(lastMatrix != Matrix.Identity)
			{
				node.RenderTransform = new MatrixTransform( lastMatrix);
			}

			LCDataRow dr = NodeSync.Instance.NewRow();
			LCDataRow drExt = NodeExtSync[node.GetFQN()].Instance.NewRow();
			drExt["NodeID"] = dr[0];
			Console.WriteLine(dr[0] + " <- " + drExt[0]);
			dr["X"] = 100;
			dr["Y"] = 100;
			dr["Width"] = 300;
			dr["Height"] = 160;
			dr["NodeTypeFQN"] = node.GetFQN();
			dr["NodeName"] = IDConverter.DecimalToID((long)dr[0]);
			dr["NodeColor"] = string.Format("#{0:X2}{1:X2}{2:X2}{3:X2}",
					node.NodeColor.A,
					node.NodeColor.R,
					node.NodeColor.G,
					node.NodeColor.B);

			if(SubmergedInThisNode != null)
			{
				List<SerializableKeyValuePair<string, long>> nodeRef = new List<SerializableKeyValuePair<string, long>>();
				nodeRef.Add(new SerializableKeyValuePair<string,long>(SubmergedInThisNode.Instance.ID.ToString(), SubmergedInThisNode.RowID));
				dr["ParentNode_ref"] = nodeRef;
			}

			node.SetDataSource(NodeSync.Instance, dr, drExt);
			//AddNode(node);
			Nodes[node.RowID] = node;
			movingNode = null;
			e.Handled = true;
		}
		//private ucLFNode _SelectedNode = null;
		//public ucLFNode SelectedNode
		//{
		//	get
		//	{
		//		return _SelectedNode;
		//	}
		//	set
		//	{ 
		//		_SelectedNode = value; 
				
		//	}
		//}
		public static readonly DependencyProperty SelectedNodeProperty =
			DependencyProperty.Register("SelectedNode", typeof(ucNode), typeof(ucNodeCanvas));
		public ucNode SelectedNode
		{
			set 
			{ 
				SetValue(SelectedNodeProperty, value); 
				OnPropertyChanged("SelectedNode"); 
				BindColorPicker(); 
				if(value != null && value is ucLFNode)
				{
					NodeSync.GotoRowID(((ucLFNode)value).RowID);
					NodeExtSync[((ucLFNode)value).GetFQN()].RowSearch(((ucLFNode)value).RowID, "NodeID");
				}
			}
			get { return (ucNode)GetValue(SelectedNodeProperty); }
		}


		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}
	}
}
