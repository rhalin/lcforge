﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace NodeLynx.Node
{
	/// <summary>
	/// Interaction logic for ucNodeLinkLine.xaml
	/// </summary>
	public partial class ucNodeLinkLine : UserControl
	{
		public Line line = new Line();
		public ucTextComment textComment = new ucTextComment();
		public Point StartPoint;
		public Point EndPoint;
		public bool EndAnchored = false;
		public ucNodeLinker StartLinker;
		public ucNodeLinker EndLinker;
		public Path BezierPath = null;
		public Ellipse centerPoint = new Ellipse();

		public Style CenterEllipseStyle;
		private ucNodeCanvas MyCanvas;

		public ucNodeLinkLine(ucNodeCanvas myCanvas)
		{
			InitializeComponent();
			MyCanvas = myCanvas;
			CenterEllipseStyle = new Style();
			CenterEllipseStyle.Setters.Add(new Setter(Ellipse.StrokeThicknessProperty,2.0));
			CenterEllipseStyle.Setters.Add(new Setter(Ellipse.FillProperty, new SolidColorBrush(Color.FromRgb(220, 220, 240))));
			CenterEllipseStyle.Setters.Add(new Setter(Ellipse.StrokeProperty, Brushes.DarkRed));
			textComment.SetVisibleObj = centerPoint;
			textComment.txtComment.PreviewKeyDown += new KeyEventHandler(txtComment_PreviewKeyDown);
			
		}

		void txtComment_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			MyCanvas.LineManager.ReculateLinkLines(StartLinker);
		}

		void txtComment_KeyDown(object sender, KeyEventArgs e)
		{
			MyCanvas.LineManager.ReculateLinkLines(StartLinker);
		}

		void txtComment_TextChanged(object sender, TextChangedEventArgs e)
		{
			MyCanvas.LineManager.ReculateLinkLines(StartLinker);
		}
		public void SetBezierPath(Path BPath)
		{
			BezierPath = BPath;
			centerPoint.MouseLeftButtonDown += new MouseButtonEventHandler(line_MouseLeftButtonDown);
			centerPoint.MouseRightButtonDown += new MouseButtonEventHandler(centerPoint_MouseRightButtonDown);
		}

		void centerPoint_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ClickCount >= 1)
			{
				ucPropertiesWindow win = new ucPropertiesWindow();
				win.Show();
			}
		}
		void line_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ClickCount >= 1)
			{
				centerPoint.Visibility = System.Windows.Visibility.Hidden;
				textComment.Visibility = System.Windows.Visibility.Visible;
				//only set focus if the label is currently unset
				if (textComment.txtComment.Text == "")
				{
					//we need to delay focus till later or it won't work
					this.Dispatcher.BeginInvoke(DispatcherPriority.Input,
						new Action(delegate()
						{
							textComment.txtComment.Focus();         // Set Logical Focus
							Keyboard.Focus(textComment.txtComment); // Set Keyboard Focus
						}));
				}
			}
		}
	}
}
