﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx;
using System.Windows.Media.Animation;

namespace NodeLynx.Node
{
	/// <summary>
	/// Interaction logic for ucNodeLinker.xaml
	/// </summary>
	/// 
	public partial class ucNodeLinker : UserControl
	{
		public int Index { get; set; }
		public ucNode MyNode { get; set; }
		public ucNodeCanvas MyCanvas { get; set; }
		public enum Direction { DIR_PREV = 1, DIR_NEXT = 2 };
		public Direction LinkerDirection { get; set; }
		public List<ucNodeLinkLine> ConnectedLines { get; set; }

		public double CONN_POINT_PREV_Y
		{
			set { }
			get { return grdMain.ActualHeight / 2; }
		}
		public double CONN_POINT_NEXT_Y
		{
			set{}
			get { return grdMain.ActualHeight / 2; }
		}
		public double CONN_POINT_NEXT_X
		{
			set{}
			get { return (brdLinker.Width + brdLinker.Margin.Right) - 1; }
		}
		public double CONN_POINT_PREV_X
		{
			set{}
			get { return brdLinker.Margin.Left + 1; }
		}

		//behavior properties
		public int MaximumConnections { get; set; }
		//can connect to these other node types
		public List<Type> ConnectableToLinkerTypes { get; set; }
		//hover-over displays preview of what it can connect to
		public bool DisplayPreview { get; set; }
		public bool CanDuplicate { get; set; }

		//styles
		public Style BaseStyle { get; set; }
		public Style ConnectingStyle { get; set; }
		public Style ConnectableStyle { get; set; }
		public Style ConnectedStyle { get; set; }


		public ucNodeLinker()
		{
			InitializeComponent();

			//initialize properties
			LinkerDirection = Direction.DIR_NEXT;
			ConnectedLines = new List<ucNodeLinkLine>();
			ConnectableToLinkerTypes = new List<Type>();

			//setup styles
			BaseStyle = new Style();
			ConnectingStyle = new Style();
			ConnectableStyle = new Style();
			ConnectedStyle = new Style();

			BaseStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(3.0)));
			BaseStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(10.0)));
			BaseStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromRgb( 220, 220, 240))));
			BaseStyle.Setters.Add(new Setter(Border.BorderBrushProperty, Brushes.DarkSlateGray));

			ConnectingStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(3.0)));
			ConnectingStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(10.0)));
			ConnectingStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromRgb(220, 220, 240))));
			ConnectingStyle.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromArgb(255, 230, 0, 0))));

			ConnectableStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(3.0)));
			ConnectableStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(10.0)));
			ConnectableStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromRgb(220, 220, 240))));
			ConnectableStyle.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromArgb(255, 0, 0, 230))));


			ConnectedStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(3.0)));
			ConnectedStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(10.0)));
			ConnectedStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromRgb(220, 220, 240))));
			ConnectedStyle.Setters.Add(new Setter(Border.BorderBrushProperty, Brushes.Black));

			MaximumConnections = 3;
			this.brdLinker.Style = BaseStyle;
			ConnectableToLinkerTypes.Add(typeof(ucNodeLinker));
			CanDuplicate = true;
		}

		public void SetNode(ucNode Parent)
		{
			MyNode = Parent;
			MyCanvas = Parent.MyCanvas;
		}
		public void InitStyle()
		{
			brdLinker.Style = ConnectedStyle;
		}
		private void brdLinker_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			//pick up
			if (e.ClickCount == 2)
			{
				if (ConnectedLines.Count == 0)
				{
					//remove this link					
					MyNode.RemoveNodeLinker(this);
				}
			}
			else
			{
				if (ConnectedLines.Count < MaximumConnections)
				{
					MyCanvas.LineManager.NewLine(this);
				}
			}
		}

		private void brdLinker_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{

			if (MyCanvas.currentLine != null)
			{
				if (ConnectedLines.Count < MaximumConnections)
				{
					if (MyCanvas.LineManager.DropCurrentLine(this))
					{
						PulseLinkerBorder();
					}
					MyCanvas.LineManager.LastLinkLine.StartLinker.PulseLinkerBorder();
				}
			}

		}
		
		private static TReturn ExtractStyleProperty<TReturn>(Style IncomingStyle, string name, TReturn defVal)
		{
			var style = IncomingStyle;
			if (style == null)
			{
				return defVal;
			}
			var setter = style.Setters.Where(s => s is Setter).Cast<Setter>().First(s => s.Property.Name.Equals(name));
			if (setter == null)
			{
				return defVal;
			}
			return (TReturn)setter.Value;
		}
		protected void PulseLinkerBorder()
		{
			ColorAnimation animMid;
			animMid = new ColorAnimation();
			brdLinker.Style = ConnectedStyle;

			if (this.LinkerDirection == Direction.DIR_NEXT)
			{
				Brush br = ExtractStyleProperty<Brush>(ConnectingStyle, "BorderBrush", Brushes.LimeGreen);
				if (br is SolidColorBrush)
				{
					animMid.From = (br as SolidColorBrush).Color;
				}
			}
			else
			{
				Brush br = ExtractStyleProperty<Brush>(ConnectableStyle, "BorderBrush", Brushes.Red);
				if (br is SolidColorBrush)
				{
					animMid.From = (br as SolidColorBrush).Color;
				}
				animMid.From = Colors.Red;
			}

			animMid.To = Colors.White;
			animMid.Duration = new Duration(TimeSpan.FromSeconds(.10));

			ColorAnimation animEnd;
			animEnd = new ColorAnimation();
			animEnd.From = Colors.White;
			Brush brto = ExtractStyleProperty<Brush>(ConnectedStyle, "BorderBrush", Brushes.Red);
			if (brto is SolidColorBrush)
			{
				animEnd.To = (brto as SolidColorBrush).Color;
			}
			animMid.From = Colors.Red;

			animEnd.BeginTime = TimeSpan.FromSeconds(.10);
			animEnd.Duration = new Duration(TimeSpan.FromSeconds(.10));

			Storyboard s = new Storyboard();
			s.Duration = new Duration(TimeSpan.FromSeconds(.20));
			s.Children.Add(animMid);
			s.Children.Add(animEnd);

			Storyboard.SetTarget(animMid, brdLinker);
			Storyboard.SetTargetProperty(animMid, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
			Storyboard.SetTarget(animEnd, brdLinker);
			Storyboard.SetTargetProperty(animEnd, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));

			s.Begin();
		}

		public virtual void Duplicate()
		{
			MyNode.AddNodeLinker<ucNodeLinker>(LinkerDirection);
		}
		private void brdLinker_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (CanDuplicate)
			{
				Duplicate();
			}
		}




		private void brdLinker_MouseEnter(object sender, MouseEventArgs e)
		{
			if (MyCanvas.currentLine != null //active line connection 
				&& MyCanvas.currentLine.StartLinker.MyNode != this.MyNode //this linker doesn't belong to the same node as the origin
				&& MyCanvas.currentLine.StartLinker.LinkerDirection != this.LinkerDirection) //linker is going a different direction
			{
				if (ConnectedLines.Count < MaximumConnections)
				{
					brdLinker.Style = ConnectableStyle;
				}
			}
		}

		private void brdLinker_MouseLeave(object sender, MouseEventArgs e)
		{
			if (MyCanvas.currentLine != null && MyCanvas.currentLine.StartLinker != this)
			{
				if (ConnectedLines.Count > 0)
				{
					brdLinker.Style = ConnectedStyle;
				}
				else
				{
					brdLinker.Style = BaseStyle;
				}
			}
		}
	}
}
