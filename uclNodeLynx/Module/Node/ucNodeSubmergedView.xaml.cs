﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.Node;

namespace NodeLynx.Module.Node
{
	/// <summary>
	/// Interaction logic for ucNodeSubmergedView.xaml
	/// </summary>
	public partial class ucNodeSubmergedView : UserControl
	{
		ucNodeCanvas myCanvas;
		public ucNodeSubmergedView(ucNodeCanvas nodeCanvas)
		{
			InitializeComponent();
			myCanvas = nodeCanvas;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			myCanvas.Emerge();
		}
	}
}
