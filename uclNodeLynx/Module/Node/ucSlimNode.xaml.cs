﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NodeLynx.Node
{
	/// <summary>
	/// Interaction logic for ucSlimNode.xaml
	/// </summary>
	public partial class ucSlimNode : UserControl
	{
		//don't think we need this, we're binding to the Node.NodeColor instead
		//public static readonly DependencyProperty NodeColorProperty =
		//	DependencyProperty.Register("NodeColor", typeof(Color), typeof(ucSlimNode));
		//public Color NodeColor
		//{
		//	set { SetValue(NodeColorProperty, value); }
		//	get { return (Color)GetValue(NodeColorProperty); }
		//}

		public static readonly DependencyProperty NodeProperty =
			DependencyProperty.Register("Node", typeof(ucNode), typeof(ucSlimNode));
		public ucNode Node
		{
			set { SetValue(NodeProperty, value); }
			get { return (ucNode)GetValue(NodeProperty); }
		}
		public ucSlimNode()
		{
			InitializeComponent();
		}
	}
}
