﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NodeLynx
{
	/// <summary>
	/// Interaction logic for ucTextComment.xaml
	/// </summary>
	public partial class ucTextComment : UserControl
	{
		public FrameworkElement SetVisibleObj = null;
		public ucTextComment()
		{
			InitializeComponent();
			this.Visibility = System.Windows.Visibility.Hidden;
		}

		private void btnClose_Click(object sender, RoutedEventArgs e)
		{
			this.Visibility = System.Windows.Visibility.Hidden;
			if (SetVisibleObj != null)
				SetVisibleObj.Visibility = System.Windows.Visibility.Visible;
		}

		private void txtComment_TextChanged(object sender, TextChangedEventArgs e)
		{
			/*
			if (txtComment.DesiredSize.Height > this.Height)
			{
				double diffHeight = txtComment.DesiredSize.Height - this.Height;
				if (diffHeight > 14)
				{
					this.Height += diffHeight;
				}
				else
				{
					this.Height += 14;
				}
			}
			 * */
		}

		private void UserControl_GotFocus(object sender, RoutedEventArgs e)
		{
			txtComment.Focus();
		}
	}
}
