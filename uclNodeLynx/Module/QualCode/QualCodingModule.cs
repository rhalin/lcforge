using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx.CustomData;
using NodeLynx.UI;
using NodeLynx.lib;
using NodeLynx.Module.QualCode;

namespace NodeLynx.Module
{
	[DatatypeID(ID = "{07bf6da2-5371-4581-9207-3ac9c7f2b3e4}", FQN = "com.dreamsmithfoundry.qualcoding")]
	public class QualCodingModule : DefaultModule
	{
		public QualCodingModule()
			: base()
		{
			Name = "QualCoding";
			Editors.Add(typeof(ucQualCode));
			Definitions.Add(GenerateDefinition_Code(RegTypes));
			Definitions.Add(GenerateDefinition_Actors(RegTypes));
			Definitions.Add(GenerateDefinition_Bookmarks(RegTypes));
			Definitions.Add(GenerateDefinition_Notes(RegTypes));
			Definitions.Add(GenerateDefinition_CodeInstances(RegTypes));
			Definitions.Add(GenerateDefinition_TranscriptionDocuments(RegTypes));
			Definitions.Add(GenerateDefinition_TranscriptionLine(RegTypes));
		}
		public DatatypeDefinition GenerateDefinition_Code(RegisteredTypes RegisteredDatatypes)
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.DisplayWidgetFQN = "widget.asset";
			def.Initialize(RegisteredDatatypes);
			def.DataName = "Code";
			def.FileName = "./defs/Code.def";
			def.PrintableName = "Code";
			def.ID = Guid.Parse("0fc44922-0c3b-4747-81d4-baf3daa7fbec");
			DatatypeField f = null;

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.short"));
			f.FieldName = "CodeName";
			f.FieldLabel = "Code Name";
			f.IsVisible = true;
			def.AddField(Guid.Parse("900d4b21-f09f-4b1c-8c1c-06d5b17eb3f6"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.short"));
			f.FieldName = "CodeID";
			f.FieldLabel = "Code ID";
			f.IsVisible = true;
			def.AddField(Guid.Parse("d5f1987e-5972-4fec-9319-64b7e35667b2"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.reference.single"));
			f.FieldName = "ParentCode";
			f.FieldLabel = "Parent Code";
			f.IsVisible = true;
			def.AddField(Guid.Parse("ad030fcd-82c1-4cdd-8acb-363452cc87e8"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.color"));
			f.FieldName = "CodeColor";
			f.FieldLabel = "Color";
			f.IsVisible = true;
			def.AddField(Guid.Parse("BD0F9A68-D049-44E9-BBBD-E1589C21116B"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("ui.keybinding.qualcode"));
			f.FieldName = "KeyBinding";
			f.FieldLabel = "Key Binding";
			f.IsVisible = true;
			def.AddField(Guid.Parse("A436E8FE-521A-4FA9-A69F-DB7FDDFDEFEF"), f);

			def.WidgetBindings.Add(new SerializableKeyValuePair<string, string>("shortText", "CodeName"));
			def.WidgetBindings.Add(new SerializableKeyValuePair<string, string>("filename", "CodeName"));
			def.WidgetBindings.Add(new SerializableKeyValuePair<string, string>("extension", "CodeID"));

			return def;
		}
		public DatatypeDefinition GenerateDefinition_Actors(RegisteredTypes RegisteredDatatypes)
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.DisplayWidgetFQN = "widget.default";
			def.Initialize(RegisteredDatatypes);
			def.DataName = "Actors";
			def.PrintableName = "Actors (Qual)";
			def.FileName = "./defs/Actors.def";
			def.ID = Guid.Parse("ef83ebe6-91dc-4e32-aa03-5adc347b8109");
			DatatypeField f = null;

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.short"));
			f.FieldName = "ActorName";
			f.FieldLabel = "Name";
			f.IsVisible = true;
			def.AddField(Guid.Parse("b328b6fa-890f-4532-a8d0-c2cba9f5640d"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.short"));
			f.FieldName = "ActorCode";
			f.FieldLabel = "Code";
			f.IsVisible = true;
			def.AddField(Guid.Parse("3212628e-b043-4b4f-a715-c97ec80aa667"), f);


			return def;
		}
		public DatatypeDefinition GenerateDefinition_Bookmarks(RegisteredTypes RegisteredDatatypes)
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.DisplayWidgetFQN = "widget.default";
			def.Initialize(RegisteredDatatypes);
			def.DataName = "Bookmarks";
			def.PrintableName = "Bookmarks";
			def.FileName = "./defs/Bookmarks.def";
			def.ID = Guid.Parse("576e417d-feb6-4b55-a7fe-db5dfffd2208");
			DatatypeField f = null;

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.time.duration"));
			f.FieldName = "Timestamp";
			f.FieldLabel = "Timestamp";
			f.IsVisible = true;
			def.AddField(Guid.Parse("a2953460-a738-414c-acc2-fd80810ce2e3"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.long"));
			f.FieldName = "BookmarkNotes";
			f.FieldLabel = "Note";
			f.IsVisible = true;
			def.AddField(Guid.Parse("d30e145d-970c-430a-860b-a86673ba40ed"), f);


			return def;
		}
		public DatatypeDefinition GenerateDefinition_Notes(RegisteredTypes RegisteredDatatypes)
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.DisplayWidgetFQN = "widget.default";
			def.Initialize(RegisteredDatatypes);
			def.DataName = "Notes";
			def.PrintableName = "Notes";
			def.FileName = "./defs/Notes.def";
			def.ID = Guid.Parse("caa9fa9e-d274-496c-ae42-28eb4e8a8dad");
			DatatypeField f = null;

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.int"));
			f.FieldName = "StartIndex";
			f.FieldLabel = "Start Index";
			f.IsVisible = true;
			def.AddField(Guid.Parse("65ce8d06-e81f-42ec-8fb0-af43b70db604"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.int"));
			f.FieldName = "Length";
			f.FieldLabel = "Length";
			f.IsVisible = true;
			def.AddField(Guid.Parse("618c50e7-8282-46ce-baf9-6f767d0d5c4d"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.long"));
			f.FieldName = "NoteData";
			f.FieldLabel = "Note";
			f.IsVisible = true;
			def.AddField(Guid.Parse("1363c0b4-77d0-4438-a13c-e207f38b7f77"), f);


			return def;
		}
		public DatatypeDefinition GenerateDefinition_CodeInstances(RegisteredTypes RegisteredDatatypes)
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.DisplayWidgetFQN = "widget.default";
			def.Initialize(RegisteredDatatypes);
			def.DataName = "CodeInstances";
			def.FileName = "./defs/CodeInstances.def";
			def.ID = Guid.Parse("54073699-9f06-4baf-8056-6edfa33b35f0");
			def.PrintableName = "Code Instances";
			DatatypeField f = null;

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.reference.single"));
			f.FieldName = "CodeRef";
			f.FieldLabel = "Code";
			f.IsVisible = true;
			def.AddField(Guid.Parse("0a2b1461-e07c-47a7-a38f-fa937861bbea"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.int"));
			f.FieldName = "StartIndex";
			f.FieldLabel = "StartIndex";
			f.IsVisible = true;
			def.AddField(Guid.Parse("f9b4a8d9-ad50-43ea-b463-943fd5c8f98c"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.int"));
			f.FieldName = "Length";
			f.FieldLabel = "Length";
			f.IsVisible = true;
			def.AddField(Guid.Parse("ad256474-8974-4a4d-b7bc-7760296e472f"), f);


			return def;
		}
		public DatatypeDefinition GenerateDefinition_TranscriptionDocuments(RegisteredTypes RegisteredDatatypes)
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.DisplayWidgetFQN = "widget.default";
			def.Initialize(RegisteredDatatypes);
			def.DataName = "TranscriptionDocuments";
			def.PrintableName = "Transcriptions";
			def.FileName = "./defs/TranscriptionDocuments.def";
			def.ID = Guid.Parse("caa80792-153d-46d9-b467-63c49273fe2c");
			
			DatatypeField f = null;

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.reference.single"));
			f.FieldName = "Media1";
			f.FieldLabel = "Media1";
			f.IsVisible = true;
			def.AddField(Guid.Parse("3ce8daf6-1787-497b-a67e-89e68283aa4b"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.complex.instance"));
			f.FieldName = "CodeSet";
			f.FieldLabel = "Selected Code Set";
			f.IsVisible = true;
			f.ComplexTypeReferences.Add(Guid.Parse("{0fc44922-0c3b-4747-81d4-baf3daa7fbec}"));
			def.AddField(Guid.Parse("C8C8D09C-2BB1-4FD3-9483-83AB52B6A4B7"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.short"));
			f.FieldName = "TranscriptionName";
			f.FieldLabel = "TranscriptionName";
			f.IsVisible = true;
			def.AddField(Guid.Parse("1a6a4d91-29a9-496e-a039-844d25636aa7"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.complex.instance"));
			f.FieldName = "Lines";
			f.FieldLabel = "TranscriptionLines";
			f.ComplexTypeReferences.Add(Guid.Parse("{2078be5b-8548-44ac-bf1a-d40832a42013}"));
			f.IsVisible = true;
			def.AddField(Guid.Parse("576420af-3a6c-43ca-ac9d-0f63fa8f6a1f"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.complex.instance"));
			f.FieldName = "Bookmarks";
			f.FieldLabel = "Bookmarks";
			f.ComplexTypeReferences.Add(Guid.Parse("{576e417d-feb6-4b55-a7fe-db5dfffd2208}"));
			f.IsVisible = true;
			def.AddField(Guid.Parse("6D211524-49DF-4FB4-B7A3-8CCA092BD5DD"), f);
			return def;
		}
		public DatatypeDefinition GenerateDefinition_TranscriptionLine(RegisteredTypes RegisteredDatatypes)
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.DisplayWidgetFQN = "widget.default";
			def.Initialize(RegisteredDatatypes);
			def.PrintableName = "Transcription Lines";
			def.DataName = "TranscriptionLine";
			def.FileName = "./defs/TranscriptionLine.def";
			def.ID = Guid.Parse("2078be5b-8548-44ac-bf1a-d40832a42013");
			DatatypeField f = null;

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.time.duration"));
			f.FieldName = "TimestampStart";
			f.FieldLabel = "TimestampStart";
			f.IsVisible = true;
			def.AddField(Guid.Parse("53aa7335-d3fe-4ec1-ba41-e42065f5f171"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.reference.single"));
			f.FieldName = "Actor";
			f.FieldLabel = "Actor";
			f.IsVisible = true;
			def.AddField(Guid.Parse("b84a77f1-8d4f-4ada-a790-d225cd077b75"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.text.long"));
			f.FieldName = "TranscriptionText";
			f.FieldLabel = "Text";
			f.IsVisible = true;
			def.AddField(Guid.Parse("455ee035-35b2-445e-ad3b-b316fff1edab"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.time.duration"));
			f.FieldName = "TimestampEnd";
			f.FieldLabel = "TimestampEnd";
			f.IsVisible = true;
			def.AddField(Guid.Parse("0c4fd950-1438-408b-940a-c5d40ce625cc"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.complex.instance"));
			f.FieldName = "NoteTable";
			f.FieldLabel = "Notes";
			f.IsVisible = true;
			f.ComplexTypeReferences.Add(Guid.Parse("{caa9fa9e-d274-496c-ae42-28eb4e8a8dad}"));
			def.AddField(Guid.Parse("bf7c0319-f7ec-4210-a82b-53b3e6bd0594"), f);

			f = new DatatypeField();
			f.SetDatatype(RegisteredDatatypes.GetDatatype("data.complex.instance"));
			f.FieldName = "Codes";
			f.ComplexTypeReferences.Add(Guid.Parse("{54073699-9f06-4baf-8056-6edfa33b35f0}"));
			f.FieldLabel = "Codes";
			f.IsVisible = true;
			def.AddField(Guid.Parse("7da40181-6e8f-448d-b020-1953c01af36f"), f);


			return def;
		}
	}
}

