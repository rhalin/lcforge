﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NodeLynx.Module.QualCode.UI
{
	/// <summary>
	/// Interaction logic for ucQCBookmark.xaml
	/// </summary>
	public partial class ucQCBookmark : UserControl
	{
		public long Bookmark = 0;
		private ucQualCode parent = null;
		private long ID = -1;
		public ucQCBookmark()
		{
			InitializeComponent();
		}
		public void Initialize(ucQualCode CodeModuleInstance, long BookmarkID)
		{
			parent = CodeModuleInstance;
			ID = BookmarkID;
		}
		private void Border_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			//move to bookmark
			parent.GotoBookmark(Bookmark);
		}

		private void Border_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
		{
			parent.DeleteBookmark(ID);
		}
	}
}
