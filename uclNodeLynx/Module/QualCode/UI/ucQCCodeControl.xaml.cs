﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using NodeLynx.CustomData;

namespace NodeLynx.Module.QualCode.UI
{
	/// <summary>
	/// Interaction logic for ucQCCodeControl.xaml
	/// </summary>
	public partial class ucQCCodeControl : UserControl
	{
		LCDataRow CodeInstanceRow;
		TextBox txtMyTextbox;
		LCDataRow CodeLookup;
		int StartIndex;
		int Length;
		int Column;
		bool _Selected = false;
		public bool Selected
		{
			get
			{
				return _Selected;
			}
			set
			{
				_Selected = value;
				if(_Selected == true)
				{
					boxFront.Fill = new SolidColorBrush(Colors.Black);
					boxBack.Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString(CodeLookup["CodeColor"].ToString()));
				}
				else
				{
					boxFront.Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString(CodeLookup["CodeColor"].ToString()));
					boxBack.Fill = new SolidColorBrush(Colors.Black);
				}
			}
		}
		public ucQCCodeControl()
		{
			InitializeComponent();
		}
		public void Initialize(LCDataRow CodeInstance, LCDataRow CodeData, TextBox txt, int Col)
		{
			CodeInstanceRow = CodeInstance;
			txtMyTextbox = txt;
			CodeLookup = CodeData;
			StartIndex = (int)CodeInstanceRow["StartIndex"];
			Length = (int)CodeInstanceRow["Length"];
			Column = Col;
			txtMyTextbox.LostFocus += txtMyTextbox_LostFocus;
		}

		void txtMyTextbox_LostFocus(object sender, RoutedEventArgs e)
		{
			Selected = false;
		}
		public void DelayedLoad()
		{
			txtMyTextbox.Select(StartIndex, Length);
			double top = txtMyTextbox.GetRectFromCharacterIndex(txtMyTextbox.SelectionStart).Top;
			double bottom = txtMyTextbox.GetRectFromCharacterIndex(txtMyTextbox.SelectionStart + txtMyTextbox.SelectionLength).Bottom;
			boxFront.Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString(CodeLookup["CodeColor"].ToString()));
			lblCodeLabel.Content = CodeLookup["CodeID"].ToString();
			if (!double.IsInfinity(top))
			{
				SetBox(top, bottom);
			}
		}
		public void SetBox(double top, double bottom)
		{
			Thickness margin = this.Margin;
			margin.Top = top + 2;
			margin.Left = Column * 30;
			//this.Height = bottom - top;
			this.Margin = margin;
			
			boxBack.Height = bottom - top - 4;
			margin = boxBack.Margin;
			//margin.Top = top + 2;
			//margin.Left = Column * 30;
			boxBack.Margin = margin;

			boxFront.Height = bottom - top;
			margin = boxFront.Margin;
			//margin.Top = top;
			//margin.Left = Column * 30;
			boxFront.Margin = margin;

			lblCodeLabel.Height = bottom - top;
			if (lblCodeLabel.Height < 40)
				lblCodeLabel.Height = 40;
			margin = lblCodeLabel.Margin;
			margin.Top = 0;
			lblCodeLabel.Margin = margin;
			//margin.Left = (Column * 40) + 31;
			//lblCodeLabel.Margin = margin;
			// * */

		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.Dispatcher.BeginInvoke(new Action(() => { DelayedLoad(); }), DispatcherPriority.ApplicationIdle);
		}

		private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			txtMyTextbox.Select(StartIndex, Length);
			//newSelectionBox.Visibility = System.Windows.Visibility.Collapsed;
			txtMyTextbox.Focus();
			Selected = true;
		}
	}
}
