﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;
using NodeLynx.lib;

namespace NodeLynx.Module.QualCode.UI
{
	/// <summary>
	/// Interaction logic for ucQCTranscriptionLineEditor.xaml
	/// </summary>
	public partial class ucQCTranscriptionLineEditor : UserControl
	{
		Dictionary<long, int> CodeColumns = new Dictionary<long, int>();
		Dictionary<Key, LCDataRow> CodeKeyBindings = new Dictionary<Key, LCDataRow>();
		LCDataRow TranscriptRow;
		DataInstance ParentInstance;
		DataInstance CodeInstances;
		DataInstance CodeLookups;
		int AtCol = 0;
		public ucQCTranscriptionLineEditor()
		{
			InitializeComponent();		
		}
		/// <summary>
		/// Set all known codes so we can allocate columns to them
		/// </summary>
		public void SetCodes(DataInstance di)
		{
			CodeLookups = di;
			CodeColumns.Clear();
			KeyConverter kv = new KeyConverter();
			Console.WriteLine("Set Codes...");
			foreach (LCDataRow row in di.Data.Rows)
			{
				CodeColumns[(long)row["RowID"]] = AtCol;
				Key k = (Key)kv.ConvertFromString(row["KeyBinding"].ToString());
				CodeKeyBindings[k] = row;
				AtCol++;
			}
		}
		public void LoadCodeInstances(DataInstance parent, LCDataRow transcriptrow, DataInstance codeInstances)
		{
			cnvCode.Children.Clear();
			ParentInstance = parent;
			TranscriptRow = transcriptrow;
			CodeInstances = codeInstances;
			txtData.Text = TranscriptRow["TranscriptionText"].ToString();
			TimeSpan ts = TimeSpan.FromMilliseconds((long)TranscriptRow["TimestampStart"]);
			lblTimestampes.Content = "From: "  + ts.ToString(@"h\:mm\:ss\.ff");
			ReloadCodes();
		}

		private void ReloadCodes()
		{
			if ((long)TranscriptRow["TimestampStart"] != (long)TranscriptRow["TimestampEnd"])
			{
				TimeSpan ts2 = TimeSpan.FromMilliseconds((long)TranscriptRow["TimestampEnd"]);
				lblTimestampes.Content += "           To: " + ts2.ToString(@"h\:mm\:ss\.ff");
			}
			foreach (LCDataRow row in CodeInstances.Data.Rows)
			{
				ucQCCodeControl codeCtrl = new ucQCCodeControl();
				cnvCode.Children.Add(codeCtrl);
				codeCtrl.Margin = new Thickness(1, 0, 0, 0);
				List<SerializableKeyValuePair<string, long>> coderef = (List<SerializableKeyValuePair<string, long>>)row["CodeRef_ref"];
				LCDataRow dr = CodeLookups.GotoRowID(coderef[0].Value);
				codeCtrl.Initialize(row, dr, txtData, CodeColumns[coderef[0].Value]);
			}
		}

		private void txtData_LostFocus(object sender, RoutedEventArgs e)
		{
			TranscriptRow["TranscriptionText"] = txtData.Text;
			DataProject.CurrentProject.SaveProject();
		}
		public bool Writable
		{
			get { 
				if(txtData == null)
				{
					return false;
				}
				return !txtData.IsReadOnly; 
			}
			set { txtData.IsReadOnly = !value; }
		}

		private void txtData_PreviewKeyUp(object sender, KeyEventArgs e)
		{
			//only code if we're in readonly mode
			if(txtData.IsReadOnly)
			{
				if(CodeKeyBindings.ContainsKey(e.Key))
				{
					LCDataRow dr = CodeInstances.NewRow();
					dr["StartIndex"] = txtData.SelectionStart;
					dr["Length"] = txtData.SelectionLength;
					List<SerializableKeyValuePair<string, long>> ls = new List<SerializableKeyValuePair<string, long>>();
					SerializableKeyValuePair<string, long> code = new SerializableKeyValuePair<string, long>();
					code.Key = CodeInstances.ID.ToString();//CodeKeyBindings[e.Key].
					code.Value = (long)CodeKeyBindings[e.Key]["RowID"];
					ls.Add(code);
					dr["CodeRef_ref"] = ls;
					CodeInstances.AddRow(dr);
					ReloadCodes();
				}
			}
			e.Handled = true;
		}

	}
}
