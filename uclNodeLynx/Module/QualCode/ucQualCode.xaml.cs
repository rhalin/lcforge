﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;
using NodeLynx.lib;
using NodeLynx.Module.QualCode.UI;
using NodeLynx.UI;

namespace NodeLynx.Module.QualCode
{
	[ValueConversion(typeof(object), typeof(string))]
	public class FormattingConverter : IValueConverter
	{
		public object Convert(object value, Type targetType,
			object parameter, System.Globalization.CultureInfo culture)
		{
			string formatString = parameter as string;
			if (formatString != null)
			{
				return string.Format(culture, formatString, value);
			}
			else
			{
				return value.ToString();
			}
		}

		public object ConvertBack(object value, Type targetType,
			object parameter, System.Globalization.CultureInfo culture)
		{
			// we don't intend this to ever be called
			return null;
		}
	}
	/// <summary>
	/// Interaction logic for ucQualCode.xaml
	/// </summary>
	public partial class ucQualCode : UserControl, INLTabClient, INotifyPropertyChanged
	{
		public void SetupToolbar(Canvas toolbar)
		{

		}
		DatatypeDefinition Definition { get; set; }
		DataInstance Instance { get; set; }

		DataInstance CurrentBookmarksInstance { get; set; }
		DataInstance CurrentTranscriptionInstance { get; set; }
		DataInstance CurrentCodeReference { get; set; }
		LCDataRow TranscriptionRow { get; set; }
		long Row { get; set; }

		bool DetailsLoaded = false;
		bool MediaOpened = false;
		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}
		double _MediaDuration;
		public double MediaDuration
		{
			get{return _MediaDuration;}
			set
			{
				_MediaDuration = value;
				OnPropertyChanged("MediaDuration");
			}
		}

		public UserControl GetControl()
		{
			return this;
		}

		public string GetTabName()
		{
			return "Transcribe Media";
		}

		public string GetButtonName()
		{
			return "Transcribe Media";
		}

		public void OnParentSizeChanged(object sender, SizeChangedEventArgs e)
		{
			//throw new NotImplementedException();
		}

		public void SetDragDropManager(DragDropManager dragdropManager)
		{
			//throw new NotImplementedException();
		}
		public ucQualCode()
		{
			InitializeComponent();
			MediaDuration = 1.1;
			mplayer1.MediaOpened += mplayer1_MediaOpened;
			mplayer1.MediaFailed += mplayer1_MediaFailed;
			mplayer1.BufferingStarted += mplayer1_BufferingStarted;
			mplayer1.BufferingEnded += mplayer1_BufferingEnded;
			sliderUpdateTimer.Tick += sliderUpdateTimer_Tick;
			sliderUpdateTimer.Interval = new TimeSpan(1000000L); //.1s
		}

		void mplayer1_BufferingEnded(object sender, RoutedEventArgs e)
		{
			Console.WriteLine("Buffering ended");
		}

		void mplayer1_BufferingStarted(object sender, RoutedEventArgs e)
		{
			Console.WriteLine("Buffering ended");
		}

		void mplayer1_MediaFailed(object sender, ExceptionRoutedEventArgs e)
		{
			Console.WriteLine(e.ErrorException.Message);
		}
		Storyboard sbMedia;
		DispatcherTimer sliderUpdateTimer = new DispatcherTimer(); 
		void mplayer1_MediaOpened(object sender, RoutedEventArgs e)
		{
			Console.WriteLine("SetupMedia - MediaOpened");
			MediaOpened = true;
			if(!mplayer1.HasVideo)
			{
				mplayer1.Height = 5;
				gridRowTop.Height = new GridLength(220);
			}
			SetupMediaInformation();
			/*
			if (sbMedia != null)
			{
				sbMedia.Seek(TimeSpan.Zero);
			}
			else
			{
				sbMedia = new Storyboard();
				sbMedia.SlipBehavior = SlipBehavior.Slip;

				//mplayer1.Stop();
				MediaTimeline mTimeline = new MediaTimeline();
				mTimeline.Source = mplayer1.Source;
				mTimeline.Duration = mplayer1.NaturalDuration;
				mTimeline.CurrentTimeInvalidated += mTimeline_CurrentTimeInvalidated;
				Storyboard.SetTarget(mTimeline, mplayer1);
				sbMedia.Children.Add(mTimeline);
				sbMedia.Begin();
			}*/
		}

		private void SetupMediaInformation()
		{
			if (DetailsLoaded == false)
			{
				sliderUpdateTimer.Stop();
				if (mplayer1.NaturalDuration.HasTimeSpan && MediaOpened)
				{
					Console.WriteLine("Media Setup Complete");
					//videoSlider.DataContext = mplayer1;
					MediaDuration = mplayer1.NaturalDuration.TimeSpan.TotalSeconds;
					SetBookmarks();
					SetCodes();
					sliderUpdateTimer.Start();
					//this.Dispatcher.BeginInvoke(new Action(() => { Pause(); }), DispatcherPriority.ApplicationIdle);					
					grdMain.IsEnabled = true;
					DetailsLoaded = true;
				}
				else
				{
					SetCodes();
					if (mplayer1.NaturalDuration.HasTimeSpan)
					{
						Console.WriteLine("Media Setup Preload Complete");
						MediaDuration = mplayer1.NaturalDuration.TimeSpan.TotalSeconds;
						SetBookmarks();

						sliderUpdateTimer.Start();
						//this.Dispatcher.BeginInvoke(new Action(() => { Pause(); }), DispatcherPriority.ApplicationIdle);					
						grdMain.IsEnabled = true;
						DetailsLoaded = false;
					} else
					{
						grdMain.IsEnabled = true;
						Console.WriteLine("Media Setup Preload Failed");
					}
					//if (!MediaOpened)
					//{
					//	Thread.Sleep(100);
					//	this.Dispatcher.BeginInvoke(new Action(() => { SetSource(); }), DispatcherPriority.ApplicationIdle);
					//}
					//else
					{
						//Console.WriteLine("Re-trying Media Setup...");
						//Console.WriteLine(mplayer1.BufferingProgress + " progress...");
						//Thread.Sleep(100);
						//this.Dispatcher.BeginInvoke(new Action(() => { SetupMediaInformation(); }), DispatcherPriority.ContextIdle);
						//Dispatcher.Run();
					}
				}
			} else
			{
				//this.Dispatcher.BeginInvoke(new Action(() => { Pause(); }), DispatcherPriority.ApplicationIdle);
			}
		}
		private void Pause()
		{
			mplayer1.Pause();
			Paused = true;
			btnPlayPause.Content = "Play";
			Console.WriteLine("Paused");
		}


		void sliderUpdateTimer_Tick(object sender, EventArgs e)
		{
			InternalUpdate = true;
			videoSlider.Value = mplayer1.Position.TotalSeconds;
			TimeSpan ts = TimeSpan.FromSeconds(mplayer1.Position.TotalSeconds); //
			lblPosition.Content = "Current - " + ts.ToString(@"h\:mm\:ss\.ff") + "s"; //String.Format("Current - {0:h:mm:ss[.FF]}s", ts);
			//lblPosition.Content = String.Format("Current - {0:0.00}s", mplayer1.Position.TotalSeconds);
			if(JumpedToBookmark != -1 &&  JumpedBookmarkExpires < DateTime.Now)
			{
				JumpedToBookmark = -1;
			}
		}
		bool InternalUpdate = false;



		public List<DatatypeDefinition> SupportedEditorFor()
		{
			List<DatatypeDefinition> defs = new List<DatatypeDefinition>();
			defs.Add(DataProject.CurrentProject.DefinitionLookup["caa80792-153d-46d9-b467-63c49273fe2c"]);
			return defs;
		}

		public FrameworkElement SetDataSource(DatatypeDefinition def, DataInstance di = null, long RowID = -1)
		{
			Definition = def;
			Instance = di;
			Row = RowID;
			DetailsLoaded = false;
			Instance.PropertyChanged += Instance_PropertyChanged;

			if (RowID != -1 && di != null)
			{
				TranscriptionRow = Instance.GotoRowID(Row);

				LoadRelatedData();

				SetMedia();
			}
			return this;
		}

		private void LoadRelatedData()
		{
			CurrentBookmarksInstance = DataProject.CurrentProject.GetInstance(TranscriptionRow["Bookmarks"].ToString());
			if (CurrentBookmarksInstance != null)
			{
				CurrentBookmarksInstance.PropertyChanged += CurrentBookmarksInstance_PropertyChanged;
			}
			CurrentTranscriptionInstance = DataProject.CurrentProject.GetInstance(TranscriptionRow["Lines"].ToString());
			if (CurrentTranscriptionInstance != null)
			{
				CurrentTranscriptionInstance.PropertyChanged += CurrentTranscriptionInstance_PropertyChanged;
			}
			//SerializableKeyValuePair<string, long> CodeRef = (SerializableKeyValuePair<string, long>)dr["CodeSet_ref"];
			CurrentCodeReference = DataProject.CurrentProject.GetInstance(TranscriptionRow["CodeSet"].ToString());
			if (CurrentCodeReference != null)
			{
				//CurrentTranscriptionInstance.PropertyChanged += CurrentBookmarksInstance_PropertyChanged;
			}
		}

		private void SetMedia()
		{
			if (TranscriptionRow != null)
			{
				if (TranscriptionRow["Media1_ref"] != DBNull.Value)
				{
					List<SerializableKeyValuePair<string, long>> Asset = (List<SerializableKeyValuePair<string, long>>)TranscriptionRow["Media1_ref"];
					if (Asset.Count > 0 && Asset[0].Key == DataProject.AssetLibraryID.ToLower())
					{
						//get asset!
						DataInstance assetlib = DataProject.CurrentProject.GetAssetLibrary();
						LCDataRow drasset = assetlib.GotoRowID(Asset[0].Value);
						string path = System.IO.Path.GetDirectoryName(DataProject.CurrentProject.FileName) + "\\" + new Uri(drasset["AssetPath"].ToString(), UriKind.Relative);
						MediaPath = new Uri(path);
						//mplayer1.Source = new Uri(path);

						Paused = true;
						//mplayer1.NaturalDuration.TimeSpan.TotalSeconds
					}
				}
			}
		}

		private void Instance_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if(e.PropertyName == "__RowUpdated")
			{
				DataInstanceRowUpdatedEventArgs args = (DataInstanceRowUpdatedEventArgs)e;
				if(args.RowID == Row)
				{

				}
			}
		}
		Uri MediaPath;
		void CurrentTranscriptionInstance_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			SetCodes();
		}

		void CurrentBookmarksInstance_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "__RowAdded" || e.PropertyName == "__RowDeleted")
			{
				//rebuild bookmarks
				SetBookmarks();
			}
		}
		List<long> BookmarkIndex = new List<long>();
		private void SetBookmarks()
		{
			cvsBookmarks.Children.Clear();
			BookmarkIndex.Clear();
			foreach (LCDataRow dr in CurrentBookmarksInstance.Data.Rows)
			{
				if (dr["Timestamp"] != DBNull.Value)
				{
					double w = cvsBookmarks.Width;
					double scale = w / mplayer1.NaturalDuration.TimeSpan.TotalSeconds;
					double stamp = (long)dr["Timestamp"];
					BookmarkIndex.Add((long)dr["Timestamp"]);
					ucQCBookmark bookmark = new ucQCBookmark();
					bookmark.Bookmark = (long)stamp;
					bookmark.Margin = new Thickness((stamp * scale), 0, 0, 0);
					bookmark.Initialize(this, (long)dr["RowID"]);
					cvsBookmarks.Children.Add(bookmark);
				}
			}
			BookmarkIndex.Sort();
		}
		private void SetCodes()
		{
			stackTranscript.Children.Clear();
			if (CurrentTranscriptionInstance != null)
			{
				//CurrentTranscriptionInstance.Data.DefaultView.Sort = "TimestampStart ASC"; 
				foreach (LCDataRow dr in CurrentTranscriptionInstance.Data.Select("", "TimestampStart ASC"))
				{
					ucQCTranscriptionLineEditor ucTEdit = new ucQCTranscriptionLineEditor();
					ucTEdit.SetCodes(CurrentCodeReference);
					DataInstance codeData = DataProject.CurrentProject.GetInstance(dr["Codes"].ToString());
					ucTEdit.LoadCodeInstances(CurrentTranscriptionInstance, dr, codeData);
					stackTranscript.Children.Add(ucTEdit);
					ucTEdit.SizeChanged += ucTEdit_SizeChanged;
					this.Dispatcher.BeginInvoke(new Action(() => { RecalcHeights(); }), DispatcherPriority.ApplicationIdle);
				}
			}
		}

		void ucTEdit_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			RecalcHeights();
		}
		private void RecalcHeights()
		{
			
			double height = 0;
			double width = 0;
			foreach(ucQCTranscriptionLineEditor edt in stackTranscript.Children)
			{
				//edt.Margin = new Thickness(0, height, 0, 0);
				height += edt.ActualHeight;
				//width = edt.ActualWidth;
			}
			stackTranscript.Height = height;
			//stackTranscript.Width = 800;
			stackTranscript.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
			stackTranscript.VerticalAlignment = System.Windows.VerticalAlignment.Top;

			//height = 0;
			//for(int i = 0; i < (TopStack.Children.Count -1); i++)
			//{
			//	FrameworkElement fr = (FrameworkElement)TopStack.Children[i];
			//	height += fr.ActualHeight;
			//}
			//stackScroller.Height = TopStack.ActualHeight - height;
			//stackScroller.VerticalAlignment = System.Windows.VerticalAlignment.Top;
			//stackScroller.Width = width;
			
		}
		public DatatypeDefinition GetDatatypeDefinition()
		{
			return Definition;
		}

		public DataInstance GetDataInstance()
		{
			return Instance;
		}

		public long GetRowID()
		{
			return Row;
		}

		private void videoSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if(!InternalUpdate)
			{
				TimeSpan ts = TimeSpan.FromSeconds(videoSlider.Value);
				//sbMedia.Seek(ts, TimeSeekOrigin.BeginTime);
				mplayer1.Position = ts;
			}
			if (InternalUpdate)
				InternalUpdate = false;
		}

		private void btnSkipBackwards_Click(object sender, RoutedEventArgs e)
		{
			SkipReverse();
		}

		private void SkipReverse()
		{
			TimeSpan ts = TimeSpan.FromSeconds(mplayer1.Position.TotalSeconds - slideSkipValue.Value);
			if (ts.TotalSeconds < 0)
				ts = TimeSpan.FromSeconds(0);
			mplayer1.Position = ts;
		}
		bool Paused = false;
		private void btnPlayPause_Click(object sender, RoutedEventArgs e)
		{
			if(Paused)
			{
				PlayMedia();
			} else
			{
				Paused = true;
				mplayer1.Pause();
				btnPlayPause.Content = "Play";
			}
		}

		private void btnSkipForward_Click(object sender, RoutedEventArgs e)
		{
			SkipForward();
		}

		private void SkipForward()
		{
			TimeSpan ts = TimeSpan.FromSeconds(mplayer1.Position.TotalSeconds + slideSkipValue.Value);
			if (ts.TotalSeconds < mplayer1.NaturalDuration.TimeSpan.TotalSeconds)
			{
				mplayer1.Position = ts;
			}
			else
			{
				mplayer1.Position = mplayer1.NaturalDuration.TimeSpan;
			}
		}

		private void Slider_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			slideSpeedRatio.Value = 1.0;
		}

		private void slideSkipValue_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			//TimeSpan ts = TimeSpan.FromSeconds(mplayer1.Position.TotalSeconds + slideSkipValue.Value);
			//sbMedia.Seek(ts, TimeSeekOrigin.BeginTime);
			//mplayer1.Position = ts;
		}
		public void GotoBookmark(long s)
		{
			TimeSpan ts = TimeSpan.FromSeconds(s);
			mplayer1.Position = ts;
		}
		public void DeleteBookmark(long ID)
		{
			CurrentBookmarksInstance.DeleteRow(ID);
		}
		private void Grid_KeyUp(object sender, KeyEventArgs e)
		{
			switch(e.Key)
			{
				case Key.Left:
					SkipReverse();
					e.Handled = true;
					break;
				case Key.Right:
					SkipForward();
					e.Handled = true;
					break;
				case Key.Up:
					if (slideSpeedRatio.Value + 0.5 < slideSpeedRatio.Maximum)
					{
						slideSpeedRatio.Value += 0.5;
					} else
					{
						slideSpeedRatio.Value = slideSpeedRatio.Maximum;
					}
					e.Handled = true;
					break;
				case Key.Down:
					if (slideSpeedRatio.Value - 0.5 > slideSpeedRatio.Minimum)
					{
						slideSpeedRatio.Value -= 0.5;
					} else
					{
						slideSpeedRatio.Value = slideSpeedRatio.Minimum;
					}
					e.Handled = true;
					break;
				case Key.Space:
					NewBookmark();
					e.Handled = true;
					break;
				case Key.OemOpenBrackets:
					FindPreviousBookmark();
					e.Handled = true;
					break;
				case Key.OemCloseBrackets:
					FindNextBookmark();
					e.Handled = true;
					break;
				case Key.PageUp:
					NewTranscriptionLine();
					e.Handled = true;
					break;
			}
		}

		private void NewTranscriptionLine()
		{
			LCDataRow row = CurrentTranscriptionInstance.NewRow();
			row["TimestampStart"] = (long)mplayer1.Position.TotalMilliseconds;
			row["TimestampEnd"] = (long)mplayer1.Position.TotalMilliseconds;
			row["TranscriptionText"] = "[" + mplayer1.Position.ToString(@"h\:mm\:ss\.ff") + "]\r\n";
			CurrentTranscriptionInstance.AddRow(row);
		}
		long JumpedToBookmark = -1;
		DateTime JumpedBookmarkExpires = DateTime.Now.AddSeconds(1);
		private void FindNextBookmark()
		{
			//yes, this can be done faster...  We dont need it
			for(int i = 0; i < BookmarkIndex.Count; i++)
			{
				if (BookmarkIndex[i] > mplayer1.Position.TotalSeconds && BookmarkIndex[i] != JumpedToBookmark)
				{
					JumpedToBookmark = BookmarkIndex[i];
					GotoBookmark(BookmarkIndex[i]);
					JumpedBookmarkExpires = DateTime.Now.AddSeconds(1);
					break;
				}
			}
		}

		private void FindPreviousBookmark()
		{
			//yes, this can be done faster...  We dont need it
			for (int i = BookmarkIndex.Count - 1; i >= 0; i--)
			{
				if (BookmarkIndex[i] < mplayer1.Position.TotalSeconds && BookmarkIndex[i] != JumpedToBookmark)
				{
					JumpedToBookmark = BookmarkIndex[i];
					GotoBookmark(BookmarkIndex[i]);
					JumpedBookmarkExpires = DateTime.Now.AddSeconds(1);
					break;
				}
			}
		}

		private void NewBookmark()
		{
			LCDataRow dr = CurrentBookmarksInstance.NewRow();
			dr["Timestamp"] = (long)mplayer1.Position.TotalSeconds;
			CurrentBookmarksInstance.AddRow(dr);
		}

		private void ucMedia_Unloaded(object sender, RoutedEventArgs e)
		{
			Pause();
			mplayer1.Stop();
			mplayer1.Close();
			MediaOpened = false;			
		}

		private void ucMedia_Loaded(object sender, RoutedEventArgs e)
		{
			

			/*this.Dispatcher.BeginInvoke(new Action(() => 
			{

				//SetupMediaInformation();
			}), DispatcherPriority.ApplicationIdle);*/
		}

		private void PlayMedia()
		{
			btnPlayPause.Content = "Pause";
			Paused = false;
			if (MediaOpened != true)
			{
				mplayer1.Stop();
				mplayer1.Source = null;
				mplayer1.Source = MediaPath;
				mplayer1.Play();
				Console.WriteLine("Media Source Reset - " + mplayer1.Source);
				this.Dispatcher.BeginInvoke(new Action(() =>
				{
					Console.WriteLine("SetupMedia - TabShown");
					SetupMediaInformation();
				}), DispatcherPriority.ContextIdle);
			}
			else
			{
				mplayer1.Play();
			}
			Console.WriteLine("Play");
		}
		public void SetSource()
		{
			//Thread.Sleep(100);

			//Thread.Sleep(100);
			//Dispatcher.Run();
		}
		public void TabShown()
		{
			txtFocusMePlease.Focus();
			Keyboard.Focus(txtFocusMePlease);
			if (mplayer1.Source != MediaPath)
			{
				//mplayer1.BeginInit();
				mplayer1.Source = MediaPath;
				Console.WriteLine("Media Source Set - " + mplayer1.Source);
				mplayer1.Play();
				//mplayer1.EndInit();
				//Dispatcher.Run();
			}
			//PlayMedia();
			//Dispatcher.Run();
			
			this.Dispatcher.BeginInvoke(new Action(() =>
			{
				Console.WriteLine("SetupMedia - TabShown");
				SetupMediaInformation();
			}), DispatcherPriority.ContextIdle); 
			//mplayer1.Play();
			/*
			this.Dispatcher.BeginInvoke(new Action(() =>
			{
				Console.WriteLine("SetupMedia - TabShown");
				SetupMediaInformation();
			}), DispatcherPriority.ApplicationIdle);*/			
		}
		public void SetEditorRow(LCDataRow dr, long RowID)
		{
			Row = RowID;
			TranscriptionRow = dr;
			MediaOpened = false;
			DetailsLoaded = false;
			LoadRelatedData();
			SetMedia();			
		}
		EditorSynchro edtSync;
		public void SetEditorSynchro(EditorSynchro sync)
		{
			edtSync = sync;
			sync.AddEditor(this);
		}
		public EditorSynchro GetEditorSynchro()
		{
			return edtSync;
		}

		private void grdMain_MouseUp(object sender, MouseButtonEventArgs e)
		{
			Keyboard.Focus(txtFocusMePlease);
			txtFocusMePlease.Focus();
			e.Handled = true;
		}

		private void ucMedia_MouseUp(object sender, MouseButtonEventArgs e)
		{
			if(e.ClickCount == 1)
			{
				txtFocusMePlease.Focus();
				Keyboard.Focus(txtFocusMePlease);
			}
		}




	}
}
