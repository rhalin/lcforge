﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeLynx.CustomData;
using NodeLynx.lib;
using NodeLynx.Module.Node;

namespace NodeLynx.Module.StoryFlow
{
	[DatatypeID(ID = "{0CF0BCD6-6891-47B6-B962-9B7957B96F75}", FQN = "com.dreamsmithfoundry.storyforge.story")]
	public class StoryFlowModule : NodeViewerModule
	{
		public StoryFlowModule()
		{
			SupportedNodes.Clear();
			Definitions.Clear();
			RegTypes = RegisteredTypes.DefaultTypes();
			Name = "Story Flow";
			ucStoryFlowNode n = new ucStoryFlowNode();
			ucSFAnnotation n2 = new ucSFAnnotation();
			ucSFDialog n3 = new ucSFDialog();
			ucSFDialogFragment n4 = new ucSFDialogFragment();
			ucSFHub n5 = new ucSFHub();
			ucActorWidget wid = new ucActorWidget();

	
			RegTypes.RegisterNode(n);
			SupportedNodes.Add(n);
			RegTypes.RegisterNode(n2);
			SupportedNodes.Add(n2);
			RegTypes.RegisterNode(n3);
			SupportedNodes.Add(n3);
			RegTypes.RegisterNode(n4);
			SupportedNodes.Add(n4);
			RegTypes.RegisterNode(n5);
			SupportedNodes.Add(n5);

			RegTypes.RegisterWidget(wid);

			AssetTypeClassifiers.Add(new Tuple<string,string>("",""));

			AssetTypeClassifiers.Add(new Tuple<string, string>(".bmp", "asset.image"));
			AssetTypeClassifiers.Add(new Tuple<string, string>(".png", "asset.image.compressed.lossless"));
			AssetTypeClassifiers.Add(new Tuple<string,string>(".jpg", "asset.image.compressed.lossy"));

			

			GenerateFlow();
		}
		protected new void GenerateFlow()
		{
			DatatypeDefinition def = new DatatypeDefinition();
			def.DisplayWidgetFQN = "widget.storyflow.actor";
			def.Initialize(RegTypes);
			def.DataName = "Actor";
			def.PrintableName = "Actor";
			def.FileName = "./defs/StoryFlow-Actor.def";
			def.ID = Guid.Parse("{56A7A194-0D00-4B92-991E-A7E0DF6A2AAC}");

			def.WidgetBindings.Add(new SerializableKeyValuePair<string, string>("name", "ActorName"));
			def.WidgetBindings.Add(new SerializableKeyValuePair<string, string>("description", "Description"));
			def.WidgetBindings.Add(new SerializableKeyValuePair<string, string>("icon", "ActorImage"));

			DatatypeField f = null;

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.short"));
			f.FieldName = "ActorName";
			f.FieldLabel = "Name";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{9F3426CE-1FBD-454A-87BB-BEEE5B40E9D1}"), f);
			Definitions.Add(def);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.reference.single"));
			f.FieldName = "ActorImage";
			f.FieldLabel = "Image";
			f.IsVisible = true;
			f.AllowedReferenceDatatypes.Add("asset.image");
			def.AddField(Guid.Parse("{AAA74AF0-F54D-4DB5-B666-13C3DFE646F4}"), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.long"));
			f.FieldName = "Description";
			f.FieldLabel = "Description";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{2A76105C-D0E4-499D-89A4-003AF72085AE}"), f);
			Definitions.Add(def);

			base.GenerateFlow();
		}
	}
}
