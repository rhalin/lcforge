﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.lib;
using NodeLynx.UI;
using WpfAnimatedGif;

namespace NodeLynx.Module.StoryFlow
{
	/// <summary>
	/// Interaction logic for ucWidget.xaml
	/// </summary>
	[DatatypeID(ID = "{1A06B50D-4BB7-45C6-B385-33D13EEB02B4}", FQN = "widget.storyflow.actor")]
	public partial class ucActorWidget : UserControl, IDataWidget
	{
		public string FQN
		{
			get
			{
				Type t = this.GetType();
				DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
				return did.FQN;
			}
			set
			{

			}
		}
		public event ReferenceDeletedHandler ReferenceDeleted;
		protected virtual void OnReferenceDeleted(EventArgs e)
		{
			if (ReferenceDeleted != null)
				ReferenceDeleted(this, e);
		}
		public ucActorWidget()
		{
			InitializeComponent();
			IconPath = "";
		}
		bool IsIcon = false;

		bool _AllowResize = true;
		bool _AllowDelete = true;
		public bool AllowResize { 
			get { return _AllowResize; } 
			set { 
				_AllowResize = value; 
				if(_AllowResize == true)
				{
					btnIconify.Visibility = System.Windows.Visibility.Visible;
				} else
				{
					btnIconify.Visibility = System.Windows.Visibility.Collapsed;
				}
			} 
		}
		private string IconPath
		{
			get;
			set;
		}
		public Dictionary<string, object> GetBindableFields()
		{
			Dictionary<string, object> dict = new Dictionary<string, object>();
			dict["name"] = txtShortText.Text;
			dict["description"] = txtLongText.Text;
			dict["icon"] = IconPath;
			return dict;
		}
		SerializableKeyValuePair<string, long> DataItem = null;
		public void SetBindings(Dictionary<string, object> binds)
		{
			if (binds.ContainsKey("name"))
			{
				txtShortText.Text = binds["name"].ToString();
				txtIconShortText.Text = binds["name"].ToString();
			}
			if (binds.ContainsKey("description"))
			{
				if (binds["description"].ToString().Length > 160)
				{
					txtLongText.Text = binds["description"].ToString().Substring(0, 160) + " ...";
				}
				else
				{
					txtLongText.Text = binds["description"].ToString();
				}
			}
			bool IconHandled = false;
			if (binds.ContainsKey("icon") && binds["icon"] != null)
			{
				object icon = binds["icon"];
				//must be a reference
				if (binds["icon"] is List<SerializableKeyValuePair<string, long>>)
				{

					List<SerializableKeyValuePair<string, long>> binding = (List<SerializableKeyValuePair<string, long>>)icon;
					//must be a reference to an asset
					if (binding.Count > 0 && binding[0].Key.ToLower() == DataProject.AssetLibraryID.ToLower())
					{
						//go get it
						LCDataRow dr = DataProject.CurrentProject.GetAssetLibrary().GotoRowID(binding[0].Value);
						if (dr != null)
						{
							//populate
							IconFilename = dr["AssetPath"].ToString();
							IconAssetID = dr["RowID"].ToString();
							string Extension = dr["Extension"].ToString();
							if (Extension == ".jpg"
								|| Extension == ".png"
								|| Extension == ".bmp"
								|| Extension == ".gif")
							{
								this.Dispatcher.BeginInvoke((Action)(() =>
								{
									BitmapImage logo = new BitmapImage();
									logo.BeginInit();
									logo.UriSource = new Uri("pack://siteoforigin:,,,/Resources/IconLoader.gif");
									logo.EndInit();

									ImageBehavior.SetAnimatedSource(imgIcon, logo);
									//imgIcon.Source = logo;

								}));
								DataProject.CurrentProject.GenerateIcon(this);
								IconHandled = true;
							}
						}
					}

				}

			}
			if (!IconHandled)
			{
				BitmapImage logo = new BitmapImage();
				logo.BeginInit();
				logo.UriSource = new Uri("pack://siteoforigin:,,,/Resources/DSFLogo_icon.png");
				logo.EndInit();
				imgIcon.Source = logo;
			}
		}


		public IDataWidget Create()
		{
			return new ucActorWidget();
		}
		public FrameworkElement GetContainer()
		{
			return (FrameworkElement)((FrameworkElement)gridContainer);
		}


		public IDataWidget Clone()
		{
			ucActorWidget wid = new ucActorWidget();
			wid.txtShortText.Text = txtShortText.Text;
			wid.txtLongText.Text = txtLongText.Text;
			wid.txtIconShortText.Text = txtIconShortText.Text;
			wid.DataItem = DataItem;
			wid.imgIcon.Source = imgIcon.Source;
			wid.DocumentManager = DocumentManager;
			wid.CurrentWindow = CurrentWindow;
			wid.ContentCanvas = ContentCanvas;
			//also copy the data item.... doesn't look like we're saving this, we need to
			return wid;
		}


		public void SetDataItem(SerializableKeyValuePair<string, long> obj)
		{
			DataItem = obj;
		}

		public SerializableKeyValuePair<string, long> GetDataItem()
		{
			return DataItem;
		}


		public void Iconify(bool ToIcon)
		{
			if (ToIcon)
			{
				this.Width = 120;
				gridContainer.Width = 120;
				IsIcon = true;
				btnIconify.Content = "+";
				iconTextBorder.Visibility = System.Windows.Visibility.Visible;
			}
			else
			{
				this.Width = 298;
				gridContainer.Width = 298;
				IsIcon = false;
				btnIconify.Content = "-";
				iconTextBorder.Visibility = System.Windows.Visibility.Collapsed;
			}
		}

		private void btnIconify_Click(object sender, RoutedEventArgs e)
		{
			if (IsIcon)
			{
				Iconify(false);
			}
			else
			{
				Iconify(true);
			}
		}



		private void btnDelete_Click(object sender, RoutedEventArgs e)
		{
			OnReferenceDeleted(null);
		}



		public bool AllowDelete
		{
			get { return _AllowDelete; }
			set
			{
				_AllowDelete = value;
				if (_AllowDelete == true)
				{

					btnDelete.Visibility = System.Windows.Visibility.Visible;
				}
				else
				{
					btnDelete.Visibility = System.Windows.Visibility.Collapsed;
				}
			} 
		}


		public bool IsAsset()
		{
			//if its in the asset library, it is an asset
			return DataItem.Key.ToLower() == DataProject.AssetLibraryID.ToLower() ? true : false;
		}

		public List<string> GetDatatypes()
		{
			//get instance
			if (IsAsset())
			{
				LCDataRow dr = (LCDataRow)DataProject.CurrentProject.GetAssetLibrary().Data.Rows.Find(DataItem.Value);
				if (dr != null)
				{
					List<SerializableKeyValuePair<string, string>> clas = DataProject.CurrentProject.GetAssetClassifiers(dr["Extension"].ToString());
					List<string> datatypes = new List<string>();
					foreach (SerializableKeyValuePair<string, string> classer in clas)
					{
						datatypes.Add(classer.Value);
					}
					return datatypes;
				}
				else
				{
					return null;
				}
			} else
			{
				DataInstance di = DataProject.CurrentProject.GetInstance(DataItem.Key);
				return di.Definition.GetInheritenceIDs();
			}
		}

		public void IconHasGenerated()
		{
			//throw new NotImplementedException();
			this.Dispatcher.BeginInvoke((Action)(() =>
			{



				Storyboard AnimFadeOut = new Storyboard();
				AnimFadeOut.Completed += AnimFadeOut_Completed;
				DoubleAnimation FadeOut = new DoubleAnimation();
				FadeOut.From = 1.0;
				FadeOut.To = 0.0;
				FadeOut.Duration = new Duration(TimeSpan.FromSeconds(1.0));

				AnimFadeOut.Children.Add(FadeOut);
				Storyboard.SetTargetName(FadeOut, imgIcon.Name);
				Storyboard.SetTargetProperty(FadeOut, new PropertyPath(Image.OpacityProperty));
				AnimFadeOut.Begin(this);
			}));
		}

		void AnimFadeOut_Completed(object sender, EventArgs e)
		{
			
			Storyboard AnimFadeIn = new Storyboard();
			DoubleAnimation FadeIn = new DoubleAnimation();
			FadeIn.From = 0.0;
			FadeIn.To = 1.0;
			FadeIn.Duration = new Duration(TimeSpan.FromSeconds(1.0));

			try
			{
				BitmapImage img = new BitmapImage();
				Directory.SetCurrentDirectory(CurrentDirectory);
				img.BeginInit();
				img.UriSource = new Uri("./icons/icon_" + IconAssetID + ".png", UriKind.Relative);
				img.CacheOption = BitmapCacheOption.OnLoad;
				img.EndInit();
				ImageBehavior.SetAnimatedSource(imgIcon, null);
				imgIcon.Source = img;
			} catch (Exception ex)
			{
				BitmapImage img = new BitmapImage();
				Directory.SetCurrentDirectory(CurrentDirectory);
				img.BeginInit();
				img.UriSource = new Uri("pack://siteoforigin:,,,/Resources/DSFLogo_icon.png", UriKind.Absolute);
				img.CacheOption = BitmapCacheOption.OnLoad;
				img.EndInit();
				ImageBehavior.SetAnimatedSource(imgIcon, null);
				imgIcon.Source = img;
			}
			AnimFadeIn.Children.Add(FadeIn);
			Storyboard.SetTargetName(FadeIn, imgIcon.Name);
			Storyboard.SetTargetProperty(FadeIn, new PropertyPath(Image.OpacityProperty));
			AnimFadeIn.Begin(this);
		}
		


		string _IconFilename = "";
		string _IconAssetID = "";
		public string IconFilename
		{
			get
			{
				return _IconFilename;
			}
			set
			{
				_IconFilename = value;
			}
		}

		public string IconAssetID
		{
			get
			{
				return _IconAssetID;
			}
			set
			{
				_IconAssetID = value;
			}
		}
		public string CurrentDirectory
		{
			get;
			set;
		}

		private void UserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if(DocumentManager != null)
			{
				//ucDataEditor ed = new ucDataEditor();
				//DataInstance inst = DataProject.CurrentProject.GetInstance(DataItem.Key);
				//ed.SetDataSource(inst.Definition, inst, DataItem.Value);
				//DocumentManager.OpenDocument(ed);

				ucDataEditor editor = new ucDataEditor();
				//DataFolder fld = (DataFolder)((TreeViewItem)sender).Tag;
				DataInstance di = DataProject.CurrentProject.GetInstance(DataItem.Key);
				EditorSynchro sync = new EditorSynchro(di, ContentCanvas, CurrentWindow, GetDataItem().Value);
				editor.SetEditorSynchro(sync);
				editor.SetDataSource(di.Definition, di);
				editor.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
				DocumentManager.OpenDocument(editor, sync);
				e.Handled = true;
			}
		}


		public INLDocumentManager DocumentManager
		{
			get;
			set;
		}
		public FrameworkElement CurrentWindow
		{
			get;
			set;
		}

		public Canvas ContentCanvas
		{
			get;
			set;
		}
	}
}
