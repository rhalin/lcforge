﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using NodeLynx.CustomData;
using NodeLynx.Node.NodeType;

namespace NodeLynx.Module.StoryFlow
{
	[DatatypeID(ID = "{52DFC7E2-49CD-4ED9-B781-A0AB35853409}", FQN = "node.story.annotation")]
	public class ucSFAnnotation : ucLFNode
	{
		ucSFAnnotationInterior AnnotInterior;
		public ucSFAnnotation()
			: base()
		{
			OnlySubmerged = false;
			NodeTypeName = "Annotation";
			AnnotInterior = new ucSFAnnotationInterior();
			MinHeight = 150;
			MinWidth = 200;
			MaxNextLinkers = 0;
			MaxPrevLinkers = 0;
			NodeColor = Colors.Black;
			ContentGrid.Children.Add(AnnotInterior);
			Grid.SetRow(AnnotInterior, 1);
			AnnotInterior.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
			AnnotInterior.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
			AnnotInterior.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
			AnnotInterior.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
		}
		public override void AddDefaultNextLinkers()
		{
		}
		public override void AddDefaultPrevLinkers()
		{
		}
		public override DatatypeDefinition GetNodeClassExtensionDataDefinition(RegisteredTypes RegTypes)
		{
			DatatypeDefinition def = base.GetNodeClassExtensionDataDefinition(RegTypes);
			def.DataName = "LFAnnotationExt";
			def.PrintableName = "Annotations";
			def.FileName = ""; //auto fill this
			def.ID = Guid.Parse("{F5EC04CF-9045-4B6D-A69C-13ED0903A280}");

			DatatypeField f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.long"));
			f.FieldName = "AnnotationText";
			f.FieldLabel = "Annotation";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{43374874-7CC9-4AFA-A6CB-99CC07230C3C}"), f);

			return def;
		}

		public override void SetDataSource(DataInstance di, LCDataRow BaseNodeInfo, LCDataRow extNodeInfo)
		{
			base.SetDataSource(di, BaseNodeInfo, extNodeInfo);
			AnnotInterior.Instance = di;
			AnnotInterior.BoundRow = extNodeInfo;
		}
	}
}
