﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;

namespace NodeLynx.Module.StoryFlow
{
	/// <summary>
	/// Interaction logic for ucSFAnnotationInterior.xaml
	/// </summary>
	public partial class ucSFAnnotationInterior : UserControl, INotifyPropertyChanged
	{
		public DataInstance Instance { get; set; }
		public ucSFAnnotationInterior()
		{
			InitializeComponent();
		}
		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}
		public static readonly DependencyProperty BoundRowProperty =
			DependencyProperty.Register("BoundRow", typeof(LCDataRow), typeof(ucSFAnnotationInterior));
		public LCDataRow BoundRow
		{
			set
			{
				SetValue(BoundRowProperty, value);
				OnPropertyChanged("BoundRow");
			}
			get { return (LCDataRow)GetValue(BoundRowProperty); }
		}
	}
}
