﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using NodeLynx.CustomData;
using NodeLynx.Node.NodeType;

namespace NodeLynx.Module.StoryFlow
{
	[DatatypeID(ID = "{C07349BF-4E85-4703-BC3E-9DB79276F80C}", FQN = "node.story.dialog")]
	public class ucSFDialog : ucLFNode
	{
		ucSFDialogInterior ctrlInterior;
		public ucSFDialog()
			: base()
		{
			OnlySubmerged = false;
			Submergable = true;
			NodeTypeName = "Dialog";
			ctrlInterior = new ucSFDialogInterior();
			MinHeight = 250;
			MinWidth = 300;
			ValidChildNodeTypes.Add(typeof(ucSFHub));
			ValidChildNodeTypes.Add(typeof(ucSFDialogFragment));
			ValidChildNodeTypes.Add(typeof(ucSFAnnotation));

			ucNodes.MouseDoubleClick += ucNodes_MouseDoubleClick;

			ContentGrid.Children.Add(ctrlInterior);
			Grid.SetRow(ctrlInterior, 1);
			ctrlInterior.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
			ctrlInterior.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
			ctrlInterior.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
			ctrlInterior.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
		}

		void ucNodes_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			//SUBMERGE!
			MyCanvas.Submerge(this);
		}
		public override DatatypeDefinition GetNodeClassExtensionDataDefinition(RegisteredTypes RegTypes)
		{
			DatatypeDefinition def = base.GetNodeClassExtensionDataDefinition(RegTypes);
			def.DataName = "LFDialogExt";
			def.FileName = ""; //auto fill this
			def.PrintableName = "Dialog";
			def.ID = Guid.Parse("{A8FAF1AE-67BA-41BE-95EA-556BCBB4A0BD}");

			DatatypeField f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.long"));
			f.FieldName = "DialogDescription";
			f.FieldLabel = "Description";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{5ECD4EEE-BF24-4F16-ABA0-EE8F4C7BB731}"), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.reference.multiple"));
			f.FieldName = "DialogReferences";
			f.FieldLabel = "References";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{E935C145-8A4E-42D6-97EB-0417831E1B04}"), f);

			return def;
		}

		public override void SetDataSource(DataInstance di, LCDataRow BaseNodeInfo, LCDataRow extNodeInfo)
		{
			base.SetDataSource(di, BaseNodeInfo, extNodeInfo);
			ctrlInterior.Instance = di;
			ctrlInterior.BoundRow = extNodeInfo;
		}
	}

}
