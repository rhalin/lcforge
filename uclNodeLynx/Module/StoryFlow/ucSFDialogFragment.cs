﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using NodeLynx.CustomData;
using NodeLynx.Node.NodeType;

namespace NodeLynx.Module.StoryFlow
{
	[DatatypeID(ID = "{C8F5F070-3C16-4C30-8D67-8DA8C04185C9}", FQN = "node.story.dialog.fragment")]
	public class ucSFDialogFragment : ucLFNode
	{
		ucSFDialogFragmentInterior ctrlInterior;
		public ucSFDialogFragment()
			: base()
		{
			OnlySubmerged = true;			
			NodeTypeName = "Flow Fragment";
			ctrlInterior = new ucSFDialogFragmentInterior();
			MinHeight = 280;
			MinWidth = 300;
			ContentGrid.Children.Add(ctrlInterior);
			Grid.SetRow(ctrlInterior, 1);
			ctrlInterior.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
			ctrlInterior.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
			ctrlInterior.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
			ctrlInterior.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
		}
		public override DatatypeDefinition GetNodeClassExtensionDataDefinition(RegisteredTypes RegTypes)
		{
			DatatypeDefinition def = base.GetNodeClassExtensionDataDefinition(RegTypes);
			def.DataName = "LFDialogFragmentExt";
			def.FileName = ""; //auto fill this
			def.ID = Guid.Parse("{E9F43AA0-B23B-453C-9B6E-B888CBC96CC0}");
			def.PrintableName = "Dialog Fragment";

			DatatypeField  f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.reference.single"));
			f.FieldName = "Actor";
			f.FieldLabel = "Actor";
			f.IsVisible = true;
			//todo - set this for the actor field
			f.AllowedReferenceDatatypes.Add("56A7A194-0D00-4B92-991E-A7E0DF6A2AAC".ToLower());
			def.AddField(Guid.Parse("{750622C0-F12D-4FB0-8436-E075186EABF0}"), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.long"));
			f.FieldName = "StageDirections";
			f.FieldLabel = "Stage Directions";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{164E4FC4-B27F-43B2-8613-81390E4322B4}"), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.long"));
			f.FieldName = "DialogPreview";
			f.FieldLabel = "Preview";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{4C198093-2A62-4C13-AE57-C07E9A672B91}"), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.long"));
			f.FieldName = "DialogLine";
			f.FieldLabel = "Dialog";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{A3D4DB05-B854-44A8-A373-4F266563A24E}"), f);


			return def;
		}
		public override void SetDataSource(DataInstance di, LCDataRow BaseNodeInfo, LCDataRow extNodeInfo)
		{
			base.SetDataSource(di, BaseNodeInfo, extNodeInfo);
			ctrlInterior.Instance = di;
			ctrlInterior.BoundRow = extNodeInfo;
		}
	}
}
