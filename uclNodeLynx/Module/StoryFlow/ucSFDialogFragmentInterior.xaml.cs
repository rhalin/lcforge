﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;

namespace NodeLynx.Module.StoryFlow
{
	/// <summary>
	/// Interaction logic for ucSFDialogFragmentInterior.xaml
	/// </summary>
	public partial class ucSFDialogFragmentInterior : UserControl, INotifyPropertyChanged
	{
		public DataInstance Instance { get; set; }
		public ucSFDialogFragmentInterior()
		{
			InitializeComponent();
			refActor.PropertyChanged += nodeRefs_PropertyChanged;
		}

		private void tmbDialogSplitter_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{

		}
		void nodeRefs_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			BoundRow["Actor_ref"] = refActor.GetRefences();
		}
		public static readonly DependencyProperty BoundRowProperty =
			DependencyProperty.Register("BoundRow", typeof(LCDataRow), typeof(ucSFDialogFragmentInterior));
		public LCDataRow BoundRow
		{
			set
			{
				SetValue(BoundRowProperty, value);
				OnPropertyChanged("BoundRow");
				//refActor.SetData(Instance, value);
				refActor.PopulateControlWithData(value["Actor_ref"]);
				
			}
			get { return (LCDataRow)GetValue(BoundRowProperty); }
		}


		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}
	}
}
