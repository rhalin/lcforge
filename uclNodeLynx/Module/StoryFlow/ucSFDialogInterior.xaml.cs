﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;

namespace NodeLynx.Module.StoryFlow
{
	/// <summary>
	/// Interaction logic for ucSFDialogInterior.xaml
	/// </summary>
	public partial class ucSFDialogInterior : UserControl, INotifyPropertyChanged
	{
		public ucSFDialogInterior()
		{
			InitializeComponent();
			refReferences.PropertyChanged += nodeRefs_PropertyChanged;
		}

		public DataInstance Instance { get; set; }

		void nodeRefs_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			BoundRow["DialogReferences_ref"] = refReferences.GetRefences();
		}
		public static readonly DependencyProperty BoundRowProperty =
			DependencyProperty.Register("BoundRow", typeof(LCDataRow), typeof(ucSFDialogInterior));
		public LCDataRow BoundRow
		{
			set {
				SetValue(BoundRowProperty, value); 
				OnPropertyChanged("BoundRow");
				//refReferences.PopulateControlWithData(value);
				refReferences.PopulateControlWithData(value["DialogReferences_ref"]);
				txtDescription.DataContext = value;
			}
			get { return (LCDataRow)GetValue(BoundRowProperty); }
		}


		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}
	}
}
