﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using NodeLynx.CustomData;
using NodeLynx.Node.NodeType;

namespace NodeLynx.Module.StoryFlow
{
	[DatatypeID(ID = "{0DAD48C4-7131-414E-884C-C4320AE645A2}", FQN = "node.story.hub")]
	public class ucSFHub : ucLFNode
	{
		public ucSFHub()
			: base()
		{
			OnlySubmerged = true;
			NodeTypeName = "Dialog Hub";
			MinHeight = 50;
			MaxHeight = 50;
			MinWidth = 300;
			Width = 300;
			Height = 50;
			MaxNextLinkers = 1;
			MaxPrevLinkers = 1;
			Resizable = false;
		}
		public override DatatypeDefinition GetNodeClassExtensionDataDefinition(RegisteredTypes RegTypes)
		{
			DatatypeDefinition def = base.GetNodeClassExtensionDataDefinition(RegTypes);
			def.DataName = "LFHubExt";
			def.PrintableName = "Dialog Hub";
			def.FileName = ""; //auto fill this
			def.ID = Guid.Parse("{CEA6D117-A973-4495-A9DC-9E1313446B68}");

			return def;
		}
		public override void SetDataSource(DataInstance di, LCDataRow BaseNodeInfo, LCDataRow extNodeInfo)
		{
			base.SetDataSource(di, BaseNodeInfo, extNodeInfo);
		}
	}
}
