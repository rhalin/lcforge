﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using NodeLynx.CustomData;
using NodeLynx.Node.NodeType;

namespace NodeLynx.Module.StoryFlow
{
	[DatatypeID(ID = "{0C70AA87-67ED-41E2-A500-85479DDAF083}", FQN = "node.story.flow")]
	public class ucStoryFlowNode : ucLFNode
	{
		ucStoryFlowNodeInterior storyFlowNodeInterior;
		public ucStoryFlowNode() : base()
		{
			OnlySubmerged = false;
			NodeTypeName = "Story Flow Part";
			storyFlowNodeInterior = new ucStoryFlowNodeInterior();
			MinHeight = 250;
			MinWidth = 300;
			ContentGrid.Children.Add(storyFlowNodeInterior);
			Grid.SetRow(storyFlowNodeInterior, 1);
			storyFlowNodeInterior.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
			storyFlowNodeInterior.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
			storyFlowNodeInterior.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
			storyFlowNodeInterior.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
		}
		public override DatatypeDefinition GetNodeClassExtensionDataDefinition(RegisteredTypes RegTypes)
		{
			DatatypeDefinition def = base.GetNodeClassExtensionDataDefinition(RegTypes);
			def.DataName = "LFStoryFlowNodeExt";
			def.PrintableName = "Base Flow Node";
			def.FileName = ""; //auto fill this
			def.ID = Guid.Parse("{8DEC128C-5CA0-418B-AB67-77ED2295C6A9}");

			DatatypeField f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.text.long"));
			f.FieldName = "NodeText";
			f.FieldLabel = "Text";
			f.IsVisible = true;
			def.AddField(Guid.Parse("{9A835CE5-2CAE-4AFB-B198-391BB79B6B57}"), f);

			f = new DatatypeField();
			f.SetDatatype(RegTypes.GetDatatype("data.reference.multiple"));
			f.FieldLabel = "Attached Items";
			f.FieldName = "Attached";
			def.AddField(Guid.Parse("{83E2DD8B-A1A2-4778-8C47-711B8D0919B0}"), f);
			return def;
		}
		public override void SetDataSource(DataInstance di, LCDataRow BaseNodeInfo, LCDataRow extNodeInfo)
		{
			base.SetDataSource(di, BaseNodeInfo, extNodeInfo);
			storyFlowNodeInterior.Instance = di;
			storyFlowNodeInterior.BoundRow = extNodeInfo;
		}
	}
}
