﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;
using NodeLynx.Node.NodeType;

namespace NodeLynx.Module.StoryFlow
{
	/// <summary>
	/// Interaction logic for ucStoryFlowNodeInterior.xaml
	/// </summary>
	public partial class ucStoryFlowNodeInterior : UserControl, INotifyPropertyChanged
	{
		public DataInstance Instance { get; set; }
		public ucStoryFlowNodeInterior()
		{
			InitializeComponent();
			nodeRefs.PropertyChanged += nodeRefs_PropertyChanged;
		}

		void nodeRefs_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			BoundRow["Attached_ref"] = nodeRefs.GetRefences();
		}
		public static readonly DependencyProperty SelectedNodeProperty =
			DependencyProperty.Register("BoundRow", typeof(LCDataRow), typeof(ucStoryFlowNodeInterior));
		public LCDataRow BoundRow
		{
			set { 
				SetValue(SelectedNodeProperty, value); 
				OnPropertyChanged("BoundRow");
				nodeRefs.SetData(Instance, value);
				nodeRefs.PopulateControlWithData(value["Attached_ref"]); 
			}
			get { return (LCDataRow)GetValue(SelectedNodeProperty); }
		}


		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}
	}
}
