﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeLynx.UI.Dialogs
{
	public interface INLDialog
	{
		bool Validate();
		string DialogTitle();
		void SetPrompt(string txt);
	}
}
