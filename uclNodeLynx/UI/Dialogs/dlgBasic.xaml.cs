﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NodeLynx.UI.Dialogs
{
	/// <summary>
	/// Interaction logic for dlgBasic.xaml
	/// </summary>
	public partial class dlgBasic : Window
	{
		public dlgBasic()
		{
			InitializeComponent();
		}
		public void SetDialogControl(INLDialog dlg)
		{
			grdCustomDialog.Children.Clear();
			grdCustomDialog.Children.Add((FrameworkElement)dlg);
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			this.DialogResult = true;
		}
	}
}
