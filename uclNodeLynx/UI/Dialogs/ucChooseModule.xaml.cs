﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;
using NodeLynx.Module;
using NodeLynx.Module.AssetViewer;
using NodeLynx.Module.Node;
using NodeLynx.Module.QualCode;
using NodeLynx.Module.StoryFlow;

namespace NodeLynx.UI.Dialogs
{
	/// <summary>
	/// Interaction logic for ucChooseModule.xaml
	/// </summary>
	public partial class ucChooseModule : UserControl, INLDialog
	{
		List<ICustomModule> Modules = new List<ICustomModule>();
		public ucChooseModule()
		{
			InitializeComponent();
			Modules.Add(new AssetViewerModule());
			Modules.Add(new DefaultModule());
			Modules.Add(new BaseModule());
			Modules.Add(new QualCodingModule());
			Modules.Add(new NodeViewerModule());
			Modules.Add(new StoryFlowModule());
			string modPath = new Uri(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath + "/Modules/";
			string[] fileEntries = Directory.GetFiles((new Uri(modPath).LocalPath));
			foreach (string fileName in fileEntries)
			{
				//load each 
				Assembly module = Assembly.LoadFile(fileName);
				Type[] Types = module.GetTypes();
				foreach (Type t in Types)
				{
					if (typeof(ICustomModule).IsAssignableFrom(t))
					{
						ICustomModule custMod = (ICustomModule)Activator.CreateInstance(t);
						Modules.Add(custMod);
					}
				}
			}
			PopulateList();
		}
		public void PopulateList()
		{
			//cboModules.Items.Clear();
			lstModules.Items.Clear();
			foreach (ICustomModule mod in Modules)
			{
				Type t = mod.GetType();
				DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
				ListBoxItem  li = new ListBoxItem ();
				li.Tag = mod;
				li.Content = did.FQN;
				//cboModules.Items.Add(li);
				lstModules.Items.Add(li);				
			}
			//cboModules.SelectedIndex = 0;
		}
		public List<string> SelectedModules()
		{
			List<string> selectedMods = new List<string>();
			foreach(ListBoxItem li in lstModules.SelectedItems)
			{
				selectedMods.Add(((ICustomModule)(li).Tag).GetFQN());
			}
			if(selectedMods.Count == 0)
			{
				selectedMods.Add(Modules[0].GetFQN());
			}
			return selectedMods;
		
		}
		public ItemCollection GetModules()
		{
			return lstModules.Items;
		}
		public bool Validate()
		{
			return true;
		}

		public string DialogTitle()
		{
			return "Choose a module for your project";
		}


		public void SetPrompt(string txt)
		{
			throw new NotImplementedException();
		}


	}
}
