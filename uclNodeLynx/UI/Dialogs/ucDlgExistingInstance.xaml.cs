﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;

namespace NodeLynx.UI.Dialogs
{
	/// <summary>
	/// Interaction logic for ucDlgExistingInstance.xaml
	/// </summary>
	public partial class ucDlgExistingInstance : UserControl, INLDialog
	{

		public List<DatatypeDefinition> InstanceTypes
		{
			get;
			set;
		}
		public DataInstance SelectedInstance
		{
			get;
			set;
		}

		public ucDlgExistingInstance()
		{
			InitializeComponent();
			InstanceTypes = new List<DatatypeDefinition>();
			InstancesOfType = new List<Tuple<string, DataInstance>>();
			//lstInstances.DataContext = this;
		}
		public void Initialize()
		{
			cboDatatypeDefinition.Items.Clear();
			foreach(DatatypeDefinition def in InstanceTypes)
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = def.PrintableName;
				item.Tag = def;
				cboDatatypeDefinition.Items.Add(item);
			}
			cboDatatypeDefinition.SelectedIndex = 0;
			PopulateInstances();
		}


		public bool Validate()
		{
			if (lstInstances.SelectedIndex != -1)
				return true;
			return false;
		}

		public string DialogTitle()
		{
			return "Select a data instance";
		}

		public void SetPrompt(string txt)
		{
			
		}

		private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			PopulateInstances();
		}
		public List<Tuple<string, DataInstance>> InstancesOfType
		{
			get;
			set;
		}
		public DataInstance GetValue()
		{
			if(lstInstances.SelectedItem != null)
			{
				return (DataInstance)((ListBoxItem)lstInstances.SelectedItem).Tag;
			} else
			{
				return null;
			}
		}
		private void PopulateInstances()
		{
			InstancesOfType = DataProject.CurrentProject.GetInstancesByType(((DatatypeDefinition)((ComboBoxItem)cboDatatypeDefinition.SelectedItem).Tag).IDStr);
			lstInstances.Items.Clear();
			foreach(Tuple<string, DataInstance> inst in InstancesOfType)
			{
				ListBoxItem li = new ListBoxItem();
				li.Content = inst.Item2.LibraryName == "" ? "[Unnamed Instance] : " + inst.Item2.ID.ToString() : inst.Item2.LibraryName;
				li.Tag = inst.Item2;
				lstInstances.Items.Add(li);
			}
		}
	}
}
