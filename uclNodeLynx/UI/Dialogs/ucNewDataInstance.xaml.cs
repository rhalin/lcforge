﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;

namespace NodeLynx.UI.Dialogs
{
	/// <summary>
	/// Interaction logic for ucNewDataInstance.xaml
	/// </summary>
	public partial class ucNewDataInstance : UserControl, INLDialog
	{
		public ucNewDataInstance()
		{
			InitializeComponent();
			PopulateDefinitions();
		}
		void PopulateDefinitions()
		{
			cboDefinitions.Items.Clear();
			foreach (DatatypeDefinition def in DataProject.CurrentProject.Definitions)
			{
				//Type t = def.GetType();
				//DatatypeID did = (DatatypeID)Attribute.GetCustomAttribute(t, typeof(DatatypeID));
				ComboBoxItem li = new ComboBoxItem();
				li.Tag = def;
				li.Content = def.PrintableName;
				cboDefinitions.Items.Add(li);
			}
			if(cboDefinitions.Items.Count > 0)
			{
				cboDefinitions.SelectedIndex = 0;
			}
		}
		public DatatypeDefinition GetDefinition()
		{
			return (DatatypeDefinition)((ComboBoxItem)cboDefinitions.SelectedItem).Tag;
		}
		public string LibraryName()
		{
			return txtLibraryName.Text;
		}
		public bool Validate()
		{
			return (cboDefinitions.Items.Count > 0) && (txtLibraryName.Text != "") ? true : false;
		}

		public string DialogTitle()
		{
			return "Please choose a definition for this library";
		}

		public void SetPrompt(string txt)
		{
			throw new NotImplementedException();
		}
	}
}
