﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NodeLynx.UI.Dialogs
{
	/// <summary>
	/// Interaction logic for ucNewFolder.xaml
	/// </summary>
	public partial class ucNewItem : UserControl, INLDialog
	{
		public ucNewItem()
		{
			InitializeComponent();
		}

		public bool Validate()
		{
			return txtItemName.Text == "" ? false : true;
		}

		public string DialogTitle()
		{
			return "New Item";
		}


		public void SetPrompt(string txt)
		{
			lblPrompt.Content = txt;
		}

		public string GetItemName()
		{
			return txtItemName.Text;
		}
	}
}
