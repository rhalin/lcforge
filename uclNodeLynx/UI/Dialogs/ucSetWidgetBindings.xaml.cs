﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.lib;

namespace NodeLynx.UI.Dialogs
{
	/// <summary>
	/// Interaction logic for ucSetWidgetBindings.xaml
	/// </summary>
	public partial class ucSetWidgetBindings : UserControl, INLDialog, INotifyPropertyChanged
	{
		DatatypeDefinition Definition = null;
		List<string> _WidgetFields = new List<string>();
		List<string> _DefinitionFields = new List<string>();
		string WidgetFQN { get; set; }
		//string _SelectedDefinitionField = "";
		public string SelectedDefinitionField
		{
			get
			{
				//if(_SelectedDefinitionField == "")
				//{
				//	return " [NONE] ";
				//} else
				//{
					//return _SelectedDefinitionField;
					for (int i = 0; i < Definition.WidgetBindings.Count; i++)
					{
						if (Definition.WidgetBindings[i].Key == _SelectedField)
						{
							return Definition.WidgetBindings[i].Value;
						}
					}
					return " [NONE] ";
				//}
			}
			set
			{
				string val = "";
				if(value == " [NONE] ")
				{
					val = "";
				} else
				{
					val = value;

				}
				if (Definition != null)
				{
					bool found = false;
					for (int i = 0; i < Definition.WidgetBindings.Count; i++)
					{
						if (Definition.WidgetBindings[i].Key == _SelectedField)
						{
							found = true;
							Definition.WidgetBindings[i].Value = val;
						}
					}
					if (!found)
					{
						SerializableKeyValuePair<string, string> binding = new SerializableKeyValuePair<string, string>();
						binding.Key = _SelectedField;
						binding.Value = "";
						Definition.WidgetBindings.Add(binding);
						SelectedDefinitionField = val;
					}
					RaisePropertyChanged("SelectedDefinitionField");
				}
			}
		}
		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			// take a copy to prevent thread issues
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		public List<string> DefinitionFields 
		{
			get { return _DefinitionFields; }
			set { }
		}
		IDataWidget Widget = null;
		string _SelectedField = "";
		public string SelectedField
		{
			get
			{
				return _SelectedField;
			}
			set
			{
				_SelectedField = value;
				if(_SelectedField != "")
				{
					//populate
					if(Definition != null)
					{
						bool found = false;
						for (int i = 0; i < Definition.WidgetBindings.Count; i++)
						{
							if(Definition.WidgetBindings[i].Key == _SelectedField)
							{
								found = true;
								if (Definition.WidgetBindings[i].Value == "")
								{
									SelectedDefinitionField = " [NONE] ";
								}
								else
								{
									SelectedDefinitionField = Definition.WidgetBindings[i].Value;
								}
							}
						}
						if(!found)
						{
							SerializableKeyValuePair<string, string> binding = new SerializableKeyValuePair<string, string>();
							binding.Key = _SelectedField;
							binding.Value = "";
							Definition.WidgetBindings.Add(binding);
							SelectedDefinitionField = " [NONE] ";
						}
						RaisePropertyChanged("SelectedField");
					}
				}
			}
		}
		public List<string> WidgetFields
		{
			get { return _WidgetFields; }
			set { _WidgetFields = value; }
		}
		public void SetDefinition(DatatypeDefinition def)
		{
			Definition = def;
			Widget = Definition.DisplayWidget;
			_DefinitionFields.Clear();
			_DefinitionFields.Add(" [NONE] ");
			foreach(SerializableKeyValuePair<Guid,DatatypeField> field in Definition.Fields)
			{
				_DefinitionFields.Add(field.Value.FieldName);
			}

			WidgetFields = new List<string>();
			foreach(KeyValuePair<string, object> bindfs in Definition.DisplayWidget.GetBindableFields())
			{
				WidgetFields.Add(bindfs.Key);
			}

			cboDefinitionFields.DataContext = this;
			lstWidgetFields.DataContext = this;
		}
		public ucSetWidgetBindings()
		{
			InitializeComponent();
		}

		public bool Validate()
		{
			throw new NotImplementedException();
		}

		public string DialogTitle()
		{
			throw new NotImplementedException();
		}

		public void SetPrompt(string txt)
		{
			throw new NotImplementedException();
		}
	}
}
