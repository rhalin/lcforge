﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;

namespace NodeLynx.UI
{
	public interface INLDocumentManager
	{
		void OpenDocument(INLTabClient ctrl, EditorSynchro sync=null);
		void RegisterDocumentEditor(Type EditorType);
		void OpenWidgets(FrameworkElement CurrentWindow, Canvas ContentPane, DataFolder df);
		//ObjectType instance = (ObjectType)Activator.CreateInstance(objectType);
	}
}
