﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;

namespace NodeLynx.UI
{
	public interface INLTabClient
	{
		UserControl GetControl();
		string GetTabName();
		string GetButtonName();
		void OnParentSizeChanged(object sender, SizeChangedEventArgs e);
		void SetDragDropManager(DragDropManager dragdropManager);

		List<DatatypeDefinition> SupportedEditorFor();
		/// <summary>
		/// Editors will always need a definition (for example, a definition editor), but they will not always need an instance, or a rowid
		/// </summary>
		/// <param name="def"></param>
		/// <param name="di"></param>
		/// <param name="RowID"></param>
		/// <returns>The FrameworkElement that will be added to the tab</returns>
		FrameworkElement SetDataSource(DatatypeDefinition def, DataInstance di = null, long RowID = -1);
		DatatypeDefinition GetDatatypeDefinition();
		DataInstance GetDataInstance();
		long GetRowID();

		void TabShown();

		void SetEditorRow(LCDataRow dr, long RowID);
		
		//for any instance where editors should be kept in sync (share the same selected row), they will share a synchro object
		void SetEditorSynchro(EditorSynchro sync);
		EditorSynchro GetEditorSynchro();

		//this will need to be handled differently later.  We probably don't want to just send in a canvas to populate
		void SetupToolbar(Canvas toolbar);
	}
}
