﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media;

namespace NodeLynx.UI
{
	public interface INLTreeViewItem
	{
		string GetTreeviewItemText();
		Color GetItemColor();
		void MouseDoubleClick(object sender, MouseButtonEventArgs e);
	}
}
