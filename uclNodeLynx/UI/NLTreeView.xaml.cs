﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using NodeLynx.CustomData;
using NodeLynx.CustomData.DataTypes;
using NodeLynx.CustomData.UI;
using NodeLynx.lib;
using NodeLynx.UI.Dialogs;

namespace NodeLynx.UI
{
	/// <summary>
	/// Interaction logic for NLTreeView.xaml
	/// </summary>
	public partial class NLTreeView : UserControl
	{
		DataProject Project;
		public INLDocumentManager docManager;
		public ucWidgetPanel2 CurrentWidgetPanel;
		public Dictionary<DataFolder, TreeViewItem> TVItemLookup = new Dictionary<DataFolder, TreeViewItem>();
		private Canvas ContentCanvas;
		private FrameworkElement CurrentWindow;
		public NLTreeView(Canvas contentCanvas, FrameworkElement currWindow)
		{
			InitializeComponent();
			ContentCanvas = contentCanvas;
			CurrentWindow = currWindow;
		}
		public void Reset()
		{
			trFolders.Items.Clear();
		}
		public void SetProject(DataProject proj)
		{
			Project = proj;
			DataFolder root = Project.RootFolder;
			AddFolder(root);
		}
		public TreeViewItem AddFolder(DataFolder at, DataFolder parent)
		{
			TreeViewItem tvi = AddFolder(at, TVItemLookup[parent]);
			tvi.BringIntoView();
			return tvi;
		}
		public void ClearChildren(DataFolder parent)
		{
			if (TVItemLookup.ContainsKey(parent))
			{
				TVItemLookup[parent].Items.Clear();
			}
		}
		public TreeViewItem AddFolder(DataFolder at, TreeViewItem parent = null)
		{
			TreeViewItem item = null;

			if (at == DataProject.CurrentProject.RootFolder)
			{
			}
			else
			{
				item = new TreeViewItem();
				TVItemLookup[at] = item;
				item.Header = at.FolderName;
				if (at.SystemFolder)
					item.Foreground = new SolidColorBrush(Colors.Red);
				else
					item.Foreground = new SolidColorBrush(Colors.White);
				if (parent == null)
					trFolders.Items.Add(item);
				else
					parent.Items.Add(item);

				if (at.Tag is DataInstance)
				{
					//item.PreviewMouseDoubleClick += library_MouseDoubleClick;
					item.MouseUp += item_MouseUp;
					item.Tag = at;
					item.Foreground = new SolidColorBrush(Colors.Yellow);
					item.ContextMenu = new ContextMenu();
					item.ContextMenu.Visibility = System.Windows.Visibility.Collapsed;
				}
				else if (at.Tag is NLDataType)
				{
					item.Foreground = new SolidColorBrush(Colors.LimeGreen);
				}
				else if (at.Tag is DatatypeDefinition)
				{
					item.Tag = at;
					item.MouseDoubleClick += definition_MouseDoubleClick;

					//addmenu: create library

				}
				else if (at.Tag is DataCollection)
				{
					item.Tag = at;
					item.AllowDrop = true;
					item.Drop +=Asset_Drop;
					item.ContextMenu = new ContextMenu();
					item.Selected +=AssetFolder_Selected;
					MenuItem menuItemAssetImport = new MenuItem();
					menuItemAssetImport.Tag = item;
					menuItemAssetImport.Header = "Import Asset Here";
					menuItemAssetImport.Click += assetImport_Click;
					item.ContextMenu.Items.Add(menuItemAssetImport);

					menuItemAssetImport = new MenuItem();
					menuItemAssetImport.Tag = item;
					menuItemAssetImport.Header = "New Asset Folder";
					menuItemAssetImport.Click += createAssetFolder_Click;
					item.ContextMenu.Items.Add(menuItemAssetImport);
				}
				else if (!at.SystemFolder && !at.IsLibrary)//basic folder
				{
					item.Tag = at;
					item.ContextMenu = new ContextMenu();
					item.Selected += basicFolder_Selected;
					if(at.FolderType == DataFolder.NLFOLDERTYPE.LIBRARY_ROW)
					{
						item.MouseUp += LibraryRow_MouseUp;
					}
					MenuItem menuItemAssetImport = new MenuItem();
					menuItemAssetImport.Tag = item;
					menuItemAssetImport.Header = "Create Subfolder";
					menuItemAssetImport.Click += createSubFolder_Click;
					item.ContextMenu.Items.Add(menuItemAssetImport);

					menuItemAssetImport = new MenuItem();
					menuItemAssetImport.Tag = item;
					menuItemAssetImport.Header = "Create Library";
					menuItemAssetImport.Click += createLibrary_Click;
					item.ContextMenu.Items.Add(menuItemAssetImport);
				}
			}
			foreach (SerializableKeyValuePair<long, string> content in at.Contents)
			{
				AddItem(content, item);
			}
			foreach(DataFolder fld in at.Subfolders)
			{
				AddFolder(fld, item);
			}

			return item;
		}

		private void LibraryRow_MouseUp(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
		}

		private void basicFolder_Selected(object sender, RoutedEventArgs e)
		{
			if (sender is TreeViewItem && sender == e.OriginalSource)
			{
				TreeViewItem tvi = (TreeViewItem)sender;
				DataFolder df = (DataFolder)tvi.Tag;
				docManager.OpenWidgets(CurrentWindow, ContentCanvas, df);
				e.Handled = true;
			}
		}



		private void Asset_Drop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				// Note that you can have more than one file.
				string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
				TreeViewItem tvi = (TreeViewItem)sender;
				// Assuming you have one file that you care about, pass it off to whatever
				// handling code you have defined.
				//HandleFileOpen(files[0]);
				for(int i = 0; i < files.Count(); i++)
				{
					FileAttributes attr = File.GetAttributes(files[i]);

					//detect whether its a directory or file
					if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
					{
						//todo:if directory, we'll want to recurse, adding the directory name and each subdirectory as well
						DirectoryInfo di = new DirectoryInfo(files[i]);
						CopyAssetDirectory(di, tvi, (DataFolder)tvi.Tag);
					}
					else
					{

						SerializableKeyValuePair<long, string> pair = null;
						pair = DataProject.CurrentProject.ImportAsset(files[i], (DataFolder)tvi.Tag);
						if (pair != null)
						{
							AddItem(pair, tvi);
						}
					}
				}
				e.Handled = true;
			}
		}
		private void CopyAssetDirectory(DirectoryInfo root, TreeViewItem CurrentTVI, DataFolder CurrentDataFolder)
		{
			//create new treeview item and step into it
			DataFolder df = DataProject.CurrentProject.AddAssetFolder(root.Name,
				CurrentDataFolder,
				(DataCollection)CurrentDataFolder.Tag);
			CurrentTVI = AddFolder(df, CurrentDataFolder);
			//step up for next recurse
			CurrentDataFolder = df;


			//add files
			FileInfo[] files = null;
			DirectoryInfo[] subDirs = null;


			try
			{
				files = root.GetFiles("*.*");
			}
			// This is thrown if even one of the files requires permissions greater 
			// than the application provides. 
			catch (UnauthorizedAccessException e)
			{
				// This code just writes out the message and continues to recurse. 
				// You may decide to do something different here. For example, you 
				// can try to elevate your privileges and access the file again.
				//log.Add(e.Message);
			}
			catch (DirectoryNotFoundException e)
			{
				Console.WriteLine(e.Message);
			}
			if(files != null)
			{
				//directories first
				subDirs = root.GetDirectories();

				foreach (System.IO.DirectoryInfo dirInfo in subDirs)
				{
					// Resursive call for each subdirectory.
					CopyAssetDirectory(dirInfo, CurrentTVI, CurrentDataFolder);
				}

				foreach (FileInfo fi in files)
				{
					// In this example, we only access the existing FileInfo object. If we 
					// want to open, delete or modify the file, then 
					// a try-catch block is required here to handle the case 
					// where the file has been deleted since the call to TraverseTree().
					//Console.WriteLine(fi.FullName);
					SerializableKeyValuePair<long, string> pair = null;
					pair = DataProject.CurrentProject.ImportAsset(fi.FullName, (DataFolder)CurrentTVI.Tag);
					if (pair != null)
					{
						AddItem(pair, CurrentTVI);
					}
				}
			}
		}

		private void createLibrary_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucNewDataInstance ndi = new ucNewDataInstance();
			chooseModule.SetDialogControl(ndi);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ndi.Validate())
				{
					DataFolder parentfld = ((DataFolder)((TreeViewItem)trFolders.SelectedItem).Tag);

					if (parentfld.ContainsFolder(ndi.LibraryName()))
					{
						MessageBox.Show("Library names must be unique");
					}
					else
					{
						string name = ndi.LibraryName();
						DataFolder df = DataProject.CurrentProject.CreateLibrary(name, ndi.GetDefinition(), parentfld);
						df.IsLibrary = true;
						TreeViewItem tvi = AddFolder(df, parentfld);

						//add child instances
						//they don't exist yet... /sigh
						//DataInstance tmpIn = (DataInstance)df.Tag;
						//foreach(SerializableKeyValuePair<Guid,DatatypeField> field in tmpIn.Definition.Fields)
						//{
						//	if (field.Value.DataTypeInfo is NLDataInstance)
						//	{
						//		tmpIn.Data.Rows[0];
						//		DataInstance childInstance = DataProject.CurrentProject.GetInstance();
						//		CreateChildInstanceFolders(name, childInstance, df);
						//	}
						//}
					}
				}
				else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}
		}
		//private void CreateChildInstanceFolders(string LibraryName, DataInstance tempIn, DataFolder CurrentParentFolder)
		//{
		//	CurrentParentFolder = DataProject.CurrentProject.CreateLibraryFolder(LibraryName, CurrentParentFolder, tempIn);
		//	foreach (SerializableKeyValuePair<Guid, DatatypeField> field in tempIn.Definition.Fields)
		//	{
		//		if (field.Value.DataTypeInfo is NLDataInstance)
		//		{
		//			DataInstance childInstance = DataProject.CurrentProject.GetInstance(tempIn.ID.ToString());
		//			CreateChildInstanceFolders(LibraryName, childInstance, CurrentParentFolder);
		//		}
		//	}
		//}

		private void createSubFolder_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucNewItem ni = new ucNewItem();
			chooseModule.SetDialogControl(ni);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ni.Validate())
				{
					DataFolder parentfld = ((DataFolder)((TreeViewItem)trFolders.SelectedItem).Tag);
					if (parentfld.ContainsFolder(ni.GetItemName()))
					{
						MessageBox.Show("Folder names must be unique at each level");
					}
					else
					{
						string name = ni.GetItemName();
						DataFolder df = parentfld.CreateSubfolder(name);
						df.FolderType = DataFolder.NLFOLDERTYPE.NONE;
						df.FolderName = name;
						AddFolder(df, parentfld);
					}
				}
				else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}
		
		}

		private void createAssetFolder_Click(object sender, RoutedEventArgs e)
		{
			//trFolders.SelectedItem;
			dlgBasic chooseModule = new dlgBasic();
			ucNewItem ni = new ucNewItem();
			chooseModule.SetDialogControl(ni);

			bool? ok = chooseModule.ShowDialog();
			if (ok != null && ok == true)
			{
				if (ni.Validate())
				{
					DataFolder parentfld = ((DataFolder)((TreeViewItem)trFolders.SelectedItem).Tag);
					if (parentfld.ContainsFolder(ni.GetItemName()))
					{
						MessageBox.Show("Folder names must be unique at each level");
					}
					else
					{
						string name = ni.GetItemName();

						DataFolder df = DataProject.CurrentProject.AddAssetFolder(name,
							parentfld,
							(DataCollection)parentfld.Tag);

						AddFolder(df, parentfld);
					}
				}
				else
				{
					MessageBox.Show("You must enter a valid name");
				}
			}

		}
		static T VisualUpwardSearch<T>(DependencyObject source) where T : DependencyObject
		{
			DependencyObject returnVal = source;

			while (returnVal != null && !(returnVal is T))
			{
				DependencyObject tempReturnVal = null;
				if (returnVal is Visual || returnVal is Visual3D)
				{
					tempReturnVal = VisualTreeHelper.GetParent(returnVal);
				}
				if (tempReturnVal == null)
				{
					returnVal = LogicalTreeHelper.GetParent(returnVal);
				}
				else returnVal = tempReturnVal;
			}

			return returnVal as T;
		}
		public void AddItem(SerializableKeyValuePair<long, string> contentItem, TreeViewItem parent)
		{
			//get instance
			DataInstance di = DataProject.CurrentProject.GetInstance(contentItem.Value);
			LCDataRow dr = di.GotoRowID(contentItem.Key);
			string name = "";
			if (di.Definition.NameField != "")
			{
				name = dr[di.Definition.NameField].ToString();
			} else
			{
				name = dr["RowID"].ToString();
			}
			TreeViewItem item = new TreeViewItem();
			item.Header = name;
			item.ContextMenu = new System.Windows.Controls.ContextMenu();
			item.ContextMenu.Visibility = System.Windows.Visibility.Collapsed;
			parent.Items.Add(item);

		}

		private void assetImport_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			DataFolder folder = (DataFolder)((TreeViewItem)((MenuItem)sender).Tag).Tag;



			// Display OpenFileDialog by calling ShowDialog method
			Nullable<bool> result = dlg.ShowDialog();
			// Get the selected file name and display in a TextBox
			SerializableKeyValuePair<long, string> pair = null;
			if (result == true)
			{
				string filename = dlg.FileName;
				pair = DataProject.CurrentProject.ImportAsset(filename, folder);
			}
			if(pair != null)
			{
				AddItem(pair, (TreeViewItem)((MenuItem)sender).Tag);
			}
		}



		private void definition_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			//todo implement opening the definition editor
			DataFolder fld = ((DataFolder)((TreeViewItem)trFolders.SelectedItem).Tag);
			DatatypeDefinition dd = ((DatatypeDefinition)fld.Tag);
			ucDefinitionEditor defEditor = new ucDefinitionEditor();
			EditorSynchro sync = new EditorSynchro(null, ContentCanvas, CurrentWindow);
			defEditor.SetEditorSynchro(sync);
			//	editor.SetEditorSynchro(sync);
			//	editor.SetDataSource(((DataInstance)fld.Tag).Definition, ((DataInstance)fld.Tag));
			//	editor.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;

			defEditor.SetDataSource(dd);
			docManager.OpenDocument(defEditor, sync);
		}

		void item_MouseUp(object sender, MouseButtonEventArgs e)
		{
			//if(CurrentWidgetPanel != null)
			{
				DataFolder fld = (DataFolder)((TreeViewItem)sender).Tag;
				//CurrentWidgetPanel.SetData(((DataInstance)fld.Tag));
				docManager.OpenWidgets(CurrentWindow, ContentCanvas, fld);
				e.Handled = true;
			}
		}

		//void library_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		//{
		//	//create editor
		//	ucDataEditor editor = new ucDataEditor();
		//	DataFolder fld = (DataFolder)((TreeViewItem)sender).Tag;
		//	EditorSynchro sync = new EditorSynchro(((DataInstance)fld.Tag), ContentCanvas, CurrentWindow);
		//	editor.SetEditorSynchro(sync);
		//	editor.SetDataSource(((DataInstance)fld.Tag).Definition, ((DataInstance)fld.Tag));
		//	editor.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
		//	docManager.OpenDocument(editor, sync);
		//	e.Handled = true;
		//}
		private void AssetFolder_Selected(object sender, RoutedEventArgs e)
		{
			if (sender is TreeViewItem && sender == e.OriginalSource)
			{
				TreeViewItem tvi = (TreeViewItem)sender;
				DataFolder df = (DataFolder)tvi.Tag;
				docManager.OpenWidgets(CurrentWindow, ContentCanvas, df);
				//if (CurrentWidgetPanel != null)
				//{
				//	CurrentWidgetPanel.SetAssetFolder(df, CurrentWindow, ContentCanvas);
				//}
				e.Handled = true;
			}
		}
		private void trFolders_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			TreeViewItem treeViewItem =
				VisualUpwardSearch<TreeViewItem>(e.OriginalSource as DependencyObject);

			if (treeViewItem != null)
			{
				treeViewItem.IsSelected = true;
				e.Handled = true;
			}
		}
		private void trFolders_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			TreeViewItem treeViewItem =
				VisualUpwardSearch<TreeViewItem>(e.OriginalSource as DependencyObject);

			if (treeViewItem != null)
			{
				treeViewItem.IsSelected = true;
				//e.Handled = true;
			}
		}
	}
}
