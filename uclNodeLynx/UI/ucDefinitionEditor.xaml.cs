﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;
using NodeLynx.lib;
using NodeLynx.UI.Dialogs;

namespace NodeLynx.UI
{
	/// <summary>
	/// Interaction logic for ucDefinitionEditor.xaml
	/// </summary>
	public partial class ucDefinitionEditor : UserControl, INLTabClient
	{
		public DatatypeDefinition Definition { get; set; }
		public List<DatatypeDefinition> InheritableDefinitions { get { return DataProject.CurrentProject.Definitions; } set { } }
		public List<string> RegWidgetTypes
		{
			get { return DataProject.CurrentProject.RegisteredDatatypes.WidgetTypeFQNs; }
			set
			{
			}
		}

		List<DatatypeField> Fields = new List<DatatypeField>();
		public string InheritsID { get{return Definition.InheritsDefinition;} set{Definition.InheritsDefinition = value;} }
		public string SelectedWidget { get{return Definition.DisplayWidgetFQN;} set{Definition.DisplayWidgetFQN=value;} }
		public string DataName
		{
			get { return Definition.DataName; }
			set { Definition.DataName = value; }
		}
		public string PrintableName
		{
			get { return Definition.PrintableName; }
			set { Definition.PrintableName = value; }
		}
		public ucDefinitionEditor()
		{
			InitializeComponent();
			Definition = new DatatypeDefinition();
			Definition.Initialize(DataProject.CurrentProject.RegisteredDatatypes);
			DataContext = this;
		}

		private void btnNewField_Click(object sender, RoutedEventArgs e)
		{
			ucDefinitionEditorField fld = new ucDefinitionEditorField();
			DatatypeField dfld = new DatatypeField();
			fld.Field = dfld;
			//default name is guid.  Feel free to leave it alone if you'd like
			dfld.FieldName = Guid.NewGuid().ToString();
			//default datatype, how can we go wrong?
			dfld.DataTypeFQN = "data.text.short";
			Fields.Add(dfld);
			FieldStack.Children.Insert(FieldStack.Children.Count - 1, fld);
			fld.PropertyChanged += fld_PropertyChanged;
			Definition.AddField(dfld.ID,dfld);
		}

		void fld_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			//remove from panel
			//remove from definition
			if(e is FieldDeletedEventArgs)
			{
				var e2 = (FieldDeletedEventArgs)e;
				foreach (FrameworkElement elem in FieldStack.Children)
				{
					if(elem is ucDefinitionEditorField)
					{
						var elem2 = (ucDefinitionEditorField)elem;
						if(elem2.Field == e2.Field)
						{
							FieldStack.Children.Remove(elem);
						}
					}
				}
				Definition.RemoveField(e2.Field);
			}
		}

		public UserControl GetControl()
		{
			return this;
		}

		public string GetTabName()
		{
			return "Definition Editor";
		}

		public void OnParentSizeChanged(object sender, SizeChangedEventArgs e)
		{
		}

		public void SetDragDropManager(CustomData.UI.DragDropManager dragdropManager)
		{
		}

		private void btnSave_Click(object sender, RoutedEventArgs e)
		{
			DataProject.CurrentProject.SaveProject();
		}

		private void GenerateFieldEditor(SerializableKeyValuePair<Guid, DatatypeField> Field)
		{
			ucDefinitionEditorField fld = new ucDefinitionEditorField();
			fld.Field = Field.Value;
			fld.PropertyChanged += fld_PropertyChanged;
			Fields.Add(Field.Value);
			FieldStack.Children.Insert(FieldStack.Children.Count - 1, fld);
			if (Field.Value.InUse)
			{
				fld.lstDatatype.IsReadOnly = true;
				fld.lstDatatype.IsEnabled = false;
			}
		}
		private string LastInherit = "";
		private bool Loading = true;
		private void cboDefinitionInherit_PreviewSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			//clear existing inherited fields
			if (!Loading
				&& cboDefinitionInherit.SelectedValue.ToString() != Definition.IDStr)
			{
				for (int i = 1; i < FieldStack.Children.Count - 1; i++)
				{
					ucDefinitionEditorField fs = (ucDefinitionEditorField)FieldStack.Children[i];
					if (fs.Field.Inheritied == true)
					{
						Definition.RemoveField(fs.Field);
						Fields.Remove(fs.Field);
						FieldStack.Children.RemoveAt(i);
						i--;
					}
				}

				//add new fields
				DatatypeDefinition def = DataProject.CurrentProject.GetDefinition(cboDefinitionInherit.SelectedValue.ToString());
				for (int i = 0; i < def.Fields.Count; i++)
				{
					if (def.Fields[i].Value.IsVisible)
					{
						//check for duplicate name
						bool exists = false;
						for(int i2 = 0; i2 < Definition.Fields.Count; i2++)
						{
							if(Definition.Fields[i2].Value.FieldName == def.Fields[i].Value.FieldName)
							{
								exists = true;
								break;
							}
						}
						if (!exists)
						{
							DatatypeField df = def.Fields[i].Value.Clone();
							SerializableKeyValuePair<Guid, DatatypeField> ser = new SerializableKeyValuePair<Guid, DatatypeField>(df.ID, df);
							df.PropertyChanged += fld_PropertyChanged;
							Definition.AddField(df.ID, df);
							GenerateFieldEditor(ser);
						} else
						{
							//todo: throw an error or popup a message
						}
					}
				}
				LastInherit = cboDefinitionInherit.SelectedValue.ToString();
				//regenerate parent list
				Definition.GenerateParentIDs();
			} else
			{
				cboDefinitionInherit.SelectedValue = LastInherit;
			}
			if (Loading)
				Loading = false;
		}


		public List<DatatypeDefinition> SupportedEditorFor()
		{
			return null;
		}

		public FrameworkElement SetDataSource(DatatypeDefinition def, DataInstance di = null, long RowID = -1)
		{
			Definition = def;
			foreach (SerializableKeyValuePair<Guid, DatatypeField> Field in Definition.Fields)
			{
				if (Field.Value.IsVisible)
				{
					GenerateFieldEditor(Field);
				}
			}
			DataContext = this;
			LastInherit = Definition.InheritsDefinition;
			if (Definition.ID == Guid.Empty)
			{
				cboDefinitionInherit.IsEnabled = false;
			}
			else
			{
				cboDefinitionInherit.SelectionChanged += cboDefinitionInherit_PreviewSelectionChanged;
			}
			return this;
		}


		public DatatypeDefinition GetDatatypeDefinition()
		{
			return Definition;
		}


		public string GetButtonName()
		{
			return "Definition Editor";
		}


		public DataInstance GetDataInstance()
		{
			return null;
		}

		public long GetRowID()
		{
			return -1;
		}

		private void btnMapWidgetFields_Click(object sender, RoutedEventArgs e)
		{
			dlgBasic chooseModule = new dlgBasic();
			ucSetWidgetBindings frmWidg = new ucSetWidgetBindings();
			frmWidg.SetDefinition(Definition);
			chooseModule.SetDialogControl(frmWidg);
			bool? ok = chooseModule.ShowDialog();

		}
		public void TabShown()
		{

		}
		public void SetEditorRow(LCDataRow dr, long RowID)
		{

		}
		EditorSynchro edtSync;
		public void SetEditorSynchro(EditorSynchro sync)
		{
			edtSync = sync;
		}
		public EditorSynchro GetEditorSynchro()
		{
			return edtSync;
		}
		public void SetupToolbar(Canvas toolbar)
		{

		}
	}
}
