﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;

namespace NodeLynx.UI
{
	/// <summary>
	/// Interaction logic for ucDefinitionEditorField.xaml
	/// </summary>
	public class FieldDeletedEventArgs : PropertyChangedEventArgs
	{
		private DatatypeField _field = null;
		public virtual DatatypeField Field { get { return _field; } }
		public FieldDeletedEventArgs(string propertyName, DatatypeField field)
			: base(propertyName)
		{
			_field = field;
		}
	}
	public partial class ucDefinitionEditorField : UserControl, INotifyPropertyChanged
	{
		private DatatypeField _Field;
		public DatatypeField Field
		{
			get { return _Field; }
			set
			{
				_Field = value;
				DataContext = _Field;
			}
		}
		public string DataInstanceType
		{ 
			set
			{
				if(_Field.ComplexTypeReferences.Count == 0)
				{
					_Field.ComplexTypeReferences.Add(Guid.Parse(value));
				} else
				{
					_Field.ComplexTypeReferences[0] = Guid.Parse(value);
				}
			}
			get
			{
				if (_Field.ComplexTypeReferences.Count == 0)
				{
					_Field.ComplexTypeReferences.Add(Guid.Empty);
				}
				return _Field.ComplexTypeReferences[0].ToString().ToLower();
			}
		}
		public List<string> RegTypes
		{
			get { return DataProject.CurrentProject.RegisteredDatatypes.TypeFQNs; }
			set
			{
			}
		}
		public List<Tuple<string,string>> Definitions
		{
			get{return DataProject.CurrentProject.GetDefinitionTypes(true);}
			set{}
		}

		public ucDefinitionEditorField()
		{
			InitializeComponent();
			lstDatatype.ItemsSource = RegTypes;
			cboInstanceType.DataContext = this;
		}

		private void btnDeleteField_Click(object sender, RoutedEventArgs e)
		{

		}

		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			// take a copy to prevent thread issues
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new FieldDeletedEventArgs(propertyName, Field));
			}
		}

		private void lstDatatype_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			//todo: I really hate having a special condition for this...
			if (lstDatatype.SelectedValue.ToString() == "data.complex.instance")
			{
				grdComplexReference.Visibility = System.Windows.Visibility.Visible;
			} else
			{
				grdComplexReference.Visibility = System.Windows.Visibility.Collapsed;
			}
		}
	}
}
