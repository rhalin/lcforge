﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.DragAndDrop;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.lib;

namespace NodeLynx.UI
{
	/// <summary>
	/// Interaction logic for ucWidgetPanel.xaml
	/// </summary>
	/// ////http://blogs.msdn.com/b/jaimer/archive/2007/07/12/drag-drop-in-wpf-explained-end-to-end.aspx
	public partial class ucWidgetPanel : UserControl
	{
		public Canvas myCanvas = null;
		public DragDropManager ddManager;
		public INLDocumentManager DocumentManager
		{
			get;
			set;
		}
		public ucWidgetPanel()
		{
			InitializeComponent();


		}
		bool IsDragging = false;

		private void StartDrag(MouseEventArgs e)
		{
			IsDragging = true;
			DataObject data = new DataObject(System.Windows.DataFormats.Text.ToString(), "abcd");
			//DragDropEffects de = DragDrop.DoDragDrop(this.DragSource, data, DragDropEffects.Move);
			
			IsDragging = false;
		}


		public void SetAssetFolder(DataFolder df)
		{
			WidgetStack.Children.Clear();
			DataInstance inst = DataProject.CurrentProject.GetAssetLibrary();
			foreach (SerializableKeyValuePair<long, string> content in df.Contents)
			{
				DataRow row = inst.GotoRowID(content.Key);
				IDataWidget defWidg = inst.Definition.DisplayWidget;
				IDataWidget newWidg = defWidg.Create();
				newWidg.DocumentManager = DocumentManager;
				newWidg.AllowResize = false;
				newWidg.AllowDelete = false;
				SerializableKeyValuePair<string, long> di = new SerializableKeyValuePair<string, long>();
				di.Key = inst.ID.ToString();
				di.Value = content.Key;
				newWidg.SetDataItem(di);

				DragDecorator decro = new DragDecorator();
				decro.SetDragDropManager(ddManager);
				decro.SetChild(newWidg);
				Dictionary<string, object> Bindings = new Dictionary<string, object>();
				foreach (SerializableKeyValuePair<string, string> binding in inst.Definition.WidgetBindings)
				{
					foreach (SerializableKeyValuePair<Guid, DatatypeField> field in inst.Definition.Fields)
					{
						if (field.Value.FieldName == binding.Value)
						{
							if (field.Value.DataTypeInfo.IsReferencetype())
							{
								Bindings[binding.Key] = row[field.Value.FieldName + "_ref"];
							}
							else
							{
								Bindings[binding.Key] = row[field.Value.FieldName].ToString();
							}
						}
					}
					//Bindings[binding.Key]
				}
				newWidg.SetBindings(Bindings);
				WidgetStack.Children.Add((UIElement)decro);

			}

		}
		public void SetData(DataInstance inst)
		{
			WidgetStack.Children.Clear();
			IDataWidget defWidg = inst.Definition.DisplayWidget;
			foreach (DataRow row in inst.Data.Rows)
			{
				IDataWidget newWidg = defWidg.Create();
				newWidg.DocumentManager = DocumentManager;
				newWidg.AllowResize = false;
				newWidg.AllowDelete = false;
				SerializableKeyValuePair<string,long> di = new SerializableKeyValuePair<string,long>();
				di.Key = inst.ID.ToString();
				di.Value = long.Parse(row[0].ToString());
				newWidg.SetDataItem(di);

				DragDecorator decro = new DragDecorator();
				decro.SetDragDropManager(ddManager);
				decro.SetChild(newWidg);
				Dictionary<string, object> Bindings = new Dictionary<string, object>();
				foreach (SerializableKeyValuePair<string, string> binding in inst.Definition.WidgetBindings)
				{
					foreach (SerializableKeyValuePair<Guid, DatatypeField> field in inst.Definition.Fields)
					{
						if(field.Value.FieldName == binding.Value)
						{
							if (field.Value.DataTypeInfo.IsReferencetype())
							{
								Bindings[binding.Key] = row[field.Value.FieldName + "_ref"];
							}
							else
							{
								Bindings[binding.Key] = row[field.Value.FieldName].ToString();
							}
						}
					}
					//Bindings[binding.Key]
				}
				newWidg.SetBindings(Bindings);
				WidgetStack.Children.Add((UIElement)decro);
				
			}
		}


		///*

		
		double FirstXPos;
		double FirstYPos;
		double FirstArrowXPos;
		double FirstArrowYPos;
		IDataWidget MovingObject = null;
		object MovingData = null;

		//*/
	}
}
