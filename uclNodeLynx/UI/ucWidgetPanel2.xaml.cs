﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeLynx.CustomData;
using NodeLynx.CustomData.UI;
using NodeLynx.CustomData.UI.DragAndDrop;
using NodeLynx.CustomData.UI.Widgets;
using NodeLynx.lib;

namespace NodeLynx.UI
{
	/// <summary>
	/// Interaction logic for ucWidgetPanel2.xaml
	/// </summary>
	public partial class ucWidgetPanel2 : UserControl
	{
		public ucWidgetPanel2()
		{
			InitializeComponent();
		}
		public Canvas myCanvas = null;
		public DragDropManager ddManager;
		public INLDocumentManager DocumentManager
		{
			get;
			set;
		}

		bool IsDragging = false;

		private void StartDrag(MouseEventArgs e)
		{
			IsDragging = true;
			DataObject data = new DataObject(System.Windows.DataFormats.Text.ToString(), "abcd");
			//DragDropEffects de = DragDrop.DoDragDrop(this.DragSource, data, DragDropEffects.Move);
			
			IsDragging = false;
		}


		public void SetAssetFolder(DataFolder df, FrameworkElement CurrentWindow, Canvas ContentCanvas)
		{
			WidgetStack.Children.Clear();
			AddWidgetFolders(df, CurrentWindow, ContentCanvas);

			DataInstance inst = DataProject.CurrentProject.GetAssetLibrary();
			foreach (SerializableKeyValuePair<long, string> content in df.Contents)
			{
				LCDataRow row = inst.GotoRowID(content.Key);
				IDataWidget defWidg = inst.Definition.DisplayWidget;
				IDataWidget newWidg = defWidg.Create();
				newWidg.DocumentManager = DocumentManager;
				newWidg.AllowResize = false;
				newWidg.AllowDelete = false;
				newWidg.ContentCanvas = ContentCanvas;
				newWidg.CurrentWindow = CurrentWindow;
				SerializableKeyValuePair<string, long> di = new SerializableKeyValuePair<string, long>();
				di.Key = inst.ID.ToString();
				di.Value = content.Key;
				newWidg.SetDataItem(di);

				DragDecorator decro = new DragDecorator();
				decro.SetDragDropManager(ddManager);
				decro.SetChild(newWidg);
				Dictionary<string, object> Bindings = new Dictionary<string, object>();
				foreach (SerializableKeyValuePair<string, string> binding in inst.Definition.WidgetBindings)
				{
					foreach (SerializableKeyValuePair<Guid, DatatypeField> field in inst.Definition.Fields)
					{
						if (field.Value.FieldName == binding.Value)
						{
							if (field.Value.DataTypeInfo.IsReferencetype())
							{
								Bindings[binding.Key] = row[field.Value.FieldName + "_ref"];
							}
							else
							{
								Bindings[binding.Key] = row[field.Value.FieldName].ToString();
							}
						}
					}
					//Bindings[binding.Key]
				}
				newWidg.SetBindings(Bindings);
				WidgetStack.Children.Add((UIElement)decro);

			}

		}
		public void AddWidgetFolders(DataFolder folder, FrameworkElement CurrentWindow, Canvas ContentCanvas)
		{
			
			foreach(DataFolder fld in folder.Subfolders)
			{
				WindowData data = new WindowData(CurrentWindow, ContentCanvas, null);
				data.Tag = fld;
				ucWidget newWidg = new ucWidget();
				Dictionary<string, object> binds = newWidg.GetBindableFields();
				binds["shortText"] = fld.FolderName;
				binds["icon"] = "pack://siteoforigin:,,,/Resources/Icons/LCForge-FolderIcon-medium.png";
				newWidg.SetBindings(binds);
				newWidg.Iconify(true);
				newWidg.DocumentManager = DocumentManager;
				newWidg.AllowResize = false;
				newWidg.AllowDelete = false;
				newWidg.ContentCanvas = ContentCanvas;
				newWidg.CurrentWindow = CurrentWindow;
				newWidg.Tag = data;	
				((UserControl)newWidg).MouseDoubleClick +=FolderWidget_MouseDoubleClick;
				WidgetStack.Children.Add((UIElement)newWidg);
			}
		}

		private void FolderWidget_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			WindowData data = (WindowData)((FrameworkElement)sender).Tag;
			DataFolder df = (DataFolder)data.Tag;
			DocumentManager.OpenWidgets(data.Window, data.ContentCanvas, df);
			e.Handled = true;
		}
		DataInstance Instance;
		public void SetData(DataFolder folder, DataInstance inst, FrameworkElement CurrentWindow, Canvas ContentCanvas)
		{
			Instance = inst;
			WidgetStack.Children.Clear();
			AddWidgetFolders(folder, CurrentWindow, ContentCanvas);
			IDataWidget defWidg = inst.Definition.DisplayWidget;
			foreach (LCDataRow row in inst.Data.Rows)
			{
				IDataWidget newWidg = defWidg.Create();
				newWidg.DocumentManager = DocumentManager;
				newWidg.AllowResize = false;
				newWidg.AllowDelete = false;
				newWidg.ContentCanvas = ContentCanvas;
				newWidg.CurrentWindow = CurrentWindow;
				((UserControl)newWidg).MouseDoubleClick +=DataReferenceWidget_MouseDoubleClick;
	
				SerializableKeyValuePair<string,long> di = new SerializableKeyValuePair<string,long>();
				di.Key = inst.ID.ToString();
				di.Value = long.Parse(row[0].ToString());
				newWidg.SetDataItem(di);

				DragDecorator decro = new DragDecorator();
				decro.SetDragDropManager(ddManager);
				decro.SetChild(newWidg);
				Dictionary<string, object> Bindings = new Dictionary<string, object>();
				foreach (SerializableKeyValuePair<string, string> binding in inst.Definition.WidgetBindings)
				{
					foreach (SerializableKeyValuePair<Guid, DatatypeField> field in inst.Definition.Fields)
					{
						if(field.Value.FieldName == binding.Value)
						{
							if (field.Value.DataTypeInfo.IsReferencetype())
							{
								Bindings[binding.Key] = row[field.Value.FieldName + "_ref"];
							}
							else
							{
								Bindings[binding.Key] = row[field.Value.FieldName].ToString();
							}
						}
					}
					//Bindings[binding.Key]
				}
				newWidg.SetBindings(Bindings);
				WidgetStack.Children.Add((UIElement)decro);
				
			}
		}

		private void DataReferenceWidget_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			IDataWidget widget = (IDataWidget)sender;
			//todo: open from document manager
			ucDataEditor editor = new ucDataEditor();
			//DataFolder fld = (DataFolder)((TreeViewItem)sender).Tag;
			DataInstance di = DataProject.CurrentProject.GetInstance(widget.GetDataItem().Key);
			EditorSynchro sync = new EditorSynchro(di, widget.ContentCanvas, widget.CurrentWindow, widget.GetDataItem().Value);
			editor.SetEditorSynchro(sync);
			editor.SetDataSource(di.Definition, di);
			editor.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
			DocumentManager.OpenDocument(editor, sync);
			e.Handled = true;
		}

		private void grdTop_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			scrollTop.Width = e.NewSize.Width;
			scrollTop.Height = e.NewSize.Height;
			WidgetStack.Width = e.NewSize.Width;
		}
	}
}
