﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace NodeLynx.lib
{
	public class ColorValueConverter: IValueConverter
	{

			public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
			{
				return (Color)ColorConverter.ConvertFromString((string)value); 
				//string text = (string)value;
				//return (text == Window11.DefaultString) ?
				//	Brushes.Transparent :
				//	Brushes.Yellow;
			}

			public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
			{
				string c;
				Color NodeColor = (Color)value;
				c = string.Format("#{0:X2}{1:X2}{2:X2}{3:X2}",
					NodeColor.A,
					NodeColor.R,
					NodeColor.G,
					NodeColor.B);
				return c;
			}
	}
}
