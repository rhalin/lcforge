﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeLynx.lib
{
	//http://www.pvladov.com/2012/05/decimal-to-arbitrary-numeral-system.html
	public class IDConverter
	{
		public static string DecimalToID(long decimalNumber)
		{
			const int BitsInLong = 64;
			const string Digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

			if (36 < 2 || 36 > Digits.Length)
				throw new ArgumentException("The radix must be >= 2 and <= " +
					Digits.Length.ToString());

			if (decimalNumber == 0)
				return "0";

			int index = BitsInLong - 1;
			long currentNumber = Math.Abs(decimalNumber);
			char[] charArray = new char[BitsInLong];

			while (currentNumber != 0)
			{
				int remainder = (int)(currentNumber % 36);
				charArray[index--] = Digits[remainder];
				currentNumber = currentNumber / 36;
			}

			string result = new String(charArray, index + 1, BitsInLong - index - 1);
			if (decimalNumber < 0)
			{
				result = "-" + result;
			}

			return result;
		}
		public static long IDToDecimal(string number)
		{
			const string Digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

			if (36 < 2 || 36 > Digits.Length)
				throw new ArgumentException("The radix must be >= 2 and <= " +
					Digits.Length.ToString());

			if (String.IsNullOrEmpty(number))
				return 0;

			// Make sure the arbitrary numeral system number is in upper case
			number = number.ToUpperInvariant();

			long result = 0;
			long multiplier = 1;
			for (int i = number.Length - 1; i >= 0; i--)
			{
				char c = number[i];
				if (i == 0 && c == '-')
				{
					// This is the negative sign symbol
					result = -result;
					break;
				}

				int digit = Digits.IndexOf(c);
				if (digit == -1)
					throw new ArgumentException(
						"Invalid character in the arbitrary numeral system number",
						"number");

				result += digit * multiplier;
				multiplier *= 36;
			}

			return result;
		}
	}
}
