﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeLynx.lib
{

	[Serializable]
	public class SerializableKeyValuePair<K,V>
	{
		public K Key;
		public V Value;
		public SerializableKeyValuePair(K key, V val)
		{
			Key = key;
			Value = val;
		}
		public SerializableKeyValuePair()
		{

		}
	}
}
