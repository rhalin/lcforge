﻿#pragma checksum "..\..\..\UI\ucDefinitionEditor.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "EAE73613FA8530C67082A6FD4CAD2478"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace NodeLynx.UI {
    
    
    /// <summary>
    /// ucDefinitionEditor
    /// </summary>
    public partial class ucDefinitionEditor : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\..\UI\ucDefinitionEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel FieldStack;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\UI\ucDefinitionEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPrintName;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\UI\ucDefinitionEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboDefinitionInherit;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\UI\ucDefinitionEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboDisplayWidget;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\UI\ucDefinitionEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnMapWidgetFields;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\UI\ucDefinitionEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtDefName;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\UI\ucDefinitionEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNewField;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\UI\ucDefinitionEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSave;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/uclNodeLynx;component/ui/ucdefinitioneditor.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UI\ucDefinitionEditor.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.FieldStack = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.txtPrintName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.cboDefinitionInherit = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 4:
            this.cboDisplayWidget = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.btnMapWidgetFields = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\..\UI\ucDefinitionEditor.xaml"
            this.btnMapWidgetFields.Click += new System.Windows.RoutedEventHandler(this.btnMapWidgetFields_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.txtDefName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.btnNewField = ((System.Windows.Controls.Button)(target));
            
            #line 24 "..\..\..\UI\ucDefinitionEditor.xaml"
            this.btnNewField.Click += new System.Windows.RoutedEventHandler(this.btnNewField_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnSave = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\..\UI\ucDefinitionEditor.xaml"
            this.btnSave.Click += new System.Windows.RoutedEventHandler(this.btnSave_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

